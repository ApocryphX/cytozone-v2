//
//  SFOGLRenderState.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SFOGLRenderState : NSObject <NSCopying> {
	
	NSUInteger sfBackplaneMode;
	NSColor* sfBackplaneColor;
	float sfAlphaThreshold;
	float sfAlpha;
	float sfBrightness;
	NSColor* sfBlendColor;
	NSUInteger sfRenderMode;
	float sfZScale;
	NSUInteger sfSampling;
	BOOL sfIsZInverted;
	NSUInteger sfVolumeIndex;	
}

+ (id) defaultState;
- (id) initWithVolumeSetting:(SFOGLRenderState*)settings;

- (void) setBackplaneMode:(NSUInteger)value;
- (void) setBackplaneColor:(NSColor*)value;
- (void) setAlphaThreshold:(float)value;
- (void) setAlpha:(float)value;
- (void) setBrightness:(float)value;
- (void) setBlendColor:(NSColor*)value;
- (void) setRenderMode:(NSUInteger)value;
- (void) setZScale:(float)value;
- (void) setSampling:(NSUInteger)value;
- (void) setIsZInverted:(BOOL)value;
- (void) setVolumeIndex:(NSUInteger)value;

//------------------------------------------------

- (NSUInteger) backplaneMode;
- (NSColor*) backplaneColor;
- (float) alphaThreshold;
- (float) alpha;
- (float) brightness;
- (NSColor*)blendColor;
- (NSUInteger) renderMode;
- (float) zScale;
- (NSUInteger) sampling;
- (BOOL) isZInverted;
- (NSUInteger)volumeIndex;

- (SFOGLRenderState*)interpolatedStateWithFraction:(float)frac ofState:(SFOGLRenderState*)state;

@end
