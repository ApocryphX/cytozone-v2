//
//  SFBitplane.h
//  Monterey
//
//  Copyright 2014 Elarity, LLC.  All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Accelerate/Accelerate.h>
#import <OpenGL/OpenGL.h>

extern NSString *SFKBitplaneColor;


@class SFVoxelType, SFChannelSetting, SFRectangleTexture, SFTiledTexture2D;

@interface SFBitplane : NSObject <NSCopying>{
    
}

@property (retain) SFChannelSetting *channelSetting;

@property (readonly) NSUInteger	height;
@property (readonly) NSUInteger	width;
@property (readonly) NSUInteger bytesPerSample;
@property (readonly) NSMutableData *data;
@property (readonly) NSMutableDictionary *properties;


+ (id) bitplaneWithBytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height;
+ (id) bitplaneWithData:(NSData*)data bytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height;
+ (id) bitplaneWithMutableData:(NSMutableData*)data bytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height;;

- (vImage_Buffer) vectorImageBuffer;
-(NSUInteger)bytesPerRow;
-(NSUInteger) bytesPerPlane;
-(NSUInteger) pixelsPerPlane;

@end


