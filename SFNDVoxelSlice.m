//
//  SFNDVoxelSlice.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFNDVoxelData.h"

#import "SFAlignment2D.h"
#import "SFNDVoxelData.h"
#import "SFNDImporter.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"
#import "SFVoxelData.h"
#import "SFChannelInfo.h"
#import "SFDataInfo.h"

@implementation SFNDVoxelData (NDVoxelSlice)

- (NSUInteger) byteStrideForChannel:(NSUInteger)channel atRank:(NSUInteger)rank {
	NSUInteger i, ret = 0;
	if(channel < [sfChannels count] && rank < [sfNDDimensions count]) {
		SFVoxelData* vdata = [sfChannels objectAtIndex:channel];
		if (rank == 0) 
			ret = [[vdata alignment]bytesPerRow];
		else {
			ret = [[vdata alignment]bytesPerFrame];
			for (i=2;i<=rank;i++)
				ret *= [sfNDDimensions extentAtIndex:i];
		}
	}
	return ret;
};

- (NSUInteger)numberOfSlicesAtRank:(NSUInteger)rank {
	return [sfNDDimensions numberOfSlicesAtRank:rank];
}

- (SFNDVoxelData*)sliceAtRank:(NSUInteger)rank atIndex:(NSUInteger)idx {
	SFNDVoxelData* ret = nil; 
	if( idx < [sfNDDimensions numberOfSlicesAtRank:rank] && rank > 1 ) {
		ret	= [SFNDVoxelData new];
		ret->sfNDParent = self;	// init sfParent;
		ret->sfNDDimensions = [sfNDDimensions dimensionsSlicedAtIndex:rank];  // init sfNDDimensions
		NSUInteger ch;
		NSUInteger length = [sfNDDimensions numberOfSlicesAtRank:rank-1]/[sfNDDimensions numberOfSlicesAtRank:rank];
		NSUInteger offset = length*idx;
		ret->sfChannels = [NSMutableArray new];    // init Channels with sliced data
		for(ch=0;ch<[sfChannels count];ch++) {
			SFVoxelData* srcvdata = [sfChannels objectAtIndex:ch];
			SFVoxelData* vslice = [srcvdata sliceAtFrame:offset count:length];
			[ret->sfChannels addObject:vslice];
		}
		ret->sfName = sfName;  // init sfName
		ret->sfChannelInfos = sfChannelInfos;  // init sfChannelInfos;
	}
	return ret;
};

@end
