//
//  SFZeissLSMInfo.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFZeissLSMInfo.h"

#import "SFNDDimension.h"
#import "SFVoxelInfo.h"
#import "SFTIFFReader.h"






@interface SFZeissLSMInfo (PrivateMethods)
- (void)decode;
@end

@implementation SFZeissLSMInfo


- (id)initTIFFReader:(SFTIFFReader*)reader {
	id ret = nil;
	NSData* zeissdata = [reader dataForTagWithIndex:0 withID:TIF_CZ_LSMINFO];
	if (zeissdata) {
		self = [super init];
		sfData = zeissdata;
		sfTIFFReader = reader;
		ret = self;
	}
	return ret;
};
/*
- (NSUInteger)numberOfChannels {
	LSMInfo *lsmInfo = (void*)[sfData bytes];
    NSUInteger channelNumber = lsmInfo->s32DimensionChannels;
	return channelNumber;
};


- (void) LUT:(unsigned) offset {
	NSData* lutdata = [sfTIFFReader dataAtOffset:offset];

    
	void *baseptr = (void*)[lutdata bytes];
	uint32_t* dataptr = (uint32_t*)baseptr;
	uint32_t size = NSSwapLittleIntToHost(dataptr[0]);
	uint32_t numsubblocks = NSSwapLittleIntToHost(dataptr[1]); //0x400494C
	uint32_t numchannels = NSSwapLittleIntToHost(dataptr[2]); //0x400494C
	uint32_t luttype = NSSwapLittleIntToHost(dataptr[3]); //0x400494C
	
}

- (SFNDDimension*) nDDimension {
	
	SFNDDimension* ret = [SFNDDimension new];

    const LSMInfo *info = [sfData bytes];
    



	NSUInteger i;
	for(i=0;i<3;i++) {
		SFDimension* newdim = [SFDimension new];
		[newdim setExtent:ext[i]];
		[newdim setMagnitude:df[i]];
		[newdim setSymbol:[NSString stringWithCharacters:&LSMSymbolMap[0][i+1] length:1]];
		[newdim setUnit:[NSString stringWithCharacters:&LSMSymbolMap[1][i+1] length:1]];
		[newdim setDimensionType:kSpace];
		[ret addDimension:newdim];
	}
	if(t > 1) {
		SFDimension* newdim = [SFDimension new];
		[newdim setExtent:t];
		[newdim setMagnitude:df[3]];
		[newdim setSymbol:[NSString stringWithCharacters:&LSMSymbolMap[0][0] length:1]];
		[newdim setUnit:[NSString stringWithCharacters:&LSMSymbolMap[1][0] length:1]];
		[newdim setDimensionType:kTime];	
		[ret addDimension:newdim];
	}
	return ret;
};
*/

- (void)getVoxelInfo:(SFVoxelInfo*)vinfo type:(unsigned)type {
	switch (type) {
		case 1: { // 8 bit int
			[vinfo setBytesPerSample:1];
			[vinfo setPrecision:8];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kInteger]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		case 2: { // 12 bit int
			[vinfo setBytesPerSample:2];
			[vinfo setPrecision:12];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kInteger]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		case 5: { // 32 bit float
			[vinfo setBytesPerSample:4];
			[vinfo setPrecision:32];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kFloatingPoint]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		default: {
			break;
		}
	}		
};

- (SFVoxelInfo*)voxelInfoForChannel:(unsigned)value {
	SFVoxelInfo* ret = [SFVoxelInfo new];
	const void* baseptr = (void*)[sfData bytes];
	int32_t* size = (int32_t*) baseptr;
	int32_t datatype = NSSwapLittleIntToHost(size[7]);	
//	if(datatype == 0) {
//		baseptr = &size[10];
//		NSSwappedDouble* ff = (NSSwappedDouble *) baseptr;
//		baseptr = (void*) &ff[3];
//		uint32_t* info = (uint32_t*)baseptr;
//		baseptr = &info[6];
//		ff = (NSSwappedDouble *) baseptr;
//		baseptr = (void*) &ff[1];
//		size = (int32_t*) baseptr;
//		NSUIntegeroffset = NSSwapLittleIntToHost(size[0]);
//		
//	}
//	else {
		[self getVoxelInfo:ret type:datatype];		
//	}
	return ret;
};
/*
- (void) channelInfoAt:(void*)ptr {
	const void* baseptr = (void*)ptr;
	int32_t* info = (int32_t*) baseptr;
	int32_t size = NSSwapLittleIntToHost(info[0]);
	int32_t numcolors = NSSwapLittleIntToHost(info[1]);
	int32_t numnames = NSSwapLittleIntToHost(info[2]);
	
}
 */
//	

@end
