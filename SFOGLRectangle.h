//
//  SFOGLRectangle.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFOGLTextureRectangle.h"

@interface SFOGLRectangle : NSObject {
	SFOGLTextureRectangle* sfTexture;
	
	float sfZPosition;
}

- (void)setTexture:(SFOGLTextureRectangle*)value;
- (void)setZPosition:(float)value;

- (float)zPosition;
- (SFOGLTextureRectangle*)texture;

- (BOOL) draw;
@end
