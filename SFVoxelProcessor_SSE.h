//
//  SFVoxelProcessor_SSE_8U.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFVoxelProcessor.h"


#if defined(__SSE2__)
#ifdef __VEC__

@interface SFVoxelProcessor_SSE_8U : SFVoxelProcessor_8U {

}

@end

@interface SFVoxelProcessor_SSE_F : SFVoxelProcessor_8U {
	
}

@end

#endif // __VEC__
#endif