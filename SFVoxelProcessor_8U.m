//
//  SFVoxelProcessor_8U.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Accelerate/Accelerate.h>

#import "SFVoxelProcessor.h"
#import "SFAlignment2D.h"
#import "SFVoxelInfo.h"

@implementation SFVoxelProcessor_8U

+ (void)  getMaximum:(void*)dst  source:(const void*)src alignment:(SFAlignment2D*) align {
	NSUInteger count = [align  bytesPerFrame];
	NSUInteger i;
	uint8_t *vsrc = (uint8_t *) src;
	uint8_t *vdst = (uint8_t *) dst;
	for(i=0;i<count;i++) {
		vdst[i] = vdst[i]>vsrc[i] ? vdst[i] : vsrc[i];
	}	
};	

+ (void) getBinary:(void*)dst source:(const void*)src alignment:(SFAlignment2D*) align threshold:(float)value {
	//value =  clamp(value, 0.0f, 1.0f);
	uint8_t threshold = value *255.0f;
	NSUInteger count = [align  bytesPerFrame];
	NSUInteger i;
	uint8_t *vsrc = (uint8_t *) src;
	uint8_t *vdst = (uint8_t *) dst;
	for(i=0;i<count;i++) {
		vdst[i] = (vsrc[i]>=threshold) ? 255 : 0;
	}		
};

+ (void) getSquareRoot:(void*)dst source:(const void*)src alignment:(SFAlignment2D*)align {
	uint8_t lut[256];
	NSUInteger i;
	for(i=0;i<256;i++)
		lut[i] = (unsigned) (sqrtf((float)i/255.0f)*255.0f);
	NSUInteger count = [align  bytesPerFrame];
	uint8_t *vsrc = (uint8_t *) src;
	uint8_t *vdst = (uint8_t *) dst;
	for(i=0;i<count;i++) {
		vdst[i] = lut[vsrc[i]];
	}		
};

+ (void) getSurface:(void*)dst top:(const void*)top middle:(const void*)middle bottom:(const void*)bottom alignment:(SFAlignment2D*)align {
	NSUInteger i, count = [align  bytesPerFrame];
	uint8_t *vtop = (uint8_t *) top;
	uint8_t *vmiddle = (uint8_t *) middle;
	uint8_t *vbottom = (uint8_t *) bottom;
	uint8_t *vdst  = (uint8_t *) dst;
	for(i=0;i<count;i++) {
		vdst[i] = vmiddle[i] & ~(vtop[i]&vbottom[i]);
	}			
};

+ (void) edgeDetect:(void*)dst source:(const void*)src alignment:(SFAlignment2D*)align {
	int16_t kernel[9] = {0, -1, 0, -1, 4, -1, 0, -1, 0 };
	struct vImage_Buffer src_buffer = {(void*)src, [align alignedSize].height, [align alignedSize].width, [align bytesPerRow]};
	struct vImage_Buffer dst_buffer = {(void*)dst, [align alignedSize].height, [align alignedSize].width, [align bytesPerRow]};
	vImageConvolve_Planar8 (&src_buffer, &dst_buffer, NULL, 0, 0, kernel, 3, 3, 1, 0, kvImageCopyInPlace);
};

+ (void) removeNoise:(void*)dst source:(const void*)src alignment:(SFAlignment2D*)align kernelSize:(NSUInteger) size {
	NSUInteger i, count = size * size;
	int16_t *kernel = malloc (count*sizeof(int16_t));
	for(i=0;i<count;i++)
		kernel[i] = 1;
	struct vImage_Buffer src_buffer = {(void*)src, [align alignedSize].height, [align alignedSize].width, [align bytesPerRow]};
	struct vImage_Buffer dst_buffer = {(void*)dst, [align alignedSize].height, [align alignedSize].width, [align bytesPerRow]};
	vImageConvolve_Planar8 (&src_buffer, &dst_buffer, NULL, 0, 0, kernel,(uint32_t) size,(uint32_t) size,(uint32_t) count, 0, kvImageCopyInPlace);
	free(kernel);
};

+ (void) getScaled:(void*)dst scale:(SFAlignment2D*)scale source:(const void*)src alignment:(SFAlignment2D*)alignment {
	struct vImage_Buffer src_buffer, dst_buffer;
	src_buffer.data = (void*)src;
	src_buffer.width = [alignment size].width;
	src_buffer.height = [alignment size].height;
	src_buffer.rowBytes = [alignment bytesPerRow];
	dst_buffer.data = dst;
	dst_buffer.width = [scale size].width;
	dst_buffer.height = [scale size].height;
	dst_buffer.rowBytes = [scale bytesPerRow];
	vImageScale_Planar8(&src_buffer,&dst_buffer,NULL,0);
}

+ (void) getTranslated:(void*)xdst  source:(const void *)src alignment:(SFAlignment2D*)align x:(float)x y:(float)y {
	NSPoint srcoffsetf, dstoffsetf;
	void* dst;
	if (xdst == src) 
		dst = malloc([align bytesPerFrame]);
	else
		dst = xdst;
	// make all coordinates positive by offseting eiher destination or source offset
	if (x < 0.0f) {
		srcoffsetf.x = 0.0f;
		dstoffsetf.x = x * -1.0f;
	}
	else {
		srcoffsetf.x = x;
		dstoffsetf.x = 0.0f;
	}
	if (x < 0.0f) {
		srcoffsetf.y = 0.0f;
		dstoffsetf.y = y * -1.0f;
	}
	else {
		srcoffsetf.y = y;
		dstoffsetf.y = 0.0f;
	}
	memset(dst, 0, [align bytesPerFrame]);
	SFSize framesize = [align size];
	NSUInteger i;
	SFPoint srcpoint = {lroundf(srcoffsetf.x), lroundf(srcoffsetf.y)};
	SFPoint dstpoint = {lroundf(dstoffsetf.x), lroundf(dstoffsetf.y)};
	
	const void* srcbytes = src + [align byteOffsetForPoint:srcpoint];
	void* dstbytes = dst + [align byteOffsetForPoint:dstpoint];
	NSUInteger transfersize = (framesize.width - srcpoint.x - dstpoint.x) * [[align voxelInfo]bytesPerSample];
	for (i=0; i < framesize.height - dstpoint.y - srcpoint.y; i++) {
		NSUInteger offset = i*[align bytesPerRow];
		memcpy(dstbytes+offset, srcbytes+offset, transfersize);
	} 
	if (xdst == src) {
		memcpy (xdst, dst, [align bytesPerFrame]);
		free (dst);
	}
};




@end
