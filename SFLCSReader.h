//
//  SFLeicaReader.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFNDImporter.h"


@class SFLCSBlockReader,SFDataInfo,SFNDVoxelData,SFVoxelInfo,SFNDDimension,SFNDDimension,SFDataAligner, SFNDDataNode, SFNDReader;

@protocol SFVoxelProcessing;

@interface SFLCSReader : NSObject <SFNDReading> {
	SFLCSBlockReader* sfLCSBlockReader;
	NSMutableDictionary* sfSeries;
	NSMutableDictionary* sfImages;
	NSMutableDictionary* sfDimDescr;
	NSMutableDictionary* sfLUT;
	NSUInteger sfChannelCount;
	NSObject <SFVoxelProcessing>* sfVoxelProcessor;
	NSArray *sfSeriesDictionaries;

}


@end
