//
//  SFNDDimension.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//
#import <Cocoa/Cocoa.h>


NSString *SFKDimensionSymbolName;
NSString *SFKDimensionUnitName;
NSString *SFKDimensionMagnitudeName;
NSString *SFKDimensionExtentName;
NSString *SFKDimensionTypeName;

NSString *SFKDimensionTypeVoidName;
NSString *SFKDimensionTypeSpaceName;
NSString *SFKDimensionTypeTimeName;
NSString *SFKDimensionTypeWavelengthName;


@interface SFNDDimension : NSObject <NSCoding, NSCopying> {
    NSMutableArray *_dimensions;
}

-(id)initWithNDDimension:(SFNDDimension*)nddimension;

- (void)addDimensionWithExtent:(NSUInteger)extent dimensionType:(NSString*)type symbol:(NSString*)symbol;
- (void)addDimensionWithExtent:(NSUInteger)extent magnitude:(NSNumber*)magnitude dimensionType:(NSString*)type symbol:(NSString*)symbol unit:(NSString*)unit;

- (SFNDDimension*)dimensionsSlicedAtIndex:(NSUInteger)index;
- (void)removeDimensionAtIndex:(NSUInteger)index;

- (NSUInteger)count;
//- (void)setMagnitude:(NSNumber*)value atIndex:(NSUInteger)rank;
- (void)setExtent:(NSUInteger)value atIndex:(NSUInteger)rank;
//- (void)setDimensionType:(NSString*)value atIndex:(NSUInteger)rank;
//- (void)setUnit:(NSString*)value atIndex:(NSUInteger)rank;
//- (void)symbol:(NSString*)value atIndex:(NSUInteger)rank;

- (NSNumber*) magnitudeAtIndex:(NSUInteger)rank;
- (NSUInteger) extentAtIndex:(NSUInteger)rank;
- (NSString*) dimensionTypeAtIndex:(NSUInteger)rank;
- (NSString*)symbolAtIndex:(NSUInteger)rank;
- (NSString*)unitAtIndex:(NSUInteger)rank;

- (NSUInteger)numberOfPlanes;
- (NSUInteger)numberOfCubes;

- (NSUInteger)strideAtIndex:(NSUInteger)rank;
- (NSUInteger)numberOfSlicesAtRank:(NSUInteger)rank;

//- (SFDimension*) power2AtIndex:(NSUInteger)rank;
//- (NSUInteger) makePower2AtIndex:(NSUInteger)rank;

- (BOOL)isVolume;
- (BOOL)isTimeSeries;
- (BOOL)isCalibrated;

@end
