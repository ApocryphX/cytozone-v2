//
//  SFNDDataset.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFNDDataset.h"
#import "SFNDDataNode.h"
#import "SFNDDimension.h"
#import "SFNDVoxelData.h"

@interface SFNDDataset (PrivateMethods)
- (void)setDimensionString;
@end

@implementation SFNDDataset

- (id)initWithNDVoxelData:(SFNDVoxelData*)vdata  {
	self = [super init];
	NSUInteger i;
	sfNDVoxelData = vdata;
	sfDimensionString = [NSMutableString new];
	for (i=0;i<[sfNDVoxelData dimensionCount];i++) 
		[sfDimensionString appendString:[sfNDVoxelData symbolAtIndex:i]];
	return self;
};

- (void)setName:(NSString*)value {
	sfName = value;
};

- (NSString*)name {
	return sfName;
};

- (NSString*)dimensionString {
	return sfDimensionString;
};

- (NSUInteger)channelCount {
	return [sfNDVoxelData countOfChannels];
};

- (SFNDVoxelData*)nDVoxelData {
	return sfNDVoxelData;
};

- (id)initWithCoder:(NSCoder *)coder
{
	self = [super init];
    if ( [coder allowsKeyedCoding] ) {
        // Can decode keys in any order
        sfName = [coder decodeObjectForKey:@"Name"];
		sfDimensionString = [coder decodeObjectForKey:@"DimensionString"];
		sfNDVoxelData = [coder decodeObjectForKey:@"nDVoxelData"];
    } 
	else {
        // Must decode keys in same order as encodeWithCoder:
		sfName = [coder decodeObject];
		sfDimensionString = [coder decodeObject];
		sfNDVoxelData = [coder decodeObject];
    }
    return self;
};

- (void)encodeWithCoder:(NSCoder *)coder
{
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject:sfName forKey:@"Name"];
		[coder encodeObject:sfDimensionString forKey:@"DimensionString"];
		[coder encodeObject:sfNDVoxelData forKey:@"nDVoxelData"];
    } 
	else {
        [coder encodeObject:sfName];
        [coder encodeObject:sfDimensionString];
		[coder encodeObject:sfNDVoxelData];
    }
    return;
};

;

@end
