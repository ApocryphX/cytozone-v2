//
//  SFOGLVolumeTexture.h
//  CytoFX
//
//  Created by ApocryphX on 2/11/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

@class SFNDDimension, SFNDVoxelData, SFVoxelData, SFAlignment2D, SFAlignment2D, SFVoxelInfo;

@interface SFOGLVolumeTexture : NSObject {
	NSOpenGLContext* sfContext;
	NSUInteger sfRenderMode; 
}

- (id)init;
- (id)initWithContext:(NSOpenGLContext*)ctx;

- (void)setRenderMode:(NSUInteger)value;
- (void)setOpenGLContext:(NSOpenGLContext*)cxt;

@end
