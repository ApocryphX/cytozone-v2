//
//  SFOGLVolumeSetting.m
//  VoxelMachine
//
//  Created by ApocryphX on 4/12/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolumeSetting.h"

@implementation SFOGLVolumeSetting

- (void) setBackplaneMode:(NSUInteger)value{
	sfBackplaneMode = value;
};
- (void) setBackplaneColor:(NSColor*)value{
	sfBackplaneColor = value;
};
- (void) setAlphaThreshold:(float)value{
	sfAlphaThreshold = value;
};
- (void) setAlpha:(float)value{
	sfAlpha = value;
};
- (void) setBrightness:(float)value {
	sfBrightness = value;
};
- (void) setBlendFactor:(float)value {
	sfBlendFactor = value;
}; 
- (void) setBlendColor:(NSColor*)value {
	sfBlendColor = value;
};
- (void) setRenderMode:(NSUInteger)value; {
	sfRenderMode = value;
};
- (void) setZScale:(float)value {
	sfZScale = value;
};
- (void) setSampling:(NSUInteger)value {
	sfSampling = value;
};

//------------------------------------------------

- (NSUInteger) backplaneMode {
	return sfBackplaneMode;
};
- (NSColor*) backplaneColor {
	return sfBackplaneColor;
};
- (float) alphaThreshold {
	return sfAlphaThreshold;
};
- (float) alpha {
	return sfAlpha;
};
- (float) brightness {
	return sfBrightness;
};
- (float) blendFactor {
	return sfBlendFactor;
};
- (NSColor*)blendColor {
	return sfBlendColor;
};
- (NSUInteger) renderMode {
	return sfRenderMode;
};
- (float) zScale {
	return sfZScale;
};
- (NSUInteger) sampling {
	return sfSampling;
};

- (id) initWithVolumeSetting:(SFOGLVolumeSetting*)settings {
	self = [super init];
	sfBackplaneMode = settings->sfBackplaneMode;
	sfBackplaneColor = settings->sfBackplaneColor;
	sfAlphaThreshold = settings->sfAlphaThreshold;
	sfAlpha = settings->sfAlpha;
	sfBrightness = settings->sfBrightness;
	sfBlendFactor = settings->sfBlendFactor;
	sfBlendColor = settings->sfBlendColor;
	sfRenderMode = settings->sfRenderMode;
	sfZScale = settings->sfZScale;
	sfSampling = settings->sfSampling;
	return self;
};

- (id) copyWithZone:(NSZone *)zone {
    SFOGLVolumeSetting* copy = [ [ [self class] allocWithZone: zone] initWithVolumeSetting:self];
    return copy;
};

- (id) copy {
	return [self copy];
};

;


@end
