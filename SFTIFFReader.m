//
//  SFTIFFReader.m
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Mon Mar 24 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import "SFTIFFReader.h"
#import "SFTIFFFile.h"
#import "SFTIFFIFD.h"
#import "SFTIFFTag.h"
#import "SFTiff.h"


@implementation SFTIFFReader


- (instancetype)initWithURL:(NSURL *) fileURL {
    if ((self = [super init])) {
        sfTIFFFile = [[SFTIFFFile alloc] initWithURL:fileURL];
        if(sfTIFFFile == nil)
            return nil;
    }
    return self;
}

-(id)initWithFileName:(NSString *)fileName {

	if ((self = [super init])) {
        sfTIFFFile = [[SFTIFFFile alloc] initWithFileName:fileName];
        if(sfTIFFFile == nil)
            return nil;
	}
    return self;
}

-(id)tagWithIndex:(NSUInteger)ifdindex withID:(uint16_t)tagID {
    NSNumber* key = [NSNumber numberWithUnsignedInt:tagID];
    NSArray* IFDs = [sfTIFFFile arrayOfIFDs];
    id  IFD = [IFDs  objectAtIndex:ifdindex];
	id ret = [IFD tagWithID:key];
    return ret;
}

- (NSString*)descriptionAtIndex:(NSUInteger)number {
    NSData *data = [self dataForTagWithIndex:(uint32_t)number withID:IMAGEDESCRIPTION];
    if (data)
        return [NSString stringWithUTF8String:data.bytes];
    else
        return nil;
}

-(NSNumber*)widthAtIndex:(NSUInteger)number {
    return [[self tagWithIndex:(uint32_t)number withID:IMAGEWIDTH] valueAsNumberAtIndex:0];
}

-(NSNumber*)lengthAtIndex:(NSUInteger)number {
    return [[self tagWithIndex:(uint32_t)number withID:IMAGELENGTH] valueAsNumberAtIndex:0];
}

-(NSNumber*)bitsPerSampleAtIndex:(NSUInteger)number {
    return [[self tagWithIndex:(uint32_t)number withID:BITSPERSAMPLE] valueAsNumberAtIndex:0];
}

-(NSNumber*)samplesPerPixelAtIndex:(NSUInteger)number {
    NSNumber *ret;
    ret = [[self tagWithIndex:(uint32_t)number withID:SAMPLESPERPIXEL] valueAsNumberAtIndex:0];
    if (ret == nil)
		ret = [NSNumber numberWithInt:1];
    return ret;
}

-(NSData*)colorMapAtIndex:(NSUInteger)number {
	return [[self tagWithIndex:(uint32_t)number withID:COLORMAP] valueAsData];
}

-(uint32_t)sizeImageDataAtIndex:(NSUInteger)number {
    uint32_t width, length, bits, samples;
    width = (uint32_t)[[self widthAtIndex:(uint32_t)number] unsignedLongValue];
    length = (uint32_t)[[self lengthAtIndex:number] unsignedLongValue];
    bits = (uint32_t)[[self bitsPerSampleAtIndex:number] unsignedLongValue];
    samples = (uint32_t)[[self samplesPerPixelAtIndex:number] unsignedLongValue]; 
    return width*length*(bits/8)*samples;
}

-(NSData*)dataForTagWithIndex:(uint32_t)ifdindex withID:(uint16_t)tagID {
	//NSData* ret = 0;
    SFTIFFTag* tag = [self tagWithIndex:ifdindex withID:tagID];
    /*
	if( [tag isValueOffset] ) {
		[[sfTIFFFile fileHandle] seekToFileOffset:[[tag valueAsNumberAtIndex:0] unsignedLongLongValue]];
		ret = [[sfTIFFFile fileHandle] readDataOfLength:[tag length]];
	}
	else {
        ret = [tag valueAsData];
    }
     */
    return [tag valueAsData];
}

-(NSMutableData*)dataAtIndex:(NSUInteger)number
{	
    //uint32_t count, i, accu, size;
	NSUInteger count, i, accu, size;
    NSUInteger sizeimage = [self sizeImageDataAtIndex:number];
    NSMutableData *data = [NSMutableData dataWithCapacity:sizeimage];
    SFTIFFTag* strips = [self tagWithIndex:(uint32_t)number withID:STRIPOFFSETS];
    SFTIFFTag* bytecounts = [self tagWithIndex:(uint32_t)number withID:STRIPBYTECOUNTS];
    if (strips != nil && bytecounts != nil && [strips tagCount] == [bytecounts tagCount]) {
		count = [strips tagCount];
		accu = 0;
		for (i=0; i<count; i++)
			accu+= [[bytecounts valueAsNumberAtIndex:(uint32_t)i] unsignedLongValue];
		if ( accu == sizeimage ) {
			for (i=0; i<count; i++) {
				size = [[bytecounts valueAsNumberAtIndex:(uint32_t)i] unsignedLongValue];
				[[sfTIFFFile fileHandle] seekToFileOffset:[[strips valueAsNumberAtIndex:(uint32_t)i] unsignedLongLongValue]];
                NSData *tiffData = [[sfTIFFFile fileHandle] readDataOfLength:size];
				[data appendBytes:tiffData.bytes length:tiffData.length];
			}
		}
    }
    return data;
};

- (NSData*)dataAtOffset:(uint32_t)offset {
	[[sfTIFFFile fileHandle] seekToFileOffset:offset];
	NSData* lengthdata = [[sfTIFFFile fileHandle] readDataOfLength:4];
	int32_t *pdata = (int32_t *)[lengthdata bytes];
	int32_t buffersize;
	if ([sfTIFFFile isBigEndian]) 
		buffersize = NSSwapBigIntToHost(pdata[0]);
	else
		buffersize = NSSwapLittleIntToHost(pdata[0]);
	[[sfTIFFFile fileHandle] seekToFileOffset:offset];
	return [[sfTIFFFile fileHandle] readDataOfLength:buffersize];
};

- (NSData*)dataWithLength:(NSUInteger)length offset:(uint32_t)offset {
	[[sfTIFFFile fileHandle] seekToFileOffset:offset];
	return [[sfTIFFFile fileHandle] readDataOfLength:length];
}

- (uint32_t)stripCountAtIndex:(NSUInteger)idx {
	return (uint32_t)[[self tagWithIndex:(uint32_t)idx withID:STRIPOFFSETS] tagCount];
};

- (uint32_t)byteCountAtStrip:(NSUInteger)number index:(NSUInteger)idx {
	SFTIFFTag* bytecounts = [self tagWithIndex:(uint32_t)idx withID:STRIPBYTECOUNTS];
	return (uint32_t)[[bytecounts valueAsNumberAtIndex:(uint32_t)number] unsignedLongLongValue];
}

-(NSMutableData*)dataAtStrip:(uint32_t)number index:(NSUInteger)idx {
	NSMutableData* ret = nil;
//    uint32_t count, i, accu;

    SFTIFFTag* strips = [self tagWithIndex:(uint32_t)idx withID:STRIPOFFSETS];
    SFTIFFTag* bytecounts = [self tagWithIndex:(uint32_t)idx withID:STRIPBYTECOUNTS];
	
    if (strips != nil && bytecounts != nil && number < [strips tagCount] ) {

		NSUInteger size = [[bytecounts valueAsNumberAtIndex:number] unsignedLongValue];
		[[sfTIFFFile fileHandle] seekToFileOffset:[[strips valueAsNumberAtIndex:number] unsignedLongLongValue]];
		ret = [NSMutableData dataWithCapacity:size];
		[ret appendBytes:[[[sfTIFFFile fileHandle] readDataOfLength:size] bytes] length:size]; ;

	} 
    return ret;	
};

- (SFTIFFFile*)tiffFile {
	return sfTIFFFile;
};


@end
