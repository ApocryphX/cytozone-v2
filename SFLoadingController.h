//
//  SFLoadingController.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFNDImporter;

@class SFSourceViewController, SFLargeBitplaneView, SFLargeBitplaneViewController;

@interface SFLoadingController : NSWindowController <NSSplitViewDelegate, NSOutlineViewDelegate> {

    IBOutlet NSSplitView            *splitView;
    IBOutlet NSTreeController       *treeControllerForDictionary;
    IBOutlet NSOutlineView          *fileNodeOutlineView;
    IBOutlet NSView                 *largeBitplaneViewPlaceholder;
    
    SFLargeBitplaneViewController  *largeBitplaneViewController;
    //SFLargeBitplaneView    *largeBitplaneView;
    
    NSOperationQueue *loadingQueue;
	NSString* sfFileName;
	BOOL sfIsThreadRunning;
    //NSSize imageSize;
    NSSize textureTileSize;
    NSString *scaleBarLength;
    double   imageScale;

    NSImage *stackImage;
    NSImage *timeSeriesImage;
    NSImage *timeStackImage;
    NSImage *planeImage;

    NSUInteger sfNodeIndex;
    NSUInteger sfPlaneIndex;
}

@property (retain) SFLargeBitplaneView    *largeBitplaneView;
@property (retain) NSNumber *isVolumeSelected;
@property (retain) NSString *scaleBarLength;
@property (assign) BOOL     showPlaneIndexSlider;

@property (assign) NSUInteger maxPlaneIndex;
@property (assign) NSUInteger planeIndex;
@property (assign) NSUInteger nodeIndex;

- (IBAction)cancelLoading:(id)sender;
- (IBAction)loadData:(id)sender;
- (IBAction)resetTransform:(id)sender;
- (IBAction)toggelScalebar:(id)sender;

- (void)showDataAtNodeIndex:(NSUInteger)nodeIndex;
- (void)setFileName:(NSString*)value;
- (NSString*)fileName;


@end
