//
//  SFNDDataOutlineViewController.m
//  SanFrancisco
//
//
//

#import "SFNDDataOutlineViewController.h"

#import "SFNDImporter.h"
#import "SFNDDimension.h"
#import "SFImageAndTextCell.h"

const CGFloat SFKMinSourceViewSplitSize	= 120.0f;
NSString *OutlineColumnName = @"outlinecolumn";

@interface SFNDDataOutlineViewController ()

- (NSMutableDictionary*)outlineDictionary:(NSDictionary*)inputDictionary;
- (NSArray *)leafNodeIndexPathsForDictionary:(NSDictionary*)rootNode;
- (void)leafNodeIndexPathsForDictionary:(NSDictionary*)node array:(NSMutableArray*)leafNodeIndexPaths indexPath:(NSIndexPath*)indexPath;

@end


@implementation SFNDDataOutlineViewController

@dynamic importer;
@synthesize selectionDelegate;
@synthesize nodeIndexNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
            // Initialization code here.
    }
    
    return self;
}

- (void)awakeFromNib {
    
    stackImage = [NSImage imageNamed:@"stack"];
    timeSeriesImage = [NSImage imageNamed:@"time"];
    timeStackImage = [NSImage imageNamed:@"time_stack"];
    planeImage = [NSImage imageNamed:@"plane"];

    

    NSTableColumn *tableColumn = [fileNodeOutlineView tableColumns][0];
    tableColumn.identifier = OutlineColumnName;
    SFImageAndTextCell *imageAndTextCell = [SFImageAndTextCell new];
    [tableColumn setDataCell:imageAndTextCell];
        // make our outline view appear with gradient selection, and behave like the Finder, iTunes, etc.
    [fileNodeOutlineView setSelectionHighlightStyle:NSTableViewSelectionHighlightStyleSourceList];
        // set self as delegate
    [fileNodeOutlineView setDelegate:self];


}

- (NSView*)insertViewInParentView:(NSView*)parentView {
    if (parentView && self.view) {
        
        [parentView addSubview:self.view];
        
        NSRect newBounds;
        newBounds.origin.x = 0;
        newBounds.origin.y = 0;
        newBounds.size.width = [parentView frame].size.width;
        newBounds.size.height = [parentView frame].size.height;
        [self.view setFrame:[parentView frame]];
        
            // make sure our added subview is placed and resizes correctly
        [self.view setFrameOrigin:NSMakePoint(0,0)];
        [self.view setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
        
    }
    
    return self.view;
}


- (void)setImporter:(SFNDImporter *)importer {
    sfImporter = importer;
    NSDictionary *outlineNodes = importer.outlineNodes[0];
    NSMutableDictionary *outlineDictionary = [self outlineDictionary:outlineNodes];
    [outlineDictionary removeObjectForKey:SFKNodeIconKey];
    SFFileNodeDictionary *rootNodeDictionary = outlineDictionary;
    [treeControllerForDictionary addObject:rootNodeDictionary];
    [fileNodeOutlineView expandItem:nil expandChildren:YES ];

    NSArray *leafNodeIndexPaths = [self leafNodeIndexPathsForDictionary:rootNodeDictionary];
    [treeControllerForDictionary setSelectionIndexPath:leafNodeIndexPaths[0]];
    
    NSArray *selection = [treeControllerForDictionary selectedObjects];
    id firstNode = selection[0];
    if([firstNode isKindOfClass:[NSDictionary class]]) {
        NSDictionary *nodeDictionary = (NSDictionary*)firstNode;
        NSNumber *indexNumber = nodeDictionary[SFKNodeDataIndexKey];
        if(indexNumber) {
            self.nodeIndexNumber=indexNumber;
            if(selectionDelegate ) {
                [selectionDelegate selectedNodeIndex:[indexNumber unsignedIntegerValue]];
            }
        }
    }
}

- (SFNDImporter*)importer {
    return sfImporter;
}


#pragma mark NSOutlineViewDelegate methods

    // ----------------------------------------------------------------------------------------
    // outlineView:isGroupItem:item
    // ----------------------------------------------------------------------------------------
-(BOOL)outlineView:(NSOutlineView*)outlineView isGroupItem:(id)item
{
        //    NSDictionary *node = [item representedObject];
    if(outlineView==fileNodeOutlineView) {
        NSDictionary *dictionary = [item representedObject];
            //        NSString *title = dictionary[SFKNodeTitleKey];
        NSImage *icon = dictionary[SFKNodeIconKey];
        if(icon==nil)
            return YES;
    }
    return  NO;
}


    // -------------------------------------------------------------------------------
    //	outlineView:willDisplayCell
    // -------------------------------------------------------------------------------

- (void)outlineView:(NSOutlineView *)outlineView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	if ([[tableColumn identifier] isEqualToString:OutlineColumnName]) {
            // we are displaying the single and only column
		if ([cell isKindOfClass:[SFImageAndTextCell class]]) {
			id outlineItem = [item representedObject];
            NSDictionary *dictionary = outlineItem;
            [(SFImageAndTextCell*)cell setImage:dictionary[SFKNodeIconKey]];
            
		}
	}
}

    // -------------------------------------------------------------------------------
    //	outlineViewSelectionDidChange:notification
    // -------------------------------------------------------------------------------
- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{
        // ask the tree controller for the current selection
	NSArray *selection = [treeControllerForDictionary selectedObjects];
	if (selection && [selection count] > 0)
	{
        NSArray *selection = [treeControllerForDictionary selectedObjects];
        id firstNode = selection[0];
        if([firstNode isKindOfClass:[NSDictionary class]]) {
            NSDictionary *nodeDictionary = (NSDictionary*)firstNode;
            if(selectionDelegate ) {
                NSNumber *indexNumber = nodeDictionary[SFKNodeDataIndexKey];
                if(indexNumber) {
                    self.nodeIndexNumber = indexNumber;
                    NSUInteger index = [indexNumber unsignedIntegerValue];
                    [selectionDelegate selectedNodeIndex:index];
                }
                else {
                    self.nodeIndexNumber = Nil;
                }
            }
                //self.isVolumeSelected = nodeDictionary[@"isVolume"];
        }
            //[self loadDataForSelection:selection];
	}
}


    // -------------------------------------------------------------------------------
    //	shouldEditTableColumn:tableColumn:item
    //
    //	Decide to allow the edit of the given outline view "item".
    // -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	return NO;
}

    // -------------------------------------------------------------------------------
    //	shouldSelectItem:item
    // -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item;
{
    if(outlineView==fileNodeOutlineView) {
        
        id dictionary = [item representedObject];
        if (dictionary[SFKNodeDataIndexKey]!=nil)
            return YES;
        
        
    }
	return NO;
}


#pragma mark - Split View Delegate

    // -------------------------------------------------------------------------------
    //	splitView:constrainMinCoordinate:
    //
    //	What you really have to do to set the minimum size of both subviews to kMinOutlineViewSplit points.
    // -------------------------------------------------------------------------------
- (CGFloat)splitView:(NSSplitView *)splitView constrainMinCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(NSInteger)index
{
	return proposedCoordinate + SFKMinSourceViewSplitSize;
}

    // -------------------------------------------------------------------------------
    //	splitView:constrainMaxCoordinate:
    // -------------------------------------------------------------------------------
- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(NSInteger)index
{
	return proposedCoordinate - SFKMinSourceViewSplitSize;
}

    // -------------------------------------------------------------------------------
    //	splitView:resizeSubviewsWithOldSize:
    //
    //	Keep the left split pane from resizing as the user moves the divider line.
    // -------------------------------------------------------------------------------
- (void)splitView:(NSSplitView*)sender resizeSubviewsWithOldSize:(NSSize)oldSize
{
	NSRect newFrame = [sender frame]; // get the new size of the whole splitView
	NSScrollView *left = [[sender subviews] objectAtIndex:0];
	NSRect leftFrame = [left frame];
	NSView *right = [[sender subviews] objectAtIndex:1];
	NSRect rightFrame = [right frame];
    
	CGFloat dividerThickness = [sender dividerThickness];
    
	leftFrame.size.height = newFrame.size.height;
    
	rightFrame.size.width = newFrame.size.width - leftFrame.size.width - dividerThickness;
	rightFrame.size.height = newFrame.size.height;
	rightFrame.origin.x = leftFrame.size.width + dividerThickness;
    
	[left setFrame:leftFrame];
	[right setFrame:rightFrame];
}


#pragma mark Outline Dictionary Helper Methods

-(void) leafNodeIndexPathsForDictionary:(NSDictionary*)node array:(NSMutableArray*)leafNodeIndexPaths indexPath:(NSIndexPath*)indexPath {
    id dataIndex = node[SFKNodeDataIndexKey];
    if(dataIndex && [dataIndex isKindOfClass:[NSNumber class]]) {
        [leafNodeIndexPaths addObject:indexPath];
    }
    else {
        id children = node[SFKNodeChildrenKey];
        if(children && [children isKindOfClass:[NSArray class]]) {
            NSUInteger childrenIndex=0;
            for(id child in children) {
                [self leafNodeIndexPathsForDictionary:child
                                                array:leafNodeIndexPaths
                                            indexPath:[indexPath indexPathByAddingIndex:childrenIndex]];
                childrenIndex++;
            }
        }
    }
}

-(NSArray *)leafNodeIndexPathsForDictionary:(NSDictionary*)rootNode {
    NSMutableArray *leafNodeIndexPaths = [NSMutableArray new];
    [self leafNodeIndexPathsForDictionary:rootNode
                                    array:leafNodeIndexPaths
                                indexPath:[NSIndexPath indexPathWithIndex:0]];
    return leafNodeIndexPaths;
}


- (NSMutableDictionary*)outlineDictionary:(NSDictionary*)inputDictionary {
    NSMutableDictionary *dictionary  = [NSMutableDictionary new];
    dictionary[SFKNodeTitleKey]=inputDictionary[SFKNodeTitleKey];
    id dataIndex = inputDictionary[SFKNodeDataIndexKey];
    if(dataIndex && [dataIndex isKindOfClass:[NSNumber class]]) {
        NSUInteger index = [(NSNumber*)dataIndex unsignedIntegerValue];
        SFNDDimension *nddim = [sfImporter nDDimensionAtNodeIndex:index];
        if(nddim.isVolume) {
            dictionary[@"isVolume"]=[NSNumber numberWithBool:YES];
            if(nddim.isTimeSeries) {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:YES];
                dictionary[SFKNodeIconKey]=timeStackImage;
            }
            else {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:NO];
                dictionary[SFKNodeIconKey]=stackImage;
            }
        }
        else {
            dictionary[@"isVolume"]=[NSNumber numberWithBool:NO];
            if(nddim.isTimeSeries) {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:YES];
                dictionary[SFKNodeIconKey]=timeSeriesImage;
            }
            else {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:NO];
                dictionary[SFKNodeIconKey]=planeImage;
            }
        }
        dictionary[SFKNodeDataIndexKey]=(NSNumber*)dataIndex;
    }
    else {
        id children = inputDictionary[SFKNodeChildrenKey];
        if(children && [children isKindOfClass:[NSArray class]]) {
            NSMutableArray *childArray = [NSMutableArray new];
            dictionary[SFKNodeChildrenKey]=childArray;
            dictionary[SFKNodeIconKey]=[NSImage imageNamed:NSImageNameFolder];
            for(id childNode in children) {
                NSDictionary *childNodeDictionary = [self outlineDictionary:childNode];
                [childArray addObject:childNodeDictionary];
            }
        }
    }
    return dictionary;
};

@end
