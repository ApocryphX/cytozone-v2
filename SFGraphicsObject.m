//
//  SFGraphicsObject.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicsObject.h"

@implementation SFAbstractGraphicsObject

@synthesize transform;
@synthesize bounds;
@synthesize isCreationFinished;

- (id)init
{
    self = [super init];
    if (self) {
        self.isCreationFinished = NO;
    }
    return self;
}

- (void)draw {
        //[[NSColor lightGrayColor] set];
        //[self.bezierPath fill];

    [[NSColor redColor] set];
    [self.bezierPath stroke];
}

@end
