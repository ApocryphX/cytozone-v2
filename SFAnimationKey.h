//
//  SFAnimationKey.h
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFAnimationKey : NSObject {
    CGFloat     duration; 
    NSUInteger  keyframeIndex;
    CGFloat     framesPerSecond;
    
    NSUInteger  numberOfFrames;
    CGFloat     stepwiseFraction;
}

@property (assign)  CGFloat     duration;
@property (assign)  NSUInteger  keyframeIndex;
@property (assign)  CGFloat     framesPerSecond;

@property (readonly) CGFloat    stepwiseFraction;
@property (readonly) NSUInteger numberOfFrames;

@end
