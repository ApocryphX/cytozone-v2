//
//  SFND2Reader.h
//  SanFrancisco
//
//
//

#import "SFNDImporter.h"
#import "SFFileNodeTag.h"

@interface SFND2Reader : NSObject <SFNDReading> {
    NSString* sfFileName;
    int LIMFileHandle;
}

@end
