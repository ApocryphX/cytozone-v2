//
//  SFOGLChannelSetting.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

@class SFChannelInfo, SFOGLColor;

@interface SFOGLChannelSetting : NSObject {
	NSOpenGLView* sfOpenGLView;
	SFOGLColor* sfColorOfVolume;
	SFOGLColor* sfColorOfShadow;
	NSString* sfName;
	BOOL sfIsActive;
	BOOL sfIsIntensitySquare;
	BOOL sfShowVolume;	
	NSUInteger sfVolumeMode;
	float sfThreshold;
	NSUInteger sfNoiseFilterSize;
	GLfloat sfOGLShadowColor[4];
	GLfloat sfOGLVolumeColor[4];
}

- (id)initWithChannelInfo:(SFChannelInfo*)chinfo;
- (id)initWithOGLChannelSetting:(SFOGLChannelSetting*)setting;

- (void)setOpenGLView:(NSOpenGLView*)view;

// KVC compliant:
- (void) setName:(NSString*)value;
- (void) setIsActive:(BOOL)value;
- (void) setThreshold:(float)value;
- (void) setVolumeMode:(NSUInteger)value;
- (void) setShowVolume:(BOOL)value;
- (void) setColorOfVolume:(NSColor*)value;
- (void) setColorOfShadow:(NSColor*)value;
- (void) setNoiseFilterSize:(NSUInteger)value;
- (void) setIsIntensitySquare:(BOOL)value;

- (NSString*) name;
- (BOOL) isActive;
- (NSColor*) colorOfVolume;
- (NSColor*) colorOfShadow;
- (float) threshold;
- (NSUInteger) volumeMode;
- (NSUInteger) noiseFilterSize;
- (BOOL) isIntensitySquare;

- (void) setOGLColorWithShadowColor;
- (void) setOGLColorWithVolumeColor;
- (void) setOGLColorWithVolumeColorWithAlpha:(float)alpha;




@end
