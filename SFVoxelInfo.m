//
//  SFVoxelInfo.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFVoxelInfo.h"


@implementation SFVoxelInfo

- (id)initWithVoxelInfo:(SFVoxelInfo *)info {
	self = [super init];
	[self setBytesPerSample:[info bytesPerSample]] ;
	[self setSamplesPerVoxel:[info samplesPerVoxel]];
	[self setPrecision:[info precision]];
	[self setDataType:[info dataType]];
	[self setInterleaveType:[info interleaveType]];
	[self setPhotometricInterpretation:[info photometricInterpretation]];
	return self;
};

- (void)setBytesPerSample:(NSUInteger)value {
	sfBytesPerSample = value;
};

- (void)setSamplesPerVoxel:(NSUInteger)value {
	sfSamplesPerVoxel = value;
};

- (void)setPrecision:(NSUInteger)value {
	sfPrecision = value;
};

- (void)setDataType:(enum SFVoxelDataType)value {
	sfDataType = value;
};

- (void)setInterleaveType:(enum SFVoxelInterleaveType)value {
	sfInterleaveType = value;
};

- (void)setPhotometricInterpretation:(enum SFPhotometricInterpretation)value {
	sfPhotometricInterpretation = value;
};

- (NSUInteger)bytesPerSample 
{
	return sfBytesPerSample;
};


- (NSUInteger)samplesPerVoxel
{
	return sfSamplesPerVoxel;
};

- (NSUInteger)bytesPerVoxel
{
	return sfSamplesPerVoxel*sfBytesPerSample;
};

- (NSUInteger)precision{
	return sfPrecision;
};

- (enum SFVoxelDataType)dataType{
	return sfDataType;
};

- (enum SFVoxelInterleaveType)interleaveType{
	return sfInterleaveType;
};

- (enum SFPhotometricInterpretation)photometricInterpretation{
	return sfPhotometricInterpretation;
};

- (id)copyWithZone:(NSZone *)zone
{
    SFVoxelInfo *copy = [ [ [self class] allocWithZone: zone] initWithVoxelInfo:self];
    return copy;
};

- (id)copy {
	return [self copyWithZone:nil];
}

@end

