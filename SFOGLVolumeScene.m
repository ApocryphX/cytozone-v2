//
//  SFOGLVolumeScene.m
//  VoxelMachine
//
//  Created by ApocryphX on Mon Jul 26 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolumeScene.h"

#include <OpenGL/gl.h>

#import "SFOGLVolume.h"
#import "SFVoxelInfo.h"
#import "SFOGLTransform.h"
#import "SFOpenGLView.h"
#import "SFNDVoxelData.h"
#import "SFVolumeFactory.h"
#import "SFVolumeWindowController.h"
#import "SFOGLChannelSetting.h"
#import "SFOGLRenderState.h"

@interface SFOGLVolumeScene (PrivateMethods)
- (void)setNumberOfCubes:(NSUInteger)value;
@end

@implementation SFOGLVolumeScene

@dynamic cubeIndex;

- (id)init {
	self = [super init];
	sfChannelSettings = [NSMutableArray new];
	sfClipPlane = -1.4;
	sfClipWidth = 2.8;
	sfCubeIndex = 1;
	sfNumberOfCubes = 1;

	[self setVolume:[SFVolumeFactory volumeForMode:sfTextureMode]];
	[sfOGLView setMouseTracker:[sfVolume transform]];
	[self setTextureMode:0];
	return self;
};

- (id)initWithView:(SFOpenGLView*)view {
	[[sfOGLView openGLContext]makeCurrentContext];
	self = [self init];
	[self setView:view];
	return self;
};

- (void)updateNDVolumeData {
	[self setNumberOfCubes:[sfNDVoxelData numberOfCubes]];
	
//	SFOGLChannelSetting* setting = [self objectInChannelSettingsAtIndex:[sfChannelSettings count]-1];
//	NSColor* rgbcolor = [[setting colorOfVolume] colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
	//float r,b,g,a;
	//[rgbcolor getRed:&r green:&g blue:&b alpha:&a];
	//if ( r==1.0f && g==1.0f && b== 1.0f ) {
	//	[setting setColorOfVolume:[[NSColor whiteColor]colorWithAlphaComponent:0.0f]];
	//	[sfVolume setBackplaneMode:2];
	//}
	[sfVolume setChannelSettings:sfChannelSettings];
    [[sfOGLView openGLContext]makeCurrentContext];
	[sfVolume setNDVoxelData:sfNDVoxelData];
};

- (void) setView:(SFOpenGLView*)value {
	sfOGLView = value;
	//[sfOGLView setTextureTransform:sfTextureTransform];
	//[sfOGLView setMouseTracker:[self textureTransform]];
    [[sfOGLView openGLContext]makeCurrentContext];
	[sfVolume setOpenGLView:sfOGLView];
};

- (SFOpenGLView*)view {
	return sfOGLView;
};


- (void)setNDVoxelData:(SFNDVoxelData*)nddata {
	sfNDVoxelData = nddata;	
	
	while( [self countOfChannelSettings]) 
		[self removeObjectFromChannelSettingsAtIndex:0];
	if (nddata) {
		NSUInteger i, count = [[sfNDVoxelData channelInfos] count];
		for(i=0; i<count; i++) {
			SFOGLChannelSetting* newsetting = [[SFOGLChannelSetting alloc]initWithChannelInfo:[[sfNDVoxelData channelInfos]objectAtIndex:i]];
            [[sfOGLView openGLContext]makeCurrentContext];
			[newsetting setOpenGLView:sfOGLView];
			[self insertObject:newsetting inChannelSettingsAtIndex:i];
		}	
		[self updateNDVolumeData];
	}
};	

- (void)drawScene {
	GLdouble clip_coords[2][4] ={{ 0.0, 0.0, -1.0, 0.0 }, { 0.0, 0.0, 1.0, 0.0 }};

	glMatrixMode(GL_MODELVIEW);
	glTranslatef(0.0f,0.0f,-1.0*sfClipPlane);
	glEnable(GL_CLIP_PLANE0);
	glClipPlane(GL_CLIP_PLANE0, clip_coords[0]);
	
	glTranslatef(0.0f,0.0f,-1.0*sfClipWidth);
	glEnable(GL_CLIP_PLANE1);
	glClipPlane(GL_CLIP_PLANE1, clip_coords[1]);
	glTranslatef(0.0f,0.0f,sfClipWidth);	
	
	glTranslatef(0.0f,0.0f, 1.0f*sfClipPlane);
    
	[sfVolume draw];
};

- (void)setTextureTransform:(SFOGLTransform*)value {
	[sfVolume setTextureTransform:value];
	[sfOGLView setMouseTracker:value];
};

- (SFOGLTransform*)textureTransform {
	return [sfVolume transform] ;
};


- (void)setNumberOfCubes:(NSUInteger)value {
	sfNumberOfCubes = value;
};

- (NSUInteger)numberOfCubes {
	return sfNumberOfCubes;
}

- (void)setCubeIndex:(NSUInteger)value {
	if (value <= sfNumberOfCubes && value > 0) {
		sfCubeIndex = value;
		[sfVolume setVolumeIndex:sfCubeIndex-1];
	}
};

- (NSUInteger)cubeIndex {
	return sfCubeIndex;
};


- (void) setTextureMode:(NSUInteger)value {
	if (value != sfTextureMode) {
		sfTextureMode = value;
		if(sfNDVoxelData) {
			SFOGLVolume* newvolume = [SFVolumeFactory volumeForMode:sfTextureMode];
			[newvolume setRenderState: [sfVolume renderState]];
			[newvolume setTextureTransform:[sfVolume transform]];
			[newvolume setOpenGLView:sfOGLView];
			if (sfCubeIndex > 1)
				[newvolume setVolumeIndex:sfCubeIndex-1];
			[self setVolume:newvolume];
			[self updateNDVolumeData];
		}			
	}
};

- (NSUInteger) textureMode {
	return sfTextureMode;
};

- (void)setClipPlane:(float)value {
	sfClipPlane = value;
	[sfOGLView setNeedsDisplay:YES];
};

- (float)clipPlane {
	return sfClipPlane;
};

- (void)setClipWidth:(float)value {
	sfClipWidth = value;
	[sfOGLView setNeedsDisplay:YES];
};

- (float)clipWidth {
	return sfClipWidth;
};


- (void) setVolumeIndex:(NSUInteger)value {
	sfVolumeIndex = value;
	[sfVolume setVolumeIndex:value];
        //[sfOGLView setNeedsDisplay:YES];
};


- (NSUInteger) volumeIndex {
	return sfVolumeIndex;
};

- (void)setVolume:(SFOGLVolume*)volume {
	sfVolume = volume;
	[sfVolume setOpenGLView:sfOGLView];
	[sfOGLView setMouseTracker:[sfVolume transform]];
	//[sfVolume setTransform:sfTextureTransform];
};

- (SFOGLVolume*)volume {
	return sfVolume;
};

- (NSUInteger)countOfChannelSettings {
	return [sfChannelSettings count];
};

- (id)objectInChannelSettingsAtIndex:(NSUInteger)idx {
	return [sfChannelSettings objectAtIndex:idx];
};

- (void)insertObject:(id)anObject inChannelSettingsAtIndex:(NSUInteger)idx {
	[sfChannelSettings insertObject:anObject atIndex:idx];
};

- (void)removeObjectFromChannelSettingsAtIndex:(NSUInteger)idx {
	[sfChannelSettings removeObjectAtIndex:idx];
};

- (void)dealloc 
{
	[sfVolume setOpenGLView:nil];
	//[sfTextureTransform release];
};

@end
