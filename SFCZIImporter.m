//
//  SFCZIImporter.m
//  Cytozone
//
//  Created by Kolja Wawrowsky on 4/26/16.
//  Copyright © 2016 Elarity, LLC. All rights reserved.
//

#import "SFCZIImporter.h"

#import "SFCZIReader.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"

#import "czifile.h"
#import "SFCZIReader.h"

const NSString *KCZIDirectoryPixelTypeName = @"pixel_type";
const NSString *KCZIDirectoryFilePositionName = @"file_position";
const NSString *KCZIDirectoryFilePartName = @"file_part";
const NSString *KCZIDirectoryCompressionName = @"compression";
const NSString *KCZIDirectoryPyramidTypeName = @"pyramid_type";
const NSString *KCZIDirectoryDimensionCount = @"dimension_count";

const NSString *KCZIDirectoryDataOffsetName = @"data_offset";
const NSString *KCZIDirectoryDataSizeName = @"data_size";
const NSString *KCZIIndexNumberName = @"index_number";


struct _CZISubBlockDirectorySegment {
    int32_t EntryCount;
    uint8_t Reserved[124];
};
typedef struct _CZISubBlockDirectorySegment CZISubBlockDirectorySegment;

const NSString *KHeaderSegmentKey = @"ZISRAWFILE";
const NSString *KDirectorySegmentKey = @"ZISRAWDIRECTORY";
const NSString *KSubBlockSegmentKey = @"ZISRAWSUBBLOCK";
const NSString *KMetaDataSegmentKey = @"ZISRAWMETADATA";
const NSString *KAttachmentSegmentKey = @"ZISRAWATTACH";
const NSString *KAttachmentDirectorySegmentKey = @"ZISRAWATTDIR";
const NSString *KDeletedSegmentKey = @"DELETED";

const NSString *KCZIVersionMajorKey = @"major";
const NSString *KCZIVersionMinorKey = @"minor";
const NSString *KCZIPrimaryFileGUIDName = @"primary_file_guid";
const NSString *KCZIFileGUIDName = @"file_guid";

const NSString *KCZIFilePartName = @"file_part";
const NSString *KCZIDirectoryPositionName = @"directory_position";
const NSString *KCZIMetadataPositionName = @"metadata_position";
const NSString *KCZIUpdatePendingName = @"update_pending";
const NSString *KCZIAttachmentDirectoryName = @"attachment_directory";

const NSString *KCZIChannelColorName = @"channel_color";
const NSString *KCZIBlockOffsetName = @"block_offset";
const NSString *KCZIUsedSizeName = @"used_size";
const NSString *KCZIBlockIDName = @"block_id_name";


@interface SFCZIImporter ()

@property (retain) NSDictionary *fileHeaderDictionary;
@property (retain) NSString *fileName;
@property (retain) NSFileHandle	*fileHandle;
@property (retain) NSMutableArray  *blockDictionaryArray;
@property (retain) NSMutableDictionary *fileDictionary;

@property (retain) NSXMLDocument *metaDataDocument;
@property (retain) NSMutableDictionary *channelDictionary;
@property (retain) SFNDDimension *ndDimensions;

@end

@interface SFCZIDimension : NSObject

@property (retain) NSString *name;
@property (assign) int32_t start;
@property (assign) int32_t size;
@property (assign) float startCoordinate;
@property (assign) int32_t storedSize;

@end

@implementation SFCZIDimension
@end

@implementation SFCZIImporter


- (NSArray*)fileNodes {
    NSMutableDictionary *dataSetDictionary = [NSMutableDictionary new];
    dataSetDictionary[SFKNodeDataIndexKey]=[NSNumber numberWithUnsignedInteger:0];
    dataSetDictionary[SFKNodeTitleKey]=[[_fileName lastPathComponent]stringByDeletingPathExtension];
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    NSString *imagePath = [[NSBundle bundleForClass:[self class]] pathForResource:SFKZeissFileIconName ofType:@"icns"];
    dictionary[SFKNodeIconKey] = [[NSImage alloc]initByReferencingFile:imagePath];
    dictionary[SFKNodeTitleKey]=[_fileName lastPathComponent];
    dictionary[SFKNodeChildrenKey] = [NSArray arrayWithObject:dataSetDictionary];
    return [NSArray arrayWithObject:dictionary];
}

- (NSArray*)blockDictionariesForID:(const NSString*)idName {
    NSMutableArray *idArray = [NSMutableArray new];
    for (NSDictionary *blockDictionary in _blockDictionaryArray) {
        NSString *blockID = [blockDictionary objectForKey:KCZIBlockIDName];
        if([idName isEqualToString:blockID ]) {
            [idArray addObject:blockDictionary];
        }
    }
    return idArray;
};

- (id)initWithPath:(NSString*)path {
    self = [super init];
    if(self) {
        
        SFCZIReader *reader = [[SFCZIReader alloc]initWithPath:path];
        
        _fileName = path;
        _fileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
        
        _blockDictionaryArray = [NSMutableArray new];
        unsigned long long fileSize = [_fileHandle seekToEndOfFile];
        unsigned long long offset = 0;
        do {
            [_fileHandle seekToFileOffset:offset];
            NSData *headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
            const CZISegmentHeader *header = [headerData bytes];
            NSString *headerID = [NSString stringWithCString:header->segmentID encoding:NSASCIIStringEncoding];
            NSNumber *offsetNumber = [NSNumber numberWithLongLong:[_fileHandle offsetInFile]];
            NSNumber *usedSizeNumber = [NSNumber numberWithLongLong:header->usedSize];
            // create dictionary
            NSMutableDictionary *blockInfoDictionary = [NSMutableDictionary new];
            [blockInfoDictionary setObject:headerID forKey:KCZIBlockIDName];
            [blockInfoDictionary setObject:offsetNumber forKey:KCZIBlockOffsetName];
            [blockInfoDictionary setObject:usedSizeNumber forKey:KCZIUsedSizeName];
            offset = _fileHandle.offsetInFile + header->allocatedSize;
            
            [_blockDictionaryArray addObject:blockInfoDictionary];
        }
        while(offset < fileSize);
        
        [_fileHandle seekToFileOffset:0];
        NSData *headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
        const CZISegmentHeader *header = [headerData bytes];
        
        NSString *headerIDString = [NSString stringWithCString:header->segmentID encoding:NSASCIIStringEncoding];
        BOOL isFileHeader = [KHeaderSegmentKey  isEqualToString:headerIDString];
        if(isFileHeader) {
            NSData *segmentData = [_fileHandle readDataOfLength:header->usedSize];
            const CZIFileHeader *fileHeader = segmentData.bytes;
            _fileHeaderDictionary = @{ KCZIVersionMajorKey:[NSNumber numberWithUnsignedInt:fileHeader->major],
                                       KCZIVersionMinorKey:[NSNumber numberWithUnsignedInt:fileHeader->minor],
                                       KCZIPrimaryFileGUIDName: [NSData dataWithBytes:fileHeader->primaryFileGUID length:16],
                                       KCZIFilePartName: [NSNumber numberWithUnsignedInt:fileHeader->filePart],
                                       KCZIDirectoryPositionName:[NSNumber numberWithUnsignedLongLong:(fileHeader->directoryPosition)],
                                       KCZIMetadataPositionName: [NSNumber numberWithUnsignedLongLong:(fileHeader->metadataPosition)],
                                       KCZIUpdatePendingName: [NSNumber numberWithUnsignedInt:(fileHeader->updatePending)],
                                       KCZIAttachmentDirectoryName: [NSNumber numberWithUnsignedLongLong:(fileHeader->attachementDirectoryPosition)]
                                       
                                       };
        }
        
        NSNumber *metadataOffsetNumber = [_fileHeaderDictionary objectForKey:KCZIMetadataPositionName];
        if(metadataOffsetNumber){
            offset = metadataOffsetNumber.unsignedLongLongValue;
            [_fileHandle seekToFileOffset:offset];
            NSData *headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
            const CZISegmentHeader *header = [headerData bytes];
            NSData *xmlBlockData =[_fileHandle readDataOfLength:header->usedSize];
            const CZIXmlHeader *xmlheader = xmlBlockData.bytes;
            NSData * xmlStringData = [NSData dataWithBytes:xmlBlockData.bytes+256 length:xmlheader->xmlsize];
            [xmlStringData writeToFile:[@"~/CZI-data.xml" stringByExpandingTildeInPath] atomically:YES];
            NSString *xmlString = [[NSString alloc]initWithData:xmlStringData encoding:NSUTF8StringEncoding];
            NSError *error;
            _metaDataDocument = [[NSXMLDocument alloc]initWithXMLString:xmlString options:NSXMLNodeOptionsNone error:&error];
            
        }
        
        
        NSNumber *directoryOffsetNumber = _fileHeaderDictionary[KCZIDirectoryPositionName];
        if(directoryOffsetNumber){
            offset = directoryOffsetNumber.unsignedLongLongValue;
            [_fileHandle seekToFileOffset:offset];
            // read the directory segment header
            NSData *segmentHeaderData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
            const CZISegmentHeader *segmentHeader = [segmentHeaderData bytes];
            int result = memcmp (segmentHeader->segmentID, "ZISRAWDIRECTORY", 15);
            if(result != 0)
                NSLog(@"internal inconsistency, segment id is:%s should be ZISRAWDIRECTORY", segmentHeader->segmentID);
            NSData *directorySegmentData = [_fileHandle readDataOfLength:sizeof(CZISubBlockDirectorySegment)];
            const CZISubBlockDirectorySegment *directorySegment = directorySegmentData.bytes;
            
            unsigned long long directoryDataOffset = _fileHandle.offsetInFile;
            // EntryCount
            NSUInteger directoryBlockIndex, directoryBlockCount = directorySegment->EntryCount;
            NSMutableArray *directoryBlockArray = [NSMutableArray arrayWithCapacity:directorySegment->EntryCount]; // array of all directory blocks
            for(directoryBlockIndex=0;directoryBlockIndex<directoryBlockCount;directoryBlockIndex++) {
                NSData *directoryEntryData = [_fileHandle readDataOfLength:sizeof(CZIDirectoryEntryDV)];
                const CZIDirectoryEntryDV *directoryEntry = directoryEntryData.bytes;
                NSData *dimensionArrayData = [_fileHandle readDataOfLength:sizeof(CZIDimensionEntryDV1)*directoryEntry->DimensionCount];
                
                NSUInteger dimensionIndex, dimensionCount = directoryEntry->DimensionCount;
                NSMutableArray *dimensionArray = [NSMutableArray arrayWithCapacity:directoryEntry->DimensionCount]; // array of all directory blocks
                for(dimensionIndex=0;dimensionIndex<dimensionCount;dimensionIndex++) {
                    const CZIDimensionEntryDV1 *dimensionEntry = dimensionArrayData.bytes+20*dimensionIndex; //CZIDimensionEntryDV1
                    SFCZIDimension *dimension = [SFCZIDimension new];
                    dimension.name = [NSString stringWithCString:dimensionEntry->Dimension encoding:NSASCIIStringEncoding];
                    dimension.start = dimensionEntry->Start;
                    dimension.size = dimensionEntry->Size;
                    dimension.startCoordinate = dimensionEntry->StartCoordinate;
                    dimension.storedSize = dimensionEntry->StoredSize;
                    NSLog(@"dimension:%@ start:%d size:%d start:%f space:%d",dimension.name, dimension.start, dimension.size, dimension.startCoordinate, dimension.storedSize);
                    [dimensionArray addObject:dimension];
                }
                [directoryBlockArray addObject:dimensionArray];
            }
            
            NSError *error;
            NSArray *experimentBlockArray = [_metaDataDocument nodesForXPath:@"/ImageDocument/Metadata/Experiment/ExperimentBlocks" error:&error];
            NSXMLNode *experimentBlockNode = experimentBlockArray[0];
            NSXMLNode *acquisitionNode = [[experimentBlockNode  nodesForXPath:@"AcquisitionBlock" error:&error] objectAtIndex:0];
            NSXMLNode *acquisitionModeNode  = [[acquisitionNode   nodesForXPath:@"AcquisitionModeSetup" error:&error] objectAtIndex:0];
            
            double scale[3];
            NSXMLElement *scalingElement;
            NSString *scalingString;
            
            scalingElement = [[acquisitionModeNode nodesForXPath:@"ScalingX" error:&error] objectAtIndex:0];
            if(scalingElement) {
                scalingString = scalingElement.stringValue;
                scale[0] = [scalingString doubleValue];
            }
            scalingElement = [[acquisitionModeNode nodesForXPath:@"ScalingY" error:&error] objectAtIndex:0];
            if(scalingElement) {
                scalingString = scalingElement.stringValue;
                scale[1] = [scalingString doubleValue];
            }
            scalingElement = [[acquisitionModeNode nodesForXPath:@"ScalingZ" error:&error] objectAtIndex:0];
            if(scalingElement) {
                scalingString = scalingElement.stringValue;
                scale[2] = [scalingString doubleValue];
            }
            
            //NSXMLElement *scalingXElement = [acquisitionModeNode elementWithName:@"ScalingX"];
            _ndDimensions = [SFNDDimension new];
            /*
             // extents calculated
             NSString *dimensionID;
             NSDictionary *dimension;
             dimensionID = @"X";
             dimension = dimensionDictionary[dimensionID];
             NSLog(@"extent %s: %lu", [dimensionID cStringUsingEncoding:NSASCIIStringEncoding], [dimension[@"extent"] unsignedIntegerValue]);
             if(scale[0])
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             magnitude:@(scale[0]*[dimension[@"extent"] doubleValue])
             dimensionType:SFKDimensionTypeSpaceName
             symbol:dimensionID
             unit:@"m"];
             else
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             dimensionType:SFKDimensionTypeSpaceName
             symbol:dimensionID];
             
             dimensionID = @"Y";
             dimension = dimensionDictionary[dimensionID];
             NSLog(@"extent %s: %lu", [dimensionID cStringUsingEncoding:NSASCIIStringEncoding], [dimension[@"extent"] unsignedIntegerValue]);
             if(scale[1])
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             magnitude:@(scale[1]*[dimension[@"extent"] doubleValue])
             dimensionType:SFKDimensionTypeSpaceName
             symbol:dimensionID
             unit:@"m"];
             else
             
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             dimensionType:SFKDimensionTypeSpaceName
             symbol:dimensionID];
             
             
             dimensionID = @"Z";
             dimension = dimensionDictionary[dimensionID];
             
             if([dimension[@"extent"] unsignedIntegerValue]>1) {
             NSLog(@"extent %s: %lu", [dimensionID cStringUsingEncoding:NSASCIIStringEncoding], [dimension[@"extent"] unsignedIntegerValue]);
             if(scale[2])
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             magnitude:@(scale[2]*[dimension[@"extent"] doubleValue])
             dimensionType:SFKDimensionTypeSpaceName
             symbol:dimensionID
             unit:@"m"];
             else
             
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             dimensionType:SFKDimensionTypeSpaceName
             symbol:dimensionID];
             }
             
             dimensionID = @"T";
             dimension = dimensionDictionary[dimensionID];
             if([dimension[@"extent"] unsignedIntegerValue]>1) {
             NSLog(@"extent %s: %lu", [dimensionID cStringUsingEncoding:NSASCIIStringEncoding], [dimension[@"extent"] unsignedIntegerValue]);
             [_ndDimensions addDimensionWithExtent:[dimension[@"extent"] unsignedIntegerValue]
             dimensionType:SFKDimensionTypeTimeName
             symbol:dimensionID];
             }
             
             
             dimensionID = @"C";
             dimension = dimensionDictionary[dimensionID];
             NSLog(@"extent %s: %lu", [dimensionID cStringUsingEncoding:NSASCIIStringEncoding], [dimension[@"extent"] unsignedIntegerValue]);
             
             dimension = dimensionDictionary[@"C"];
             NSUInteger channelCount = [dimension[@"extent"] unsignedIntegerValue];
             
             NSUInteger extent[] = {
             [dimension[@"X"] unsignedIntegerValue],
             [dimension[@"Y"] unsignedIntegerValue],
             [dimension[@"Z"] unsignedIntegerValue],
             [dimension[@"T"] unsignedIntegerValue] };
             
             [_fileHandle seekToFileOffset:directoryDataOffset];
             
             _channelDictionary = [NSMutableDictionary new];
             
             for(directoryBlockIndex=0;directoryBlockIndex<directoryBlockCount;directoryBlockIndex++) {
             NSData *directoryEntryData = [_fileHandle readDataOfLength:sizeof(CZIDirectoryEntryDV)];
             const CZIDirectoryEntryDV *directoryEntry = directoryEntryData.bytes;
             
             NSMutableDictionary *directoryDictionary = [NSMutableDictionary new];
             directoryDictionary[KCZIDirectoryPixelTypeName] = @(directoryEntry->PixelType) ;
             directoryDictionary[KCZIDirectoryFilePositionName] = @(directoryEntry->FilePosition);
             directoryDictionary[KCZIDirectoryFilePartName] = @(directoryEntry->FilePart);
             directoryDictionary[KCZIDirectoryCompressionName] = @(directoryEntry->Compression);
             directoryDictionary[KCZIDirectoryPyramidTypeName] = @(directoryEntry->PyramidType);
             
             NSData *dimensionArrayData = [_fileHandle readDataOfLength:sizeof(CZIDimensionEntryDV1)*directoryEntry->DimensionCount];
             //const CZIDimensionEntryDV1 *dimensionEntry = dimensionArrayData.bytes; //CZIDimensionEntryDV1
             
             NSUInteger dimensionIndex, dimensionCount = directoryEntry->DimensionCount;
             for(dimensionIndex=0;dimensionIndex<dimensionCount;dimensionIndex++) {
             const CZIDimensionEntryDV1 *dimensionEntry = dimensionArrayData.bytes+20*dimensionIndex; //CZIDimensionEntryDV1
             NSString *dimensionNameString = [NSString stringWithCString:dimensionEntry->Dimension encoding:NSASCIIStringEncoding];
             NSMutableDictionary *dimension = dimensionDictionary[dimensionNameString];
             if(dimension==nil)
             return nil;
             dimension[@"extent"] = @(dimensionEntry->Start+dimensionEntry->Size);
             }
             
             
             dimension = dimensionDictionary[@"C"];
             NSUInteger channel =  [dimension[@"extent"] unsignedIntegerValue]-1;
             
             dimension = dimensionDictionary[@"Z"];
             NSUInteger zPlane = [dimension[@"extent"] unsignedIntegerValue]-1;
             
             dimension = dimensionDictionary[@"T"];
             NSUInteger timePoint =  [dimension[@"extent"] unsignedIntegerValue]-1;
             
             
             unsigned long long directoryBlockOffset = _fileHandle.offsetInFile;
             // seek to
             [_fileHandle seekToFileOffset:directoryEntry->FilePosition];
             NSData *headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
             const CZISegmentHeader *header = [headerData bytes];
             NSData *subBlockSegmentData = [_fileHandle readDataOfLength:sizeof(CZISubBlockSegment)];
             const CZISubBlockSegment *subBlockSegment = subBlockSegmentData.bytes;
             unsigned long long dataOffsetInBlock = header->usedSize-subBlockSegment->DataSize+sizeof(CZISegmentHeader);
             unsigned long long dataOffset = directoryEntry->FilePosition+dataOffsetInBlock;
             NSDictionary *dataDictionary = @{ KCZIDirectoryDataOffsetName : @(dataOffset),
             KCZIDirectoryDataSizeName : @(subBlockSegment->DataSize) };
             
             if(timePoint==0)
             NSLog(@"");
             NSUInteger planeIndex = directoryBlockIndex/channelCount;
             //zPlane+timePoint*extent[2];
             
             NSMutableDictionary *planeDictionary = _channelDictionary[@(channel)];
             if(planeDictionary == nil) {
             planeDictionary = [NSMutableDictionary new];
             _channelDictionary[@(channel)] = planeDictionary;
             NSLog(@"new channel:%ld ",channel);
             }
             planeDictionary[@(planeIndex)] = dataDictionary;
             NSLog (@"channel:%ld index: %ld, z:%ld t:%ld",channel, planeIndex, zPlane, timePoint);
             [_fileHandle seekToFileOffset:directoryBlockOffset];
             }
             */
        }
    }
    return self;
};

+ (NSArray*)fileExtensions {
    return [NSArray arrayWithObjects:@"czi", nil];
};

+ (BOOL)isConfocalFile:(NSString*)filePath {
    return YES;
};

- (NSUInteger)count {
    return 1;
};

- (NSString*)name{
    return @"Zeiss CZI";
};

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex{
    NSUInteger count = _channelDictionary.allKeys.count;
    return count;
};

- (SFNDDimension*)nDDimensionAtNodeIndex: (NSUInteger)nodeIndex {
    return _ndDimensions;
};

- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex{
    NSArray *pixelTypeArray = [_metaDataDocument nodesForXPath:@"/ImageDocument/Metadata/Information/Image/PixelType" error:nil];
    NSXMLElement *pixelTypeElement = pixelTypeArray[0];
    NSString *valueString = pixelTypeElement.stringValue;
    SFVoxelInfo *voxelInfo = nil;
    
    if([valueString isEqualToString:@("Gray8")]) {
        voxelInfo = [SFVoxelInfo new];
        voxelInfo.bytesPerSample = 1;
        voxelInfo.samplesPerVoxel = 1;
        voxelInfo.precision = 8;
        voxelInfo.interleaveType = kPlanar;
    }
    else if([valueString isEqualToString:@("Gray16")]) {
        voxelInfo = [SFVoxelInfo new];
        voxelInfo.bytesPerSample = 2;
        voxelInfo.samplesPerVoxel = 1;
        voxelInfo.interleaveType = kPlanar;
        NSArray *bitCountArray = [_metaDataDocument nodesForXPath:@"/ImageDocument/Metadata/Information/Image/ComponentBitCount" error:nil];
        NSXMLElement *pixelTypeElement = bitCountArray[0];
        NSString *valueString = pixelTypeElement.stringValue;
        voxelInfo.precision = valueString.integerValue;
        
    }
    return voxelInfo;
};

- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex{
    NSColor *channelColor = nil;
    NSArray *channelArray = [_metaDataDocument nodesForXPath:@"/ImageDocument/Metadata/DisplaySetting/Channels/Channel" error:nil];
    NSXMLElement *channelElement = channelArray[channelIndex];
    if(channelElement) {
        union  {
            uint32_t colorValue;
            uint8_t component[4];
        } color;
        NSArray *colorNodeArray = [channelElement elementsForName:@"Color"];
        if(colorNodeArray) {
            NSXMLElement *colorElement = colorNodeArray[0];
            if(colorElement) {
                NSString *colorString = colorElement.stringValue;
                NSString *hexValueString = [colorString substringFromIndex:1];
                NSScanner *hexScanner = [NSScanner scannerWithString:hexValueString];
                
                if([hexScanner scanHexInt:&color.colorValue]) {
                    NSColor *channelRGBColor = [NSColor colorWithDeviceRed:color.component[2]
                                                                     green:color.component[1]
                                                                      blue:color.component[0]
                                                                     alpha:1.0];
                    channelColor = channelRGBColor;
                }
            }
        }
    }
    return channelColor;
};

- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex{
    NSData *imageData = nil;
    NSDictionary *dataDictionary = _channelDictionary[@(channelIndex)];
    if(dataDictionary) {
        NSDictionary *planeDictionary = dataDictionary[@(planeIndex)];
        if(planeDictionary) {
            NSNumber *offsetNumber = planeDictionary[KCZIDirectoryDataOffsetName];
            NSNumber *sizeNumber = planeDictionary[KCZIDirectoryDataSizeName];
            [_fileHandle seekToFileOffset:offsetNumber.unsignedLongLongValue];
            imageData = [_fileHandle readDataOfLength:sizeNumber.unsignedLongLongValue];
        }
    }
    return imageData;
};


@end
