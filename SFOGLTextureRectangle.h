//
//  SFOGLTexture2D.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFGeometry.h"

#include <OpenGL/OpenGL.h>
 
@class  SFVoxelInfo, SFOGLVoxelInfo, SFAlignment2D, SFVoxelData, SFNDDimension;

@interface SFOGLTextureRectangle : NSObject {
	NSOpenGLContext* sfContext;
	SFOGLVoxelInfo* sfOGLVoxelInfo;
	SFAlignment2D* sfAlignment;
	//SFNDDimension* sfNDDimension;
	NSMutableData* sfTexData;
	
	BOOL sfIsDownsampled;
	GLenum sfFormat;
	GLuint sfTexName;
    GLuint sfMaxTextureSize;
};


- (id)initWithContext:(NSOpenGLContext*)cxt;

- (void)setContext:(NSOpenGLContext*)cxt;
- (void)setNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo;

- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment;

- (SFSize)size;

- (BOOL) isDownsampled;
- (SFAlignment2D*)alignment;

- (void) bind;
+ (void) unbind;

@end


