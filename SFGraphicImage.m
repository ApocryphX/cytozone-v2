//
//  SFGraphicImage.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicImage.h"

@implementation SFGraphicImage

@synthesize image;

- (void)draw
{
        //[image drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    
        // Or stretch image to fill view
        [image drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
}


@end
