//
//  SFVolumeViewController.h
//  VoxelMachine
//
//  Created by ApocryphX on Sat Jul 10 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define kMinOutlineViewSplit 220.0f

@class SFOpenGLView, SFOGLVolumeScene, SFNDVoxelData, SFOGLTransform, SFNDDataset, SFMovieFrameViewController, SFVolumeAnimator;

@interface SFVolumeWindowController : NSWindowController <NSToolbarDelegate> {
	IBOutlet NSTreeController	*sfTreeController;
	
	IBOutlet NSArrayController  *sfDatasetsController;
	IBOutlet SFOpenGLView       *sfOGLView;
    IBOutlet NSView            *lowerViewPlaceholder;
    IBOutlet NSButton           *sfDeleteKeyframeButton;
    IBOutlet NSProgressIndicator *progressIndicator;
    NSUInteger                  sfBottomViewHeight;
    
    CGFloat                     movieViewHeight;

    SFMovieFrameViewController *movieFrameViewController;
    
    SFVolumeAnimator    *volumeAnimator;
	SFOGLVolumeScene    *sfVolumeScene;
	SFNDDataset         *sfNDDataset;
	NSString            *nodeTitle;
	NSUInteger sfIndex;
	NSUInteger sfTransformIndex;
    
}

@property (retain)  SFNDDataset *nDDataset;
@property (assign) BOOL isTimeSeries;
@property (assign) BOOL isVolume;
@property (retain) NSString *nodeTitle;
@property (readonly) NSProgressIndicator *progressIndicator;

- (BOOL) isTimeSeries;
//- (void)setNDDataset:(SFNDDataset*)nddata;

-(IBAction)moveView:(id)sender;

- (IBAction) updateView:(id)sender;
- (IBAction) useZoom:(id)sender;
- (IBAction) useRotation:(id)sender;
- (IBAction) useTranslation:(id)sender;
	
- (IBAction)firstCube:(id)sender;
- (IBAction)lastCube:(id)sender;
- (IBAction)nextCube:(id)sender;
- (IBAction)prevCube:(id)sender;

- (IBAction) transformTexture:(id)sender;
//- (IBAction) transformClipPlane:(id)sender;
//- (IBAction) transformBackplane:(id)sender;

- (IBAction) makeMovie:(id)sender;

- (IBAction) showSelectedDataset:(id)sender;

- (IBAction) showInfoPanel:(id)sender;

- (IBAction) resetRotation:(id)sender;

- (IBAction)saveShapshot:(id)sender;
//- (IBAction) processSelectedChannel:(id)sender;


- (SFOGLTransform*)textureTransform;

- (void)setVolumeScene:(SFOGLVolumeScene*)value;
- (SFOGLVolumeScene*)volumeScene;

- (void)setIndex:(NSUInteger)idx;
- (NSUInteger)index;

- (void)setTransformIndex:(NSUInteger)idx;
- (NSUInteger)transformIndex;

- (SFOpenGLView*)oGLView;

//- (NSArray*)selectedDatasets;
	
@end

@interface SFVolumeWindowController (Toolbar) 

- (void) setupToolbar;



@end


