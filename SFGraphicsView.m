//
//  SFGraphicsView.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "SFGraphicsView.h"

#import "SFDrawingHandle.h"
#import "SFGraphicRectangle.h"
#import "SFGraphicPolygon.h"
#import "SFGraphicImage.h"
#import "SFGraphicOval.h"
#import "SFGraphicOpenGL.h"
#import "SFGraphicNDDataImage.h"

NSString *KModeSelectionKey = @"selection";
NSString *KModeNewObjectKey = @"new_object";

NSString *KObjectRectangleClassKey = @"SFGraphicRectangle";
NSString *KObjectPolygonClassKey = @"SFGraphicPolygon";
NSString *KObjectOvalClassKey = @"SFGraphicOval";

NSDictionary *KModeDictionary;

@implementation SFGraphicsView

@synthesize minimumScale;
@synthesize maximumScale;

@synthesize transform;

@synthesize zoom;
@synthesize handleSize;
@synthesize handleColor;
@synthesize handleShadowColor;

@synthesize graphicsObjects;
@synthesize selectionRectangle;

@synthesize mouseMode;
@synthesize objectType;


- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        self.wantsLayer=YES;
        graphicsObjects = [NSMutableArray new];        
        minimumScale = 0.1;
        maximumScale = 10.0;
        zoom=1.0;
        mouseMode = KModeSelectionKey;
        self.transform = [NSAffineTransform transform];
    }
    return self;
}

- (void)awakeFromNib {
    graphicsObjects = [NSMutableArray new];
    NSRect rect = NSMakeRect(20.0,10.0, 300.0, 300.0);
    /*
    SFGraphicImage *imageGraphic = [SFGraphicImage new];
    imageGraphic.image = [NSImage imageNamed:NSImageNameBonjour];
    SFGraphicsObject *graphic = imageGraphic;
    graphic.bounds = rect;
    [graphicsObjects addObject:graphic];

     
    SFGraphicOpenGL *openGLGraphic = [SFGraphicOpenGL new];
    openGLGraphic.bounds = rect;
    [graphicsObjects addObject:openGLGraphic];
    */
    
    
    if (NO) {
        CALayer *newLayer = [CALayer layer];
        NSImage *image = [NSImage imageNamed:@"Earth.jpg"];
        newLayer.backgroundColor = [NSColor redColor].CGColor;
        newLayer.contents = (id)[image CGImageForProposedRect:NULL context:NULL hints:nil];
        newLayer.frame = NSMakeRect(0,0,image.size.width,image.size.height);
        [self.layer addSublayer:newLayer];
    }
}

- (IBAction)setModeSelection:(id)sender {
    self.mouseMode = KModeSelectionKey;
};

-(void)createHitLayer {
    uint64_t width  = ceil(self.frame.size.width);
    uint64_t height = ceil(self.frame.size.height);
    uint64_t count  = width * height;
    
        // We render directly into our result if it takes only 1 byte.
    if(hitTestLayerData)
        memset(hitTestLayerData.mutableBytes,0,hitTestLayerData.length);
    else
        hitTestLayerData = [NSMutableData dataWithLength:count];
    uint8_t *pPixel = hitTestLayerData.mutableBytes;
    NSUInteger pixelIndex;
    for(pixelIndex=0;pixelIndex<count;pixelIndex++)
        if(pPixel[pixelIndex]!=0)
            break;
    NSLog(@"pixel index:%ld",(unsigned long)pixelIndex);
    CGContextRef theMask = CGBitmapContextCreate(hitTestLayerData.mutableBytes, width, height, 8, width, NULL, kCGImageAlphaOnly);
    
    if (theMask) {
        
            // Translate so that our bounds origin is at 0,0 in our buffer
            //CGContextTranslateCTM(theMask, -bounds.origin.x, -bounds.origin.y);
        
            // By adding a blurry shadow we make it easier to click on little things
            //CGContextSetShadow(theMask, CGSizeMake(0, 0.0f), 2.0f);
        
            // We can now render the presentatinLayer
        [self.layer.presentationLayer renderInContext:theMask];
        
            // CAShapeLayers don't renderInContext so we need a workaround
        
        for (id object in graphicsObjects) {
            if([object isKindOfClass:[SFGraphicsObject class]]) {
                SFGraphicsObject *graphicsObject = object;
                NSBezierPath *path = graphicsObject.bezierPath;
                [path stroke];
            }
        }
    }
    for(pixelIndex=0;pixelIndex<count;pixelIndex++)
        if(pPixel[pixelIndex]!=0)
            break;
    NSLog(@"pixel index:%ld",(unsigned long)pixelIndex);
    CGContextRelease(theMask);
}


- (void)drawRect:(NSRect)dirtyRect {
    [self.transform concat];
    
    // Drawing code here.
    for (id object in graphicsObjects) {
        if([object isKindOfClass:[SFGraphicsObject class]]) {
            SFGraphicsObject *graphicsObject = object;
            [graphicsObject draw];
        }
    }
    if(createdGraphic) {
        [createdGraphic draw];
    }
}

#pragma mark View Transform Calculation
-(NSAffineTransform*)calculateTransform {
    NSAffineTransform *viewTransfrom = [NSAffineTransform transform];
    [viewTransfrom scaleBy:zoom];
    return viewTransfrom;
}

#pragma mark Mouse Button and Dragging Events

- (void)mouseDown:(NSEvent *)theEvent
{
    NSPoint pointInWindow = [theEvent locationInWindow];
    NSPoint pointInView = [self convertPoint:pointInWindow fromView:nil];
    NSAffineTransform *inverseTransform = [self.transform copy];
    [inverseTransform invert];
    NSPoint transformedPoint = [inverseTransform transformPoint:pointInView];
    
    trackingStartPoint = transformedPoint;
    if([mouseMode isEqualToString:KModeSelectionKey]) {
        
    }
    else if ([mouseMode isEqualToString:KModeNewObjectKey ]) {
        if(createdGraphic==nil) {
            Class graphicsClass = NSClassFromString(objectType);
            createdGraphic=[graphicsClass new];
        }
        [createdGraphic mouseDownAtPoint:trackingStartPoint clickCount:[theEvent clickCount]];    }
    self.needsDisplay=YES;
};

- (void)mouseDragged:(NSEvent *)theEvent
{
    NSPoint pointInWindow = [theEvent locationInWindow];
    NSPoint pointInView = [self convertPoint:pointInWindow fromView:nil];
    NSAffineTransform *inverseTransform = [self.transform copy];
    [inverseTransform invert];
    NSPoint transformedPoint = [inverseTransform transformPoint:pointInView];

    if ([mouseMode isEqualToString:KModeNewObjectKey ]) {
        [createdGraphic mouseDragToPoint:transformedPoint];
    }
    self.needsDisplay=YES;
};

- (void)mouseUp:(NSEvent *)theEvent
{
    if(createdGraphic) {
        NSPoint pointInWindow = [theEvent locationInWindow];
        NSPoint pointInView = [self convertPoint:pointInWindow fromView:nil];
        NSAffineTransform *inverseTransform = [self.transform copy];
        [inverseTransform invert];
        NSPoint transformedPoint = [inverseTransform transformPoint:pointInView];

        [createdGraphic mouseUpAtPoint:transformedPoint];
        if([createdGraphic isCreationFinished]) {
            [graphicsObjects addObject:createdGraphic];
            createdGraphic=nil;
        }
    }
};


#pragma mark Touchpad Zoom Event

- (void)magnifyWithEvent:(NSEvent *)event {
    zoom = ([event magnification] + 1.0)*zoom;
    self.transform = [self calculateTransform];
    self.needsDisplay=YES;
}

#pragma mark Mouse Tracking

- (void)mouseEntered:(NSEvent*)event
{
    /*
    NSDictionary *userDictionary = [event userData];
    id whichTracker = [userDictionary objectForKey:KTrackerKey];
    [whichTracker setColor:[NSColor orangeColor]];
    self.needsDisplay=YES;
    */
}

// -------------------------------------------------------------------------------
//	mouseExited:event
// -------------------------------------------------------------------------------
//	Because we installed NSTrackingArea with "NSTrackingMouseEnteredAndExited",
//	as an option, this method will be called.
// -------------------------------------------------------------------------------
- (void)mouseExited:(NSEvent*)event
{
    /*
    NSDictionary *userDictionary = [event userData];
    id whichTracker = [userDictionary objectForKey:KTrackerKey];
    [whichTracker setColor:[NSColor redColor]];
    self.needsDisplay=YES;
     */
}


@end
