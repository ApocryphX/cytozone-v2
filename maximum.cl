/*
 *  CLKernel_Maximum_8.cl
 *  Monterey
 *
 *  Copyright 2014 Elarity, LLC. All rights reserved.
 *
 */

__kernel void maximum_8(
					  __global uchar16* input,
					  __global uchar16* output,
					  const unsigned int count)
{
	int i = get_global_id(0);  
	if(i < count)
        output[i] = max(input[i], output[i]);
};

__kernel void maximum_16(
					  __global ushort8* input,
					  __global ushort8* output,
					  const unsigned int count)
{
	int i = get_global_id(0);  
	if(i < count)
        output[i] = max(input[i], output[i]);
};

__kernel void maximum_F(
                        __global float4* input,
                        __global float4* output,
                        const unsigned int count)
{
    int i = get_global_id(0);  
    if(i < count)
        output[i] = max(input[i], output[i]);
};


