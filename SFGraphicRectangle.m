//
//  SFRectangleObject.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicRectangle.h"

@implementation SFGraphicRectangle

@synthesize fillColor;
@synthesize color;


-(NSBezierPath*)bezierPath {
    // Draw the handle itself.
    [color set];
    return [NSBezierPath bezierPathWithRect:self.bounds];
}

- (void)mouseDownAtPoint:(NSPoint)point clickCount:(NSUInteger)clickCount {
    NSRect rect = NSZeroRect;
    rect.origin = point;
    self.bounds = rect;
};

- (void)mouseDragToPoint:(NSPoint)point {
    NSRect rect = self.bounds;
    NSSize size = NSMakeSize(point.x-rect.origin.x,point.y-rect.origin.y);
    rect.size = size;
    self.bounds = rect;
};

- (void)mouseUpAtPoint:(NSPoint)point {
    NSRect rect = self.bounds;
    NSSize size = NSMakeSize(point.x-rect.origin.x,point.y-rect.origin.y);
    rect.size = size;
    self.bounds = rect;
    self.isCreationFinished = YES;
};


@end
