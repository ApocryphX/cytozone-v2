//
//  SFHistogram.m
//  SanFrancisco
//
//
//

#import "SFHistogram.h"
#import "SFBitplane.h"

#define GRAYLEVEL 256

@implementation SFHistogram

@synthesize histogramData;

+ (id)histogramForBitplane:(SFBitplane*)bitplane {
/* binarization by Otsu's method
 based on maximization of inter-class variance */
    SFHistogram *histogram = [SFHistogram new];
    if(histogram) {
        int i, x, y; /* Loop variable */
        
        printf("Otsu's binarization process starts now.\n");
        NSMutableData *data = bitplane.data;
        uint8_t *image1 = (uint8_t*)data.bytes;
        histogram->sampleNumber = bitplane.width * bitplane.height;
        /* Histogram generation */
        for (i = 0; i < GRAYLEVEL; i++)
            histogram->histogramBin[i] = 0;
        for (y = 0; y < bitplane.height; y++) {
            for (x = 0; x < bitplane.width; x++) {
                histogram->histogramBin[image1[y*bitplane.bytesPerRow+x]]++;
            }
        }
    }
    return histogram;
}

- (NSUInteger)otsuThreshold {
    
    double prob[GRAYLEVEL], omega[GRAYLEVEL]; /* prob of graylevels */
    double myu[GRAYLEVEL];   /* mean value for separation */
    double max_sigma = 0.0, sigma[GRAYLEVEL]; /* inter-class variance */
    int threshold = 0; /* threshold for binarization */
    int i;
    
    /* calculation of probability density */
    for ( i = 0; i < GRAYLEVEL; i ++ ) {
        prob[i] = (double)histogramBin[i] /(double)sampleNumber;
    }
    
    /* omega & myu generation */
    omega[0] = prob[0];
    myu[0] = 0.0;       /* 0.0 times prob[0] equals zero */
    for (i = 1; i < GRAYLEVEL; i++) {
        omega[i] = omega[i-1] + prob[i];
        myu[i] = myu[i-1] + i*prob[i];
    }
    
    /* sigma maximization
     sigma stands for inter-class variance
     and determines optimal threshold value */
    threshold = 128;
    max_sigma = 0.0;
    for (i = 0; i < GRAYLEVEL-1; i++) {
        if (omega[i] != 0.0 && omega[i] != 1.0)
            sigma[i] = pow(myu[GRAYLEVEL-1]*omega[i] - myu[i], 2) /
            (omega[i]*(1.0 - omega[i]));
        else
            sigma[i] = 0.0;
        if (sigma[i] > max_sigma) {
            max_sigma = sigma[i];
            threshold = i;
        }
    }
    
    NSLog(@"\nthreshold value = %d\n", threshold);
    
    /* binarization output into image2
     x_size2 = x_size1;
     y_size2 = y_size1;
     for (y = 0; y < y_size2; y++)
     for (x = 0; x < x_size2; x++)
     if (image1[y][x] > threshold)
     image2[y][x] = MAX_BRIGHTNESS;
     else
     image2[y][x] = 0;
     */
    return threshold;
    
}

@end
