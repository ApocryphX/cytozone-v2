//
//  SFNDDimension.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFNDDimension.h"

NSString *SFKDimensionSymbolName = @"symbol";
NSString *SFKDimensionUnitName = @"unit";
NSString *SFKDimensionMagnitudeName = @"magnitude";
NSString *SFKDimensionExtentName = @"extent";
NSString *SFKDimensionTypeName = @"type";

NSString *SFKDimensionTypeVoidName = @"void";
NSString *SFKDimensionTypeSpaceName = @"space";
NSString *SFKDimensionTypeTimeName = @"time";
NSString *SFKDimensionTypeWavelengthName = @"wavelength";


@implementation SFNDDimension

-(id) init {
    self = [super init];
    _dimensions = [NSMutableArray new];
    return self;
};

-(id)initWithNDDimension:(SFNDDimension*)nddimension {
    if((self = [self init])) {
        _dimensions = [NSMutableArray new];
        for (NSDictionary *dimension in nddimension->_dimensions) {
            [_dimensions addObject:dimension];
         }
    }
    return self;
};

- (void)addDimensionWithExtent:(NSUInteger)extent dimensionType:(NSString*)type symbol:(NSString*)symbol  {
    NSMutableDictionary *dimension = [NSMutableDictionary new];
    dimension[SFKDimensionExtentName] = [NSNumber numberWithUnsignedInteger:extent];
    dimension[SFKDimensionTypeName] = type;
    dimension[SFKDimensionSymbolName] = symbol;
    [_dimensions addObject:dimension] ;
};

- (void)addDimensionWithExtent:(NSUInteger)extent magnitude:(NSNumber*)magnitude dimensionType:(NSString*)type symbol:(NSString*)symbol unit:(NSString*)unit {
    NSMutableDictionary *dimension = [NSMutableDictionary new];
    dimension[SFKDimensionExtentName] = [NSNumber numberWithUnsignedInteger:extent];
    dimension[SFKDimensionMagnitudeName] = magnitude;
    dimension[SFKDimensionTypeName] = type;
    dimension[SFKDimensionSymbolName] = symbol;
    dimension[SFKDimensionUnitName] = unit;
    [_dimensions addObject:dimension] ;
};

- (SFNDDimension*)dimensionsSlicedAtIndex:(NSUInteger)index {
    SFNDDimension *nddimension = [SFNDDimension new];
    if (nddimension) {
        NSRange theRange;
        theRange.location = 0;
        theRange.length = index+1;
        nddimension->_dimensions =[NSMutableArray arrayWithArray:[_dimensions subarrayWithRange:theRange]];
    }
    return nddimension;
};

- (void)removeDimensionAtIndex:(NSUInteger)idx {
	[_dimensions removeObjectAtIndex:idx];
};

- (id)copyWithZone:(NSZone *)zone
{
    SFNDDimension *ret=[[[self class]allocWithZone: zone]initWithNDDimension:self];
    return ret;
};

/*
 - (void) addDimension:(SFDimension*) diminfo {
 [_dimensions addObject:diminfo];
 };
 
 - (SFDimension*) power2AtIndex:(NSUInteger)rank
 {
 return [[_dimensions objectAtIndex:rank]asPower2];
 };
 
 - (NSUInteger) makePower2AtIndex:(NSUInteger)rank
 {
 return [[_dimensions objectAtIndex:rank]makePower2];
 };
 
 - (void)setMagnitude:(NSNumber*)value atIndex:(NSUInteger)rank {
 SFDimension *dimension = _dimensions[rank];
 dimension.magnitude = value;
 };
 */
- (void)setExtent:(NSUInteger)value atIndex:(NSUInteger)rank {
    NSMutableDictionary *dimension = _dimensions[rank];
    dimension[SFKDimensionExtentName] = [NSNumber numberWithUnsignedInteger: value];
};
/*
 - (void)setDimensionType:(NSString*)value atIndex:(NSUInteger)rank {
 SFDimension *dimension = _dimensions[rank];
 dimension.dimensionType = value;
 };
 
 - (void)setUnit:(NSString*)value atIndex:(NSUInteger)rank {
 SFDimension *dimension = _dimensions[rank];
 dimension.unit = value;
 };
 
 - (void)symbol:(NSString*)value atIndex:(NSUInteger)rank {
 SFDimension *dimension = _dimensions[rank];
 dimension.symbol = value;
 };
 */

-(NSUInteger)extentAtIndex:(NSUInteger)index{
    NSUInteger extent = 1;
    if ( index < _dimensions.count) {
		NSDictionary* dictionary = _dimensions[index];
		NSNumber *extentNumber  = dictionary[SFKDimensionExtentName];
        if(extentNumber)
            extent = extentNumber.unsignedIntegerValue;
	}
    return extent;
};

- (NSNumber*) magnitudeAtIndex:(NSUInteger)index {
    NSNumber* magnitudeNumber = nil;
    if ( index < _dimensions.count) {
        NSDictionary *dimension = _dimensions[index];
        magnitudeNumber = dimension[SFKDimensionMagnitudeName];
    }
    return magnitudeNumber;
};


- (NSString*) dimensionTypeAtIndex:(NSUInteger)index {
    NSString *type = SFKDimensionTypeVoidName;
    if ( index < _dimensions.count ) {
        NSDictionary *dimension = _dimensions[index];
        type = dimension[SFKDimensionTypeName];
    }
    return type;
};

- (NSString*) symbolAtIndex:(NSUInteger)index{
    NSString* symbol = @"";
    if ( index < _dimensions.count) {
        NSDictionary *dimension = _dimensions[index];
        symbol = dimension[SFKDimensionSymbolName];
    }
    return symbol;
};

- (NSString*) unitAtIndex:(NSUInteger)index{
    NSString* unit = @"";
    if ( index <= _dimensions.count) {
        NSDictionary *dimension = _dimensions[index];
        unit = dimension[SFKDimensionUnitName];
    }
    return unit;
};



-(NSUInteger)count{
	return _dimensions.count;
}

-(NSUInteger)strideAtIndex:(NSUInteger)rank{
    NSUInteger ret=1,i;
    NSUInteger count = [_dimensions count];
    //in case rank is bigger than number of dimensions: return stride for highest existing dimension
    //rank is 0 based, count is 1 based: hence count-1
    NSUInteger limit = ( rank > count-1 ) ? count-1:rank;
    for(i=0; i<=limit; i++)
        ret *= [self extentAtIndex:i];
    return ret;
}

- (NSUInteger)numberOfPlanes {
	return [self strideAtIndex:[self count]-1] / [self strideAtIndex:1];
};

- (NSUInteger)numberOfCubes  {
	return [self strideAtIndex:[self count]-1] / [self strideAtIndex:2];
};

- (NSUInteger)numberOfSlicesAtRank:(NSUInteger)rank {
	NSUInteger ret = 1;
	if (rank < [self count])
		ret =  [self strideAtIndex:[self count]-1]/[self strideAtIndex:rank];
	return ret;
};

- (BOOL)isVolume {
    NSUInteger count = 0;
    for(NSDictionary *dimension in _dimensions) {
        if([dimension[SFKDimensionTypeName] isEqualToString:SFKDimensionTypeSpaceName]) {
            count++;
        }
    }
    if(count==3)
        return YES;
    else
        return NO;
};

- (BOOL)isTimeSeries {
    BOOL isTimeSeries = NO;
    for(NSDictionary *dimension in _dimensions) {
        if([dimension[SFKDimensionTypeName] isEqualToString:SFKDimensionTypeTimeName]) {
            isTimeSeries = YES;
        }
    }
    return isTimeSeries;
};

- (BOOL)isCalibrated {
    for (NSDictionary *dimension in _dimensions) {
        if(dimension[SFKDimensionMagnitudeName] == Nil)
            return NO;
    }
    return YES;
};

// NSCoding methods ---------------------------------------------------

- (id)initWithCoder:(NSCoder *)coder
{
	self = [super init];
    if ( [coder allowsKeyedCoding] ) {
        // Can decode keys in any order
		_dimensions = [coder decodeObjectForKey:@"Dimensions"];
    }
	else {
        // Must decode keys in same order as encodeWithCoder:
		_dimensions = [coder decodeObject];
    }
    return self;
};

- (void)encodeWithCoder:(NSCoder *)coder
{
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject:_dimensions forKey:@"Dimensions"];
    } else {
        [coder encodeObject:_dimensions];
    }
    return;
};


@end
