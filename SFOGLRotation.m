//
//  SFOGLRotation.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLRotation.h"
//#import <math.h>

#include <OpenGL/gl.h>

//static const float kTol = 0.001;
static const double kRad2Deg = 180. / 3.1415927;
static const double kDeg2Rad = 3.1415927 / 180.;

@implementation SFOGLRotation

-(id)init 
{
    self = [super init];
    [self reset];
    return self;
};

+ (id) initWithRotation: (struct GLRotation)theRotation
{
	SFOGLRotation *new_object;
	new_object = [[SFOGLRotation alloc]init]; 
	new_object->sfRotation = theRotation;
	return new_object;
};

+ (id) initWithAngle: (float)theA X:(float)theX Y:(float) theY Z:(float)theZ {
	SFOGLRotation *new_object;
	new_object = [[SFOGLRotation alloc]init];
	new_object->sfRotation.a = theA;
	new_object->sfRotation.x = theX;
	new_object->sfRotation.y = theY;
	new_object->sfRotation.z = theZ;
	return new_object;
};

-(void)execute
{
	glRotatef(sfRotation.a, sfRotation.x, sfRotation.y, sfRotation.z);
};

static void rotation2Quat( struct GLRotation A, double* q)
{
    double ang2;  // The half-angle
    double sinAng2; // sin(half-angle)
	
    
    // Convert a GL-style rotation to a quaternion.  The GL rotation looks like this:
    // {angle, x, y, z}, the corresponding quaternion looks like this:
    // {{v}, cos(angle/2)}, where {v} is {x, y, z} / sin(angle/2).
    
    ang2 = A.a * kDeg2Rad * 0.5;  // Convert from degrees ot radians, get the half-angle.
    sinAng2 = sin(ang2);
    q[0] = A.x * sinAng2; 
    q[1] = A.y * sinAng2; 
    q[2] = A.z * sinAng2;
    q[3] = cos(ang2);
}

- (void)add:(SFOGLRotation *)dA 
{
    double q0[4], q1[4], q2[4];
    double theta2, sinTheta2;
    
    // Figure out A' = A . dA
    // In quaternions: let q0 <- A, and q1 <- dA.
    // Figure out q2 = q1 + q0 (note the order reversal!).
    // A' <- q3.
    
    rotation2Quat(sfRotation, q0);
    rotation2Quat([dA GLRotation], q1);
    
    // q2 = q1 + q0;
    q2[0] = q1[1]*q0[2] - q1[2]*q0[1] + q1[3]*q0[0] + q1[0]*q0[3];
    q2[1] = q1[2]*q0[0] - q1[0]*q0[2] + q1[3]*q0[1] + q1[1]*q0[3];
    q2[2] = q1[0]*q0[1] - q1[1]*q0[0] + q1[3]*q0[2] + q1[2]*q0[3];
    q2[3] = q1[3]*q0[3] - q1[0]*q0[0] - q1[1]*q0[1] - q1[2]*q0[2];
    // Here's an excersize for the reader: it's a good idea to re-normalize your quaternions
    // every so often.  Experiment with different frequencies.
    
    // An identity rotation is expressed as rotation by 0 about any axis.
    // The "angle" term in a quaternion is really the cosine of the half-angle.
    // So, if the cosine of the half-angle is one (or, 1.0 within our tolerance),
    // then you have an identity rotation.
    if (fabs(fabs(q2[3] - 1.0f)) < 1.0e-5) {
        // Identity rotation.
        [self setA: 0.0];
        [self setX: 0.0];
        [self setY: 0.0];
        [self setZ: 0.0];
    }
    else {
		// If you get here, then you have a non-identity rotation.  In non-identity rotations,
		// the cosine of the half-angle is non-0, which means the sine of the angle is also
		// non-0.  So we can safely divide by sin(theta2).
		
		// Turn the quaternion back into an {angle, {axis}} rotation.
		if (q2[3] > 1.0f)
			q2[3] = 1.0f;
		theta2 = acos(q2[3]);
		sinTheta2 = 1./sin(theta2);
		float a,x,y,z;
		
		a = theta2 * 2.0f * kRad2Deg;
		x = q2[0] * sinTheta2;
		y = q2[1] * sinTheta2;
		z = q2[2] * sinTheta2;
			
		float lr = sqrt(x*x + y*y + z*z);
		
		[self setA: a ];
		[self setX: x/lr];
		[self setY: y/lr];
		[self setZ: z/lr];
    }
}

- (void)setRotation:(struct GLRotation)theRotation {
	[self setX: theRotation.x];
	[self setY: theRotation.y];
	[self setZ: theRotation.z];
	[self setA: theRotation.a];
};

- (void)setX:(float)theAngle {
	sfRotation.x = theAngle;
}

- (void)setY:(float)theAngle {
	sfRotation.y = theAngle;
}

- (void)setZ:(float)theAngle {
	sfRotation.z = theAngle;
}

- (void)setA:(float)theAngle
{
	sfRotation.a = theAngle;
}


- (struct GLRotation) GLRotation
{
	return sfRotation;
};

- (float) a
{
	return sfRotation.a;
};

- (float) x 
{
	return sfRotation.x;
};

- (float) y
{
	return sfRotation.y;
};

- (float) z
{
	return sfRotation.z;
};

- (SFOGLRotation*)interpolatedRotationWithFraction:(float)frac ofRotation:(SFOGLRotation*)rotation {
	SFOGLRotation* ret = [SFOGLRotation new];
	float ifrac = 1.0f - frac;
	ret->sfRotation.a = ifrac * sfRotation.a + frac * rotation->sfRotation.a; 
	ret->sfRotation.x = ifrac * sfRotation.x + frac * rotation->sfRotation.x; 
	ret->sfRotation.y = ifrac * sfRotation.y + frac * rotation->sfRotation.y; 
	ret->sfRotation.z = ifrac * sfRotation.z + frac * rotation->sfRotation.z; 
	return ret;
}


-(void)reset
{
	[self setA: 0.0f];
	[self setX: 0.0f];
	[self setY: 0.0f];
	[self setZ: 0.0f];
};

- (void)resetAxis:(NSUInteger)axis {
	
	switch (axis) {
		case 0: {
			sfRotation.x = 0.0f;
			break;
		}
		case 1: {
			sfRotation.y = 0.0f;
			break;
		}
		case 2: {
			sfRotation.z = 0.0f;
			break;
		}
	}

	if (sfRotation.x != 0.0f || sfRotation.y != 0.0f || sfRotation.z != 0.0f ) {
		float lr = sqrt(sfRotation.x*sfRotation.x + sfRotation.y*sfRotation.y + sfRotation.z*sfRotation.z);
		sfRotation.x = sfRotation.x/lr;
		sfRotation.y = sfRotation.y/lr;
		sfRotation.z = sfRotation.z/lr;
	}
	else {
		sfRotation.a = 0.0f;
	}
}


@end
