//
//  SFGRaphicOval.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicOval.h"

@implementation SFGraphicOval

-(NSBezierPath*)bezierPath {
    NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:[self bounds]];
    [path setLineWidth:1.0];
    return path;
}

@end
