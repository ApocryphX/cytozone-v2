//
//  SFVoxelInfo.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

enum SFVoxelInterleaveType {
	kPlanar
//	,kInterleavedLuminaceAlpha,
	,kInterleavedRGB
//	,kInterleavedRGBA
};

enum SFPhotometricInterpretation {
	kLuminance,
	kChrominance,
	kHue,
	kSaturation,
	kIntensity,
	kAlpha,
	kGreen,
	kRed,
	kBlue
};

enum SFVoxelDataType {
	kInteger,
	kFloatingPoint,
	kComplex
};

@interface SFVoxelInfo : NSObject <NSCopying> {
	NSUInteger sfBytesPerSample;
	NSUInteger sfPrecision;
	NSUInteger sfSamplesPerVoxel;
	enum SFVoxelDataType sfDataType;
	enum SFVoxelInterleaveType sfInterleaveType;
	enum SFPhotometricInterpretation  sfPhotometricInterpretation;
}

- (id)initWithVoxelInfo:(SFVoxelInfo *)info;

- (void)setBytesPerSample:(NSUInteger)value;
- (void)setSamplesPerVoxel:(NSUInteger)value;
- (void)setPrecision:(NSUInteger)value;
- (void)setDataType:(enum SFVoxelDataType)value;
- (void)setInterleaveType:(enum SFVoxelInterleaveType)value;
- (void)setPhotometricInterpretation:(enum SFPhotometricInterpretation)value;

- (NSUInteger)bytesPerSample;
- (NSUInteger)samplesPerVoxel;
- (NSUInteger)bytesPerVoxel;
- (NSUInteger)precision;
- (enum SFVoxelDataType)dataType;
- (enum SFVoxelInterleaveType)interleaveType;
- (enum SFPhotometricInterpretation)photometricInterpretation;


//NSCopying methods
- (id)copyWithZone:(NSZone *)zone;

@end
