//
//  SFTrackball.m
//  VoxelMachine
//
//  Created by ApocryphX on 1/6/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFTrackball.h"
#import "SFOGLRotation.h"

@implementation SFTrackball

//static const float kTol = 0.001;
static const float kRad2Deg = 180. / 3.1415927;
static const float kDeg2Rad = 3.1415927 / 180.;


- (void)start:(NSPoint)pt frame:(NSRect)frame
{
    float xxyy;
    float nx, ny;
	
    /* Start up the trackball.  The trackball works by pretending that a ball
		encloses the 3D view.  You roll this pretend ball with the mouse.  For
		example, if you click on the center of the ball and move the mouse straight
		to the right, you roll the ball around its Y-axis.  This produces a Y-axis
		rotation.  You can click on the "edge" of the ball and roll it around
		in a circle to get a Z-axis rotation.
		
		The math behind the trackball is simple: start with a vector from the first
		mouse-click on the ball to the center of the 3D view.  At the same time, set the radius
		of the ball to be the smaller dimension of the 3D view.  As you drag the mouse
		around in the 3D view, a second vector is computed from the surface of the ball
		to the center.  The axis of rotation is the cross product of these two vectors,
		and the angle of rotation is the angle between the two vectors.
		*/
    nx = frame.size.width;
    ny = frame.size.height;
    if (nx > ny)
        sfRadius = ny * 0.5;
    else
        sfRadius = nx * 0.5;
    // Figure the center of the view.
    sfCenter.x = frame.origin.x + frame.size.width / 2.0f;
    sfCenter.y = frame.origin.y + frame.size.height / 2.0f;
	
    // Compute the starting vector from the surface of the ball to its center.
    sfStartPoint[0] = pt.x - sfCenter.x;
    sfStartPoint[1] = pt.y - sfCenter.y;
    xxyy = sfStartPoint[0]*sfStartPoint[0] + sfStartPoint[1]*sfStartPoint[1];
    if (xxyy > sfRadius*sfRadius) {
        // Outside the sphere.
        sfStartPoint[2] = 0.;
    } else
        sfStartPoint[2] = sqrt(sfRadius*sfRadius - xxyy);
	
}

- (struct GLRotation)rollTo:(NSPoint)pt 
{
    float xxyy;
    struct GLRotation rot;
    // float rot[4];
    float cosAng, sinAng;
    float ls, le, lr;
	
    sfEndPoint[0] = pt.x - sfCenter.x;
    sfEndPoint[1] = pt.y - sfCenter.y;
    //    if (fabs(sfEndPoint[0] - sfStartPoint[0]) < kTol && fabs(sfEndPoint[1] - sfStartPoint[1]) < kTol)
    //        return; // Not enough change in the vectors to have an action.
	
    // Compute the ending vector from the surface of the ball to its center.
    xxyy = sfEndPoint[0]*sfEndPoint[0] + sfEndPoint[1]*sfEndPoint[1];
    if (xxyy > sfRadius*sfRadius) {
        // Outside the sphere.
        sfEndPoint[2] = 0.;
    } else
        sfEndPoint[2] = sqrt(sfRadius*sfRadius - xxyy);
	
    // Take the cross product of the two vectors. r = s X e
    rot.x =  sfStartPoint[1] * sfEndPoint[2] - sfStartPoint[2] * sfEndPoint[1];
    rot.y = -sfStartPoint[0] * sfEndPoint[2] + sfStartPoint[2] * sfEndPoint[0];
    rot.z =  sfStartPoint[0] * sfEndPoint[1] - sfStartPoint[1] * sfEndPoint[0];
	
    // Use atan for a better angle.  If you use only cos or sin, you only get
    // half the possible angles, and you can end up with rotations that flip around near
    // the poles.
	
    // cos(a) = (s . e) / (||s|| ||e||)
    cosAng = sfStartPoint[0]*sfEndPoint[0] + sfStartPoint[1]*sfEndPoint[1] + sfStartPoint[2]*sfEndPoint[2]; // (s . e)
    ls = sqrt(sfStartPoint[0]*sfStartPoint[0] + sfStartPoint[1]*sfStartPoint[1] + sfStartPoint[2]*sfStartPoint[2]);
    ls = 1. / ls; // 1 / ||s||
    le = sqrt(sfEndPoint[0]*sfEndPoint[0] + sfEndPoint[1]*sfEndPoint[1] + sfEndPoint[2]*sfEndPoint[2]);
    le = 1. / le; // 1 / ||e||
    cosAng = cosAng * ls * le;
	
    // sin(a) = ||(s X e)|| / (||s|| ||e||)
    sinAng = lr = sqrt(rot.x*rot.x + rot.y*rot.y + rot.z*rot.z); // ||(s X e)||;
																 // keep this length in lr for normalizing the rotation vector later.
    sinAng = sinAng * ls * le;
    rot.a = (float) atan2 ( sinAng, cosAng ) * kRad2Deg; // GL rotations are in degrees.
	
    // Normalize the rotation axis.
    lr = 1. / lr;
    rot.x *= lr; rot.y *= lr; rot.z *= lr;
    return rot;
}

static void rotation2Quat( struct GLRotation A, float* q)
{
    float ang2;  // The half-angle
    float sinAng2; // sin(half-angle)
	
	
    // Convert a GL-style rotation to a quaternion.  The GL rotation looks like this:
    // {angle, x, y, z}, the corresponding quaternion looks like this:
    // {{v}, cos(angle/2)}, where {v} is {x, y, z} / sin(angle/2).
	
    ang2 = A.a * kDeg2Rad * 0.5;  // Convert from degrees ot radians, get the half-angle.
    sinAng2 = sin(ang2);
    q[0] = A.x * sinAng2;
    q[1] = A.y * sinAng2;
    q[2] = A.z * sinAng2;
    q[3] = cos(ang2);
}

+ (struct GLRotation)add:(struct GLRotation)dA toRotation:(struct GLRotation)A
{
    float q0[4], q1[4], q2[4];
    float theta2, sinTheta2;
    struct GLRotation ret;
	
    // Figure out A' = A . dA
    // In quaternions: let q0 <- A, and q1 <- dA.
    // Figure out q2 = q1 + q0 (note the order reversal!).
    // A' <- q3.
	
    rotation2Quat(A, q0);
    rotation2Quat(dA, q1);
	
    // q2 = q1 + q0;
    q2[0] = q1[1]*q0[2] - q1[2]*q0[1] + q1[3]*q0[0] + q1[0]*q0[3];
    q2[1] = q1[2]*q0[0] - q1[0]*q0[2] + q1[3]*q0[1] + q1[1]*q0[3];
    q2[2] = q1[0]*q0[1] - q1[1]*q0[0] + q1[3]*q0[2] + q1[2]*q0[3];
    q2[3] = q1[3]*q0[3] - q1[0]*q0[0] - q1[1]*q0[1] - q1[2]*q0[2];
    // Here's an excersize for the reader: it's a good idea to re-normalize your quaternions
    // every so often.  Experiment with different frequencies.
	
    // An identity rotation is expressed as rotation by 0 about any axis.
    // The "angle" term in a quaternion is really the cosine of the half-angle.
    // So, if the cosine of the half-angle is one (or, 1.0 within our tolerance),
    // then you have an identity rotation.
    if (fabs(fabs(q2[3] - 1.)) < 1.0e-7) {
        // Identity rotation.
        ret.a = 0.;
        ret.x = 1.;
        ret.y = ret.z = 0.;
    }
    else {
		// If you get here, then you have a non-identity rotation.  In non-identity rotations,
		// the cosine of the half-angle is non-0, which means the sine of the angle is also
		// non-0.  So we can safely divide by sin(theta2).
		
		// Turn the quaternion back into an {angle, {axis}} rotation.
		if (q2[3] > 1.0f)
			q2[3] = 1.0f;
		theta2 = acos(q2[3]);
		sinTheta2 = 1./sin(theta2);
		ret.a = theta2 * 2. * kRad2Deg;
		ret.x = q2[0] * sinTheta2;
		ret.y = q2[1] * sinTheta2;
		ret.z = q2[2] * sinTheta2;
		float max;
		max = fmax(fabs(ret.x),fabs(ret.y));
		max = fmax(fabs(ret.z),fabs(max));
		max = 1.0f / fabs(max);
		ret.x *= max; ret.y *= max; ret.z *= max;
		
    }
    return ret;
}

@end
