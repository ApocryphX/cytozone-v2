//
//  SFSelectionRectangle.h
//  ROI Prototypes
//
//  Created by ApocryphX on 4/7/13.
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//


extern NSString *SFKDrawingHandleKey;

@class SFDrawingHandle;

@interface SFSelectionRectangle : NSObject {
    NSPoint handelPoints[8];
}

@property (assign) NSRect bounds;
@property (assign) NSSize handleSize;
@property (retain) NSColor *handleColor;
@property (retain) NSColor *handleShadowColor;

@end
