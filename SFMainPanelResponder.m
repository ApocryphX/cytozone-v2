//
//  SFMainPanelResponder.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFMainPanelResponder.h"


@implementation SFMainPanelResponder

- (IBAction)showVolumeSettings:(id)sender {
	[sfTabView selectTabViewItemAtIndex:0]; 
};

- (IBAction)showChannelSettings:(id)sender {
	[sfTabView selectTabViewItemAtIndex:1]; 
};

@end
