//
//  SFBigTIFFIFD.m
//  SanFrancisco
//
//
//

#import "SFBigTIFFIFD.h"
#import "SFBigTIFFTag.h"

@implementation SFBigTIFFIFD

// init contstructs all Tags of IFD and reads offset to the next IFD
-(id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian {
    uint64_t i;
    uint64_t baseoffset, nextoffset;
	
    self = [super init];
    baseoffset = (uint64_t)[handle offsetInFile]; //read current offset (=start of IFD)
	
    //read number of entries in the directory
    NSData *tagCountData = [handle readDataOfLength:sizeof(uint64_t)];
    const uint64_t *count = tagCountData.bytes;
    if(isBigEndian)
        IFDTagCount = NSSwapBigLongLongToHost(*count);
    else
        IFDTagCount = NSSwapLittleLongLongToHost(*count);
    
    IFDDictionary = [NSMutableDictionary dictionaryWithCapacity:IFDTagCount]; //init temp dictionary with count
    for (i=0; i<IFDTagCount; i++) {
		nextoffset = baseoffset + sizeof(uint64_t) + i*[SFBigTIFFTag sizeDirEntry]; // calc offset for next tag
		[handle seekToFileOffset:nextoffset]; //seek to the next tag posistion in IFD
		SFBigTIFFTag* temp = [[SFBigTIFFTag alloc] initWithFileHandle:handle withEndian:isBigEndian]; //contruct a SFTIFFTag object
		NSNumber* tagnumber = [NSNumber numberWithUnsignedInt:(unsigned)[temp tagID]];
		[IFDDictionary setObject:temp forKey:tagnumber];
    }
    //seek to position holding uint32_t offset to next IFD and read / rotate the offset
    [handle seekToFileOffset:baseoffset + sizeof(uint64_t) + IFDTagCount*[SFBigTIFFTag sizeDirEntry]];
    NSData *offsetData = [handle readDataOfLength:sizeof(uint64_t)];
    const uint64_t *pOffset = offsetData.bytes;
    if(isBigEndian)
        nextOffset = NSSwapBigLongLongToHost(*pOffset);
    else
        nextOffset = NSSwapLittleLongLongToHost(*pOffset);
    //create NSDictionary holding all IFD Tags from mutable dictionary
    //IFDDictionary = [NSDictionary dictionaryWithDictionary:newIFDDictionary];
    return self;
}

-(SFBigTIFFTag*)tagWithID:(NSNumber*)tagID{
    return [IFDDictionary objectForKey:tagID];
}

-(NSDictionary*)ditionaryOfTags{
    return IFDDictionary;
}


-(uint32_t)nextIFDOffset{
    return (uint32_t) nextOffset;
};

@end
