//
//  SFOGLVolumeTexture.m
//  CytoFX
//
//  Created by ApocryphX on 2/11/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolumeTexture.h"

@implementation SFOGLVolumeTexture

- (id)init {
	self = [super init];
	[self setRenderMode:0];
	return self;
};

- (id)initWithContext:(NSOpenGLContext*)ctx  {
	self = [self init];
	[self setOpenGLContext:ctx];
	return self;
};

- (void)setRenderMode:(NSUInteger)value {
	sfRenderMode = value;
}; 

- (void)setOpenGLContext:(NSOpenGLContext*)cxt {
	sfContext = cxt;
};

;

@end
