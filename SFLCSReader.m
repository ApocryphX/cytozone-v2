//
//  SFLeicaReader.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//
#import "SFLCSReader.h"

#import "SFVoxelInfo.h"
#import "SFLCSBlockReader.h"
#import "SFTIFFReader.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFNDVoxelData.h"
#import "SFDataInfo.h"
#import "SFAlignment2D.h"
#import "SFGeometry.h"
#import "SFChannelInfo.h"

#import "utility.h"

#define MAPSIZE 6

const uint32_t LCSIDTypeMap[MAPSIZE] = { 116,120,121,122,6815843,0 };

static NSMutableDictionary* sfColorMap;

static NSArray *SFKLCSUnits;
static NSArray *SFKLCSSymbols;
static NSArray *SFKLCSTypes;

@interface SFLCSReader (private)
@end

@implementation SFLCSReader

+ (void)initialize {
	sfColorMap = [NSMutableDictionary new];
	//RGB colors
	[sfColorMap setObject:[NSColor redColor] forKey:@"Red"];
	[sfColorMap setObject:[NSColor greenColor] forKey:@"Green"];
	[sfColorMap setObject:[NSColor blueColor] forKey:@"Blue"];
	//CYMK colors
	[sfColorMap setObject:[NSColor yellowColor] forKey:@"Yellow"];
	[sfColorMap setObject:[NSColor cyanColor] forKey:@"Cyan"];
	[sfColorMap setObject:[NSColor magentaColor] forKey:@"Magenta"];
	[sfColorMap setObject:[NSColor whiteColor] forKey:@"Gray"];
	// other
	[sfColorMap setObject:[NSColor orangeColor] forKey:@"Glow"];
    
    SFKLCSUnits   = [NSArray arrayWithObjects:@"s",@"m",@"m",@"m",@"m",@"-", nil];
    SFKLCSSymbols = [NSArray arrayWithObjects:@"t",@"x",@"y",@"z",@"\\u03BB",@"-", nil];
    // { kTime,kSpace,kSpace,kSpace,kWavelength,kVoid }
    SFKLCSTypes = [NSArray arrayWithObjects:
                   SFKDimensionTypeTimeName,
                   SFKDimensionTypeSpaceName,
                   SFKDimensionTypeSpaceName,
                   SFKDimensionTypeSpaceName,
                   SFKDimensionTypeWavelengthName,
                   SFKDimensionTypeVoidName,
                   nil];
    
};

- (NSUInteger)lookupLCSDimID:(NSUInteger)DimID
{
	NSUInteger i;
	for ( i=0; i<(MAPSIZE-1) && LCSIDTypeMap[i]!=DimID; i++);
	return i;
};

+ (BOOL)isConfocalFile:(NSString*)filePath {
	return YES;
};

+ (NSArray*)fileExtensions {
    return [NSArray arrayWithObject:@"lei"];
};


- (id)initWithPath:(NSString*)fileName
{
	self = [super init];
    if(self) {
        sfLCSBlockReader = [[SFLCSBlockReader alloc] initWithFile:fileName];
        sfSeries = [sfLCSBlockReader blockWithIDSeries];
    }
	return self;
};


- (NSString*) seriesTitleAtIndex:(NSUInteger)idx {
	NSDictionary* dimensionDescription = [sfLCSBlockReader blockWithIDDimDescr:(uint32_t)idx];
	NSString* title = [dimensionDescription objectForKey:@"name"];
	return title;
}; 

- (NSArray*) fileNodes {
	NSMutableArray *childNodes = [NSMutableArray arrayWithCapacity:[self count]];
    // add all child nodes; LCS files have a linear array of subnodes without hierarchy 
	NSUInteger i, count = [self count];
	for(i=0;i<count;i++) {
		SFFileNodeDictionary *newNodeDictionary = [SFNDImporter fileNodeDictionary];
		NSDictionary* dimensionDescription = [sfLCSBlockReader blockWithIDDimDescr:(uint32_t)i];
		newNodeDictionary[SFKNodeTitleKey] = [dimensionDescription objectForKey:@"name"];
        //NSString *imagePath = [[NSBundle bundleForClass:[self class]] pathForResource:SFKLeicaFileIconName ofType:@"icns"];
        //newNodeDictionary[SFKNodeIconKey] = [[NSImage alloc]initByReferencingFile:imagePath];
        newNodeDictionary[SFKNodeDataIndexKey] = [NSNumber numberWithUnsignedInteger:i];
		[childNodes addObject:newNodeDictionary];
	}
    SFFileNodeDictionary *rootNodeDictionary = [SFNDImporter fileNodeDictionary];
	rootNodeDictionary[SFKNodeTitleKey] = [[[sfLCSBlockReader fileName] lastPathComponent]stringByDeletingPathExtension];
    rootNodeDictionary[SFKNodeChildrenKey] = [NSArray arrayWithArray:childNodes];
	return [NSArray arrayWithObject:rootNodeDictionary];
};


- (void)setVoxelProcessor:(NSObject<SFVoxelProcessing>*)processor 
{
	sfVoxelProcessor = processor;
};

- (NSString*)name 
{
	NSString* ret = [sfDimDescr objectForKey:@"name"];
	return ret;
};


- (NSUInteger)count 
{
	NSNumber* number = [sfSeries objectForKey:@"series count"];
	NSUInteger ret = [number unsignedIntValue];
	return ret;
};

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex
{
    NSUInteger channelCount = 0;
    NSDictionary *descriptionDictionary = [sfLCSBlockReader blockWithIDDimDescr:(uint32_t)nodeIndex];
    NSArray* dims = [descriptionDictionary objectForKey:@"dimensions"];
    if (6815843==[[[dims objectAtIndex:2] objectForKey:@"dimension id"]unsignedIntValue])
        channelCount = [[[dims objectAtIndex:2] objectForKey:@"extent"]unsignedIntValue];
    else
        channelCount = 1;

	return channelCount;
};

- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)idx channelIndex:(NSUInteger)channelIndex
{
	SFVoxelInfo* ret = [SFVoxelInfo new];
    @autoreleasepool {
        sfImages = [sfLCSBlockReader blockWithIDImages:(uint32_t)idx];
        sfLUT = [sfLCSBlockReader blockWithIDLUTDescr:(uint32_t)idx];
        sfDimDescr = [sfLCSBlockReader blockWithIDDimDescr:(uint32_t)idx];
        NSArray* dims = [sfDimDescr objectForKey:@"dimensions"];
        if (6815843==[[[dims objectAtIndex:2] objectForKey:@"dimension id"]unsignedIntValue])
            sfChannelCount = [[[dims objectAtIndex:2] objectForKey:@"extent"]unsignedIntValue];
        else
            sfChannelCount = 1;
        
        [ret setSamplesPerVoxel:[[sfImages objectForKey:@"samples per pixel"]unsignedIntValue]];
        [ret setBytesPerSample:[[sfDimDescr objectForKey:@"voxel size in bytes"]unsignedIntValue]/[ret samplesPerVoxel]];
        [ret setPrecision:[[sfDimDescr objectForKey:@"real world resolution"]unsignedIntValue]];
        [ret setDataType:kInteger];
        
        if ( [[sfImages objectForKey:@"samples per pixel"]unsignedIntValue] == 3 )
            [ret setInterleaveType:kInterleavedRGB];
        else {
            [ret setInterleaveType:kPlanar];	
            [ret setPhotometricInterpretation:kLuminance];
        }
    }
	return ret;
	
};

- (SFNDDimension*)nDDimensionAtNodeIndex:(NSUInteger)idx
{
	SFNDDimension* ret = [SFNDDimension new];
//	@autoreleasepool {
        sfDimDescr = [sfLCSBlockReader blockWithIDDimDescr:(uint32_t)idx];

		NSArray* dims = [sfDimDescr objectForKey:@"dimensions"];
		NSUInteger i, limit = [dims count];
		for (i=0; i<limit; i++) {
			NSDictionary* dim = [dims objectAtIndex:i];
			NSUInteger dimID = [[dim objectForKey:@"dimension id"]unsignedIntValue];
			if (dimID != 6815843) {
				NSUInteger idx = [self lookupLCSDimID:dimID];
                
                [ret addDimensionWithExtent:[[dim objectForKey:@"extent"]unsignedIntValue]
                                  magnitude:[dim objectForKey:@"magnitude"]
                              dimensionType:SFKLCSTypes[idx]
                                     symbol:SFKLCSSymbols[idx]
                                       unit:SFKLCSUnits[idx]];

			}
		}
//	}
	return ret;
};

- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex
{
	NSData* ret = nil;
	if(channelIndex<[self channelCountAtNodeIndex:nodeIndex]) {
        NSDictionary *imagesDictionary = [sfLCSBlockReader blockWithIDImages:(uint32_t)nodeIndex];

		NSArray* filenames = [imagesDictionary objectForKey:@"filenames"];
		NSString* ext = [sfSeries objectForKey:@"filename extension"];
		NSString* filename = [filenames objectAtIndex:(planeIndex*sfChannelCount+channelIndex)];
		NSString *folder = [[sfLCSBlockReader fileName] stringByDeletingLastPathComponent];
		NSString* path = [folder stringByAppendingPathComponent:[filename stringByAppendingPathExtension:ext]];
		if ([ext compare:@"tif"] == NSOrderedSame) {
			SFTIFFReader* _reader = [[SFTIFFReader alloc]initWithFileName:path];
			ret = [_reader dataAtIndex:0];
		}
		else if ([ext compare:@"raw"] == NSOrderedSame) {
			ret = [NSData dataWithContentsOfFile:path];
		}
	}
	return ret;
};


- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex
 {
    NSDictionary *LUTDictionary = [sfLCSBlockReader blockWithIDLUTDescr:(uint32_t)nodeIndex];

	NSString* colorname = [[LUTDictionary objectForKey:@"LUT names"]objectAtIndex:channelIndex];
	colorname = [NSString stringWithCString:[colorname cStringUsingEncoding:NSASCIIStringEncoding] encoding:NSASCIIStringEncoding]; 
	NSColor* color = [sfColorMap objectForKey:colorname];
	return color;
};

@end
