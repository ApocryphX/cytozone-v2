//
//  SFNDDataOutlineViewController.h
//  SanFrancisco
//
//
//

#import <Cocoa/Cocoa.h>


@protocol SFNodeIndexSelection <NSObject>

- (void)selectedNodeIndex:(NSUInteger)nodeIndex;

@end

@class SFNDImporter;

@interface SFNDDataOutlineViewController : NSViewController <NSSplitViewDelegate,NSOutlineViewDelegate> {
    
    IBOutlet NSTreeController       *treeControllerForDictionary;
    IBOutlet NSOutlineView          *fileNodeOutlineView;
    
    NSImage *stackImage;
    NSImage *timeSeriesImage;
    NSImage *timeStackImage;
    NSImage *planeImage;

    SFNDImporter *sfImporter;
}

@property (retain) SFNDImporter *importer;
@property (retain) NSObject <SFNodeIndexSelection> *selectionDelegate;
@property (assign) NSNumber *nodeIndexNumber;

- (NSView*)insertViewInParentView:(NSView*)parentView;

@end
