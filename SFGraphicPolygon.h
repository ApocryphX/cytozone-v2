//
//  SFGraphicPolygon.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicsObject.h"

@interface SFGraphicPolygon : SFAbstractGraphicsObject <SFGrapicInteraction> {
    BOOL isComplete;
}


@property (retain) NSMutableData *points;
@end
