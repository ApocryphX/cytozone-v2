//
//  SFOMEReader.h
//  SanFrancisco
//
//  Created by ApocryphX on 10/20/13.
//
//
#import "SFNDImporter.h"

@class SFTIFFReader;

@interface SFOMEReader : NSObject <SFNDReading> {
    NSString *filePath;
    SFTIFFReader   *sfTIFFReader;
    NSXMLDocument  *sfXMLDocument;
    NSUInteger      channelCount;
    NSMutableArray *tiffReaderArray;
    SFNDDimension   *dimensions;
    
    NSMutableDictionary *fileNameDictionary;
    NSMutableArray *channelColor;
    NSMutableArray *channelDictionaryArray;
    NSMutableArray *voxelInfoArray;
}

@end
