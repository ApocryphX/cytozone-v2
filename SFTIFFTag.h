//
//  SFTIFFTag.h
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Mon Mar 17 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFTIFFTag : NSObject {
    uint16_t tag;
    uint16_t sfDataType;
    uint32_t count;
    NSData* valueData;
}

+ (uint32_t)sizeDirEntry;

- (id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian;

- (uint16_t)tagID;
- (uint16_t)tagType;
- (uint32_t)tagCount;
- (uint32_t)length;
-(BOOL)isValueOffset;

- (NSData*)valueAsData;
- (NSNumber*)valueAsNumberAtIndex:(uint32_t)number;

@end
