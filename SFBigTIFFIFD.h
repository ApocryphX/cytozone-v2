//
//  SFBigTIFFIFD.h
//  SanFrancisco
//
//
//

#import <Foundation/Foundation.h>

@class SFBigTIFFTag;

@interface SFBigTIFFIFD : NSObject {
    
    uint64_t nextOffset;
    uint64_t IFDTagCount;
    NSMutableDictionary* IFDDictionary;
}

- (id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian;
- (uint32_t)nextIFDOffset;

- (SFBigTIFFTag*)tagWithID:(NSNumber*)tagID;
- (NSDictionary*)ditionaryOfTags;

@end
