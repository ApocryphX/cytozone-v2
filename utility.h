/*
 *  utility.h
 *  VoxelMachine
 *
 *  Copyright 2014 Elarity, LLC. All rights reserved.
 *
 */

#import <Carbon/Carbon.h>

#ifndef _UTILITY_h_
#define _UTILITY_h_

extern const float mat[3][5][3];

void convertRGBToPlanar(void* red, void* green, void* blue, const void* rgb, unsigned long count, unsigned long bytes);
float clamp (float value, float min, float max);
void invertMat(float matrix[16]);

#endif