//
//  SFLargeImageViewController.h
//  SanFrancisco
//
//
//

#import <Cocoa/Cocoa.h>
#import <Accelerate/Accelerate.h>

@class SFBitplane;

@interface SFLargeBitplaneViewController : NSViewController {
    NSUInteger m_TileSize[2];
    NSUInteger m_TextureCount;
    GLuint *m_TextureIDs;
    
//    NSMutableData *texData;
}

-(void)addImageWithCurrentContext:(vImage_Buffer)imageBuffer imageRect:(NSRect)imageRect;
-(IBAction)resetTransform:(id)sender;

@end
