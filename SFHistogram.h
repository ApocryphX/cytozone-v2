//
//  SFHistogram.h
//  SanFrancisco
//
//
//

#import <Foundation/Foundation.h>


@class SFBitplane;

@interface SFHistogram : NSObject {
    NSUInteger histogramBin[256];
    NSUInteger sampleNumber;
}

@property (readonly) NSData *histogramData;

+ (id)histogramForBitplane:(SFBitplane*)bitplane;
- (NSUInteger)otsuThreshold;

@end
