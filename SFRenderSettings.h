//
//  SFRenderSettings.h
//  VoxelMachine
//
//  Created by ApocryphX on 6/19/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFOGLVolumeScene, SFOGLTransform;

@interface SFRenderSettings : NSObject {
	
	float sfClipPlane;	
	float sfClipWidth;
	NSUInteger sfNumberOfCubes;
	NSUInteger sfCubeIndex;
	BOOL active;
	
	NSColor* sfBlendColor;
	NSColor* sfBackplaneColor;
	NSUInteger sfVolumeIndex;	
	NSUInteger sfRenderMode;
	NSUInteger sfBackplaneMode;
	float sfAlphaThreshold;
	float sfAlpha;
	float sfBlendFactor;
	float sfZScale;
	BOOL sfIsZInverted;
	BOOL sfIsPlaneTextureVisible;
	
	SFOGLTransform* sfTextureTransform;
	
}

- (id)initWithVolumeScene:(SFOGLVolumeScene*)scene;
- (void)readStateWithVolumeScene:(SFOGLVolumeScene*)scene;
- (void)restoreStateWithVolumeScene:(SFOGLVolumeScene*)scene;
- (SFRenderSettings*)interpolatedStateWithFraction:(double)frac ofState:(SFRenderSettings*)state;

@end
