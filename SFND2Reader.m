//
//  SFND2Reader.m
//  SanFrancisco
//
//
//

#import "SFND2Reader.h"
#include <nd2sdk/nd2ReadSDK.h>


int openFile(const wchar_t *path);

@implementation SFND2Reader

- (id)initWithPath:(NSString*)path
{
    self = [super init];
    if (self) {
        sfFileName = path;
        const wchar_t  *pString = (const wchar_t *)[path cStringUsingEncoding:NSUTF32StringEncoding];
        LIMFILEHANDLE hFile =  Lim_FileOpenForRead(pString);
        LIMATTRIBUTES pFileAttributes;
        memset(&pFileAttributes,0,sizeof(pFileAttributes));
        LIMRESULT attributeResult = Lim_FileGetAttributes(hFile, &pFileAttributes);
        
        LIMMETADATA_DESC pFileMetadata;
        memset(&pFileMetadata,0,sizeof(pFileMetadata));
        LIMRESULT  metaDataResult = Lim_FileGetMetadata(hFile, &pFileMetadata);
        
        LIMTEXTINFO pFileTextinfo;
        LIMRESULT  textInfoResult = Lim_FileGetTextinfo(hFile, &pFileTextinfo);
        
        LIMEXPERIMENT experiment;
        LIMRESULT experimentResult = Lim_FileGetExperiment(hFile, &experiment);
        
        LIMBINARIES pBinaries;
        LIMRESULT binaryDescriptorResult = Lim_FileGetBinaryDescriptors(hFile,&pBinaries);
    }
    return self;
}

+ (NSArray*)fileExtensions {
    return [NSArray arrayWithObject:@"nd2"];
    
};

+ (BOOL)isConfocalFile:(NSString*)filePath {
    return YES;
};

- (NSUInteger)count {
    return 1;
};
- (NSString*)name{
    return @"Nikon ND2 file";
};
- (NSArray*)fileNodes{
    NSMutableDictionary *dataSetDictionary = [NSMutableDictionary new];
    dataSetDictionary[SFKNodeDataIndexKey]=[NSNumber numberWithUnsignedInteger:0];
    dataSetDictionary[SFKNodeTitleKey]=[[sfFileName lastPathComponent]stringByDeletingPathExtension];
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    dictionary[SFKNodeTitleKey]=[sfFileName lastPathComponent];
    dictionary[SFKNodeChildrenKey] = [NSArray arrayWithObject:dataSetDictionary];
    return [NSArray arrayWithObject:dictionary];
};

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex{
    return 0;
};

- (SFNDDimension*)nDDimensionAtNodeIndex: (NSUInteger)nodeIndex{
    return nil;
};
- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex{
    return nil;
};
- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex{
    return nil;
};
- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex{
    return nil;
};

@end
