//
//  SFGraphicOpenGL.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicOpenGL.h"
#import <OpenGL/OpenGL.h>

@implementation SFGraphicOpenGL

@synthesize image;

- (NSImage*) renderImage
{
    
    NSOpenGLPixelFormatAttribute attrs[] = {
        
        // Specifying "NoRecovery" gives us a context that cannot fall back to the software renderer.  This makes the View-based context a compatible with the layer-backed context, enabling us to use the "shareContext" feature to share textures, display lists, and other OpenGL objects between the two.
        NSOpenGLPFANoRecovery, // Enable automatic use of OpenGL "share" contexts.
        
        NSOpenGLPFAColorSize, 24,
        NSOpenGLPFAAlphaSize, 8,
        NSOpenGLPFADepthSize, 16,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAAccelerated,
        0
    };
    NSOpenGLPixelFormat* pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    NSOpenGLContext *context = [[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil];
    [context makeCurrentContext];
    
    NSRect bounds = [self bounds];
    int height = bounds.size.height;
    int width = bounds.size.width;
    

    NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc]
                                  initWithBitmapDataPlanes:NULL
                                  pixelsWide:width
                                  pixelsHigh:height
                                  bitsPerSample:8
                                  samplesPerPixel:4
                                  hasAlpha:YES
                                  isPlanar:NO
                                  colorSpaceName:NSDeviceRGBColorSpace
                                  bytesPerRow:4 * width
                                  bitsPerPixel:0
                                  ];
        
    GLuint framebuffer, renderbuffer;
    GLenum status;
    // Set the width and height appropriately for your image
    GLuint imageWidth = width, imageHeight = height;
    //Set up a FBO with one renderbuffer attachment
    glGenFramebuffersEXT(1, &framebuffer);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebuffer);
    glGenRenderbuffersEXT(1, &renderbuffer);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, renderbuffer);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_RGBA8, imageWidth, imageHeight);
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
                                 GL_RENDERBUFFER_EXT, renderbuffer);
    status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    if (status != GL_FRAMEBUFFER_COMPLETE_EXT){
        // Handle errors
    }
    //Your code to draw content to the renderbuffer
    
    glViewport( 0.0, 0.0, width, height );
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    //gluPerspective( 30, width / height, 0.5, 1000.0 );
    
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glClearColor(0.0, 1.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
    
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glRotatef(45.0, 0.0, 0.0, 1.0);
	glBegin(GL_QUADS);
    glColor3f(1.0, 0.0, 0.0);
    glVertex2f(-0.5, -0.5);
    glVertex2f(-0.5,  0.5);
    glVertex2f( 0.5,  0.5);
    glVertex2f( 0.5, -0.5);
	glEnd();
	glPopMatrix();

    glFinish();
    
    //Your code to use the contents
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, [imageRep bitmapData]);
    
    // Make the window the target
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    // Delete the renderbuffer attachment
    glDeleteRenderbuffersEXT(1, &renderbuffer);
    
    NSImage *renderedImage=[[NSImage alloc] initWithSize:NSMakeSize(width,height)];
    [renderedImage addRepresentation:imageRep];
    [renderedImage setFlipped:YES];
    [renderedImage lockFocusOnRepresentation:imageRep]; // This will flip the rep.
    [renderedImage unlockFocus];
    
    return renderedImage;
    
}


- (void)draw
{
    //[image drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    if(image==nil)
        image = [self renderImage];
        //image = [NSImage imageNamed:NSImageNameBonjour];
    // Or stretch image to fill view
    [self.image drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
}

@end
