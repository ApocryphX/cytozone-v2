//
//  SFNDReader.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFNDImporter.h"

#import "SFVoxelInfo.h"
#import "SFLCSReader.h"
#import "SFTIFFReader.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFNDVoxelData.h"
#import "SFDataInfo.h"
#import "SFAlignment2D.h"
#import "SFGeometry.h"
#import "SFChannelInfo.h"
#import "SFNDDataNode.h"
#import "SFBitplane.h"

#import "utility.h"

static NSDictionary* sfNDReaderClassDictionary;
static NSDictionary* sfFileExtensionDictionary;

const NSString *SFKNodeTitleKey = @"title";
const NSString *SFKNodeChildrenKey = @"children";
const NSString *SFKNodeDataIndexKey = @"index";
const NSString *SFKNodeIconKey = @"icon";

const NSString *SFKLeicaFileIconName = @"File_Leica";
const NSString *SFKNikonFileIconName = @"File_Nikon";
const NSString *SFKZeissFileIconName = @"File_Zeiss";

const NSString *SFKStage_X_Position = @"x-position";
const NSString *SFKStage_Y_Position = @"y-position";

@implementation SFNDImporter

@synthesize outlineNodes = sfFileNodes;
@synthesize operationQueue;

+ (void)initialize {
	sfNDReaderClassDictionary = [NSMutableDictionary new];
	[sfNDReaderClassDictionary setValue:@"SFLCSReader" forKey:@"Leica LCS"];
	[sfNDReaderClassDictionary setValue:@"SFLIFReader" forKey:@"Leica LIF"];
	[sfNDReaderClassDictionary setValue:@"SFLSMReader" forKey:@"Zeiss LSM"];
	[sfNDReaderClassDictionary setValue:@"SFND2Reader" forKey:@"Nikon ND2"];
	[sfNDReaderClassDictionary setValue:@"SFCZIImporter" forKey:@"Zeiss CZI"];
	[sfNDReaderClassDictionary setValue:@"SFOMEReader" forKey:@"OME TIFF"];
	//[sfNDReaderClassDictionary setValue:@"SFND2Reader" forKey:@"Nikon ND2"];

    NSMutableDictionary *fileExtensionDictionary = [NSMutableDictionary new];
    for(NSString* fileType in sfNDReaderClassDictionary.allKeys) {
        NSString *classString = sfNDReaderClassDictionary[fileType];
        Class readerClass = NSClassFromString(classString);
        NSArray *extensions = [readerClass fileExtensions];
        for (NSString *extension in extensions) {
            fileExtensionDictionary[extension] = classString;
        }
    }
    sfFileExtensionDictionary = fileExtensionDictionary;
}

+ (NSArray*)fileExtensions {
    return [sfFileExtensionDictionary allKeys];
}

+ (NSString*)docTypeForExtension:(NSString*)extension {
    return [sfFileExtensionDictionary objectForKey:extension];
};

+ (SFFileNodeDictionary*)fileNodeDictionary {
    NSMutableDictionary *fileNodeDictionary = [NSMutableDictionary new];
    if(fileNodeDictionary) {
        
    }
    return fileNodeDictionary;
};

- (id)initWithPath:(NSString*)path  ofType:(NSString *)docType {
	if((self=[self init])) {
        NSString* classname = nil;
        if(docType)
            classname = [sfNDReaderClassDictionary valueForKey:docType];
		if(classname){
			Class readerclass = NSClassFromString(classname);
			sfNDReader = [[readerclass alloc] initWithPath:path];
			if(sfNDReader) {
				sfFileNodes = [sfNDReader fileNodes];
                operationQueue = [NSOperationQueue new];
                operationQueue.maxConcurrentOperationCount=1;
			}
		}
		if(!sfNDReader) {
			self = nil;
		}
	}
	return self;
};

- (void)stopLoadingData {
	sfStopLoadingFlag = YES;
};

- (void)setProgressIndicator:(NSProgressIndicator*)ctrl {
	progressIndicator = ctrl;
	[progressIndicator setDoubleValue:sfProgress];
};

- (NSProgressIndicator*)progressIndicator {
	return progressIndicator;
};

- (void)updateProgressIndicator {
		[progressIndicator setDoubleValue:sfProgress];		
};

- (void)syncProgressIndicator {
	[self performSelectorOnMainThread:@selector(updateProgressIndicator) 
							   withObject:self 
							waitUntilDone:YES ];						
}

- (void)setProgressValue:(float)value {
	sfProgress = value;
	if (progressIndicator) {
		[self performSelectorOnMainThread:@selector(updateProgressIndicator) 
							   withObject:self 
							waitUntilDone:YES ];						
	}
};

- (NSArray*)bitplaneArrayForNodeIndex:(NSUInteger)nodeIndex planeIndex:(NSUInteger)planeIndex {
    NSMutableArray *channelArray = nil;
	SFNDDimension* nddim = [sfNDReader nDDimensionAtNodeIndex:nodeIndex];
    NSUInteger width = [nddim extentAtIndex:0];
    NSUInteger height = [nddim extentAtIndex:1];
    SFVoxelInfo* voxelinfo = [sfNDReader voxelInfoAtNodeIndex:nodeIndex channelIndex:0];
    switch ([voxelinfo interleaveType]) {
		case kInterleavedRGB: {
            NSData* vdat = [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:0 planeIndex:planeIndex];
            NSUInteger rowBytes = vdat.length / height;
            vImage_Buffer rgbSrc;
            rgbSrc.height = height;
            rgbSrc.width = width;
            rgbSrc.rowBytes = rowBytes;
            vImage_Buffer dest[3];
            NSUInteger colorIndex;
            NSMutableData *data[3];
            for (colorIndex=0;colorIndex<3;colorIndex++) {
                NSMutableData *imageData = [NSMutableData dataWithLength:width*height];
                dest[colorIndex].height = height;
                dest[colorIndex].width = width;
                dest[colorIndex].rowBytes = width;
                data[colorIndex] = imageData;
            }
            vImageConvert_RGB888toPlanar8 (&rgbSrc,&dest[0],&dest[1],&dest[2],kvImageNoFlags);
            NSMutableData *bitplane[3];
            for (colorIndex=0;colorIndex<3;colorIndex++) {
                bitplane[colorIndex] = [SFBitplane bitplaneWithMutableData:data[colorIndex]
                                                     bytesPerSample:voxelinfo.bytesPerSample
                                                              width:width
                                                             height:height];
            }

            channelArray = [NSMutableArray arrayWithObjects:bitplane[0],bitplane[1],bitplane[2],nil];
            break;
        }
        case kPlanar: {
            NSUInteger channelCount = [sfNDReader channelCountAtNodeIndex:nodeIndex], channelIndex;
            channelArray = [NSMutableArray arrayWithCapacity:channelCount];
            for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
                SFVoxelInfo* voxelinfo = [sfNDReader voxelInfoAtNodeIndex:nodeIndex channelIndex:channelIndex];
                NSData* data = [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:channelIndex planeIndex:planeIndex];
                SFBitplane *bitplane = [SFBitplane bitplaneWithData:data
                                                    bytesPerSample:voxelinfo.bytesPerSample
                                                             width:width
                                                            height:height];
                bitplane.properties[SFKBitplaneColor]=[sfNDReader colorAtNodeIndex:nodeIndex channelIndex:channelIndex];
                [channelArray addObject:bitplane];
            }
            break;
        }
    };
    return channelArray;
}
/*
- (SFNDVoxelData*)readNDVoxelDataAtNodeIndex:(NSUInteger)nodeIndex
{
	[self setProgressValue:0.0];
	SFNDDimension* nddim = [sfNDReader nDDimensionAtNodeIndex:nodeIndex];
    
    
	SFNDVoxelData* ret = [[SFNDVoxelData alloc]initWithNDDimensionInfo:nddim];
	
	//[ret setName:[sfDimDescr objectForKey:@"name"]];
	
	SFVoxelInfo* voxelinfo = [sfNDReader voxelInfoAtNodeIndex:nodeIndex channelIndex:0];
	SFSize size = { [nddim extentAtIndex:0], [nddim extentAtIndex:1] };
	NSUInteger planesperchannel = [nddim strideAtIndex:[nddim count]-1] / [nddim strideAtIndex:1];
	switch ([voxelinfo interleaveType]) {
		case kInterleavedRGB: {
			SFVoxelInfo* planarvoxelinfo = [[SFVoxelInfo alloc]initWithVoxelInfo:voxelinfo];
			//create intermediate data info and buffers
			[planarvoxelinfo setInterleaveType:kPlanar];
			[planarvoxelinfo setSamplesPerVoxel:1];
			
			SFAlignment2D* planaralignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:planarvoxelinfo];
			[planaralignment setIsVectorAligned:NO];
			NSUInteger planarsize = [planaralignment bytesPerFrame];
			
			// create final info and buffers
			SFAlignment2D* targetalignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:planarvoxelinfo];
			[targetalignment setIsVectorAligned:NO];
			NSUInteger planesize = [targetalignment bytesPerFrame];
			NSMutableData* aligned_data = [NSMutableData dataWithBytesNoCopy:malloc(planesize) length:planesize];
			
			NSUInteger channelsize = planesperchannel * planesize;
			NSUInteger i, ii;
			void *pcolor[3];
			NSMutableArray* newchannels = [NSMutableArray new];
			for(ii=0; ii<3; ii++) { 
				pcolor[ii] = malloc(planarsize);
				[newchannels addObject:[NSMutableData dataWithCapacity:channelsize]];
			}
			
			NSUInteger bytesize = [voxelinfo bytesPerSample];
			for(i=0;i<planesperchannel;i++) {
				if (sfStopLoadingFlag == YES)
					break;
				@autoreleasepool {
					//NSData* vdat = [sfNDReader dataAtPlaneIndex:(uint32_t)i withChannel:0];
                    NSData* vdat = [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:0 planeIndex:i ];
					convertRGBToPlanar(pcolor[0],pcolor[1],pcolor[2],[vdat bytes],size.width*size.height,bytesize);
					for(ii=0;ii<3; ii++) { 
						if (sfStopLoadingFlag == YES)
							break;
						@autoreleasepool {
							[targetalignment getRealignedData:[aligned_data mutableBytes] source:pcolor[ii] alignment:planaralignment];
							[[newchannels objectAtIndex:ii] appendData:aligned_data];
						}	
						[self setProgressValue:(double)(ii*planesperchannel+i)/(double)(planesperchannel*3.0f)];
					}
					[self syncProgressIndicator];
				}
			}	
			
			SFChannelInfo* chinfo[3];
			chinfo[0] = [SFChannelInfo new];
			[chinfo[0] setColor:[NSColor redColor]];
			[chinfo[0] setName:@"Channel 1"];
			
			chinfo[1] = [SFChannelInfo new];
			[chinfo[1] setColor:[NSColor greenColor]];
			[chinfo[1] setName:@"Channel 2"];
			
			chinfo[2] = [SFChannelInfo new];
			[chinfo[2] setColor:[NSColor blueColor]];
			[chinfo[2] setName:@"Channel 3"];
			
			for(ii=0;ii<3;ii++) { 
				SFVoxelData* newvoxeldata = [[SFVoxelData alloc]initWithData:[newchannels objectAtIndex:ii]
																   alignment:targetalignment ];
				[ret addChannel:newvoxeldata withChannelInfo:chinfo[ii]];		
				free(pcolor[ii]);
			}
			break;
		}
		case kPlanar: {
            SFAlignment2D* sourcealignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:voxelinfo];
            SFVoxelInfo *targetVoxelInfo = [voxelinfo copy];
            targetVoxelInfo.bytesPerSample=1;
			SFAlignment2D* targetalignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:targetVoxelInfo];
			[targetalignment setIsVectorAligned:YES];
			NSUInteger planesize = [targetalignment bytesPerFrame];
			NSUInteger channelcount = [sfNDReader channelCountAtNodeIndex:nodeIndex];
			NSUInteger channelsize = planesperchannel * planesize;
			NSUInteger i,ii;
            void* pbuffer = malloc(planesize);
            //NSData* aligned_data = [NSData dataWithBytesNoCopy:pbuffer length:planesize];
			for(i=0;i<channelcount;i++) {
				if (sfStopLoadingFlag == YES)
					break;
				@autoreleasepool {
					NSMutableData *newchannel = [NSMutableData dataWithCapacity:channelsize];
					for(ii=0;ii<planesperchannel;ii++) {
						if (sfStopLoadingFlag == YES)
							break;
						@autoreleasepool {
							//[sfNDReader dataAtPlaneIndex:ii withChannel:i];
                            NSData* imageData = [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:i planeIndex:ii];
                            if(voxelinfo.bytesPerSample==2) {
                                NSMutableData *byteData = [NSMutableData dataWithLength:imageData.length/2];
                                NSUInteger pixelIndex, pixelCount = byteData.length;
                                uint16_t *src = (void*)imageData.bytes;
                                uint8_t *dst = byteData.mutableBytes;
                                NSUInteger shift = voxelinfo.precision-8;
                                for (pixelIndex=0;pixelIndex<pixelCount;pixelIndex++) {
                                    dst[pixelIndex] = src[pixelIndex]>>shift;
                                }
                                [newchannel appendData:byteData];
                            }
                            else 
                            [targetalignment getRealignedData:pbuffer source:[imageData bytes] alignment:sourcealignment];
							[newchannel appendData:imageData];
						}
						[self setProgressValue:(double)(i*planesperchannel+ii)/(double)(planesperchannel*channelcount) ];
					}
//				NSString* colorname = [[sfLUT objectForKey:@"LUT names"]objectAtIndex:i];
//				colorname = [NSString stringWithCString:[colorname cString]];
//				NSColor* color = [sfColorMap objectForKey:colorname];
					NSColor* color = [sfNDReader colorAtNodeIndex:nodeIndex channelIndex:i];
					SFChannelInfo* chinfo = [SFChannelInfo new];
					[chinfo setColor:color];
					[chinfo setName:[@"Channel " stringByAppendingString:[[NSNumber numberWithInteger:i+1] stringValue] ]];
					//				[chinfo  setIsActive:YES];
					
					SFVoxelData* newvoxeldata = [[SFVoxelData alloc]initWithData:newchannel	
																	   alignment:targetalignment];
					[ret addChannel:newvoxeldata withChannelInfo:chinfo];
				}	
			}	
			[self syncProgressIndicator];
			break;
		}
//		case kInterleavedLuminaceAlpha:
//		case kInterleavedRGBA:
//			break;
			
	}
		
	return ret;
};
 */

- (NSUInteger)count {
	return [sfNDReader count];
};
- (NSString*)name {
		return [sfNDReader name];
};

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex {
	return [sfNDReader channelCountAtNodeIndex:nodeIndex];
};

- (SFNDDimension*)nDDimensionAtNodeIndex:(NSUInteger)nodeIndex {
	return [sfNDReader nDDimensionAtNodeIndex:nodeIndex];
};

- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex {
   return [sfNDReader voxelInfoAtNodeIndex:nodeIndex channelIndex:channelIndex]; 
};


- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex {
	return [sfNDReader colorAtNodeIndex:nodeIndex channelIndex:channelIndex];
};

- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex {
    return [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:channelIndex planeIndex:planeIndex];
};


@end
