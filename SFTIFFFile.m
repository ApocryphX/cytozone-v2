//
//  SFTIFFFile.m
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Mon Mar 24 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//


#import "SFTIFFFile.h"
#import "SFTIFFIFD.h"
#import "SFBigTIFFIFD.h"
#import "SFTiff.h"

@interface SFTIFFFile (private)
@property (assign) BOOL isBigEndian;
@property (assign) BOOL isBigTIFF;
@end

#pragma pack (push, 1)
struct TIFFHeader {
    uint8_t byteorder[2];
    uint16_t magicbytes;
    uint32_t offset;
} ;
typedef struct TIFFHeader TIFFHeader;

struct BigTIFFHeader {
    uint8_t byteorder[2];
    uint16_t magicbytes;
    uint16_t offsetSize;
    uint16_t constant;
    uint64_t offset;
} ;
typedef struct BigTIFFHeader BigTIFFHeader;

#pragma pack (pop)

@implementation SFTIFFFile


- (void) readIFDs
{
    
    [sfFileHandle seekToFileOffset:0];

    headerData = [sfFileHandle readDataOfLength:sizeof(TIFFHeader)];
    const struct TIFFHeader *header = headerData.bytes;
    IFDs = [NSMutableArray new];
    NSUInteger offset;
    if(_isBigEndian)
        offset =  NSSwapBigIntToHost(header->offset);
    else
        offset =  NSSwapLittleIntToHost(header->offset);
    //count the number of IFDs and store offsets in IDFOffsets array
    while (offset!=0 ) {
		[sfFileHandle seekToFileOffset:offset];
		SFTIFFIFD* newIFD = [[SFTIFFIFD alloc] initWithFileHandle:sfFileHandle withEndian:_isBigEndian];
		[IFDs addObject:newIFD];
		offset = [newIFD nextIFDOffset];
		[sfFileHandle seekToFileOffset:offset];
    }
    NSLog(@"TIFF imported");
};

- (void) readBigIFDs
{
    
    [sfFileHandle seekToFileOffset:0];
    
    headerData = [sfFileHandle readDataOfLength:sizeof(BigTIFFHeader)];
    const BigTIFFHeader *header = headerData.bytes;
    IFDs = [NSMutableArray new];
    NSUInteger offset;
    if(_isBigEndian)
        offset =  NSSwapBigLongLongToHost(header->offset);
    else
        offset =  NSSwapLittleLongLongToHost(header->offset);
    //count the number of IFDs and store offsets in IDFOffsets array
    while (offset!=0 ) {
		[sfFileHandle seekToFileOffset:offset];
		SFBigTIFFIFD* newIFD = [[SFBigTIFFIFD alloc] initWithFileHandle:sfFileHandle withEndian:_isBigEndian];
		[IFDs addObject:newIFD];
		offset = [newIFD nextIFDOffset];
		[sfFileHandle seekToFileOffset:offset];
    }
};

- (BOOL) readTIFFHeader {
    BOOL success = NO;
    if (sfFileHandle != nil ) {
        headerData = [sfFileHandle readDataOfLength:sizeof(TIFFHeader)];
        const TIFFHeader *tiffheader = headerData.bytes;
        if( ( tiffheader->byteorder[0]=='I' && tiffheader->byteorder[1]=='I' ) || (tiffheader->byteorder[0]=='M' && tiffheader->byteorder[1]=='M')) {
            if( tiffheader->byteorder[0]=='I' && tiffheader->byteorder[1]=='I' ) {
                _isBigEndian = NO;
            }
            else if (tiffheader->byteorder[0]=='M' && tiffheader->byteorder[1]=='M') {
                _isBigEndian = YES;
            }
            uint16_t type = 0;
            if(self.isBigEndian)
                type = NSSwapBigShortToHost(tiffheader->magicbytes);
            else
                type = NSSwapLittleShortToHost(tiffheader->magicbytes);
            
            if (type == 0x002A) {
                _isBigTIFF = NO;
                [self readIFDs];
                success = YES;
            }
            else if (type == 0x002B) {
                _isBigTIFF = YES;
                [self readBigIFDs];
                success = YES;
            }
        }
    }
    return success;
}

- (instancetype)initWithURL:(NSURL *) fileURL
{
    // structure holds TIFF header and offset to first TIFF directory
    if((self = [super init])) {
        NSError *error;
        sfFileHandle = [NSFileHandle fileHandleForReadingFromURL:fileURL error:&error];
        if (error || sfFileHandle == nil)
            return nil;
        BOOL success = [self readTIFFHeader];
        if (success == NO)
            return nil;
    }
    // if this is not a valid TIFF file: deallocate self and return nil instead
    return self;
}

- (instancetype)initWithFileName:(NSString *) fileName
{
    // structure holds TIFF header and offset to first TIFF directory
    if((self = [super init])) {
        sfFileHandle = [NSFileHandle fileHandleForReadingAtPath:fileName];
        BOOL success = [self readTIFFHeader];
        if (success == NO)
            self = nil;
    }
    // if this is not a valid TIFF file: deallocate self and return nil instead
    return self;
}

-(NSFileHandle*)fileHandle {
    return sfFileHandle;
};

-(NSArray*)arrayOfIFDs {
    return IFDs;
};

;

@end
