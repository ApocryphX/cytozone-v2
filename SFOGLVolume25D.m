//
//  SFOGLVolume25D.m
//  CytoFX
//
//  Created by ApocryphX on 3/19/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolume25D.h"
@import GLKit;

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

#import "SFGeometry.h"
#import "SFOGLTexture3D.h"
#import "SFOGLTextureStack3D.h"
#import "SFNDDimension.h"
#import "SFNDVoxelData.h"
#import "SFVoxelData.h"
#import "SFVolumeWindowController.h"
#import "SFOGLTransform.h"
#import "SFOGLBackplane.h"
#import "SFOGLChannelSetting.h"
#import "SFOGLColor.h"

#import "utility.h"


@implementation SFOGLVolume25D	


- (void) setNDVoxelData:(SFNDVoxelData*)nddata {
	[super setNDVoxelData:nddata];
	if (nddata) {
		NSUInteger channelIndex, channelCount = [[nddata channels]count];
		for(channelIndex=0; channelIndex<channelCount; channelIndex++) {
			SFOGLTexture3D *tex3D = [[SFOGLTexture3D alloc]initWithContext:[[self openGLView]openGLContext]];
			[(SFOGLTexture3D*)tex3D setRenderMode:[self renderMode]];
			[tex3D setNDDimension:[sfNDVoxelData nDDimension] voxelInfo:[[nddata voxelDataAtIndex:channelIndex]voxelInfo]];
			if(sfOGLView)
				[tex3D setOpenGLContext:[sfOGLView openGLContext]];
			[sfVolumeTextures addObject:tex3D];
			
		}
	[self setBackplaneNDDimension:[[sfVolumeTextures objectAtIndex:0]nDDimension]];
	[self processAllChannels];
	}	
};

/*
- (void) setupMultiTex {
	// Program texture environment zero: 
	glActiveTexture(GL_TEXTURE0); 
	
	glBlendColor(1.0f,1.0f,1.0f,1.0f);
	
	glEnable(GL_TEXTURE_3D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE); 
	// --- RGB combiner
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE); 
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC0_RGB, GL_TEXTURE);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC1_RGB, GL_CONSTANT); 
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR); 
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC2_RGB, GL_CONSTANT);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA); 
	/// alpha combiner
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE); 
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_TEXTURE); 
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_ALPHA, GL_PREVIOUS); 
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, GL_SRC_ALPHA); 
	
	glActiveTexture(GL_TEXTURE1);  
	
	glEnable(GL_TEXTURE_3D);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
	
	// --- RGB combiner
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE); //GL_REPLACE, GL_MODULATE, GL_INTERPOLATE
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC0_RGB, GL_TEXTURE);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_ALPHA);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC1_RGB, GL_PREVIOUS); 
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR); 
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC2_RGB, GL_PRIMARY_COLOR);
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_COLOR); 
	
	/// alpha combiner
	glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_REPLACE); 
	
	glTexEnvf(GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_TEXTURE); 
	glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
	
};
*/
- (void)scaleVolume {
	
	float xscale,yscale,zscale,maxscale;
	SFNDVoxelData* nddata = [self nDVoxelData];
    if([nddata.nDDimension isCalibrated]) {
        xscale = fabs([[nddata magnitudeAtIndex:0]doubleValue]);
        yscale = fabs([[nddata magnitudeAtIndex:1]doubleValue]);
        zscale = fabs([[nddata magnitudeAtIndex:2]doubleValue]);
    }
    else {
        xscale = fabs((float)[nddata extentAtIndex:0]);
        yscale = fabs((float)[nddata extentAtIndex:1]);
        zscale = fabs((float)[nddata extentAtIndex:2]);
    }
    maxscale = (xscale>yscale)?xscale:yscale;
    maxscale = (maxscale>zscale)?maxscale:zscale;
    SFOGLTexture3D* tex3D = [sfVolumeTextures objectAtIndex:0];
    zscale = zscale / maxscale * sfZScale * [tex3D paddingRatioAtAxis:2];
    yscale = yscale / maxscale * [tex3D paddingRatioAtAxis:1];
    xscale = xscale / maxscale * [tex3D paddingRatioAtAxis:0];
    glScalef(xscale,yscale,zscale);

}

-(BOOL)draw {
    [super draw];
    unsigned int sliceIndex,j;
    //GLdouble mvmatrix[16], projmatrix[16], wx,wy,wz;
	
    NSInteger axis,slices;
	CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	glMatrixMode(GL_MODELVIEW);
	[[self transform] execute];

    glScalef(1.0, -1.0, 1.0); // flip volume upside down

	//calculate major viewing axis
    //glGetIntegerv(GL_VIEWPORT, viewport);
	//float centerx = (float)(viewport[2]-viewport[0])/2.0f;
	//float centery = (float)(viewport[3]-viewport[1])/2.0f;
    //glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
    //glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);
    //gluUnProject( centerx, centery, 1.0, mvmatrix, projmatrix, viewport, &wx, &wy, &wz);

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    GLKMatrix4 modelviewMatrix;
    glGetFloatv(GL_MODELVIEW_MATRIX, (float*)&modelviewMatrix);
    GLKMatrix4 projectionMatrix;
    glGetFloatv(GL_PROJECTION_MATRIX, (float*)&projectionMatrix);
    bool result;
    GLKVector3 window_coord = GLKVector3Make((float)(viewport[2]-viewport[0])/2.0f,(float)(viewport[3]-viewport[1])/2.0f, 1.0f);
    GLKVector3 transformedPoint = GLKMathUnproject(window_coord, modelviewMatrix, projectionMatrix, &viewport[0], &result);
    //need to get z=0 from

    
    //setup the texture coord generation
    if ( fabs(transformedPoint.x) > fabs(transformedPoint.y) )
        if ( fabs(transformedPoint.x) > fabs(transformedPoint.z) )
            axis = 0;
        else 
            axis = 2;
        else 
            if ( fabs(transformedPoint.y) > fabs(transformedPoint.z) )
                axis = 1;
            else
                axis = 2;
    NSUInteger  supersample = pow(2.0f, sfSampling);

	int dir = -1;
    if (axis == 0 && transformedPoint.x < 0.0) dir = 1;
    else if (axis == 1 && transformedPoint.y < 0.0) dir = 1;
    else if (axis == 2 && transformedPoint.z < 0.0) dir = 1;
	
	[self scaleVolume];

	glTranslatef(-0.5f, -0.5f, 0.0f );	
	
	glActiveTexture(GL_TEXTURE0);
	[[self backdrop] draw];	
	
	glMatrixMode(GL_MODELVIEW);
	//enable alpha testing (=threshold)
	//glEnable(GL_ALPHA_TEST);
	
        //glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_BLEND );
	
	slices = [[sfVolumeTextures objectAtIndex:0] extentAtAxis:axis];

	/*
	float xPlane[] = {1.0f, 0.0f, 0.0f, 0.0f};
	float yPlane[] = {0.0f, 1.0f, 0.0f, 0.0f};
	float zPlane[] = {0.0f, 0.0f, 1.0f, 0.0f};

	GLenum texunit[] = {GL_TEXTURE0, GL_TEXTURE1};
	
	for (sliceIndex=0;sliceIndex<1;sliceIndex++) {
		glActiveTexture(texunit[sliceIndex]);
		//setup the tex gen
		glTexGenfv(GL_S,GL_EYE_PLANE,xPlane);
		glTexGenfv(GL_T,GL_EYE_PLANE,yPlane);
		glTexGenfv(GL_R,GL_EYE_PLANE,zPlane);
		
		glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);
		glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);
		glTexGeni(GL_R,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);  
		
        glEnable(GL_TEXTURE_GEN_S);
        glEnable(GL_TEXTURE_GEN_T);
        glEnable(GL_TEXTURE_GEN_R);
        
		
		
	}
	*/
    glEnable(GL_TEXTURE_3D);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	
	//enable alpha testing (=threshold)
	//glEnable(GL_ALPHA_TEST);
	
	[self programBlendMode];
	
	if (dir < 0) 
		glTranslatef((float)mat[axis][4][0],(float)mat[axis][4][1],(float)mat[axis][4][2]);			
	
	
	glEnable(GL_TEXTURE_3D);
	glMatrixMode(GL_MODELVIEW);
    NSUInteger sliceCount = slices*supersample;
    switch (axis) {
        case 2:
                //glDisable(GL_TEXTURE_GEN_S);
                //glDisable(GL_TEXTURE_GEN_T);
                //glDisable(GL_TEXTURE_GEN_R);

            for(sliceIndex=0; sliceIndex < sliceCount; sliceIndex++) {
                for(j=0; j < [sfVolumeTextures count]; j++) {
                    if ([[sfChannelSettings objectAtIndex:j] isActive]) {
                        if([[sfChannelSettings objectAtIndex:j]volumeMode]==0)
                            glAlphaFunc(GL_GREATER,[[sfChannelSettings objectAtIndex:j]threshold]*[self alpha]);
                        else
                            glAlphaFunc(GL_ALWAYS,0);
                        glActiveTexture(GL_TEXTURE0);
                        [[sfColors objectAtIndex:j]setOpenGLColor];
                        [[sfVolumeTextures objectAtIndex:j]bind];
                        double z;
                        
                        if(transformedPoint.z<0.0)
                            z = (double)sliceIndex/(double)sliceCount;
                        else
                            z = 1.0-(double)sliceIndex/(double)sliceCount;
                        
                        glBegin(GL_QUADS);
                        
                        glTexCoord3d(0.0, 0.0, z);
                        glVertex3f(mat[axis][0][0], mat[axis][0][1],mat[axis][0][2]);
                        
                        glTexCoord3d(1.0, 0.0, z);
                        glVertex3f(mat[axis][1][0], mat[axis][1][1],mat[axis][1][2]);
                        
                        glTexCoord3d(1.0, 1.0, z);
                        glVertex3f(mat[axis][2][0], mat[axis][2][1],mat[axis][2][2]);
                        
                        glTexCoord3d(0.0, 1.0, z);
                        glVertex3f(mat[axis][3][0], mat[axis][3][1],mat[axis][3][2]);
                        
                        glEnd();
                    
                    }
                }
                glTranslatef(0.0,
                             0.0,
                             (float)mat[axis][4][2]/((float) (slices-1)*(float)supersample*dir) );			
            }
            break;
        case 1:
                //glDisable(GL_TEXTURE_GEN_S);
                //glDisable(GL_TEXTURE_GEN_T);
                //glDisable(GL_TEXTURE_GEN_R);

            for(sliceIndex=0; sliceIndex < slices; sliceIndex++) {
                for(j=0; j < [sfVolumeTextures count]; j++) {
                    if ([[sfChannelSettings objectAtIndex:j] isActive]) {
                        if([[sfChannelSettings objectAtIndex:j]volumeMode]==0)
                            glAlphaFunc(GL_GREATER,[[sfChannelSettings objectAtIndex:j]threshold]*[self alpha]);
                        else
                            glAlphaFunc(GL_ALWAYS,0);
                        glActiveTexture(GL_TEXTURE0);
                        [[sfColors objectAtIndex:j]setOpenGLColor];
                        [[sfVolumeTextures objectAtIndex:j]bind];
                        double z;
                        if(transformedPoint.y<0.0)
                            z = (double)sliceIndex/(double)slices;
                        else
                            z = 1.0-(double)sliceIndex/(double)slices;
                        

                        glBegin(GL_QUADS);
                        glTexCoord3d(0.0, z, 0.0);
                        glVertex3f(mat[axis][0][0], mat[axis][0][1],mat[axis][0][2]);
                        
                        glTexCoord3d(0.0, z, 1.0);
                        glVertex3f(mat[axis][1][0], mat[axis][1][1],mat[axis][1][2]);
                        
                        glTexCoord3d(1.0, z, 1.0);
                        glVertex3f(mat[axis][2][0], mat[axis][2][1],mat[axis][2][2]);
                        
                        glTexCoord3d(1.0, z, 0.0);
                        glVertex3f(mat[axis][3][0], mat[axis][3][1],mat[axis][3][2]);
                        glEnd();
                    }
                }
                glTranslatef((float)mat[axis][4][0]/((float) (slices-1)*dir),
                             (float)mat[axis][4][1]/((float) (slices-1)*dir),
                             0.0 );			
            }

            break;

        case 0:
                //glDisable(GL_TEXTURE_GEN_S);
                //glDisable(GL_TEXTURE_GEN_T);
                //glDisable(GL_TEXTURE_GEN_R);

            for(sliceIndex=0; sliceIndex < slices; sliceIndex++) {
                for(j=0; j < [sfVolumeTextures count]; j++) {
                    if ([[sfChannelSettings objectAtIndex:j] isActive]) {
                        if([[sfChannelSettings objectAtIndex:j]volumeMode]==0)
                            glAlphaFunc(GL_GREATER,[[sfChannelSettings objectAtIndex:j]threshold]*[self alpha]);
                        else
                            glAlphaFunc(GL_ALWAYS,0);
                        glActiveTexture(GL_TEXTURE0);
                        [[sfColors objectAtIndex:j]setOpenGLColor];
                        [[sfVolumeTextures objectAtIndex:j]bind];
                        
                        if(transformedPoint.x<0.0) {
                            double z = (double)sliceIndex/(double)slices;
                            
                            glBegin(GL_QUADS);
                            glTexCoord3d(z, 0.0, 0.0);
                            glVertex3f(mat[axis][0][0], mat[axis][0][1],mat[axis][0][2]);
                            
                            glTexCoord3d(z, 1.0, 0.0);
                            glVertex3f(mat[axis][1][0], mat[axis][1][1],mat[axis][1][2]);
                            
                            glTexCoord3d(z, 1.0, 1.0);
                            glVertex3f(mat[axis][2][0], mat[axis][2][1],mat[axis][2][2]);
                            
                            glTexCoord3d(z, 0.0, 1.0);
                            glVertex3f(mat[axis][3][0], mat[axis][3][1],mat[axis][3][2]);
                            
                            glEnd();
                        }
                        else {
                            double z = 1.0-(double)sliceIndex/(double)slices;
                            
                            glBegin(GL_QUADS);
                            glTexCoord3d(z, 0.0, 0.0);
                            glVertex3f(mat[axis][0][0], mat[axis][0][1],mat[axis][0][2]);
                            
                            glTexCoord3d(z, 1.0, 0.0);
                            glVertex3f(mat[axis][1][0], mat[axis][1][1],mat[axis][1][2]);
                            
                            glTexCoord3d(z, 1.0, 1.0);
                            glVertex3f(mat[axis][2][0], mat[axis][2][1],mat[axis][2][2]);
                            
                            glTexCoord3d(z, 0.0, 1.0);
                            glVertex3f(mat[axis][3][0], mat[axis][3][1],mat[axis][3][2]);
                            
                            glEnd();
                        }
                    }
                }
                glTranslatef((float)mat[axis][4][0]/((float) (slices-1)*dir),
                             (float)mat[axis][4][1]/((float) (slices-1)*dir),
                             0.0 );			
            }

            break;

        default:
            break;
    }

	glDisable(GL_TEXTURE_3D);
    /*
	for (sliceIndex=0;sliceIndex<1;sliceIndex++) {
		glActiveTexture(texunit[sliceIndex]);		
		glDisable(GL_TEXTURE_GEN_S);
		glDisable(GL_TEXTURE_GEN_T);
		glDisable(GL_TEXTURE_GEN_R);
		glDisable(GL_TEXTURE_3D);
	}
     */
        //glActiveTexture(GL_TEXTURE0);
	
	glMatrixMode(GL_MODELVIEW);	
        //glPopMatrix();
	CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
    return YES;
}; 

@end
