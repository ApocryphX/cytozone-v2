//
//  SFCLComputeKernel.m
//  Monterey
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFCLComputeKernel.h"


@implementation SFCLComputeKernel

@synthesize device_id;
@synthesize context;
@synthesize commands;
@synthesize kernel;


-(id)initWithSource:(NSString*)kernelSource functionName:(NSString*)functionName {
	int err;
	device_id = NULL;
	context = NULL;
	commands = NULL;
	kernel = NULL;
	
	if((self=[super init])) {
		err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_CPU, 1, &device_id, NULL);
		if (err != CL_SUCCESS) {
			NSLog(@"Error: Failed to create a device group!\n");
		}
		else {
			context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
			if (!context) {
				NSLog(@"Error: Failed to create a compute context!\n");
			}
			else {
				commands = clCreateCommandQueue(context, device_id, 0, &err); // compute command queue
				if (!commands) {
					NSLog(@"Error: Failed to create a command commands!\n");
				}
				else {
					const char *pSource = [kernelSource cStringUsingEncoding:NSASCIIStringEncoding];
					program = clCreateProgramWithSource(context, 1, (const char **) &pSource, NULL, &err);
					if (!program) {
						NSLog(@"Error: Failed to create compute program!\n");
					}
					else {
						err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
						if (err != CL_SUCCESS) {
							size_t len;
							char buffer[2048];
							
							NSLog(@"Error: Failed to build program executable!\n");
							clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
							NSLog(@"%s\n", buffer);
						}
						else {
							const char *pFunctionName = [functionName cStringUsingEncoding:NSASCIIStringEncoding];
							kernel = clCreateKernel(program, pFunctionName, &err);
							if (!kernel || err != CL_SUCCESS) {
								printf("Error: Failed to create compute kernel!\n");
							}
							else {
								return self;
							}
						}
						clReleaseProgram(program);
						program = NULL;
					}
					clReleaseCommandQueue(commands);
					commands = NULL;
				}
				clReleaseContext(context);
				context = NULL;
			}
		}
	}
	return nil;	
}

-(void)dealloc  {
	if(kernel)
		clReleaseKernel(kernel);
	if(program)
		clReleaseProgram(program);
	if(commands)
		clReleaseCommandQueue(commands);
	if(context)
		clReleaseContext(context);
	[super finalize];
}



@end
