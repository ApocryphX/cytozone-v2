//
//  SFTrackball.h
//  VoxelMachine
//
//  Created by ApocryphX on 1/6/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFOGLRotation;

@interface SFTrackball : NSObject {
    float sfRadius;
    float sfStartPoint[3];
    float sfEndPoint[3];
    NSPoint sfCenter;
}

- (void)start:(NSPoint)pt frame:(NSRect)frame;
- (struct GLRotation) rollTo:(NSPoint)pt;
+ (struct GLRotation) add:(struct GLRotation)dA toRotation:(struct GLRotation)A;//  Created by daves on Wed May 03 2000.

@end
