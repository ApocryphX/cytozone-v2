//
//  SFLoadingController.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFLoadingController.h"
#import <Accelerate/Accelerate.h>

#import "SFNDVoxelData.h"
#import "SFNDDataset.h"
#import "SFNDImporter.h"
#import "SFNDDimension.h"
#import "SFNDExperiment.h"
#import "SFImageAndTextCell.h"
#import "SFLargeBitplaneView.h"
#import "SFLargeBitplaneViewController.h"

#import "SFVoxelInfo.h"
#import "SFVoxelData.h"
#import "SFAlignment2D.h"
#import "SFChannelInfo.h"
#import "utility.h"

#import "SFVolumeWindowController.h"

const CGFloat SFKMinSourceViewSplitSize	= 120.0f;

NSString *OutlineColumnName = @"outlinecolumn";

@interface SFLoadingController ()
- (void)createTextures;
@end

@implementation SFLoadingController

@dynamic scaleBarLength;
@synthesize isVolumeSelected;
@synthesize showPlaneIndexSlider;
@dynamic planeIndex;
@synthesize maxPlaneIndex;
@dynamic nodeIndex;

- (void)showDataAtNodeIndex:(NSUInteger)index {
    self.nodeIndex = index;
}

- (void)setNodeIndex:(NSUInteger)index {
    @synchronized(self) {
        sfNodeIndex = index;
        [largeBitplaneViewController resetTransform:self];
        SFNDExperiment *experiment = [self document];
        SFNDImporter *sfNDReader = experiment.importer;
        [loadingQueue cancelAllOperations];
        [loadingQueue waitUntilAllOperationsAreFinished];
        
        
        [loadingQueue addOperationWithBlock:^{
            @synchronized (sfNDReader) {
                [_largeBitplaneView removeAllImages];
                
                SFNDDimension* nddim = [sfNDReader nDDimensionAtNodeIndex:sfNodeIndex];
                NSUInteger dimcount = [nddim count];
                if (dimcount >= 2 && [[nddim dimensionTypeAtIndex:2] isEqualToString:SFKDimensionTypeSpaceName]) {
                    self.maxPlaneIndex = [nddim extentAtIndex:2]-1;
                    self.planeIndex = maxPlaneIndex/2;
                }
                else {
                    self.maxPlaneIndex = 0;
                    self.planeIndex = 0;
                }
                if(maxPlaneIndex>1)
                    self.showPlaneIndexSlider = YES;
                else
                    self.showPlaneIndexSlider = NO;
            }
        }];    
    }
}

- (NSUInteger)nodeIndex {
    return sfNodeIndex;
}

- (void)setPlaneIndex:(NSUInteger)planeIndex {
    @synchronized(self){
        sfPlaneIndex = planeIndex;
        [self createTextures];
    }
}

- (NSUInteger)planeIndex {
    return sfPlaneIndex;
}

- (IBAction)resetTransform:(id)sender {
    [largeBitplaneViewController resetTransform:self];
};

- (void)setScaleBarLength:(NSString*)value {
    scaleBarLength = value;
    _largeBitplaneView.scaleBarLength = [value doubleValue]/1000000/imageScale;
    _largeBitplaneView.needsDisplay=YES;
}

- (NSString*)scaleBarLength {
    return scaleBarLength;
}

- (void)toggelScalebar:(id)sender {
    
    _largeBitplaneView.showScalebar=!_largeBitplaneView.showScalebar;
    _largeBitplaneView.needsDisplay=YES;
}


- (NSMutableDictionary*)outlineDictionary:(NSDictionary*)inputDictionary {
    NSMutableDictionary *dictionary  = [NSMutableDictionary new];
    dictionary[SFKNodeTitleKey]=inputDictionary[SFKNodeTitleKey];
    id dataIndex = inputDictionary[SFKNodeDataIndexKey];
    if(dataIndex && [dataIndex isKindOfClass:[NSNumber class]]) {
        NSUInteger index = [(NSNumber*)dataIndex unsignedIntegerValue];
        SFNDDimension *nddim = [[self.document importer] nDDimensionAtNodeIndex:index];
        if(nddim.isVolume) {
            dictionary[@"isVolume"]=[NSNumber numberWithBool:YES];
            if(nddim.isTimeSeries) {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:YES];
                dictionary[SFKNodeIconKey]=timeStackImage;
            }
            else {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:NO];
                dictionary[SFKNodeIconKey]=stackImage;
            }
        }
        else {
            dictionary[@"isVolume"]=[NSNumber numberWithBool:NO];
            if(nddim.isTimeSeries) {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:YES];
                dictionary[SFKNodeIconKey]=timeSeriesImage;
            }
            else {
                dictionary[@"isTimeSeries"]=[NSNumber numberWithBool:NO];
                dictionary[SFKNodeIconKey]=planeImage;
            }
        }
        dictionary[SFKNodeDataIndexKey]=(NSNumber*)dataIndex;
    }
    else {
        id children = inputDictionary[SFKNodeChildrenKey];
        if(children && [children isKindOfClass:[NSArray class]]) {
            NSMutableArray *childArray = [NSMutableArray new];
            dictionary[SFKNodeChildrenKey]=childArray;
            dictionary[SFKNodeIconKey]=[NSImage imageNamed:NSImageNameFolder];
            for(id childNode in children) {
                NSDictionary *childNodeDictionary = [self outlineDictionary:childNode];
                [childArray addObject:childNodeDictionary];
            }
        }
    }
    return dictionary;
};

-(void) leafNodeIndexPathsForDictionary:(NSDictionary*)node array:(NSMutableArray*)leafNodeIndexPaths indexPath:(NSIndexPath*)indexPath {
    id dataIndex = node[SFKNodeDataIndexKey];
    if(dataIndex && [dataIndex isKindOfClass:[NSNumber class]]) {
        [leafNodeIndexPaths addObject:indexPath];
    }
    else {
        id children = node[SFKNodeChildrenKey];
        if(children && [children isKindOfClass:[NSArray class]]) {
            NSUInteger childrenIndex=0;
            for(id child in children) {
                [self leafNodeIndexPathsForDictionary:child
                                                array:leafNodeIndexPaths 
                                            indexPath:[indexPath indexPathByAddingIndex:childrenIndex]];
                childrenIndex++;
            }
        }
    }
}

    // the tree controller contents has to be initialized before windowDidLoad is called
- (void)awakeFromNib {
    loadingQueue = [NSOperationQueue new];
        //loadingQueue.maxConcurrentOperationCount=1;

    
    stackImage = [NSImage imageNamed:@"stack"];
    timeSeriesImage = [NSImage imageNamed:@"time"];
    timeStackImage = [NSImage imageNamed:@"time_stack"];
    planeImage = [NSImage imageNamed:@"plane"];

    SFNDExperiment *experiment = self.document;
    NSDictionary *outlineNodes = experiment.outlineNodes[0];
    NSMutableDictionary *outlineDictionary = [self outlineDictionary:outlineNodes];
    [outlineDictionary removeObjectForKey:SFKNodeIconKey];
    SFFileNodeDictionary *rootNodeDictionary = outlineDictionary;
    [treeControllerForDictionary addObject:rootNodeDictionary];
    NSArray *leafNodeIndexPaths = [self leafNodeIndexPathsForDictionary:rootNodeDictionary];
    [treeControllerForDictionary setSelectionIndexPath:leafNodeIndexPaths[0]];

    largeBitplaneViewController = [[SFLargeBitplaneViewController alloc]
                                   initWithNibName:@"SFLargeBitplaneViewController" bundle:nil];
    if (largeBitplaneViewController) {
        _largeBitplaneView = (SFLargeBitplaneView*)largeBitplaneViewController.view;
        [largeBitplaneViewPlaceholder addSubview:_largeBitplaneView];
        
        NSRect newBounds;
        newBounds.origin.x = 0;
        newBounds.origin.y = 0;
        newBounds.size.width = [[_largeBitplaneView superview] frame].size.width;
        newBounds.size.height = [[_largeBitplaneView superview] frame].size.height;
        [_largeBitplaneView setFrame:[[_largeBitplaneView superview] frame]];
        
            // make sure our added subview is placed and resizes correctly
        [_largeBitplaneView setFrameOrigin:NSMakePoint(0,0)];
        [_largeBitplaneView setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
        
    }

}

-(NSArray *)leafNodeIndexPathsForDictionary:(NSDictionary*)rootNode {
    NSMutableArray *leafNodeIndexPaths = [NSMutableArray new];
    [self leafNodeIndexPathsForDictionary:rootNode
                                    array:leafNodeIndexPaths
                                indexPath:[NSIndexPath indexPathWithIndex:0]];
    return leafNodeIndexPaths;
}

- (void)windowDidLoad {
    [super windowDidLoad];
    

    self.scaleBarLength = @"5 µm";
    //largeBitplaneView.scaleBarLength = [scaleBarLength doubleValue]*1000000;
    [self setFileName:@""];
    NSTableColumn *tableColumn2 = [fileNodeOutlineView tableColumns][0];
    tableColumn2.identifier = OutlineColumnName;
    SFImageAndTextCell *imageAndTextCell = [SFImageAndTextCell new];
    [tableColumn2 setDataCell:imageAndTextCell];
    tableColumn2.identifier = OutlineColumnName;
    // make our outline view appear with gradient selection, and behave like the Finder, iTunes, etc.
    [fileNodeOutlineView setSelectionHighlightStyle:NSTableViewSelectionHighlightStyleSourceList];
    // set self as delegate
    [fileNodeOutlineView setDelegate:self];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc]initWithKey:@"index" ascending:NO];
    //[m_TreeController setSortDescriptors:[NSArray arrayWithObject:descriptor]];
    //[fileNodeOutlineView reloadItem:nil reloadChildren:YES];

    [fileNodeOutlineView expandItem:nil expandChildren:YES ];
    
    NSArray *selection = [treeControllerForDictionary selectedObjects];
	if (selection && [selection count] > 0)
	{
        NSArray *selection = [treeControllerForDictionary selectedObjects];
        id firstNode = selection[0];
        if([firstNode isKindOfClass:[NSDictionary class]]) {
            NSDictionary *nodeDictionary = (NSDictionary*)firstNode;
            self.isVolumeSelected = nodeDictionary[@"isVolume"];
        }
        [self loadDataForSelection:selection];
	}

    [splitView setDelegate:self];
    /*
    NSMutableArray *leafIndexPath = [NSMutableArray array];
    // add the root node
    [treeController addObject:experiment.outlineNodes[0]];
    // recursively collect all leaf nodes
    [rootNode indexPathOfSelectableNodes:[NSIndexPath indexPathWithIndex:0] nodeIndexArray:leafIndexPath];
   //[treeController setSelectionIndexPaths:dataIndexPath];
    [treeController setSelectionIndexPath:leafIndexPath[0]];
    // Initialization code here.
    NSTableColumn *tableColumn = [outlineView tableColumns][0];
    tableColumn.identifier = OutlineColumnName;
   // SFImageAndTextCell *imageAndTextCell = [SFImageAndTextCell new];
    [tableColumn setDataCell:imageAndTextCell];
    tableColumn.identifier = OutlineColumnName;
    // make our outline view appear with gradient selection, and behave like the Finder, iTunes, etc.
    [outlineView setSelectionHighlightStyle:NSTableViewSelectionHighlightStyleSourceList];
    // set self as delegate
    [outlineView setDelegate:self];
    //NSSortDescriptor *descriptor = [[NSSortDescriptor alloc]initWithKey:@"index" ascending:NO];
    //[m_TreeController setSortDescriptors:[NSArray arrayWithObject:descriptor]];
    [outlineView reloadItem:Nil reloadChildren:YES];
    [outlineView expandItem:Nil expandChildren:YES ];
    
    textureTileSize = NSMakeSize(128,128);
    // texture drawing setup
    SFNDImporter<SFNDReading>* importer = [self.document importer];
    NSArray *selection = treeController.selectedObjects;
    SFNDFileNode* firstNode = selection[0];
    NSUInteger channelIndex, channelCount = [importer channelCountAtNodeIndex:firstNode.nodeIndex.integerValue];
    for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
        [largeBitplaneView addImageWithReader:importer nodeIndex:firstNode.nodeIndex.integerValue channelIndex:channelIndex];
    }
    */
}


- (BOOL)windowShouldClose:(id)sender {
	return !sfIsThreadRunning;
}

- (IBAction)cancelLoading:(id)sender {
	[[self.document importer] stopLoadingData];
}

- (void)setFileName:(NSString*)value {
	sfFileName = value;
};

- (NSString*)fileName {
	return sfFileName;
}

-(void)loadImageAtIndex:(NSUInteger)index {
    
}

- (IBAction)loadData:(id)sender {
    SFNDExperiment *ndDocument = self.document;
        //SFNDImporter *importer = ndDocument.importer;
    
        //[progressIndicator setHidden:NO];
        //[[self.document importer] setProgressIndicator:progressIndicator];
    NSArray *selection = [treeControllerForDictionary selectedObjects];
    id node = selection[0];
    if([node isKindOfClass:[NSDictionary class]]) {
        NSDictionary *fileDictionary = node;
        NSNumber *indexNumber = fileDictionary[SFKNodeDataIndexKey];
        if(indexNumber) {
            
            NSUInteger index = [indexNumber unsignedIntegerValue];
            SFNDDimension *nddim = [[self.document importer] nDDimensionAtNodeIndex:index];
            NSUInteger dimcount = [nddim count];
            if (dimcount > 2 && [[nddim dimensionTypeAtIndex:2] isEqualToString:SFKDimensionTypeSpaceName]) {
                
                SFVolumeWindowController* ctrl = [[SFVolumeWindowController alloc] initWithWindowNibName:@"VolumeView"];
                [ctrl setShouldCloseDocument:NO];
                [ctrl showWindow:self];
                ctrl.nodeTitle = fileDictionary[SFKNodeTitleKey];
                    //SFNDExperiment *experiment = [self document];
                    //SFNDImporter *sfNDReader = experiment.importer;
                [loadingQueue addOperationWithBlock:^{
                    
                    SFNDVoxelData* vdata = [self readNDVoxelDataAtNodeIndex:index progress:ctrl.progressIndicator];
                    if (vdata) {
                        SFNDDataset* dataset = [[SFNDDataset alloc]initWithNDVoxelData:vdata];
                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                            [ctrl setNDDataset:dataset];
                        }];
                    }
                }];
                [ndDocument addWindowController:ctrl];
            }
        }
    }
}

- (void)createTextures {
    
    SFNDExperiment *experiment = [self document];
    SFNDImporter *sfNDReader = experiment.importer;
    [sfNDReader.operationQueue  addOperationWithBlock:^{
        @synchronized (sfNDReader) {
                //[largeBitplaneView removeAllImages];
            SFNDDimension* nddim = [sfNDReader nDDimensionAtNodeIndex:sfNodeIndex];
            
            //// remove calibartion for testing purposes
            //NSUInteger dimensionIndex, dimensionCount = nddim.count;
            //for (dimensionIndex=0;dimensionIndex < dimensionCount; dimensionIndex++) {
            //    [nddim setMagnitude:nil atIndex:dimensionIndex];
            //}

            NSUInteger dimcount = [nddim count];
            if (dimcount >= 2 && [[nddim dimensionTypeAtIndex:1] isEqualToString:SFKDimensionTypeSpaceName]) {
                NSRect imageRect;
                
                if([nddim isCalibrated]) {
                    CGFloat maxSize = MAX([[nddim magnitudeAtIndex:0]doubleValue],[[nddim magnitudeAtIndex:1]doubleValue]);
                    imageScale = maxSize;
                    _largeBitplaneView.scaleBarLength = [scaleBarLength doubleValue]/1000000/imageScale;
                    imageRect.size =  NSMakeSize([[nddim magnitudeAtIndex:0]doubleValue]/maxSize,[[nddim magnitudeAtIndex:1]doubleValue]/maxSize);
                    imageRect.origin =  NSMakePoint(-0.5+(1.0-imageRect.size.width)/2.0,-0.5+(1.0-imageRect.size.height)/2.0);
                    _largeBitplaneView.imageRect=imageRect;
                }
                else {
                    NSSize imageSize = NSMakeSize([nddim extentAtIndex:0],[nddim extentAtIndex:1]);
                    CGFloat maxSize = MAX(imageSize.width,imageSize.height);
                    imageRect.size =  NSMakeSize(imageSize.width/maxSize,imageSize.height/maxSize);
                    imageRect.origin =  NSMakePoint(-0.5+(1.0-imageRect.size.width)/2.0,-0.5+(1.0-imageRect.size.height)/2.0);
                    _largeBitplaneView.imageRect=imageRect;
                    
                }

                [_largeBitplaneView reshape];
                vImage_Buffer imageBuffer;
                imageBuffer.width = [nddim extentAtIndex:0];
                imageBuffer.height = [nddim extentAtIndex:1];
                imageBuffer.rowBytes = [nddim extentAtIndex:0];
                SFVoxelInfo* voxelinfo = [sfNDReader voxelInfoAtNodeIndex:sfNodeIndex channelIndex:0];
                switch ([voxelinfo interleaveType]) {
                    case kInterleavedRGB: {
                        NSMutableData *planarBuffer[3];
                        vImage_Buffer imagePlanarBuffer[3];
                        const NSColor *colors[] = {[NSColor redColor],[NSColor greenColor],[NSColor blueColor]};
                        NSUInteger channelIndex, channelCount = 3;
                        for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
                            planarBuffer[channelIndex] = [NSMutableData dataWithLength:imageBuffer.height*imageBuffer.rowBytes];
                            imagePlanarBuffer[channelIndex]=imageBuffer;
                            imagePlanarBuffer[channelIndex].data=planarBuffer[channelIndex].mutableBytes;
                        }
                        
                        NSData* rgbData = [sfNDReader dataAtNodeIndex:sfNodeIndex channelIndex:0 planeIndex:sfPlaneIndex];
                        imageBuffer.data=(void*)rgbData.bytes;
                        convertRGBToPlanar(planarBuffer[0].mutableBytes,
                                           planarBuffer[1].mutableBytes,
                                           planarBuffer[2].mutableBytes,
                                           rgbData.bytes,imageBuffer.width*imageBuffer.height,1);
                        
                        for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
                            
                            imageBuffer.data = planarBuffer[channelIndex].mutableBytes;
                            NSMutableDictionary *tiledImageDictionary = nil;
                            tiledImageDictionary = [_largeBitplaneView createTiledTexturesForImage:imageBuffer
                                                                                        imageRect:imageRect];
                            if(tiledImageDictionary)
                                tiledImageDictionary[SFKTiledTextureColorKey]=colors[channelIndex];
                            if(tiledImageDictionary) {
                                NSMutableArray *tileArray = _largeBitplaneView.tileArray;
                                @synchronized (tileArray) {
                                    if(tileArray.count>channelIndex)
                                        [_largeBitplaneView.tileArray replaceObjectAtIndex:channelIndex withObject:tiledImageDictionary];
                                    else
                                        [_largeBitplaneView.tileArray addObject:tiledImageDictionary];
                                    _largeBitplaneView.needsDisplay=YES;
                                }
                            }
                        }
                        break;
                    }
                    case kPlanar: {
                        
                        NSUInteger channelIndex, channelCount = [sfNDReader channelCountAtNodeIndex:sfNodeIndex];
                        for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
                            NSData* imageData = [sfNDReader dataAtNodeIndex:sfNodeIndex channelIndex:channelIndex planeIndex:sfPlaneIndex];
                            if(imageData) {
                                SFVoxelInfo *vinfo = [sfNDReader voxelInfoAtNodeIndex:sfNodeIndex channelIndex:channelIndex];
                                NSMutableDictionary *tiledImageDictionary = nil;
                                if(vinfo.bytesPerSample==2) {
                                    SFNDDimension *ndDimension = [sfNDReader nDDimensionAtNodeIndex:sfNodeIndex];
                                    
                                    NSUInteger pixelIndex, pixelCount = [ndDimension extentAtIndex:0]*[ndDimension extentAtIndex:1];
                                    NSMutableData *convertedData = [NSMutableData dataWithLength:pixelCount];
                                    
                                    uint16_t *src = (void*)imageData.bytes;
                                    uint8_t *dst = convertedData.mutableBytes;
                                    NSUInteger shift = vinfo.precision-8;
                                    for (pixelIndex=0;pixelIndex<pixelCount;pixelIndex++) {
                                        uint16_t value = src[pixelIndex];
                                        uint8_t byteValue = value>>shift;
                                        dst[pixelIndex] = byteValue;
                                    }
                                    vImage_Buffer imageBuffer;
                                    imageBuffer.width = [ndDimension extentAtIndex:0];
                                    imageBuffer.height = [ndDimension extentAtIndex:1];
                                    imageBuffer.rowBytes = imageBuffer.width;
                                    imageBuffer.data = (void*)convertedData.bytes;
                                    
                                    tiledImageDictionary = [_largeBitplaneView createTiledTexturesForImage:imageBuffer imageRect:imageRect];
                                    NSColor *color = [sfNDReader colorAtNodeIndex:sfNodeIndex channelIndex:channelIndex];
                                    tiledImageDictionary[SFKTiledTextureColorKey]=color;
                                }
                                else {
                                    imageBuffer.data = (void*)imageData.bytes;
                                    tiledImageDictionary = [_largeBitplaneView createTiledTexturesForImage:imageBuffer imageRect:imageRect];
                                    NSColor *color = [sfNDReader colorAtNodeIndex:sfNodeIndex channelIndex:channelIndex];
                                    tiledImageDictionary[SFKTiledTextureColorKey]=color;
                                }
                                if(tiledImageDictionary) {
                                    NSMutableArray *tileArray = _largeBitplaneView.tileArray;
                                    @synchronized (tileArray) {
                                        if(tileArray.count>channelIndex)
                                            [tileArray replaceObjectAtIndex:channelIndex withObject:tiledImageDictionary];
                                        else
                                            [tileArray addObject:tiledImageDictionary];
                                        _largeBitplaneView.needsDisplay=YES;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }
    }];
};


- (SFNDVoxelData*)readNDVoxelDataAtNodeIndex:(NSUInteger)nodeIndex progress:(NSProgressIndicator*)progressIndicator;
{
    [progressIndicator setHidden:NO];
    progressIndicator.indeterminate= NO;
	progressIndicator.doubleValue = 0.0;
    SFNDExperiment *experiment = [self document];
    SFNDImporter *sfNDReader = experiment.importer;
    BOOL sfStopLoadingFlag=NO;
    
	SFNDDimension* nddim = [sfNDReader nDDimensionAtNodeIndex:nodeIndex];
	SFNDVoxelData* ret = [[SFNDVoxelData alloc]initWithNDDimensionInfo:nddim];
	
	SFVoxelInfo* voxelinfo = [sfNDReader voxelInfoAtNodeIndex:nodeIndex channelIndex:0];
	SFSize size = { [nddim extentAtIndex:0], [nddim extentAtIndex:1] };
	NSUInteger planesperchannel = [nddim strideAtIndex:[nddim count]-1] / [nddim strideAtIndex:1];
	switch ([voxelinfo interleaveType]) {
		case kInterleavedRGB: {
			SFVoxelInfo* planarvoxelinfo = [[SFVoxelInfo alloc]initWithVoxelInfo:voxelinfo];
                //create intermediate data info and buffers
			[planarvoxelinfo setInterleaveType:kPlanar];
			[planarvoxelinfo setSamplesPerVoxel:1];
			
			SFAlignment2D* planaralignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:planarvoxelinfo];
			[planaralignment setAlignment:SFKVectorAlignedName];
			NSUInteger planarsize = [planaralignment bytesPerFrame];
			
                // create final info and buffers
			SFAlignment2D* targetalignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:planarvoxelinfo];
			[targetalignment setAlignment:SFKVectorAlignedName];
			NSUInteger planesize = [targetalignment bytesPerFrame];
			
            NSMutableData* aligned_data = [NSMutableData dataWithBytesNoCopy:malloc(planesize) length:planesize];
			
			NSUInteger channelsize = planesperchannel * planesize;
			NSUInteger i, ii;
			void *pcolor[3];
			NSMutableArray* newchannels = [NSMutableArray new];
			for(ii=0; ii<3; ii++) {
				pcolor[ii] = malloc(planarsize);
				[newchannels addObject:[NSMutableData dataWithCapacity:channelsize]];
			}
			
			NSUInteger bytesize = [voxelinfo bytesPerSample];
			for(i=0;i<planesperchannel;i++) {
				if (sfStopLoadingFlag == YES)
					break;
				@autoreleasepool {
                    NSData* vdat = [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:0 planeIndex:i ];
					convertRGBToPlanar(pcolor[0],pcolor[1],pcolor[2],[vdat bytes],size.width*size.height,bytesize);
					for(ii=0;ii<3; ii++) {
						if (sfStopLoadingFlag == YES)
							break;
						@autoreleasepool {
							[targetalignment getRealignedData:[aligned_data mutableBytes] source:pcolor[ii] alignment:planaralignment];
							[[newchannels objectAtIndex:ii] appendData:aligned_data];
						}
						progressIndicator.doubleValue=(double)(ii*planesperchannel+i)/(double)(planesperchannel*3.0f)*100;
					}
				}
			}
			if (sfStopLoadingFlag == YES)
				break;
			
			SFChannelInfo* chinfo[3];
			chinfo[0] = [SFChannelInfo new];
			[chinfo[0] setColor:[NSColor redColor]];
			[chinfo[0] setName:@"Channel 1"];
			
			chinfo[1] = [SFChannelInfo new];
			[chinfo[1] setColor:[NSColor greenColor]];
			[chinfo[1] setName:@"Channel 2"];
			
			chinfo[2] = [SFChannelInfo new];
			[chinfo[2] setColor:[NSColor blueColor]];
			[chinfo[2] setName:@"Channel 3"];
			
			for(ii=0;ii<3;ii++) {
				SFVoxelData* newvoxeldata = [[SFVoxelData alloc]initWithData:[newchannels objectAtIndex:ii]
																   alignment:targetalignment ];
				[ret addChannel:newvoxeldata withChannelInfo:chinfo[ii]];
				free(pcolor[ii]);
			}
			break;
		}
		case kPlanar: {
            //SFAlignment2D* sourcealignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:voxelinfo];
            SFVoxelInfo *targetVoxelInfo = [voxelinfo copy];
            targetVoxelInfo.bytesPerSample=1;
            targetVoxelInfo.precision=8;
            targetVoxelInfo.bytesPerSample=1;
            
            SFAlignment2D* sourcealignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:targetVoxelInfo];
			SFAlignment2D* targetalignment = [[SFAlignment2D alloc] initWithSize:size voxelInfo:targetVoxelInfo];
			[targetalignment setAlignment:SFKVectorAlignedName];
			NSUInteger channelcount = [sfNDReader channelCountAtNodeIndex:nodeIndex];
			NSUInteger channelIndex,planeIndex;
			for(channelIndex=0;channelIndex<channelcount;channelIndex++) {
				@autoreleasepool {
                    size_t channelSize = [targetalignment bytesPerFrame]*planesperchannel;
					NSMutableData *alignedImageData = [NSMutableData dataWithLength:channelSize];
					for(planeIndex=0;planeIndex<planesperchannel;planeIndex++) {
						@autoreleasepool {
                            NSData* imageData = [sfNDReader dataAtNodeIndex:nodeIndex channelIndex:channelIndex planeIndex:planeIndex];
                            if(voxelinfo.bytesPerSample==2) {
                                NSMutableData *byteData = [NSMutableData dataWithLength:imageData.length/2];
                                NSUInteger pixelIndex, pixelCount = byteData.length;
                                uint16_t *src = (void*)imageData.bytes;
                                uint8_t *dst = byteData.mutableBytes;
                                NSUInteger shift = voxelinfo.precision-8;
                                for (pixelIndex=0;pixelIndex<pixelCount;pixelIndex++) {
                                    uint16_t value = src[pixelIndex];
                                    uint8_t byteValue = value>>shift;
                                    dst[pixelIndex] = byteValue;
                                }
                                imageData = byteData;
                            }
                            void *pDst = alignedImageData.mutableBytes+[targetalignment bytesPerFrame]*planeIndex;
                            [targetalignment getRealignedData:pDst source:imageData.bytes alignment:sourcealignment];
						}
						progressIndicator.doubleValue=(double)(channelIndex*planesperchannel+planeIndex)/(double)(planesperchannel*channelcount)*100;
					}
					NSColor* color = [sfNDReader colorAtNodeIndex:nodeIndex channelIndex:channelIndex];
					SFChannelInfo* chinfo = [SFChannelInfo new];
					[chinfo setColor:color];
					[chinfo setName:[@"Channel " stringByAppendingString:[[NSNumber numberWithInteger:channelIndex+1] stringValue] ]];
					SFVoxelData* newvoxeldata = [[SFVoxelData alloc]initWithData:alignedImageData
																	   alignment:targetalignment];
					[ret addChannel:newvoxeldata withChannelInfo:chinfo];
				}
			}	
			break;
		}
//		case kInterleavedLuminaceAlpha:
//		case kInterleavedRGBA:
			break;
			
	}
	
	if (sfStopLoadingFlag == YES)
		ret = nil;
    [progressIndicator setHidden:YES];
	return ret;
};


- (void)loadSelectedDataThreaded
{
    /*
     [self performSelectorOnMainThread:@selector(close) 
     withObject:nil 
     waitUntilDone:NO ];	
     */
};

-(void)loadDataForSelection:(NSArray*)selectedObjects {
    id firstNode = selectedObjects[0];
    if([firstNode isKindOfClass:[NSDictionary class]]) {
        NSNumber *nodeIndexNumber = [firstNode objectForKey:SFKNodeDataIndexKey];
        if(nodeIndexNumber) {
            self.nodeIndex = nodeIndexNumber.integerValue;
        }
    }
}

- (id)volumeScene {
	return nil;
};

- (IBAction)saveShapshot:(id)sender {
    NSHomeDirectory();
	NSSavePanel* panel = [NSSavePanel savePanel];
        //[panel setRequiredFileType:@"tiff"];
    [panel setAllowedFileTypes:[NSArray arrayWithObject:@"tiff"]];
	[panel setAllowsOtherFileTypes:NO];
    
    [panel beginSheetModalForWindow:[self window]
                  completionHandler:^(NSInteger returnCode){
                      if (returnCode == NSOKButton){
                          NSImage* image = [_largeBitplaneView snapShot];
                          NSData* tiffdata = [image TIFFRepresentation];
                          [tiffdata writeToFile:[[panel URL] path] atomically:NO];
                      }
                  }];
}


#pragma mark NSOutlineViewDelegate methods

// ----------------------------------------------------------------------------------------
// outlineView:isGroupItem:item
// ----------------------------------------------------------------------------------------
-(BOOL)outlineView:(NSOutlineView*)outlineView isGroupItem:(id)item
{
//    NSDictionary *node = [item representedObject];
    if(outlineView==fileNodeOutlineView) {
        NSDictionary *dictionary = [item representedObject];
//        NSString *title = dictionary[SFKNodeTitleKey];
        NSImage *icon = dictionary[SFKNodeIconKey];
        if(icon==nil)
            return YES;
    }
    return  NO;
}


// -------------------------------------------------------------------------------
//	outlineView:willDisplayCell
// -------------------------------------------------------------------------------

- (void)outlineView:(NSOutlineView *)outlineView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
	if ([[tableColumn identifier] isEqualToString:OutlineColumnName]) {
		// we are displaying the single and only column
		if ([cell isKindOfClass:[SFImageAndTextCell class]]) {
			id outlineItem = [item representedObject];
            NSDictionary *dictionary = outlineItem;
            [(SFImageAndTextCell*)cell setImage:dictionary[SFKNodeIconKey]];
            
		}
	}
}

// -------------------------------------------------------------------------------
//	outlineViewSelectionDidChange:notification
// -------------------------------------------------------------------------------
- (void)outlineViewSelectionDidChange:(NSNotification *)notification
{
	// ask the tree controller for the current selection
	NSArray *selection = [treeControllerForDictionary selectedObjects];
	if (selection && [selection count] > 0)
	{
        NSArray *selection = [treeControllerForDictionary selectedObjects];
        id firstNode = selection[0];
        if([firstNode isKindOfClass:[NSDictionary class]]) {
            NSDictionary *nodeDictionary = (NSDictionary*)firstNode;
            self.isVolumeSelected = nodeDictionary[@"isVolume"];
        }
        [self loadDataForSelection:selection];
	}
}


// -------------------------------------------------------------------------------
//	shouldEditTableColumn:tableColumn:item
//
//	Decide to allow the edit of the given outline view "item".
// -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	return NO;
}

// -------------------------------------------------------------------------------
//	shouldSelectItem:item
// -------------------------------------------------------------------------------
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item;
{
    if(outlineView==fileNodeOutlineView) {

            id dictionary = [item representedObject];
            if (dictionary[SFKNodeDataIndexKey]!=nil)
                return YES;
            
        
    }
	return NO;
}


#pragma mark - Split View Delegate

// -------------------------------------------------------------------------------
//	splitView:constrainMinCoordinate:
//
//	What you really have to do to set the minimum size of both subviews to kMinOutlineViewSplit points.
// -------------------------------------------------------------------------------
- (CGFloat)splitView:(NSSplitView *)splitView constrainMinCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(NSInteger)index
{
	return proposedCoordinate + SFKMinSourceViewSplitSize;
}

// -------------------------------------------------------------------------------
//	splitView:constrainMaxCoordinate:
// -------------------------------------------------------------------------------
- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(NSInteger)index
{
	return proposedCoordinate - SFKMinSourceViewSplitSize;
}

// -------------------------------------------------------------------------------
//	splitView:resizeSubviewsWithOldSize:
//
//	Keep the left split pane from resizing as the user moves the divider line.
// -------------------------------------------------------------------------------
- (void)splitView:(NSSplitView*)sender resizeSubviewsWithOldSize:(NSSize)oldSize
{
	NSRect newFrame = [sender frame]; // get the new size of the whole splitView
	NSScrollView *left = [[sender subviews] objectAtIndex:0];
	NSRect leftFrame = [left frame];
	NSView *right = [[sender subviews] objectAtIndex:1];
	NSRect rightFrame = [right frame];
    
	CGFloat dividerThickness = [sender dividerThickness];
    
	leftFrame.size.height = newFrame.size.height;
    
	rightFrame.size.width = newFrame.size.width - leftFrame.size.width - dividerThickness;
	rightFrame.size.height = newFrame.size.height;
	rightFrame.origin.x = leftFrame.size.width + dividerThickness;
    
	[left setFrame:leftFrame];
	[right setFrame:rightFrame];
}




@end
