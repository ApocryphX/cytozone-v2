//
//  SFOMEReader.m
//  SanFrancisco
//
//  Created by ApocryphX on 10/20/13.
//
//

#import "SFOMEReader.h"
#import "SFTIFFReader.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"

static NSDictionary *SFKDataTypeMap;

NSString *SFKOMEIFDIndexNumberName = @"IFDIndex";
NSString *SFKTIFFReaderName = @"TIFFReader";
NSString *SFKOMEChannelColorName = @"channel_color";


@implementation SFOMEReader

//int8, int16, int32, uint8, uint16, uint32, float, bit, double, complex, double-complex

+ (void)initialize {
    SFKDataTypeMap = @{ @"int8" : @1, @"int16" : @2 ,@"int32" : @4,  @"uint8" : @1, @"uint16" : @2 ,@"uint32" : @4 };
}

- (id)initWithPath:(NSString*)path {
    self = [super init];
	if(self) {
        filePath = path;
        sfTIFFReader = [[SFTIFFReader alloc]initWithFileName:filePath];
        if(sfTIFFReader == nil)
            return nil;

        NSString *omeXMLString = [sfTIFFReader descriptionAtIndex:0];
        NSError *error;
		sfXMLDocument = [[NSXMLDocument alloc]initWithXMLString:omeXMLString options:NSXMLNodeOptionsNone error:&error];
        if(sfXMLDocument == nil)
            return nil;
        //[[sfXMLDocument XMLData] writeToFile:[@"~/OME-XML-data.xml" stringByExpandingTildeInPath] atomically:YES];
        
        NSError *xmlParserError;
        //check if this is the main file: binaryOnlyArray does nto exist, otherwiase open main TIF
        NSArray *binaryOnlyArray = [sfXMLDocument nodesForXPath:@"/OME/BinaryOnly" error:&xmlParserError];
        if(binaryOnlyArray.count == 1) {
            NSXMLElement *binaryDataNode = binaryOnlyArray[0];
            NSXMLNode *metadataFileNode = [binaryDataNode  attributeForName:@"MetadataFile"];
            NSString *rootFileName = metadataFileNode.stringValue;
            NSString *path = [filePath stringByDeletingLastPathComponent];
            NSString *newFilePath = [path stringByAppendingPathComponent:rootFileName];
            filePath = newFilePath;
            sfTIFFReader = [[SFTIFFReader alloc]initWithFileName:filePath];
            if(sfTIFFReader == nil)
                return nil;
            NSString *omeXMLString = [sfTIFFReader descriptionAtIndex:0];
            NSError *error;
            sfXMLDocument = [[NSXMLDocument alloc]initWithXMLString:omeXMLString options:NSXMLNodeOptionsNone error:&error];
            if(sfXMLDocument == nil)
                return nil;
        }
        

        //NSXMLElement *rootNode = sfXMLDocument.rootElement;
        NSArray *pixelArray = [sfXMLDocument nodesForXPath:@"/OME/Image/Pixels" error:nil];
        NSXMLElement *pixelElement = pixelArray[0];
        NSArray *attributeArray = [pixelElement attributes];
        for(NSXMLElement *element in attributeArray) {
            NSLog(@"%s = %s", [element.name cStringUsingEncoding:NSASCIIStringEncoding],
                  [element.stringValue  cStringUsingEncoding:NSASCIIStringEncoding] );

        }
        
        // number of channels
        NSXMLNode *sizeCNode = [pixelElement attributeForName:@"SizeC"];
        channelCount = [sizeCNode.stringValue integerValue];
        
        dimensions = [SFNDDimension new];
 
        NSXMLNode *sizeNode, *physicalSizeNode;

        
        
        NSUInteger extent[]={1,1,1,1};
        sizeNode = [pixelElement attributeForName:@"SizeX"];
        if(sizeNode) {
            extent[0] = [sizeNode.stringValue integerValue];
            NSNumber *magnitudeNumber = nil;
            physicalSizeNode = [pixelElement attributeForName:@"PhysicalSizeX"];
            if(physicalSizeNode) {
                double magnitude = [physicalSizeNode.stringValue doubleValue]*extent[0];
                magnitudeNumber = [NSNumber numberWithDouble:magnitude];
            }
            [dimensions addDimensionWithExtent:extent[0]
                                     magnitude:magnitudeNumber
                                 dimensionType:SFKDimensionTypeSpaceName
                                        symbol:@"x"
                                          unit:@"m"];
        }
        
        sizeNode = [pixelElement attributeForName:@"SizeY"];
        
        if(sizeNode) {
            extent[1] = [sizeNode.stringValue integerValue];
            NSNumber *magnitudeNumber = nil;
            physicalSizeNode = [pixelElement attributeForName:@"PhysicalSizeY"];
            if(physicalSizeNode) {
                double magnitude = [physicalSizeNode.stringValue doubleValue]*extent[1];
                magnitudeNumber = [NSNumber numberWithDouble:magnitude];
            }
            
            [dimensions addDimensionWithExtent:extent[1]
                                     magnitude:magnitudeNumber
                                 dimensionType:SFKDimensionTypeSpaceName
                                        symbol:@"y"
                                          unit:@"m"];
        }
        
        sizeNode = [pixelElement attributeForName:@"SizeZ"];

        if(sizeNode != 0  ) {
            extent[2] = [sizeNode.stringValue integerValue];
            if(extent[2]>1) {
                NSNumber *magnitudeNumber = nil;
                physicalSizeNode = [pixelElement attributeForName:@"PhysicalSizeZ"];
                if(physicalSizeNode) {
                    double magnitude = [physicalSizeNode.stringValue doubleValue]*extent[2];
                    magnitudeNumber = [NSNumber numberWithDouble:magnitude];
                }
                [dimensions addDimensionWithExtent:extent[2]
                                         magnitude:magnitudeNumber
                                     dimensionType:SFKDimensionTypeSpaceName
                                            symbol:@"y"
                                              unit:@"m"];

            }
        }

        sizeNode = [pixelElement attributeForName:@"SizeT"];
        
        if(sizeNode != 0  ) {
            extent[3] = [sizeNode.stringValue integerValue];
            if(extent[3]>1) {
                NSNumber *magnitudeNumber = nil;
                physicalSizeNode = [pixelElement attributeForName:@"TimeIncrement"];
                if(physicalSizeNode) {
                    double magnitude = [physicalSizeNode.stringValue doubleValue]*extent[3];
                    magnitudeNumber = [NSNumber numberWithDouble:magnitude];
                }
                
                [dimensions addDimensionWithExtent:extent[3]
                                         magnitude:magnitudeNumber
                                     dimensionType:SFKDimensionTypeTimeName
                                            symbol:@"t"
                                              unit:@"s"];
            }
        }
        
        tiffReaderArray = [NSMutableArray new];
        fileNameDictionary = [NSMutableDictionary new];

        NSArray *channelArray = [sfXMLDocument nodesForXPath:@"/OME/Image/Pixels/Channel" error:nil];
        NSLog(@"channel count:%ld", channelArray.count);
        
        channelDictionaryArray = [NSMutableArray new];
        NSUInteger channelIndex;
        for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
            NSMutableDictionary *channelDictionary = [NSMutableDictionary new];
            [channelDictionaryArray addObject:channelDictionary];
            NSXMLElement *channelElement = channelArray[channelIndex];
            if(channelElement) {
                union  {
                    uint32_t colorValue;
                    uint8_t component[4];
                } color;
                NSXMLNode *colorNode = [channelElement attributeForName:@"Color"];
                if(colorNode) {
                    color.colorValue = [colorNode.stringValue intValue];
                    NSColor *channelRGBColor = [NSColor colorWithDeviceRed:color.component[3]
                                                                     green:color.component[2]
                                                                      blue:color.component[1]
                                                                     alpha:color.component[0]];
                    channelDictionary[SFKOMEChannelColorName] = channelRGBColor;
                }
            }
        }

        NSUInteger imageCount = extent[2]*extent[3]*channelCount;
        
        NSArray *tiffDataArray = [sfXMLDocument nodesForXPath:@"/OME/Image/Pixels/TiffData" error:nil];
        NSLog(@"image count:%ld", tiffDataArray.count);
        // in OME TIFF 2013 schema data location is contained in /OME/Image/Pixels/TiffData
        // but not in 2014 schema used by Zeiss: check if this is in 2013 or 2014 format
        // 2013 uses the TIFF data array for meta information
        if (imageCount == tiffDataArray.count) {
            for(NSXMLElement *element in tiffDataArray) {
                NSUInteger channel, timePoint, zPlane;
                
                NSXMLNode *channelNode = [element attributeForName:@"FirstC"];
                if(channelNode)
                    channel = channelNode.stringValue.integerValue;
                else
                    channel= 1;
                // determine time point if any
                NSXMLNode *timeNode = [element attributeForName:@"FirstT"];
                if(timeNode)
                    timePoint = timeNode.stringValue.integerValue;
                else
                    timePoint= 1;
                
                // determine z plane if any
                NSXMLNode *zPlaneNode = [element attributeForName:@"FirstZ"];
                if(zPlaneNode)
                    zPlane = zPlaneNode.stringValue.integerValue;
                else
                    zPlane= 1;
                NSXMLNode *IDFNode = [element attributeForName:@"IFD"];
                if(IDFNode) {
                    NSUInteger ifdIndex = IDFNode.stringValue.integerValue;
                    NSMutableDictionary *planeDictionary = [NSMutableDictionary new];
                    
                    planeDictionary[SFKOMEIFDIndexNumberName] = @(ifdIndex);
                    //NSMutableDictionary *dictionary = channelDictionaryArray[channel];
                    
                    NSArray *UUIDArray = [element elementsForName:@"UUID"];
                    NSString *fileName = [UUIDArray[0] attributeForName:@"FileName"].stringValue;
                    SFTIFFReader *tiffReader = fileNameDictionary[fileName];
                    if(tiffReader == Nil) {
                        NSString *path = [filePath stringByDeletingLastPathComponent];
                        NSString *newFilePath = [path stringByAppendingPathComponent:fileName];
                        tiffReader = [[SFTIFFReader alloc ]initWithFileName:newFilePath];
                        if(tiffReader) {
                            fileNameDictionary[fileName]=tiffReader;
                        }
                    }
                    if(tiffReader)
                        planeDictionary[SFKTIFFReaderName] = tiffReader;
                    NSMutableDictionary *channelDictionary = channelDictionaryArray[channel];
                    channelDictionary[@(zPlane+timePoint*extent[2])] = planeDictionary;
                }
            }
        }
        else {
            NSArray *planeArray = [sfXMLDocument nodesForXPath:@"/OME/Image/Pixels/Plane" error:nil];
            NSLog(@"image count:%ld", planeArray.count);
            if (imageCount == planeArray.count) {
                
                //NSUInteger bytes = [sfTIFFReader bitsPerSampleAtIndex:0].unsignedIntegerValue/8;
                for(NSXMLElement *element in planeArray) {
                    NSUInteger channel, timePoint, zPlane;
                    
                    NSXMLNode *channelNode = [element attributeForName:@"TheC"];
                    if(channelNode)
                        channel = channelNode.stringValue.integerValue;
                    else
                        channel= 1;
                    // determine time point if any
                    NSXMLNode *timeNode = [element attributeForName:@"TheT"];
                    if(timeNode)
                        timePoint = timeNode.stringValue.integerValue;
                    else
                        timePoint= 1;
                    
                    // determine z plane if any
                    NSXMLNode *zPlaneNode = [element attributeForName:@"TheZ"];
                    if(zPlaneNode)
                        zPlane = zPlaneNode.stringValue.integerValue;
                    else
                        zPlane= 1;
                    NSMutableDictionary *planeDictionary = [NSMutableDictionary new];
                    
                    NSUInteger ifdIndex = zPlane+timePoint*extent[2]+channel*extent[2]*extent[3];
                    planeDictionary[SFKOMEIFDIndexNumberName] = @(ifdIndex);
                    //NSMutableDictionary *dictionary = channelDictionaryArray[channel];
                    
                    planeDictionary[SFKTIFFReaderName] = sfTIFFReader;
                    NSMutableDictionary *channelDictionary = channelDictionaryArray[channel];
                    channelDictionary[@(zPlane+timePoint*extent[2])] = planeDictionary;
                    NSLog(@"z:%lu channel:%lu IFD index:%lu",(unsigned long)zPlane,(unsigned long)channel,(unsigned long)ifdIndex);

                }
            }
        }
    }
    return self;
};

+ (NSArray*)fileExtensions {
    return [NSArray arrayWithObjects:@"tif",@"tiff", nil];
};

+ (BOOL)isConfocalFile:(NSString*)filePath {
    BOOL isConfocalFile = NO;
    SFTIFFReader *tiffReader = [[SFTIFFReader alloc]initWithFileName:filePath];
    NSString *omeXMLString = [tiffReader descriptionAtIndex:0];
    if(omeXMLString) {
        NSError *error;
        NSXMLDocument *xmlDocument = [[NSXMLDocument alloc]initWithXMLString:omeXMLString options:NSXMLNodeOptionsNone error:&error];
        if(error == nil && xmlDocument) {
            NSArray *omeArray = [xmlDocument nodesForXPath:@"/OME" error:nil];
            if(omeArray) {
                isConfocalFile = YES;
            }
        }
    }
    return isConfocalFile;
}

- (NSUInteger)count {
    return 1;
};

-(NSString*)fileName {
    return filePath;
};

- (NSString*)name {
	return @"OME TIFF File";
};


- (NSArray*)fileNodes {
    NSMutableDictionary *dataSetDictionary = [NSMutableDictionary new];
    dataSetDictionary[SFKNodeDataIndexKey]=[NSNumber numberWithUnsignedInteger:0];
    dataSetDictionary[SFKNodeTitleKey]=[[filePath lastPathComponent]stringByDeletingPathExtension];
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    dictionary[SFKNodeTitleKey]=[filePath lastPathComponent];
    dictionary[SFKNodeChildrenKey] = [NSArray arrayWithObject:dataSetDictionary];
    return [NSArray arrayWithObject:dictionary];
}


// <Channel Color="16711935" ID="Channel:46:1" Name="Leica/ALEXA 488" PinholeSize="102.707773596423" SamplesPerPixel="1">
// <Pixels DimensionOrder="XYCZT" ID="Pixels:46" PhysicalSizeX="0.48053062678062675" PhysicalSizeY="0.4805306813372831" PhysicalSizeZ="0.50354051927616" SizeC="3" SizeT="1" SizeX="4212" SizeY="2363" SizeZ="81" TimeIncrement="0.0" Type="uint8">


- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex {
    if(nodeIndex==0)
        return channelCount;
    else
        return 0;
};

- (SFNDDimension*)nDDimensionAtNodeIndex: (NSUInteger)nodeIndex {
    return dimensions;
};

- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)idx channelIndex:(NSUInteger)channelIndex
{
    SFVoxelInfo* voxelInfo = nil;
    
    //NSArray *channelArray = [sfXMLDocument nodesForXPath:@"/OME/Image/Pixels/Channel" error:nil];
    //NSLog(@"channel count:%ld", channelArray.count);
    NSArray *pixelArray = [sfXMLDocument nodesForXPath:@"/OME/Image/Pixels" error:nil];
    //NSLog(@"image count:%ld", tiffDataArray.count);
    NSXMLElement *pixelElement = pixelArray[0];
    //NSArray *attributeArray = [pixelElement attributes];
    
    // patch for incorrect data type info
    NSUInteger bitsPerTiffInfo = [sfTIFFReader bitsPerSampleAtIndex:0].unsignedIntegerValue;
    
    NSXMLNode *dataTypeNode = [pixelElement attributeForName:@"Type"];
    if(dataTypeNode) {
        NSString *dataTypeString = dataTypeNode.stringValue;
        NSNumber *bytesPerVoxelNumber = SFKDataTypeMap[dataTypeString];
        NSUInteger bytesPerVoxel = bytesPerVoxelNumber.integerValue;
        // patch for incorrect data type info
        if(bitsPerTiffInfo==8)
            bytesPerVoxel = 1;
        voxelInfo = [SFVoxelInfo new];
        [voxelInfo setBytesPerSample:bytesPerVoxel];
        [voxelInfo setSamplesPerVoxel:1];
        [voxelInfo setPrecision:bytesPerVoxel*8];
        [voxelInfo setDataType:kInteger];
        [voxelInfo setInterleaveType:kPlanar];
        [voxelInfo setPhotometricInterpretation:kLuminance];
    }
    return voxelInfo;
}

- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex {
    NSDictionary *channelDictionary = channelDictionaryArray[channelIndex];
    NSColor *channelRGBColor = channelDictionary[SFKOMEChannelColorName];
    if (channelRGBColor) {
        return channelRGBColor;
    }
    else {
        switch (channelIndex) {
            case 0:
                return [NSColor blueColor];
                break;
            case 1:
                return [NSColor greenColor];
                break;
            case 2:
                return [NSColor redColor];
                break;
            default:
                return [NSColor whiteColor];
                break;
        }
    }
    return nil;
};

- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex {

    NSMutableDictionary *channelDictionary = channelDictionaryArray[channelIndex];
    NSMutableDictionary *planeDictionary = channelDictionary[@(planeIndex)];
    NSNumber *ifdIndexNumber = planeDictionary[SFKOMEIFDIndexNumberName];
    SFTIFFReader *tiffReader =  planeDictionary[SFKTIFFReaderName];
    NSMutableData *imageData = [tiffReader dataAtIndex:[ifdIndexNumber unsignedIntegerValue]];
    return imageData;
};




@end
