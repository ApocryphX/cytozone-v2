//
//  SFOGLRenderState.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLRenderState.h"

@implementation SFOGLRenderState

+ (id) defaultState {
	SFOGLRenderState* ret = [SFOGLRenderState new];
	ret->sfBackplaneMode = 0;
	ret->sfBackplaneColor = [NSColor darkGrayColor];
	ret->sfAlphaThreshold = 0.0f;
	ret->sfAlpha = 0.6f;
	ret->sfBrightness = 0.5f;
	ret->sfBlendColor = [[NSColor whiteColor]colorWithAlphaComponent:0.0f];
	ret->sfRenderMode = 0;
	ret->sfZScale = 1.0f;
	ret->sfSampling = 1;
	return ret;	
};

- (id) initWithVolumeSetting:(SFOGLRenderState*)settings {
	self = [super init];
	sfBackplaneMode = settings->sfBackplaneMode;
	sfBackplaneColor = settings->sfBackplaneColor;
	sfAlphaThreshold = settings->sfAlphaThreshold;
	sfAlpha = settings->sfAlpha;
	sfBrightness = settings->sfBrightness;
	sfBlendColor = settings->sfBlendColor;
	sfRenderMode = settings->sfRenderMode;
	sfZScale = settings->sfZScale;
	sfSampling = settings->sfSampling;
	sfVolumeIndex = settings->sfVolumeIndex;
	return self;
};

- (void) setBackplaneMode:(NSUInteger)value{
	sfBackplaneMode = value;
};
- (void) setBackplaneColor:(NSColor*)value{
	sfBackplaneColor = value;
};
- (void) setAlphaThreshold:(float)value{
	sfAlphaThreshold = value;
};
- (void) setAlpha:(float)value{
	sfAlpha = value;
};
- (void) setBrightness:(float)value {
	sfBrightness = value;
};
- (void) setBlendColor:(NSColor*)value {
	sfBlendColor = value;
};
- (void) setRenderMode:(NSUInteger)value; {
	sfRenderMode = value;
};
- (void) setZScale:(float)value {
	sfZScale = value;
};
- (void) setSampling:(NSUInteger)value {
	sfSampling = value;
};

- (void) setIsZInverted:(BOOL)value {
	sfIsZInverted = value;
};

- (void) setVolumeIndex:(NSUInteger)value {
	sfVolumeIndex = value;
};


//------------------------------------------------

- (NSUInteger) backplaneMode {
	return sfBackplaneMode;
};
- (NSColor*) backplaneColor {
	return sfBackplaneColor;
};
- (float) alphaThreshold {
	return sfAlphaThreshold;
};
- (float) alpha {
	return sfAlpha;
};
- (float) brightness {
	return sfBrightness;
};
- (NSColor*)blendColor {
	return sfBlendColor;
};
- (NSUInteger) renderMode {
	return sfRenderMode;
};
- (float) zScale {
	return sfZScale;
};
- (NSUInteger) sampling {
	return sfSampling;
};

- (BOOL) isZInverted {
	return sfIsZInverted;
};

- (NSUInteger)volumeIndex {
	return sfVolumeIndex;
};

- (SFOGLRenderState*)interpolatedStateWithFraction:(float)frac ofState:(SFOGLRenderState*)state {
	SFOGLRenderState* ret = [SFOGLRenderState new];
	float ifrac = 1.0f - frac;
	if (frac < 0.5) {
		ret->sfRenderMode = sfRenderMode;
		ret->sfBackplaneMode = sfBackplaneMode;
	}
	else {
		ret->sfRenderMode = state->sfRenderMode;
		ret->sfBackplaneMode = state->sfBackplaneMode;
	}
	ret->sfBlendColor = [sfBlendColor blendedColorWithFraction:frac ofColor:state->sfBlendColor];
	ret->sfBackplaneColor = [sfBackplaneColor blendedColorWithFraction:frac ofColor:state->sfBackplaneColor];
	
	ret->sfAlphaThreshold = ifrac * sfAlphaThreshold + frac * state->sfAlphaThreshold;
	ret->sfAlpha = ifrac * sfAlpha + frac * state->sfAlpha;
	ret->sfBrightness = ifrac * sfBrightness + frac * state->sfBrightness;
	ret->sfZScale = ifrac * sfZScale + frac * state->sfZScale;
	ret->sfSampling = (NSUInteger)(ifrac * (float)sfSampling + frac * (float)state->sfSampling);
	ret->sfVolumeIndex = (NSUInteger)(ifrac * (float)sfVolumeIndex + frac * (float)state->sfVolumeIndex);
	return ret;	
};

- (id) copyWithZone:(NSZone *)zone {
    SFOGLRenderState* copy = [ [ [self class] allocWithZone: zone] initWithVolumeSetting:self];
    return copy;
};

- (id) copy {
	return [self copy];
};

;


@end
