//
//  SFOGLVolume.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolume.h"

#import <OpenGL/gl.h>

#import "SFVoxelData.h"
#import "SFNDVoxelData.h"
#import "SFOGLBackplane.h"
#import "SFOGLChannelSetting.h"
#import "SFOGLVolumeSetting.h"
#import "SFOGLColor.h"
#import "SFOGLRenderState.h"
#import "SFNDDimension.h"
#import "SFOGLVolumeTexture.h"
#import "SFOGLTexturing.h"
#import "SFOGLTransform.h"

@implementation SFOGLVolume

- (id) init {
	self = [super init];
	[self setAlpha:0.6f];
	[self setBackplaneMode:0];
	[self setBackplaneColor:[[NSColor darkGrayColor]colorUsingColorSpaceName:NSCalibratedRGBColorSpace]];
	[self setBlendColor:[[NSColor whiteColor]colorWithAlphaComponent:0.0f]];
	[self setBrightness:1.0f];
	[self setZScale:1.0f];	
	[self setSampling:0];
	[self setTextureTransform:[SFOGLTransform new]];
	return self;
}

- (void)setOpenGLView:(NSOpenGLView*)value {
	if (value != sfOGLView) {
		sfOGLView = value;
		NSUInteger i, count = [sfVolumeTextures count];
		for (i=0; i<count; i++) {
			SFOGLVolumeTexture <SFOGLTexturing3D>* tex3D = [sfVolumeTextures objectAtIndex:i];
			[tex3D setOpenGLContext:[value openGLContext]];
		}		
		[sfBackplane setContext:[sfOGLView openGLContext]]; 
		[sfOGLView setNeedsDisplay:YES];
	}
};

- (void) setTextureTransform:(SFOGLTransform*)value {
	sfTransform = value;
	[sfOGLView setNeedsDisplay:YES];
};

- (void) setRenderMode:(NSUInteger)value {
	sfRenderMode = value;
	NSUInteger i;
	for (i=0; i<[sfVolumeTextures count];i++){
		SFOGLVolumeTexture <SFOGLTexturing3D>* tex3D = [sfVolumeTextures objectAtIndex:i];
		[tex3D setRenderMode:value]; 
	}
	[sfOGLView  setNeedsDisplay:YES];
};

- (void) setNDVoxelData:(SFNDVoxelData*)nddata {

	sfNDVoxelData = nddata;	

	sfVolumeTextures = [NSMutableArray new];
	sfShadowTextures = [NSMutableArray new];		

	SFOGLBackplane* backplane = [SFOGLBackplane new];
	[self setBackplane:backplane];
	
};

- (void) setBackplaneNDDimension:(SFNDDimension*)nddim {
	NSMutableArray* vinfos = [NSMutableArray new];
	NSUInteger i, count = [[sfNDVoxelData channels]count];
	for (i=0;i<count;i++) {
		SFVoxelInfo* vinfo = [[sfNDVoxelData voxelDataAtIndex:i]voxelInfo];
		[vinfos addObject:vinfo];
	}
	[sfBackplane setNDDimension:nddim voxelInfos:vinfos];
};


- (void) setChannelSettings:(NSArray*)value {
	sfChannelSettings = value;
	NSUInteger i, count = [sfChannelSettings count];
	for(i=0; i<count; i++) {
		[[sfChannelSettings objectAtIndex:i] addObserver:self forKeyPath:@"threshold" options:NSKeyValueObservingOptionNew context:NULL];
		[[sfChannelSettings objectAtIndex:i] addObserver:self forKeyPath:@"volumeMode" options:NSKeyValueObservingOptionNew context:NULL];
		[[sfChannelSettings objectAtIndex:i] addObserver:self forKeyPath:@"noiseFilterSize" options:NSKeyValueObservingOptionNew context:NULL];
		[[sfChannelSettings objectAtIndex:i] addObserver:self forKeyPath:@"isIntensitySquare" options:NSKeyValueObservingOptionNew context:NULL];
	}
};

- (void) setBackplane:(SFOGLBackplane*)value {
	if (value != sfBackplane) {
		sfBackplane = value;
		[[sfOGLView openGLContext]makeCurrentContext];
		[sfBackplane setContext:[sfOGLView openGLContext]];
		[sfBackplane setChannelSettings:sfChannelSettings];
		[sfBackplane setMode:sfBackplaneMode];
		[sfBackplane setColor:sfBackplaneColor];
		[sfBackplane setAlpha:sfAlpha];
		
	}
};

- (SFNDVoxelData*) nDVoxelData {
	return sfNDVoxelData;
};

- (void)setSampling:(NSUInteger)value{
    [[sfOGLView openGLContext]makeCurrentContext];
	sfSampling = value;
	[sfOGLView setNeedsDisplay:YES];

};

- (void) setBlendColor:(NSColor*)value{
    [[sfOGLView openGLContext]makeCurrentContext];
	sfBlendColor = value;
	[sfOGLView setNeedsDisplay:YES];
};

- (void) setBrightness:(float)value {
    [[sfOGLView openGLContext]makeCurrentContext];
	sfBrightness = value;
	[sfOGLView setNeedsDisplay:YES];
};

- (void) setBackplaneColor:(NSColor*)value {
    [[sfOGLView openGLContext]makeCurrentContext];
	sfBackplaneColor = value;
	if (sfBackplane) {
		[sfBackplane setColor:value];
		[sfOGLView setNeedsDisplay:YES];		
	}
};

- (void) setAlpha:(float)value {
    [[sfOGLView openGLContext]makeCurrentContext];
	sfAlpha = value;
	[sfBackplane setAlpha:value];
	[sfOGLView setNeedsDisplay:YES];
};

- (void) setIsZInverted:(BOOL)value {
	if(value != sfIsZInverted) {
        [[sfOGLView openGLContext]makeCurrentContext];
		sfIsZInverted = value;
		[self processAllChannels];	
		[sfOGLView setNeedsDisplay:YES];		
	}
};

- (void) setBackplaneMode:(NSUInteger)value {
	sfBackplaneMode = value;
	if (sfBackplane) {
        [[sfOGLView openGLContext]makeCurrentContext];
		[sfBackplane setMode:value];
		[sfOGLView setNeedsDisplay:YES];
	}
};

- (void) setZScale:(float)value {
    [[sfOGLView openGLContext]makeCurrentContext];
	sfZScale = value;
	[sfOGLView setNeedsDisplay:YES];
};

- (void) setTransform:(SFOGLTransform*)value {
    [[sfOGLView openGLContext]makeCurrentContext];
	[sfTransform setTransform:value];
	[sfOGLView setNeedsDisplay:YES];
};

- (NSUInteger) sampling {
	return sfSampling;
};

- (void) setVolumeIndex:(NSUInteger)value {
	if (value != sfVolumeIndex) {
		sfVolumeIndex = value;
        [[sfOGLView openGLContext]makeCurrentContext];
		[self processAllChannels];
            //[sfOGLView setNeedsDisplay:YES];
	}
};

- (NSOpenGLView*) openGLView {
	return sfOGLView;
};

- (SFOGLBackplane*)backdrop {
	return sfBackplane;
};

- (NSUInteger) renderMode {
	return sfRenderMode;
};

- (NSColor*) blendColor {
	return sfBlendColor;
};

- (NSColor*) backplaneColor {
	return sfBackplaneColor;
};

- (NSUInteger) backplaneMode {
	return sfBackplaneMode;
};

- (float) alpha {
	return sfAlpha;
};

- (float) zScale {
	return sfZScale;
};

- (BOOL) isZInverted {
	return sfIsZInverted;
};

- (NSUInteger) volumeIndex {
	return sfVolumeIndex;
};


- (SFSize) adjustedTextureSize:(SFSize)value {
	return value;
};

- (SFOGLTransform*)transform {
	return sfTransform;
};

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	NSUInteger i, count = [sfChannelSettings count];
	for (i=0; i < count; i++) {
		if( [sfChannelSettings objectAtIndex:i] == object ) {
			BOOL reprocess = YES;
			if ( [keyPath compare:@"threshold"] == NSOrderedSame && [object volumeMode] == 0 )
				reprocess = NO;
//			if ( [keyPath compare:@"isIntensitySquare"] == NSOrderedSame && [object volumeMode] != 0 )
//				reprocess = NO;

			if( reprocess )
				[self processChannel:i];
			[sfOGLView setNeedsDisplay:YES];
		}
	}
};

- (void)programBlendMode {
    
    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	switch ([self renderMode]) {
		case 0: {	
            // enable blending
			glEnable(GL_BLEND);
			//enable maxiumum intensity
			glBlendEquation(GL_MAX);	
			
			//disable alpha testing (=threshold)
			glDisable(GL_ALPHA_TEST);
            
			glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_MODULATE );
			break;

		}
		case 1: {
            
            // enable frame buffer alpha blending
			glEnable(GL_BLEND);
			glBlendEquation(GL_FUNC_ADD);
			glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);			
			
			//enable alpha testing (=threshold)
			glEnable(GL_ALPHA_TEST);
            
			// enable multi-texturing
			glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_COMBINE );
            
			// --- RGB combiner
			glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE); 
			
			glTexEnvf(GL_TEXTURE_ENV, GL_SRC0_RGB, GL_PRIMARY_COLOR);
			glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR);
			
			glTexEnvf(GL_TEXTURE_ENV, GL_SRC1_RGB, GL_PRIMARY_COLOR); 
			glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR); 			
			
			/// alpha combiner
            // possible args: GL_REPLACE, GL_MODULATE, GL_ADD, GL_ADD_SIGNED, GL_INTERPOLATE, GL_SUBTRACT
			glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_ALPHA, GL_MODULATE); 
			
			glTexEnvf(GL_TEXTURE_ENV, GL_SRC0_ALPHA, GL_TEXTURE); 
			glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_ALPHA, GL_SRC_ALPHA);
			
			glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_ALPHA, GL_PRIMARY_COLOR); 
			glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_ALPHA, GL_SRC_ALPHA); 			
            
            
			break;

		}
		case 2: {
            // enable blending
            glEnable(GL_BLEND);
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
            
            //enable alpha testing (=threshold)
			glEnable(GL_ALPHA_TEST);
			glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_MODULATE );
			break;
		}
	}
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
}

- (BOOL) draw {
	NSUInteger i;
    [[sfOGLView openGLContext]makeCurrentContext];
	sfColors = [NSMutableArray new];
	for(i=0;i<[sfChannelSettings count];i++) {
		NSColor* color = [[sfChannelSettings objectAtIndex:i]colorOfVolume];
		switch(sfRenderMode) {
			case 0: {
                NSColor* rgbcolor = [color colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
				//NSColor* blendcolor = [color blendedColorWithFraction:[sfBlendColor alphaComponent] ofColor:sfBlendColor];
				//blendcolor = [[NSColor blackColor] blendedColorWithFraction:sfAlpha ofColor:blendcolor ];
				SFOGLColor* oglcolor = [SFOGLColor colorWithColor:rgbcolor];
				[sfColors addObject:oglcolor];
				break;					

			}
			case 1: {
                
                SFOGLColor* oglcolor = [SFOGLColor colorWithColor:[color colorWithAlphaComponent:sfAlpha*sfAlpha*[color alphaComponent]]];
				[sfColors addObject:oglcolor];
				break;	

			}
			case 2: {
				//NSColor* rgbcolor = [color colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
				NSColor* blendcolor = [color blendedColorWithFraction:[sfBlendColor alphaComponent] ofColor:sfBlendColor];
				blendcolor = [[NSColor blackColor] blendedColorWithFraction:sfAlpha ofColor:blendcolor ];
				SFOGLColor* oglcolor = [SFOGLColor colorWithColor:blendcolor];
				[sfColors addObject:oglcolor];
				break;	
			}
			default: {
				SFOGLColor* oglcolor = [SFOGLColor colorWithColor:color];
				[sfColors addObject:oglcolor];
				break;					
			}
		}
	}
	return YES;
};


- (SFOGLRenderState*) renderState {
	SFOGLRenderState* ret = [SFOGLRenderState new];
	[ret setBackplaneMode:sfBackplaneMode];
	[ret setBackplaneColor:sfBackplaneColor];
	[ret setAlpha:sfAlpha];
	[ret setBrightness:sfBrightness];
	[ret setBlendColor:sfBlendColor];
	[ret setRenderMode:sfRenderMode];
	[ret setZScale:sfZScale];
	[ret setSampling:sfSampling];
	[ret setIsZInverted:sfIsZInverted];
	[ret setVolumeIndex:sfVolumeIndex];
	return ret;
};

- (void) setRenderState:(SFOGLRenderState*)state {
	[self setVolumeIndex:[state volumeIndex]];
	[self setBackplaneMode:[state backplaneMode]];
	[self setBackplaneColor:[state backplaneColor]];
	[self setAlpha:[state alpha]];
	[self setBrightness:[state brightness]];
	[self setBlendColor:[state blendColor]];
	[self setRenderMode:[state renderMode]];
	[self setZScale:[state zScale]];
	[self setSampling:[state sampling]];
	[self setIsZInverted:[state isZInverted]];
}
	
- (void) dealloc {
	NSUInteger i, count = [sfChannelSettings count];
	for(i=0; i<count; i++) {
		[[sfChannelSettings objectAtIndex:i] removeObserver:self forKeyPath:@"threshold"];
		[[sfChannelSettings objectAtIndex:i] removeObserver:self forKeyPath:@"volumeMode"];
		[[sfChannelSettings objectAtIndex:i] removeObserver:self forKeyPath:@"noiseFilterSize"];
		[[sfChannelSettings objectAtIndex:i] removeObserver:self forKeyPath:@"isIntensitySquare"];
	}
};


@end
