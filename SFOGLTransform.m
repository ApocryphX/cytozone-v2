//
//  SFOGLTransform.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLTransform.h"
#import "SFOGLRotation.h"
#import "SFTrackball.h"

#include <OpenGL/gl.h>

@implementation SFOGLTransform

/*
- (void)awakeFromNib {
	sfRotation = [SFOGLRotation new];
	sfTrackball = [SFTrackball new];
	sfTrackRotation = [SFOGLRotation new];	
	sfTranslation.x = 0.0f;
	sfTranslation.y = 0.0f;
	sfZoom = 1.0f;
	[self setTrackmode:kRotation];
};
*/
- (id)init {
	self = [super init];
	if (self) {
		sfRotation = [SFOGLRotation new];
		sfTrackball = [SFTrackball new];
		sfTrackRotation = [SFOGLRotation new];	
		sfTranslation.x = 0.0f;
		sfTranslation.y = 0.0f;
		sfZoom = 1.0f;
		[self setTrackmode:kRotation];
	}
	return self;
}

- (void)setTrackmode:(enum SFTrackMode)mode {
	sfTrackMode = mode;
};

- (enum SFTrackMode)trackMode {
	return sfTrackMode;
};

- (void)startTracking:(NSPoint)point inRectangle:(NSRect)rect
{
	sfSize.width = rect.size.width;
	sfSize.height = rect.size.height;
	
    switch(sfTrackMode) {
        case kRotation:{
			[sfTrackRotation reset];
			[sfTrackball start:point frame:rect ];
            break;
        }
        case kZoom: {
            sfStartPoint = point;
            sfOldZoom = sfZoom;
            break;
        }
        case kTranslation:{
            sfStartPoint = point;
			sfOldTranslation.x = sfTranslation.x;
			sfOldTranslation.y = sfTranslation.y;
            break;
        }
        default:{
            break;
        }
    }
};

- (void)draggedTo:(NSPoint)point
{
    switch(sfTrackMode) {
        case kRotation:{
            [sfTrackRotation setRotation:[sfTrackball rollTo:point ] ];			
            break;
        }
        case kZoom: {
            float new_zoom = sfOldZoom + (point.y - sfStartPoint.y)/100.0f*sfOldZoom;
            if (new_zoom < 0.01 )
                sfZoom = 0.01;
			else 
				sfZoom = new_zoom;
            break;
        }
        case kTranslation:{
			float size = (sfSize.width<sfSize.height)?sfSize.width:sfSize.height;
            sfTranslation.x = (point.x - sfStartPoint.x)/(size/1.4142f*sfZoom)+sfOldTranslation.x;
            sfTranslation.y = (point.y - sfStartPoint.y)/(size/1.4142f*sfZoom)+sfOldTranslation.y;
            break;
        }
        default:{
            break;
        }
    }
};

- (void)stopTracking {
    switch(sfTrackMode) {
        case kRotation:{
            [sfRotation add:sfTrackRotation ];
            [sfTrackRotation reset];
            break;
        }
        case kZoom: {
            break;
        }
        case kTranslation:{
            break;
        }
        default:{
            break;
        }
    }
};

- (void)useZoom {
    sfTrackMode = kZoom;
};

- (void)useRotation {
    sfTrackMode = kRotation;
};

- (void)useTranslation {
    sfTrackMode = kTranslation;
};

- (void)setZoom:(float)value {
	sfZoom = value;
};

- (void)setTranslation:(NSPoint)value {
	sfTranslation = value;
};

- (void)setRotation:(SFOGLRotation*)value {
	struct GLRotation rot = [value GLRotation];
	[sfRotation setRotation:rot];
};

- (void)setTransform:(SFOGLTransform*)value {
	[self setZoom:[value zoom]];
	[self setTranslation:[value translation]];
	[self setRotation:[value rotation]];
};

- (float)zoom {
	return sfZoom;
};

- (NSPoint)translation {
	return sfTranslation;
};

- (SFOGLRotation*)rotation {
	return sfRotation;
};

- (SFOGLTransform*)interpolatedTransformWithFraction:(float)frac ofTransform:(SFOGLTransform*)transform {
	SFOGLTransform* ret = [SFOGLTransform new];
	float ifrac = 1.0f - frac;
	[ret setRotation: [sfRotation interpolatedRotationWithFraction:frac ofRotation:transform->sfRotation] ];

	ret->sfTranslation.x = ifrac * sfTranslation.x + frac * transform->sfTranslation.x;
	ret->sfTranslation.y = ifrac * sfTranslation.y + frac * transform->sfTranslation.y;
	ret->sfZoom = ifrac * sfZoom + frac * transform->sfZoom;	
	return ret;
}

- (void)execute {
	// zoom
	glScalef(sfZoom*sqrtf(2.0f),sfZoom*sqrtf(2.0f),1.0f);
	// translate
	glTranslatef(sfTranslation.x,sfTranslation.y,0.0f);
	// rotate
	[sfTrackRotation execute];	
	[sfRotation execute];
};

- (void)executeRotation {
	[sfTrackRotation execute];	
	[sfRotation execute];
	
};

;

@end
