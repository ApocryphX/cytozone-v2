//
//  SFImageScrollView.h
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFImageScrollView : NSScrollView {
    IBOutlet   NSButton *movieDeleteFrameButton;
    IBOutlet   NSButton *restoreButton;
    IBOutlet   NSButton *movieRecordButton;
    IBOutlet   NSButton *movieDeleteButton;
    NSNumber            *duration;
    NSMutableArray      *animationKeys; 

}

@property (retain)  NSNumber         *duration;
@property (readonly) NSMutableArray *animationKeys;

- (void)updateUIState;

@end
