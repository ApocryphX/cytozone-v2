//
//  SFGraphicsViewController.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicsViewController.h"
#import "SFGraphicsView.h"

#import "SFGraphicNDDataImage.h"

@interface SFGraphicsViewController ()

@end

@implementation SFGraphicsViewController

@dynamic toolMode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (SFGraphicsView*)insertViewInParentView:(NSView*)parentView {
    if (parentView && self.view) {
        
        [parentView addSubview:self.view];
        
        NSRect newBounds;
        newBounds.origin.x = 0;
        newBounds.origin.y = 0;
        newBounds.size.width = [parentView frame].size.width;
        newBounds.size.height = [parentView frame].size.height;
        [self.view setFrame:[parentView frame]];
        
        // make sure our added subview is placed and resizes correctly
        [self.view setFrameOrigin:NSMakePoint(0,0)];
        [self.view setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
        
    }
    
    return (SFGraphicsView*) self.view;
}


- (IBAction)setModeSelection:(id)sender {
    SFGraphicsView *graphicView = (SFGraphicsView *)self.view;
    graphicView.mouseMode = KModeSelectionKey;
};

- (IBAction)setModeRectangle:(id)sender {
    SFGraphicsView *graphicView = (SFGraphicsView *)self.view;
    graphicView.mouseMode = KModeNewObjectKey;
    graphicView.objectType = KObjectRectangleClassKey;
};

- (IBAction)setModeOval:(id)sender {
    SFGraphicsView *graphicView = (SFGraphicsView *)self.view;
    graphicView.mouseMode = KModeNewObjectKey;
    graphicView.objectType = KObjectOvalClassKey;
    
};

- (IBAction)setModePolygon:(id)sender {
    SFGraphicsView *graphicView = (SFGraphicsView *)self.view;
    graphicView.mouseMode = KModeNewObjectKey;
    graphicView.objectType = KObjectPolygonClassKey;
};

@end
