//
//  SFFileNode.h
//  Cytozone
//
//  Created by Kolja Wawrowsky on 10/19/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFNDImporter;

@interface SFFileNode : NSObject

@property (retain) SFNDImporter *importer;
@property (retain) NSNumber *index;
@property (retain) NSMutableArray *children;

@property (retain) NSString *title;
@property (retain) NSImage *icon;

@property (assign) BOOL isVolume;
@property (assign) BOOL isTimeSeries;

@end
