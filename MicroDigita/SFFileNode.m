//
//  SFFileNode.m
//  Cytozone
//
//  Created by Kolja Wawrowsky on 10/19/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFFileNode.h"
#import "SFNDImporter.h"
#import "SFNDDimension.h"

static NSImage *SFKFileNodeTimeSeriesStackImage;
static NSImage *SFKFileNodeStackImage;
static NSImage *SFKFileNodeTimeSeriesImage;
static NSImage *SFKFileNodeImage;

@implementation SFFileNode

+(void)initialize {
    SFKFileNodeTimeSeriesStackImage = [NSImage imageNamed:@"time_stack_icon"];
    SFKFileNodeStackImage = [NSImage imageNamed:@"stack_icon"];
    SFKFileNodeTimeSeriesImage = [NSImage imageNamed:@"time_icon"];
    SFKFileNodeImage = [NSImage imageNamed:@"image_icon"];
}

+ (SFFileNode*)fileNodeWithNDImporter:(SFNDImporter*)importer dictionary:inputDictionary {
    SFFileNode *fileNode  = [SFFileNode new];
    fileNode.title = inputDictionary[SFKNodeTitleKey];
    fileNode.importer = importer;
    id dataIndex = inputDictionary[SFKNodeDataIndexKey];
    if(dataIndex && [dataIndex isKindOfClass:[NSNumber class]]) {
        NSNumber *dataIndexNumber = (NSNumber*)dataIndex;
        fileNode.index = dataIndexNumber;
        NSUInteger index = [dataIndexNumber unsignedIntegerValue];
        SFNDDimension *nddim = [importer nDDimensionAtNodeIndex:index];
        if(nddim.isVolume) {
            fileNode.isVolume=YES;
            if(nddim.isTimeSeries) {
                fileNode.isTimeSeries=YES;
                fileNode.icon=SFKFileNodeTimeSeriesStackImage;
            }
            else {
                fileNode.isTimeSeries=NO;
                fileNode.icon=SFKFileNodeStackImage;
            }
        }
        else {
            fileNode.isVolume=NO;
            if(nddim.isTimeSeries) {
                fileNode.isTimeSeries=YES;
                fileNode.icon=SFKFileNodeTimeSeriesImage;
            }
            else {
                fileNode.isTimeSeries=NO;
                fileNode.icon=SFKFileNodeImage;
            }
        }
    }
    else {
        id children = inputDictionary[SFKNodeChildrenKey];
        if(children && [children isKindOfClass:[NSArray class]]) {
            fileNode.children=[NSMutableArray new];
            fileNode.icon=[NSImage imageNamed:NSImageNameFolder];
            for(NSDictionary *childNodeDictionary in children) {
                SFFileNode *childNode = [self fileNodeWithNDImporter:importer dictionary:childNodeDictionary];
                [fileNode.children addObject:childNode];
            }
        }
    }
    return fileNode;
};

+ (NSMutableArray*)fileNodeArrayWithNDImporter:(SFNDImporter*)importer {
    NSMutableArray *fileNodeArray = [NSMutableArray new];
    NSArray *outlineNodeArray = importer.outlineNodes;
    for(NSDictionary *outlineNodeDictionary in outlineNodeArray) {
        SFFileNode *fileNode  = [self fileNodeWithNDImporter:importer dictionary:outlineNodeDictionary];
        [fileNodeArray addObject:fileNode];
    }
    return fileNodeArray;
}

@end
