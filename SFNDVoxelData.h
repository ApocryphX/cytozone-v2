//
//  SFNDVoxelData.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFNDDimension,SFVoxelInfo,SFVoxelData,SFChannelInfo,SFDataInfo;

@interface SFNDVoxelData : NSObject <NSCoding> {
	SFNDVoxelData* sfNDParent;
    NSString* sfName;
    NSMutableArray* sfChannels;
    SFNDDimension*  sfNDDimensions;
	NSMutableArray* sfChannelInfos;
}

- (id) initWithNDDimensionInfo:(SFNDDimension*)diminfo;
- (id) initWithNDVoxelData:(SFNDVoxelData*)nddata;

- (BOOL) addChannel:(SFVoxelData*)channel withChannelInfo:(SFChannelInfo*)chinfo;
- (void) setName:(NSString*)name;
- (NSString*) name;

//channel methods
- (NSUInteger) countOfChannels;
- (NSArray*) channels;
- (SFVoxelData*) voxelDataAtIndex:(NSUInteger)idx;
- (SFNDDimension*) nDDimension;
- (NSColor*) colorAtIndex:(NSUInteger)idx;
- (BOOL) isActiveAtIndex:(NSUInteger)rank;

// dimension methods
- (NSUInteger) dimensionCount;

- (NSUInteger) numberOfElements;

- (NSArray*) channelInfos;

- (NSUInteger) numberOfCubes;
- (NSUInteger) numberOfPlanes;

//- (SFNDDimension*) dimensionInfo;

- (NSUInteger)strideAtIndex:(NSUInteger)rank;
- (NSUInteger) extentAtIndex:(NSUInteger)rank;
- (NSNumber*) magnitudeAtIndex:(NSUInteger)rank;
- (NSString*) voxelTypeAtIndex:(NSUInteger)rank;
- (NSString*) symbolAtIndex:(NSUInteger)rank;
- (NSString*) unitAtIndex:(NSUInteger)rank;

@end

@interface SFNDVoxelData (NDVoxelSlice)

- (NSUInteger) byteStrideForChannel:(NSUInteger)channel atRank:(NSUInteger)rank;
- (SFNDVoxelData*)sliceAtRank:(NSUInteger)rank atIndex:(NSUInteger)idx;

@end


@interface SFNDVoxelData (Processing)

- (SFNDVoxelData*)maximumProjectionForRank:(NSUInteger)rank;
 
@end


