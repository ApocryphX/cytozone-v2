//
//  SFLargeImageViewController.m
//  SanFrancisco
//
//
//

#import "SFLargeBitplaneViewController.h"
#import "SFLargeBitplaneView.h"

#import <OpenGL/gl.h>

@interface SFLargeBitplaneViewController ()

@end


@implementation SFLargeBitplaneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
        m_TileSize[0] = 128;
        m_TileSize[1] = 128;
    }
    
    return self;
}

-(IBAction)resetTransform:(id)sender {
    SFLargeBitplaneView *bitplaneView = (SFLargeBitplaneView *)self.view;
    bitplaneView.translationX=0.0;
    bitplaneView.translationY=0.0;
    bitplaneView.zoom=1.0;
    [bitplaneView reshape];
    bitplaneView.needsDisplay=YES;
};

-(void)addImageWithCurrentContext:(vImage_Buffer)imageBuffer imageRect:(NSRect)imageRect{

    NSUInteger gridSize[2];
    gridSize[0] = (imageBuffer.width+(m_TileSize[0]-1))/m_TileSize[0];
    gridSize[1] = (imageBuffer.height+(m_TileSize[1]-1))/m_TileSize[1];

 	m_TextureCount = gridSize[0]*gridSize[1];
    NSMutableData *texData = [NSMutableData dataWithLength:m_TextureCount*m_TileSize[0]*m_TileSize[1]];
    NSMutableData *tileData = [NSMutableData dataWithLength:sizeof(SFBitplaneTile)*m_TextureCount];
    
	m_TextureIDs = malloc(m_TextureCount*sizeof(GLuint));
    glGenTextures((GLuint)m_TextureCount, m_TextureIDs);

    NSOpenGLView *view = (NSOpenGLView *)self.view;
    
    NSOpenGLContext *context = [view openGLContext];
    [context makeCurrentContext];

    CGLLockContext([context CGLContextObj]);
	// Enable the rectangle texture extenstion
	glEnable(GL_TEXTURE_RECTANGLE_EXT);
	// Eliminate a data copy by the OpenGL driver using the Apple texture range extension along with the rectangle texture extension
	// This specifies an area of memory to be mapped for all the textures. It is useful for tiled or multiple textures in contiguous memory.
	glTextureRangeAPPLE(GL_TEXTURE_RECTANGLE_EXT, (GLsizei)texData.length, texData.mutableBytes);
    CGLUnlockContext([context CGLContextObj]);

        //void *dst = texData.mutableBytes;
    NSSize fractionalTileSize = NSMakeSize((double)imageBuffer.width/(double)m_TileSize[0],
                                           (double)imageBuffer.height/(double)m_TileSize[1]);
    NSSize scaledTileSize = NSMakeSize(fractionalTileSize.width*imageRect.size.width,
                                       fractionalTileSize.height*imageRect.size.height);

        //unsigned char *checkImage = texData.mutableBytes;

    NSUInteger gridIndex[2];
    SFBitplaneTile *pTileData = tileData.mutableBytes;
    for(gridIndex[1]=0;gridIndex[1]<gridSize[1];gridSize[1]++) {
        for(gridIndex[0]=0;gridIndex[0]<gridSize[0];gridSize[0]++) {
            NSUInteger tileIndex = gridIndex[1]*gridSize[0]+gridIndex[0];
            pTileData[tileIndex].texID = m_TextureIDs[tileIndex];
            // calculate the floating point coordinated of texture tiles
            pTileData[tileIndex].rect.size = scaledTileSize;
            pTileData[tileIndex].rect.origin = NSMakePoint(scaledTileSize.width*gridIndex[0]+imageRect.origin.x,
                                                           scaledTileSize.height*gridIndex[1]+imageRect.origin.y);
            NSIntersectionRect(imageRect, pTileData[tileIndex].rect);
        }
    }
};



@end
