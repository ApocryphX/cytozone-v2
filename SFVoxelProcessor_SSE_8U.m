//
//  SFVoxelProcessor_SSE_8U.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFVoxelProcessor_SSE.h"

#if defined(__SSE2__)
#ifdef __VEC__


@implementation SFVoxelProcessor_SSE_8U

+ (void) getMaximum:(void*)dst  source:(const void *)src alignment:(SFAlignment2D*) align {
	NSUIntegercount = [align  bytesPerFrame] / 16 ;
	vUInt8 *vsrc = (vUInt8 *) src;
	vUInt8 *vdst = (vUInt8 *) dst;
	NSUInteger i;
	for(i=0;i<count;i++) {
		vdst[i] = _mm_max_epu8(vdst[i], vsrc[i]);
        
	}	
};

+ (void) getBinary:(void*)dst source:(const void*)src alignment:(SFAlignment2D*) align threshold:(float)value {
	uint8_t threshold = 255 * value;
	NSUIntegercount = [align  bytesPerFrame] / 16 ;
	vUInt8 *vsrc = (vUInt8 *) src;
	vUInt8 *vdst = (vUInt8 *) dst;
	vUInt8 *vthreshold = malloc(sizeof(vUInt8));
	memset(vthreshold,threshold);
	NSUInteger i;
	for(i=0;i<count;i++) {
		vdst[i] = _mm_max_epu8(vsrc[i], vthreshold[0]) != vthreshold;
	}	
	free(vthreshold);
};


@end

#endif // __VEC__
#endif