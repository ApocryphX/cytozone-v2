//
//  SGLargeImageView.h
//  SanFrancisco
//
//
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>
#import <Accelerate/Accelerate.h>

@class SFNDImporter;
@protocol SFNDReading;

struct SFBitplaneTile {
    NSRect rect;
    NSUInteger size[2];
    GLuint texID;
};
typedef struct SFBitplaneTile SFBitplaneTile;

extern NSString *SFKTiledTextureColorKey;


@interface SFLargeBitplaneView : NSOpenGLView {    
    NSMutableArray  *m_TileArray;
    NSRect          m_ImageRect;
    NSUInteger      sfTileSize[2];
        //double          m_ScaleFactor;
}

@property (readonly) NSMutableArray *tileArray;
@property (assign) NSRect imageRect;
@property (assign) double scaleBarLength;
@property (assign) BOOL showScalebar;
@property (assign) double zoom;
@property (assign) double translationX;
@property (assign) double translationY;

- (NSImage*) snapShot;
- (NSMutableDictionary*)createTiledTexturesForImage:(vImage_Buffer)imageBuffer imageRect:(NSRect)imageRect;
- (void)deleteTiledTexturesForDictionary:(NSMutableDictionary*)textureDictionary;
-(void)removeAllImages;

@end
