//
//  SFBitplane.m
//  Monterey
//
//  Copyright 2014 Elarity, LLC.  All rights reserved.
//

#import "SFBitplane.h"

const NSString *SFKBitplaneColor = @"bitplanecolor";

@interface SFBitplane (private) 

@end

@implementation SFBitplane

@synthesize height;
@synthesize width;
@synthesize bytesPerSample;
@synthesize data;
@synthesize properties;


- (id)init
{
    self = [super init];
    if (self) {
        properties = [NSMutableDictionary new];
    }
    return self;
}

+ (id) bitplaneWithBytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height {
    SFBitplane *bitplane = [SFBitplane new];
	if(bitplane) {
        bitplane->height = height;
        bitplane->width = width;
        bitplane->bytesPerSample = bytesPerSample;
        bitplane->data = [NSMutableData dataWithLength:width*height*bytesPerSample];
    }
    return bitplane;
};

+ (id) bitplaneWithData:(NSData*)data bytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height {
    return [self bitplaneWithMutableData:[NSMutableData dataWithData:data] bytesPerSample:bytesPerSample width:width height:height];
};

+ (id) bitplaneWithMutableData:(NSMutableData*)data bytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height {
    SFBitplane *bitplane = [SFBitplane new];
	if(bitplane) {
        if(data.length == bytesPerSample*width*height) {
            bitplane->height = height;
            bitplane->width = width;
            bitplane->bytesPerSample = bytesPerSample;
            bitplane->data = data;
        }
        else {
            bitplane = nil;
        }
    }
    return bitplane;
};

+ (id) bitplaneWithData:(NSMutableData*)data bytesPerSample:(NSUInteger)bytesPerSample width:(NSUInteger)width height:(NSUInteger)height padding:(NSUInteger)padding {
    SFBitplane *bitplane = [SFBitplane new];
	if(bitplane) {
        if(data.length == bytesPerSample*width*height+padding) {
            bitplane->height = height;
            bitplane->width = width;
            bitplane->bytesPerSample = bytesPerSample;
            bitplane->data = [NSMutableData dataWithData:data];
        }
        else {
            bitplane = nil;
        }
    }
    return bitplane;
};

-(NSUInteger)bytesPerRow {
    return width*bytesPerSample;
};

-(NSUInteger) bytesPerPlane {
    return height*width*bytesPerSample;
};

-(NSUInteger) pixelsPerPlane {
    return  width*height;
};

- (vImage_Buffer) vectorImageBuffer {
	vImage_Buffer ret;
	ret.height = height;
	ret.width = width;
	ret.rowBytes = width*bytesPerSample;
    if (data)
		ret.data = data.mutableBytes;
	else
		ret.data = 0;
	return ret;
}

#pragma mark Method for NSCopying Protocol

- (id)copyWithZone:(NSZone *)zone {
    SFBitplane *bitplane = [SFBitplane new];
	if(bitplane) {
        bitplane->height = height;
        bitplane->width = width;
        bitplane->bytesPerSample = bytesPerSample;
        bitplane->data = [NSMutableData dataWithData:data];
        bitplane->properties = [properties copy];
    }
    return bitplane;    
};

@end


