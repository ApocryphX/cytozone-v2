//
//  SFVoxelProcessor.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class SFAlignment2D;

@protocol SFVoxelProcessing 
+ (void) getScaled:(void*)dst scale:(SFAlignment2D*)scale source:(const void*)src alignment:(SFAlignment2D*)alignment;
+ (void) getTranslated:(void*)dst  source:(const void *)src alignment:(SFAlignment2D*)align x:(float)x y:(float)y;
+ (void) getMaximum:(void*)dst  source:(const void *)src alignment:(SFAlignment2D*) align;
+ (void) getBinary:(void*)dst source:(const void*)src alignment:(SFAlignment2D*) align threshold:(float)value;
+ (void)edgeDetect:(void*)dst source:(const void*)src alignment:(SFAlignment2D*)align;
+ (void) removeNoise:(void*)dst source:(const void*)src alignment:(SFAlignment2D*)align kernelSize:(NSUInteger)size;
+ (void) getSquareRoot:(void*)dst source:(const void*)src alignment:(SFAlignment2D*)align;
+ (void) getSurface:(void*)dst top:(const void*)top middle:(const void*)middle bottom:(const void*)bottom alignment:(SFAlignment2D*)align;
//+ (void) getTranslated:(void*)dst  source:(const void *)src alignment:(SFAlignment2D*)align x:(float)x y:(float)y;
//- (void) minimum:(void*)dst  source:(const void*)src alignment:(SFAlignment2D*) align;
//- (void) invert:(void*) dst  source:(const void*)src alignment:(SFAlignment2D*) align;

@end

@interface SFVoxelProcessor_8U : NSObject <SFVoxelProcessing> {

}
@end

@interface SFVoxelProcessor_F : NSObject <SFVoxelProcessing> {
	
}



@end
