//
//  SFOpenGLView.h
//  VoxelMachine
//
//  Created by ApocryphX on Sat Jul 10 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFOGLVolumeScene, SFOGLContext, SFOGLTransform;

@interface SFOpenGLView : NSOpenGLView {
	SFOGLVolumeScene* sfScene;
	//SFOGLTransform* sfTextureTransform;
	NSColor* sfClearColor;
	
	SFOGLTransform* sfMouseTracker;
}

- (IBAction)updateView:(id)sender;
- (void) setClearColor:(NSColor*)value;
- (NSColor*) clearColor;

- (NSImage*) snapShot;

- (void) setTextureTransform:(SFOGLTransform*)value;
- (SFOGLTransform*) textureTransform;

- (void)setMouseTracker:(SFOGLTransform*)tracker;


- (void)setScene:(SFOGLVolumeScene*)value;
- (SFOGLVolumeScene*)scene;

@end
