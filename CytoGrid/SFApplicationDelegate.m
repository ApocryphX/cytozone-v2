//
//  SFApplicationDelegate.m
//  Cytozone
//
//  Created by Kolja Wawrowsky on 10/12/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFApplicationDelegate.h"



@implementation SFApplicationDelegate

@synthesize textureMode = sfTextureMode;

- (void)awakeFromNib {
    [self setTextureMode:0];
}

- (BOOL) applicationShouldOpenUntitledFile:(NSApplication*) sender {
    return NO;
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    [[NSColorPanel sharedColorPanel] setShowsAlpha:YES];
};

- (IBAction) applicationShowRenderPanel:(id)sender {
    [sfRenderPanel orderFront:self];
};

- (IBAction) quitApplication:(id)sender {
    [NSApp terminate:self];
};



@end
