//
//  SFApplicationDelegate.h
//  Cytozone
//
//  Created by Kolja Wawrowsky on 10/12/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SGRegistrationInfo;

@interface SFApplicationDelegate : NSObject {
    IBOutlet NSPanel* sfRenderPanel;
    NSUInteger sfTextureMode;
    
}

@property (assign) NSUInteger textureMode;

- (IBAction) applicationShowRenderPanel:(id)sender;

@end
