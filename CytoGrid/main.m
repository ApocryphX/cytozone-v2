//
//  main.m
//  CytoGrid
//
//  Created by Kolja Wawrowsky on 10/9/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
