//
//  SFGraphicsView.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFGraphicsObject.h"

extern NSString *KModeSelectionKey;
extern NSString *KModeNewObjectKey;

extern NSString *KObjectRectangleClassKey;
extern NSString *KObjectPolygonClassKey;
extern NSString *KObjectOvalClassKey;

@class SFSelectionRectangle;

@interface SFGraphicsView : NSView {
    NSPoint trackingStartPoint;
    NSArray *seletectedObjects;
    SFGraphicsObject *createdGraphic;
    NSMutableData *hitTestLayerData;
}

@property (assign) CGFloat zoom;
@property (readonly) CGFloat minimumScale;
@property (readonly) CGFloat maximumScale;

@property (retain) NSAffineTransform *transform;

@property (assign) NSSize handleSize;
@property (retain) NSColor *handleColor;
@property (retain) NSColor *handleShadowColor;
@property (readonly) NSMutableArray *graphicsObjects;
@property (retain) SFSelectionRectangle *selectionRectangle;

@property (retain) NSString *mouseMode;
@property (retain) NSString *objectType;

-(void)createHitLayer;

@end
