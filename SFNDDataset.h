//
//  SFNDDataset.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//
 
#import <Cocoa/Cocoa.h>
#import  "SFNDImporter.h"

@class SFNDVoxelData, SFNDDataNode;

@interface SFNDDataset : NSObject <NSCoding> {
	SFNDVoxelData* sfNDVoxelData;

	NSString* sfName;
	NSMutableString* sfDimensionString;	
}

- (id)initWithNDVoxelData:(SFNDVoxelData*)vdata;

- (void)setName:(NSString*)value;
- (NSString*)name;
- (NSString*)dimensionString;
- (NSUInteger)channelCount;
- (SFNDVoxelData*)nDVoxelData;

@end
