//
//  SFOGLVoxelFormat.m
//  VoxelMachine
//
//  Created by ApocryphX on 12/6/04.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVoxelFormat.h"
#import "SFVoxelInfo.h"

#import <OpenGL/gl.h>

@implementation SFOGLVoxelFormat

+ (GLenum)lookupFormat:(SFVoxelInfo*)vinfo {
	GLenum ret;
	switch ([vinfo interleaveType]) {
		case kPlanar: {
			ret = GL_INTENSITY;
			break;
		}
//		case kInterleavedLuminaceAlpha: {
//			ret = GL_LUMINANCE_ALPHA;
//			break;
//		}
		case kInterleavedRGB: {
			ret = GL_RGB;
			break;
		}
//		case kInterleavedRGBA: {
//			ret = GL_RGBA;
//			break;
//		}
		default: {
			ret = 0;
			break;
		}
	}
	return ret;
};

+ (GLenum)lookupType:(SFVoxelInfo*)vinfo {
	GLenum ret;
	switch ([vinfo dataType]) {
		case kInteger: {
			switch ([vinfo precision]) {
				case 8: {
					ret = GL_UNSIGNED_BYTE;
					break;
				}
				case 12: {
					ret = GL_UNSIGNED_SHORT;
					break;
				}
				case 16: {
					ret = GL_UNSIGNED_SHORT;
					break;
				}
				case 32: {
					ret = GL_UNSIGNED_INT;
					break;
				}
				default: {
					ret = 0;
					break;
				}
			}
			break;
		}
		case kFloatingPoint:{
			ret = GL_FLOAT;
			break;
		}
		default: {
			ret = 0;
			break;
		}
	}
	return ret;	
};


@end
