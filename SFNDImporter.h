//
//  SFNDImporter.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h> 

typedef NSMutableDictionary SFFileNodeDictionary;

extern NSString *SFKNodeTitleKey;
extern NSString *SFKNodeChildrenKey;
extern NSString *SFKNodeDataIndexKey;
extern NSString *SFKNodeIconKey;

extern NSString *SFKLeicaFileIconName;
extern NSString *SFKNikonFileIconName;
extern NSString *SFKZeissFileIconName;

extern NSString *SFKStage_X_Position;
extern NSString *SFKStage_Y_Position;

@class NSProgressIndicator, SFNDDimension, SFNDVoxelData, SFNDDataNode, SFVoxelInfo;

@protocol  SFNDReading 

- (id)initWithPath:(NSString*)path;
+ (NSArray*)fileExtensions;
+ (BOOL)isConfocalFile:(NSString*)filePath;

- (NSUInteger)count;
- (NSString*)name;
- (NSArray*)fileNodes;

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex;
- (SFNDDimension*)nDDimensionAtNodeIndex: (NSUInteger)nodeIndex;
- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex;
- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex;
- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex;

@end


@interface SFNDImporter  : NSObject {
	NSString* sfPath;
	NSObject <SFNDReading>* sfNDReader;
	float sfProgress;
	NSProgressIndicator* progressIndicator;
	BOOL sfStopLoadingFlag;
	NSMutableDictionary* sfClassMap;
	NSArray*	sfFileNodes;
}

@property (readonly) NSArray* outlineNodes; //(readonly), because this class is for reading only
@property (readonly) NSOperationQueue *operationQueue;

+ (SFFileNodeDictionary*)fileNodeDictionary;
+ (NSArray*)fileExtensions;
+ (NSString*)docTypeForExtension:(NSString*)extension;

- (id)initWithPath:(NSString*)path ofType:(NSString *)docType;

- (void)stopLoadingData;
- (void)setProgressIndicator:(NSProgressIndicator*)ctrl;
- (NSProgressIndicator*)progressIndicator;
- (void)setProgressValue:(float)value;
- (void)syncProgressIndicator;
//- (SFNDVoxelData*)readNDVoxelDataAtNodeIndex:(NSUInteger)nodeIndex;

- (NSUInteger)count;
- (NSString*)name;

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex;
- (SFNDDimension*)nDDimensionAtNodeIndex:(NSUInteger)nodeIndex;;
- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex;
- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex;

- (NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex;
// - (NSArray*)bitplaneArrayForNodeIndex:(NSUInteger)nodeIndex planeIndex:(NSUInteger)planeIndex;


@end

