//
//  SFVoxelData.m
//  VoxelMachine
//
//  Created by ApocryphX on Fri May 28 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//


#import "SFVoxelData.h"
#import "SFVoxelInfo.h"
#import "SFAlignment2D.h"
#import "SFVoxelProcessor.h"


@implementation SFVoxelData

- (id)initWithAlignment:(SFAlignment2D*)alignment {
	self = [super init];
	sfParent = nil;
	sfAlignment = alignment;
	sfData = [NSData dataWithBytesNoCopy:malloc([alignment bytesPerFrame]) length:[alignment bytesPerFrame]];
	return self;
};

- (id)initWithSize:(SFSize)size voxelInfo:(SFVoxelInfo*)vinfo {
	self = [super init];
	sfParent = nil;
	sfAlignment = [[SFAlignment2D alloc]initWithSize:size voxelInfo:vinfo];
	sfData = [NSData dataWithBytesNoCopy:malloc([sfAlignment bytesPerFrame]) length:[sfAlignment bytesPerFrame]];
	return self;
};

- (id)initWithData:(NSData*)data alignment:(SFAlignment2D*)alignment 
{
	self = [super init];
	sfParent = nil;
	sfAlignment = alignment;
	sfData = data;
	return self;
};

- (void)replaceBytes:(const void*)data alignment:(SFAlignment2D*)alignment {
	NSUInteger i, count = [sfData length] / [sfAlignment bytesPerFrame];
	for (i=0;i<count;i++) {
		[sfAlignment getRealignedData:(void*)[sfData bytes]+[sfAlignment bytesPerFrame]*i  
							   source:(void*) data + [alignment bytesPerFrame]*i
							alignment:alignment ];
	}
};

- (NSUInteger) bytesPerVoxel {
	return [[sfAlignment voxelInfo] bytesPerVoxel];
};

- (NSUInteger) samplesPerVoxel {
	return [[sfAlignment voxelInfo] samplesPerVoxel];
};

- (const void*) bytes
{
	return [sfData bytes];
};

- (SFVoxelInfo*)voxelInfo {
	return [sfAlignment voxelInfo];
};

- (NSData*) data
{
	return sfData;
};

- (NSData*) dataAtFrameIndex:(NSUInteger)idx {
	NSData* ret = nil;
	NSUInteger bytesperframe = [sfAlignment bytesPerFrame];
	if ( idx <= [sfData length]/bytesperframe ) 
		ret = [NSData dataWithBytesNoCopy:(void*)[sfData bytes]+idx*bytesperframe length:bytesperframe freeWhenDone:NO];
	return ret;
};

//- (NSObject<SFVoxelProcessing>*)processor {
// 	return [[SFVoxelProcessor new]autorelease];
//};

- (SFAlignment2D*)alignment 
{
	return sfAlignment;
};

- (NSUInteger) numberOfFrames {
	return [sfData length] / [sfAlignment bytesPerFrame];
};

- (NSUInteger) bytesPerFrame {
	return [sfAlignment bytesPerFrame];
};

- (SFVoxelData*)frameAtIndex:(NSUInteger)idx {
	SFVoxelData* ret = nil;
	if (idx < [self numberOfFrames]) {
		ret = [SFVoxelData new];
		ret->sfParent = self;
		ret->sfAlignment = sfAlignment;
		ret->sfData = [NSData dataWithBytesNoCopy:(void*)[sfData bytes]+idx*[sfAlignment bytesPerFrame]
										   length:[sfAlignment bytesPerFrame] 
									 freeWhenDone:NO];
	}
	return ret;
}

- (SFVoxelData*)slice:(NSUInteger)offset length:(NSUInteger)length {
	SFVoxelData* ret = nil;
	if ( offset+length <= [sfData length] ) {
		ret = [SFVoxelData new];
		ret->sfData = [NSData dataWithBytesNoCopy:(void*)[sfData bytes]+offset length:length freeWhenDone:NO];
		ret->sfAlignment = sfAlignment;
		ret->sfParent = self;
	}
	else 
		ret = nil;
	return ret;
};

- (SFVoxelData*)sliceAtFrame:(NSUInteger)idx count:(NSUInteger)count {
	NSUInteger offset = [sfAlignment bytesPerFrame]*idx;
	NSUInteger length = [sfAlignment bytesPerFrame]*count;
	return [self slice:offset length:length];
};

- (void)clearBorderPixels {
    // create a black border
    NSUInteger bytesPerRow =  sfAlignment.bytesPerRow;
    NSUInteger bytesPerVoxel =  sfAlignment.voxelInfo.bytesPerVoxel;
    NSUInteger rowIndex, numberOfRows = sfAlignment.bytesPerFrame/sfAlignment.bytesPerRow;
    void *pData = (void*)[sfData bytes];
    memset(pData,0,bytesPerRow);
    for(rowIndex=1;rowIndex<numberOfRows-1;rowIndex++) {
        char *dst = pData+rowIndex*bytesPerRow-bytesPerVoxel;
        memset(dst,0,bytesPerVoxel*2);
    }
    memset(pData+(numberOfRows-1)*bytesPerRow,0,bytesPerRow);
}
@end
