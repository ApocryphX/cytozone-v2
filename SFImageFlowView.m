//
//  SFImageFlowView.m
//  Image Flow
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFImageFlowView.h"

#import "SFImageScrollView.h"

@implementation SFImageFlowView

@dynamic indexSet;
@dynamic imagePadding;
@synthesize images;

- (void) setupView {
    [self setAutoresizingMask:NSViewNotSizable];             
    imagePadding = 10.0;
    [self setFrameSize:NSMakeSize(imagePadding, self.frame.size.height)];
    images = [NSMutableArray new];
    
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.        
        [self setupView];
    }
    return self;
}

-(void)awakeFromNib {
    [self setupView];
}


-(NSRect)frameForImage:(NSImage*)image {
    CGFloat ratio = image.size.width/image.size.height;
    CGFloat scale = (self.frame.size.height-imagePadding*2)/image.size.height;
    NSRect imageFrame = NSMakeRect(0.0, 
                                   imagePadding, 
                                   image.size.height*ratio*scale, 
                                   image.size.height*scale);
    return imageFrame;
    
}


- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
    [super drawRect:dirtyRect];
    NSUInteger imageIndex, imageCount = images.count;
    NSUInteger drawingOffset = imagePadding;
    for (imageIndex=0;imageIndex<imageCount;imageIndex++) {
        NSImage *image = [images objectAtIndex:imageIndex];
        NSRect imageFrame = [self frameForImage:image];
        imageFrame.origin.x=drawingOffset;
        drawingOffset = drawingOffset+imageFrame.size.width+imagePadding;
        if(NSIntersectsRect(imageFrame, self.frame)) {
            //[image drawInRect:imageFrame fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
            [image drawInRect:imageFrame fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
            if (indexSet && [indexSet containsIndex:imageIndex]) {
                NSRect rect = imageFrame;
                rect = NSInsetRect(rect, imagePadding*-0.5, imagePadding*-0.5);
                float clampedRadius = 4.0;
                NSPoint topLeft = NSMakePoint(NSMinX(rect), NSMaxY(rect));
                NSPoint topRight = NSMakePoint(NSMaxX(rect), NSMaxY(rect));
                NSPoint bottomRight = NSMakePoint(NSMaxX(rect), NSMinY(rect));
                [[NSColor whiteColor] set];
                NSBezierPath *path = [NSBezierPath bezierPath];
                [path moveToPoint:NSMakePoint(NSMidX(rect), NSMaxY(rect))];
                [path appendBezierPathWithArcFromPoint:topLeft     toPoint:rect.origin radius:clampedRadius];
                [path appendBezierPathWithArcFromPoint:rect.origin toPoint:bottomRight radius:clampedRadius];
                [path appendBezierPathWithArcFromPoint:bottomRight toPoint:topRight    radius:clampedRadius];
                [path appendBezierPathWithArcFromPoint:topRight    toPoint:topLeft     radius:clampedRadius];
                [path closePath];
                [path stroke];
            }
        }
    }
}

- (void) recalculateFrameRect {
    NSUInteger drawingOffset = imagePadding;
    for (NSImage *image in images) {
        NSRect imageFrame = [self frameForImage:image];
        drawingOffset = drawingOffset+imageFrame.size.width+imagePadding;
    }
    NSSize newSize = self.frame.size ;
    newSize.width = drawingOffset+imagePadding;
    [self setFrameSize:newSize];
}


- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent {
    return YES;
}

- (void)mouseDown:(NSEvent *)theEvent {
    NSPoint curPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];    
    NSUInteger drawingOffset = imagePadding;
    for (NSImage *image in images) {
        NSRect imageFrame = [self frameForImage:image];
        imageFrame.origin.x=drawingOffset;
        drawingOffset = drawingOffset+imageFrame.size.width+imagePadding;
        if(NSPointInRect(curPoint, imageFrame)) {
            if (indexSet && ([theEvent modifierFlags] & NSCommandKeyMask)) {
                NSUInteger index = [images indexOfObject:image];
                [indexSet addIndex:index];
            }
            else {
                indexSet = [NSMutableIndexSet indexSetWithIndex:[images indexOfObject:image]];
            }
            [self setNeedsDisplay:YES];
            break;
        }

    }
    [super mouseDown:theEvent];
}


- (void)setImagePadding:(CGFloat)value {
    imagePadding = value;
    [self setNeedsDisplay:YES];
}

- (CGFloat)imagePadding {
    return imagePadding;
}

- (void)setIndexSet:(NSMutableIndexSet *)value {
    if(value!=indexSet) {
        indexSet = value;
        [self setNeedsDisplay:YES];
    }
}

- (NSMutableIndexSet *)indexSet {
    return indexSet;
}

- (void)mouseDragged:(NSEvent *)theEvent {
    [super mouseDragged:theEvent];
}

- (void)mouseUp:(NSEvent *)theEvent {
    [super mouseUp:theEvent];

}

@end
