//
//  SVSReader.c
//  Cytozone
//
//  Created by Kolja Wawrowsky on 3/31/15.
//  Copyright (c) 2015 Elarity, LLC. All rights reserved.
//

#include "SVSReader.h"
#include <stdlib.h>
#include <tiffio.h>

struct SVSInfo {
    TIFF *tif;
} ;

typedef struct SVSInfo SVSInfo;


void* SVSOpen(const char *path) {
    SVSInfo *SVS = NULL;
    TIFF* tif = TIFFOpen(path, "r");
    if (tif) {
        SVS = malloc(sizeof(SVSHandle));
        SVS->tif = tif;
        uint32 w, h;
        uint32 tileWidth, tileLength, nTiles;
        //size_t npixels;

        int dircount = 0;
        do {
            TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
            TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
            nTiles = TIFFNumberOfTiles(tif);
            TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tileWidth);
            TIFFGetField(tif, TIFFTAG_TILELENGTH, &tileLength);
            printf("Directory:%d Size:%d,%d Tiles:%d Tile Size: %d, %d\n", dircount, w,h,nTiles,tileWidth,tileLength);
            dircount++;
        } while (TIFFReadDirectory(tif));
        printf("%d directories in %s\n", dircount, path);
        ;
        TIFFSetDirectory(tif,0);

        uint64_t tileSize = TIFFTileSize(tif);
        void *raster = _TIFFmalloc(tileSize);
        if (raster != NULL) {
            //if (TIFFReadRGBAImage(tif, w, h, raster, 0)) {}
            //tiffreadtile(tif, buf, x, y, 0);
            uint32_t tileIndex,tileCount= TIFFNumberOfTiles(tif);
            for (tileIndex=0;tileIndex<tileCount;tileIndex++) {
                printf("Reading tile number:%d ",tileIndex);
                tmsize_t bytesRead = TIFFReadEncodedTile(tif, tileIndex, raster, tileSize);
                printf("bytes read:%ld bytes\n",bytesRead);
            }
            _TIFFfree(raster);
        }
    }
    
    return SVS;
};

void SVSClose(SVSHandle handle) {
    if(handle) {
        SVSInfo *svsHandle = handle;
        if(svsHandle->tif) {
            TIFFClose(svsHandle->tif);
        }
    }
    
}