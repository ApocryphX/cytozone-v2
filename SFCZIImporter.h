//
//  SFCZIImporter.h
//  Cytozone
//
//  Created by Kolja Wawrowsky on 4/26/16.
//  Copyright © 2016 Elarity, LLC. All rights reserved.
//

@import Cocoa;

#import "SFNDImporter.h"

@interface SFCZIImporter : NSObject <SFNDReading>

@end
