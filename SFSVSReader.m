//
//  SFSVSReader.m
//  Cytozone
//
//  Created by Kolja Wawrowsky on 3/31/15.
//  Copyright (c) 2015 Elarity, LLC. All rights reserved.
//

@import Cocoa;

#import "SFSVSReader.h"
#include "SVSReader.h"

@interface SFSVSReader ()

@property (assign) SVSHandle svsHandle;

@end



@implementation SFSVSReader

- (instancetype)initWithPath:(NSString *)path {
    self = [super init];
    if (self) {
        const char *cPath = [[path stringByExpandingTildeInPath] cStringUsingEncoding:NSUTF8StringEncoding];
        _svsHandle = SVSOpen(cPath);
    }
    return self;
}

-(void)dealloc {
    SVSClose(_svsHandle);
}

@end
