//
//  SFBigTIFFTag.h
//  SanFrancisco
//
//
//

#import <Foundation/Foundation.h>

@interface SFBigTIFFTag : NSObject {
    uint16_t tag;
    uint16_t sfDataType;
    uint64_t count;
    NSData* valueData;
}

+ (uint32_t)sizeDirEntry;

- (id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian;

- (uint16_t)tagID;
- (uint16_t)tagType;
- (uint64_t)tagCount;
- (uint64_t)length;
-(BOOL)isValueOffset;

- (NSData*)valueAsData;
- (NSNumber*)valueAsNumberAtIndex:(uint32_t)number;

@end
