//
//  SFOGLRotation.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

struct GLRotation {
	double a;
	double x;
	double y;
	double z;
};

@interface SFOGLRotation : NSObject {
	struct GLRotation sfRotation;
}

+ (id) initWithRotation: (struct GLRotation)theRotation;
+ (id) initWithAngle: (float)A X:(float)theX Y:(float) theY Z:(float)theZ;

- (void)add:(SFOGLRotation *)dA; 
- (void)execute;

- (void)setRotation:(struct GLRotation)theRotation;
- (void)setX:(float)value;
- (void)setY:(float)value;
- (void)setZ:(float)value;
- (void)setA:(float)value;
- (void)reset;
- (void)resetAxis:(NSUInteger)axis;

- (struct GLRotation) GLRotation;
- (float) a;
- (float) x;
- (float) y;
- (float) z;

- (SFOGLRotation*)interpolatedRotationWithFraction:(float)frac ofRotation:(SFOGLRotation*)rot;

@end
