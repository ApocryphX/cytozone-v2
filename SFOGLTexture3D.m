//
//  SFOGLTexture3D.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLTexture3D.h"

#include <OpenGL/gl.h>

#import "SFNDVoxelData.h"
#import "SFVoxelData.h"
#import "SFAlignment2D.h"
#import "SFOGLVoxelFormat.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"
#import "SFOGLVoxelInfo.h"

@interface SFOGLTexture3D (PrivateMethods)
- (void) deleteOGLTexture;
- (void) loadTexture;
@end

@implementation SFOGLTexture3D

- (id)init {
	self = [super init];
	sfTexName = 0;
    sfMaxTextureSize = 1024;
	return self;
};

- (void)setOpenGLContext:(NSOpenGLContext*)cxt {
	if (cxt != sfContext) {
		[self deleteOGLTexture];
		[super setOpenGLContext:cxt];		
		[self loadTexture];
	}
};

- (void)setNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo {
	sfNDDimension = [nddim dimensionsSlicedAtIndex:2];
	sfIsDownsampled = NO;
    double factor = 1.0;
	if (  MAX([sfNDDimension extentAtIndex:0], [sfNDDimension extentAtIndex:1]) > sfMaxTextureSize ) {
		factor = sfMaxTextureSize / (double) MAX([sfNDDimension extentAtIndex:0], [nddim extentAtIndex:1]);
		sfIsDownsampled = YES;
	}	

	SFSize size = { [sfNDDimension extentAtIndex:0]*factor, [sfNDDimension extentAtIndex:1]*factor };
	sfAlignment = [[SFAlignment2D alloc]initWithSize:size voxelInfo:vinfo];
	[sfAlignment setAlignment:SFKAppleAlignedName];
	sfPlaneCount = pow(2.0f, ceil(log2( (double) [nddim extentAtIndex:2]+2.0f)));
	sfPaddingPlanes = sfPlaneCount-[sfNDDimension extentAtIndex:2];	
	NSMutableData* data = [NSMutableData dataWithLength:[sfAlignment bytesPerFrame]*sfPlaneCount];
	sfOGLVoxelInfo = [[SFOGLVoxelInfo alloc]initWithVoxelInfo:vinfo];
	sfTexData = [[SFVoxelData alloc]initWithData:data alignment:sfAlignment];
	[self deleteOGLTexture];
	[self loadTexture];
};


- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment index:(NSUInteger)idx {
    
	SFVoxelData* vdata = [sfTexData frameAtIndex:idx];  //padding: (idx+sfPaddingPlanes/2)
	[vdata replaceBytes:bytes alignment:alignment];

    
	if (sfTexName) {
        
        CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
		glBindTexture(GL_TEXTURE_3D, sfTexName);
		glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, (GLint)idx, 
						(GLsizei)[[sfTexData alignment]size].width,(GLsizei) [[sfTexData alignment]size].height, 1,
						[sfOGLVoxelInfo format],  [sfOGLVoxelInfo type], [vdata bytes] );
		glBindTexture(GL_TEXTURE_3D, 0);
        CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	}
};

- (NSUInteger) countOfPlanes {
	return sfPlaneCount;
} 

- (void)setRenderMode:(NSUInteger)value {
	[super setRenderMode:value];
	//[self loadTexture];
}; 


- (NSUInteger)extentAtAxis:(NSUInteger)axis {
	switch (axis) {
		case 0: 
			return [[sfTexData alignment] alignedSize].width;
		case 1: 
			return [[sfTexData alignment] alignedSize].height;
		case 2: 
			return  sfPlaneCount;
 		default:
			return 1.0f;

	}
};


- (float) paddingRatioAtAxis:(NSUInteger)axis {
	switch (axis) {
		case 0: 
			return (float)[[sfTexData alignment] alignedSize].width / (float)[[sfTexData alignment]size].width;
		case 1: 
			return [[sfTexData alignment] alignedSize].height / (float)[[sfTexData alignment]size].height;
		case 2: 
			return (float) sfPlaneCount / (float)(sfPlaneCount-sfPaddingPlanes); 
		default:
			return 1.0f;
			
	}
};

- (BOOL) isDownsampled {
	return sfIsDownsampled;
};

- (SFNDDimension*)nDDimension {
	return sfNDDimension;
};

- (SFAlignment2D*)alignment {
	return [sfTexData alignment];
};

- (SFVoxelData*)texData {
	return sfTexData;
};

-(void) loadTexture {
	if (sfTexData != nil) {
        
        CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
		glEnable(GL_TEXTURE_3D);
		
		if(!sfTexName)
			glGenTextures(1, &sfTexName);
		glBindTexture(GL_TEXTURE_3D, sfTexName);
		
		// enable AGP texturing and main memory storage		
		glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_PRIORITY, 1.0);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_STORAGE_HINT_APPLE , GL_STORAGE_SHARED_APPLE); 
		glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
		
		
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);

		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // required if no mipmap levels defined
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // required if no mipmap levels defined
				
		glTexImage3D ( GL_TEXTURE_3D, 0, [sfOGLVoxelInfo internalFormat], (GLsizei)sfAlignment.size.width ,
					   (GLsizei)sfAlignment.size.height, (GLsizei)sfPlaneCount,  0, 
					   [sfOGLVoxelInfo format],  [sfOGLVoxelInfo type], (GLvoid*) [[sfTexData data] bytes]);	
		glBindTexture(GL_TEXTURE_3D, 0);
		glDisable(GL_TEXTURE_3D);
        CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	}
};

- (void) deleteOGLTexture {
	if (sfTexName != 0) {
        CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
        glBindTexture(GL_TEXTURE_3D, 0);	
        glDisable(GL_TEXTURE_3D);
    	glDeleteTextures( 1,&sfTexName);
		sfTexName = 0;
        CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	}
};

-(void) bind {	
    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	glBindTexture(GL_TEXTURE_3D, sfTexName);
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
};

+ (void) unbind {
    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	glBindTexture(GL_TEXTURE_3D, 0);	
	glDisable(GL_TEXTURE_3D);
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
};

- (void)dealloc {
	[self deleteOGLTexture];
};

@end
