//
//  SFOGLVoxelInfo.m
//  CytoFX
//
//  Created by ApocryphX on 10/22/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVoxelInfo.h"

#include <OpenGL/gl.h>

@implementation SFOGLVoxelInfo

- (id)initWithVoxelInfo:(SFVoxelInfo *)info {
	self = [super init];
	[self setBytesPerSample:[info bytesPerSample]] ;
	[self setSamplesPerVoxel:[info samplesPerVoxel]];
	[self setPrecision:[info precision]];
	[self setDataType:[info dataType]];
	[self setInterleaveType:[info interleaveType]];
	[self setPhotometricInterpretation:[info photometricInterpretation]];
	return self;
};

- (GLenum) format {
	GLenum ret;
	switch ([self interleaveType]) {
		case kPlanar: {
			ret = GL_LUMINANCE;
			break;
		}
		case kInterleavedRGB: {
			ret = GL_RGB;
			break;
		}
/*
		case kInterleavedLuminaceAlpha: {
			ret = GL_LUMINANCE_ALPHA;
			break;
		}
		case kInterleavedRGBA: {
			ret = GL_RGBA;
			break;
		}
 */
		default: {
			ret = 0;
			break;
		}
	}
	return ret;
};

- (GLenum) internalFormat {
	GLenum ret;
	switch ([self dataType]) {
		case kInteger: {	
			switch ([self interleaveType]) {
				case kPlanar: {
					switch ([self precision]) {
						case 8: {
							ret = GL_INTENSITY8;
							break;
						}
						case 12: {
							ret = GL_INTENSITY8; //GL_INTENSITY12;
							break;
						}
						case 16: {
							ret = GL_INTENSITY8; //GL_INTENSITY16;
							break;
						}
						default: {
							ret = 0;
							break;
						}
					}
					break;
				}
                case kInterleavedRGB: {
					switch ([self precision]) {
						case 8: {
							ret = GL_RGB8;
							break;
						}
						case 12: {
							ret = GL_RGB12;
							break;
						}
						case 16: {
							ret = GL_RGB16;
							break;
						}
						default: {
							ret = 0;
							break;
						}
					}
					break;
				}
/*
				case kInterleavedLuminaceAlpha: {
					switch ([self precision]) {
						case 8: {
							ret = GL_LUMINANCE8_ALPHA8;
							break;
						}
						case 12: {
							ret = GL_LUMINANCE12_ALPHA12;
							break;
						}
						case 16: {
							ret = GL_LUMINANCE16_ALPHA16;
							break;
						}
						default: {
							ret = 0;
							break;
						}
					}
					break;
				}
				case kInterleavedRGBA: {
					switch ([self precision]) {
						case 8: {
							ret = GL_RGBA8;
							break;
						}
						case 12: {
							ret = GL_RGBA12;
							break;
						}
						case 16: {
							ret = GL_RGBA16;
							break;
						}
						default: {
							ret = 0;
							break;
						}
					}
					break;
				}
 */
				default: {
					ret = 0;
					break;
				}
			}
			break;
		}
		case kFloatingPoint:{
			ret = GL_FLOAT;
			break;
		}
		default: {
			ret = 0;
			break;
		}
	}
	return ret;	
};

- (GLenum) type {
	GLenum ret;
	switch ([self dataType]) {
		case kInteger: {
			switch ([self precision]) {
				case 8: {
					ret = GL_UNSIGNED_BYTE;
					break;
				}
				case 12: {
					ret = GL_UNSIGNED_BYTE; //GL_UNSIGNED_SHORT;
					break;
				}
				case 16: {
					ret = GL_UNSIGNED_BYTE; //GL_UNSIGNED_SHORT;
					break;
				}
				case 32: {
					ret = GL_UNSIGNED_BYTE; //GL_UNSIGNED_INT;
					break;
				}
				default: {
					ret = 0;
					break;
				}
			}
			break;
		}
		case kFloatingPoint:{
			ret = GL_FLOAT;
			break;
		}
		default: {
			ret = 0;
			break;
		}
	}
	return ret;	
};

@end
