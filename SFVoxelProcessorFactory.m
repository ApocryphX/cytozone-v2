//
//  SFVoxelProcessorFactory.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFVoxelProcessorFactory.h"


#import "SFAlignment2D.h"
#import "SFVoxelInfo.h"
#import "SFVoxelProcessor.h"
#import "SFVoxelProcessor_SSE.h"

SFVoxelProcessorFactory* sfSharedVoxelProcessorFactory;

@implementation SFVoxelProcessorFactory

/*
static int sysctlbynameuint64 (const char* name, uint64_t *value) {
	int result;
	size_t size = sizeof(*value);
	result = sysctlbyname(name, value, &size, NULL, 0);
	if (result==0) {
		if(size == sizeof(uint64_t));
		else if(size == sizeof(uint32_t))
			*value =*(uint32_t*)value;
		else if(size == sizeof(uint16_t))
			*value =*(uint16_t*)value;
		else if(size == sizeof(uint8_t))
			*value =*(uint8_t*)value;
	}
	return result;
}

*/

// valid: "altivec", "mmx", "sse", "sse2", "sse3"
+ (BOOL)hasNamedVectorUnit:(NSString*)value {
//	uint64_t val;
//	NSString* keystring = [[NSString stringWithString:@"hw.optional."] stringByAppendingString:value];
//	return sysctlbynameuint64([keystring UTF8String], &val) == 0 ? val ==1:0;
    return YES;
};

+ (BOOL)hasSupportedVectorUnit {
	BOOL ret = FALSE;
	if ([self hasNamedVectorUnit:@"sse2"])
		ret = TRUE;
	else if ([self hasNamedVectorUnit:@"sse3"])
		ret = TRUE;
	return ret;	
}

+(BOOL) isAlignmentSupported:(SFAlignment2D*)align {
	BOOL ret = NO;
	switch ([[align voxelInfo]dataType]) {
		case kInteger: {
			if([[align voxelInfo]bytesPerSample]==1)
				ret = YES;
			break;					
		}
		case kFloatingPoint: {
			if([[align voxelInfo]bytesPerSample]==4) 
				ret = YES;
			break;									
		}				
		default:
			break;
	}
	return ret;
}

+ (Class <SFVoxelProcessing> ) classForAlignment:(SFAlignment2D*)align {
	if ([self isAlignmentSupported:align] == NO)
		return Nil;
    switch ([[align voxelInfo]dataType]) {
        case kInteger: 
            return [SFVoxelProcessor_8U class];
            
        case kFloatingPoint: 
            return [SFVoxelProcessor_F class];
        default:
            return Nil;
    }
};


@end
