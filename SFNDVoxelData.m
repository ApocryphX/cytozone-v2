//
//  SFNDVoxelData.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFNDVoxelData.h"
#import "SFNDImporter.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"
#import "SFVoxelData.h"
#import "SFChannelInfo.h"
#import "SFDataInfo.h"

@implementation SFNDVoxelData

- (id) init 
{
	self = [super init];
	sfChannelInfos = [NSMutableArray new];
    sfChannels = [NSMutableArray new];	
	return self;
};

- (id) initWithNDDimensionInfo: (SFNDDimension*)diminfo 
{
    self = [self init];
    sfNDDimensions = [diminfo copy];
    return self;
};

- (id)initWithNDVoxelData:(SFNDVoxelData*)nddata {
	self = [super init];
	sfNDParent = nddata->sfNDParent;
    sfName = nddata->sfName;
    sfChannels = nddata->sfChannels;
    sfNDDimensions = nddata->sfNDDimensions;
	sfChannelInfos = nddata->sfChannelInfos;
	return self;
}

- (void)addChannel:(SFVoxelData*)voxeldata 
{
	[sfChannels addObject:voxeldata];
};

- (BOOL) addChannel:(SFVoxelData*)channel withChannelInfo:(SFChannelInfo*)chinfo
{
	[sfChannels addObject:channel];
	[sfChannelInfos addObject:chinfo];
    return YES;
};


-(void)setName:(NSString*)name
{
    sfName = name;
};

- (NSString*)name
{
    return sfName;
};

- (BOOL)isActiveAtIndex:(NSUInteger)rank 
{
	return [[sfChannelInfos objectAtIndex:rank]isActive];
};


- (NSColor*)colorAtIndex:(NSUInteger)rank 
{
	return [[sfChannelInfos objectAtIndex:rank]color];
};

- (NSUInteger)dimensionCount
{
	return [sfNDDimensions count];
};


- (NSUInteger) countOfChannels 
{
    return [sfChannels count];
};

- (NSArray*)channels
{
    return sfChannels;
};


- (SFVoxelData*) voxelDataAtIndex:(NSUInteger)idx
{
    SFVoxelData* ret = Nil;
    if (idx < [sfChannels count] )
        ret = [sfChannels objectAtIndex:idx];
    return ret;
};

- (NSUInteger) numberOfElements
{
    return [sfNDDimensions strideAtIndex:[sfNDDimensions count]];
};

- (NSUInteger) numberOfPlanes {
    return [sfNDDimensions numberOfPlanes];
};

-(NSUInteger) numberOfCubes {
	return [sfNDDimensions numberOfCubes];
};

- (SFNDDimension*) nDDimension; {
    return sfNDDimensions;
};

- (NSUInteger)strideAtIndex:(NSUInteger)rank {
	return [sfNDDimensions	strideAtIndex:rank];
};

- (NSNumber*) magnitudeAtIndex:(NSUInteger)rank {
	return [sfNDDimensions	magnitudeAtIndex:rank];
};

- (NSUInteger) extentAtIndex:(NSUInteger)rank {
	return [sfNDDimensions	extentAtIndex:rank];
};

- (NSString*) voxelTypeAtIndex:(NSUInteger)rank {
	return [sfNDDimensions dimensionTypeAtIndex:rank];	
};

- (NSString*) symbolAtIndex:(NSUInteger)rank {
	return [sfNDDimensions symbolAtIndex:rank];	
};

- (NSArray*)channelInfos {
	return sfChannelInfos;
};

- (NSString*) unitAtIndex:(NSUInteger)rank 
{
	return [sfNDDimensions unitAtIndex:rank];
};

- (id)initWithCoder:(NSCoder *)coder
{
	self = [super init];
    if ( [coder allowsKeyedCoding] ) {
        // Can decode keys in any order
        sfName = [coder decodeObjectForKey:@"Name"];
		sfNDDimensions = [coder decodeObjectForKey:@"nDDimensions"];
		sfChannelInfos = [coder decodeObjectForKey:@"ChannelInfos"];
    } 
	else {
        // Must decode keys in same order as encodeWithCoder:
		sfName = [coder decodeObject];
		sfNDDimensions = [coder decodeObject];
		sfChannelInfos = [coder decodeObject];
    }
    return self;
};

- (void)encodeWithCoder:(NSCoder *)coder
{
    if ( [coder allowsKeyedCoding] ) {
        [coder encodeObject:sfName forKey:@"Name"];
		[coder encodeObject:sfNDDimensions forKey:@"nDDimensions"];
		[coder encodeObject:sfChannelInfos forKey:@"ChannelInfos"];
    } 
	else {
        [coder encodeObject:sfName];
        [coder encodeObject:sfNDDimensions];
		[coder encodeObject:sfChannelInfos];
    }
    return;
};

;

@end
