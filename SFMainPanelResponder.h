//
//  SFMainPanelResponder.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SFMainPanelResponder : NSResponder {
	IBOutlet NSTabView* sfTabView;
}

- (IBAction)showVolumeSettings:(id)sender;
- (IBAction)showChannelSettings:(id)sender;
	
@end
