//
//  SFAlignment2D.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFGeometry.h"

@class SFVoxelInfo,SFAlignment2D,SFNDDimension;

extern NSString* SFKAppleAlignedName;
extern NSString* SFKVectorAlignedName;
extern NSString* SFKPower2AlignedName;

@interface SFAlignment2D : NSObject {
	SFSize sfSize;
	NSUInteger sfBytesPerRow;
	SFVoxelInfo *sfVoxelInfo;
    
	SFRect sfRect;
}

@property (readonly) SFVoxelInfo* voxelInfo;
@property (retain) NSString *alignment;

+ (id)alignmentWithAlignment:(SFAlignment2D*)alignment;
+ (id)alignmentWithSize:(SFSize)size voxelInfo:(SFVoxelInfo*)vinfo;
+ (id)alignmentWithNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo;

-(id)initWithAlignment:(SFAlignment2D*)alignment;
-(id)initWithSize:(SFSize)size voxelInfo:(SFVoxelInfo*)vinfo;

-(SFSize)size;
-(SFSize)alignedSize;
-(SFVoxelInfo*)voxelInfo;
-(NSUInteger)bytesPerRow;
-(NSUInteger)bytesPerFrame;
-(NSUInteger)byteOffsetForPoint:(SFPoint)point;

-(void)getRealignedData:(void*)dest source:(const void*)src alignment:(SFAlignment2D*)align;
-(void)getAlignedData:(void*)dest source:(const void*)src size:(SFSize)size;

@end
