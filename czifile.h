//
//  czifile.h
//  Cytozone
//
//  Created by Kolja Wawrowsky on 4/27/16.
//  Copyright © 2016 Elarity, LLC. All rights reserved.
//

#ifndef czifile_h
#define czifile_h

#pragma pack(1)

struct _CZISegmentHeader {
    char segmentID[16];
    uint64_t allocatedSize;
    uint64_t usedSize;
};

typedef struct _CZISegmentHeader CZISegmentHeader;

struct _CZIFileHeader {
    uint32_t major;
    uint32_t minor;
    uint32_t reserved[2];
    uint8_t primaryFileGUID[16];
    uint8_t fileGUID[16];
    uint32_t filePart;
    uint64_t directoryPosition;
    uint64_t metadataPosition;
    uint32_t updatePending;
    uint64_t attachementDirectoryPosition;
};
typedef struct _CZIFileHeader CZIFileHeader;


struct _CZISubBlockSegment {
    int32_t MetadataSize;
    int32_t AttachmentSize;
    int64_t DataSize;
};
typedef struct _CZISubBlockSegment CZISubBlockSegment;

struct _CZIXmlHeader {
    uint32_t xmlsize;
    uint32_t attachementsize;
    char reserved[248];
};
typedef struct _CZIXmlHeader CZIXmlHeader;

struct _CZIXmlSegment {
    uint32_t xmlsize;
    uint32_t attachementsize;
    char reserved[248];
    char xmlString[];
};
typedef struct _CZIXmlSegment CZIXmlSegment;

struct _CZIDimensionEntryDV1 {
    char Dimension[4];
    int32_t Start;
    int32_t Size;
    float StartCoordinate;
    int32_t StoredSize;
};
typedef struct _CZIDimensionEntryDV1 CZIDimensionEntryDV1;

struct _CZIDirectoryEntryDV {
    char SchemaType[2];
    int32_t PixelType;
    int64_t FilePosition;
    int32_t FilePart;
    int32_t Compression;
    uint8_t PyramidType;
    uint8_t spare[5];
    int32_t DimensionCount;
};
typedef struct _CZIDirectoryEntryDV CZIDirectoryEntryDV;


struct _CZIDirectorySegment {
    int32_t count;
    uint8_t reserved[124];
};

typedef struct _CZIDirectorySegment CZIDirectorySegment;

#pragma pack()

#endif /* czifile_h */
