//
//  SFMovieFrameViewController.m
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFMovieFrameViewController.h"

#import "SFImageScrollView.h"
#import "SFOpenGLView.h"
#import "SFRenderSettings.h"
#import "SFAnimationKey.h"
#import "SFImageFlowView.h"

@implementation SFMovieFrameViewController

@synthesize volumeScene;
@synthesize openGLView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.  
        renderStates = [NSMutableArray new];
        defaultFrameDuration = 5.0;
    }
    return self;
}

- (IBAction) deleteAllMovieFrames:(id)sender {
    //[imageScrollView  deleteMovie];
    SFImageFlowView *documentView = [imageScrollView  documentView];
    
    [imageScrollView.animationKeys removeAllObjects];
    [documentView.images removeAllObjects];
    [renderStates removeAllObjects];

    documentView.indexSet = nil;
    [imageScrollView updateUIState];
    [documentView recalculateFrameRect];

    [imageScrollView setNeedsDisplay:YES];
    [documentView setNeedsDisplay:YES];
}

- (IBAction) deleteSelectedImages:(id)sender 
{
    SFImageFlowView *documentView = [imageScrollView  documentView];
    NSIndexSet *indexSet = documentView.indexSet;
    if(indexSet && [indexSet count] > 0) {
        [imageScrollView.animationKeys removeObjectsAtIndexes:indexSet];
        [documentView.images  removeObjectsAtIndexes:indexSet];
        [renderStates removeObjectsAtIndexes:indexSet];
        documentView.indexSet = nil;
    }
    [imageScrollView updateUIState];
    [imageScrollView setNeedsDisplay:YES];
    [documentView recalculateFrameRect];
    [documentView setNeedsDisplay:YES];
    
}

- (IBAction) restoreState:(id)sender 
{
    SFImageFlowView *documentView = [imageScrollView  documentView];
    NSUInteger index = [documentView.indexSet firstIndex];
    SFRenderSettings *renderSetting = [renderStates objectAtIndex:index];
    [renderSetting restoreStateWithVolumeScene:volumeScene];
    [openGLView setNeedsDisplay:YES];
};

- (IBAction) addMovieFrame:(id)sender 
{
    NSImage* image = [openGLView snapShot];
    SFImageFlowView *documentView = [imageScrollView  documentView];
    NSUInteger newImageIndex = 0;
    NSIndexSet *indexSet = documentView.indexSet;
    if(indexSet==nil) 
        newImageIndex = documentView.images.count;
    else 
        newImageIndex = documentView.indexSet.lastIndex+1;
    SFAnimationKey *animationKey = [SFAnimationKey new];
    animationKey.keyframeIndex = newImageIndex;
    animationKey.duration = 5.0;
    [imageScrollView.animationKeys insertObject:animationKey atIndex:newImageIndex];

    SFRenderSettings *setting = [[SFRenderSettings alloc]initWithVolumeScene:volumeScene];
    [renderStates insertObject:setting atIndex:newImageIndex];

    [documentView.images insertObject:image atIndex:newImageIndex];
    
    [imageScrollView updateUIState];
    [documentView recalculateFrameRect];
    
    if(documentView.frame.size.width > [imageScrollView contentView ].frame.size.width ) {
        NSPoint scrollPoint = NSMakePoint(documentView.frame.size.width - [imageScrollView contentView].frame.size.width , 0.0); // 
        [[imageScrollView contentView] scrollToPoint:scrollPoint];
        [imageScrollView flashScrollers];
    }

    [documentView setNeedsDisplay:YES];
    [imageScrollView setNeedsDisplay:YES];
    
}



- (void) makeMovieForPath:(NSString*)filePath {
    QTMovie * mMovie = nil;
    //char	*mktemp(char *);
    //char fileName[] = "movie.XXXXXX";
    //char *tempName = mktemp(fileName);
    //NSString *tempNameString = [NSString stringWithCString:tempName encoding:[NSString defaultCStringEncoding]];
    
    
    NSString *tempFileTemplate = [NSTemporaryDirectory() stringByAppendingPathComponent:@"movietempfile.XXXXXX"];
    const char *tempFileTemplateCString = [tempFileTemplate fileSystemRepresentation];
    char *tempFileNameCString = (char *)malloc(strlen(tempFileTemplateCString) + 1);
    strcpy(tempFileNameCString, tempFileTemplateCString);
    char *movieTempName = mktemp(tempFileNameCString);
    NSString *movieTempNameString = [NSString stringWithCString:movieTempName encoding:[NSString defaultCStringEncoding]];
    
    // Create a QTMovie with a writable data reference
    mMovie = [[QTMovie alloc] initToWritableFile:movieTempNameString error:NULL];
    // mark the movie as editable
    [mMovie setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieEditableAttribute];
    NSDictionary *movieDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                     @"mp4v",                                   QTAddImageCodecType,
                                     [NSNumber numberWithLong:codecHighQuality],QTAddImageCodecQuality,
                                     nil];
    
    // create a QTTime value to be used as a duration when adding 
    // the image to the movie
	long long timeValue = 20;
	long timeScale      = 600;
	QTTime duration     = QTMakeTime(timeValue, timeScale);


    NSOperationQueue *movieEncodeingQueue = [NSOperationQueue new];
    movieEncodeingQueue.maxConcurrentOperationCount=1;
    [movieEncodeingQueue addOperationWithBlock:^{
        [QTMovie enterQTKitOnThread];
        [mMovie attachToCurrentThread];
    }];
    [movieEncodeingQueue waitUntilAllOperationsAreFinished];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSUInteger keyFrameIndex, countOfKeyframes = renderStates.count; 
        for(keyFrameIndex=0;keyFrameIndex<countOfKeyframes-1;keyFrameIndex++) {
            SFAnimationKey *animationKey = [imageScrollView.animationKeys objectAtIndex:keyFrameIndex];
            NSUInteger frameIndex, frameCount = animationKey.duration * 30;
            SFRenderSettings *state[2];
            state[0] = [renderStates objectAtIndex:keyFrameIndex];
            state[1] = [renderStates objectAtIndex:keyFrameIndex+1];
            for (frameIndex = 0;frameIndex<frameCount;frameIndex++ ) {
                double fraction = (double)frameIndex/(double)(frameCount-1);
                SFRenderSettings *interpolatedSetting = [state[0] interpolatedStateWithFraction:fraction ofState:state[1]];
                    [interpolatedSetting restoreStateWithVolumeScene:volumeScene];
                    [openGLView drawRect:[openGLView convertRectToBacking:openGLView.bounds]];
                    NSImage *image = [openGLView snapShot];
                    [movieEncodeingQueue waitUntilAllOperationsAreFinished];
                    [movieEncodeingQueue addOperationWithBlock:^{
                        [mMovie addImage:image forDuration:duration withAttributes:movieDictionary];
                        if(keyFrameIndex==0&&frameIndex==0)
                            [mMovie updateMovieFile];
                    }];
            }
        }
        // add one more frame
        [movieEncodeingQueue waitUntilAllOperationsAreFinished];
        [movieEncodeingQueue addOperationWithBlock:^{
            NSImage *image = [openGLView snapShot];
            [mMovie addImage:image forDuration:duration withAttributes:movieDictionary];
            [mMovie updateMovieFile];
        }];
        [movieEncodeingQueue waitUntilAllOperationsAreFinished];
        [movieEncodeingQueue  addOperationWithBlock:^{
            
            NSDictionary	*dict = nil;
            dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] 
                                               forKey:QTMovieFlatten];
            // create a new movie file and flatten the movie to the file
            // passing the QTMovieFlatten attribute here means the movie
            // will be flattened
            [mMovie writeToFile:filePath withAttributes:dict];
        }];
    }];
}


- (void)mouseDown:(NSEvent *)theEvent 
{
    NSPoint curPoint = [[self view] convertPoint:[theEvent locationInWindow] fromView:nil];
    if(!NSPointInRect(curPoint, [[(SFImageScrollView*)[self view]documentView]frame])) 
    {
        [(SFImageFlowView *)[(SFImageScrollView*)[self view]documentView] setIndexSet:nil];
    }
    [super mouseDown:theEvent];   
}

@end
