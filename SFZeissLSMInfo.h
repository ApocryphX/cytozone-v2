//
//  SFZeissLSMInfo.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include <stdint.h>

#define TIF_CZ_LSMINFO 34412

struct LSMInfo {
    UInt32 u32MagicNumber; // 0x00300494C (release 1.3) or 0x00400494C (release 1.5 to 3.0).
    SInt32 s32StructureSize; // Number of bytes in the structure.
    SInt32 s32DimensionX;
    SInt32 s32DimensionY;
    SInt32 s32DimensionZ;
    SInt32 s32DimensionChannels; //Number of channels.
    SInt32 s32DimensionTime; //Number of intensity values in time-direction.
    SInt32 s32DataType; // Format of the Intensity values; 1 for 8-bit unsigned integer,
                        // 2 for 12-bit unsigned integer and //
                        // 5 for 32-bit float (for “Time Series Mean-of-ROIs”)  or
                        // 0 in case of different data types for different channels. In the latter case the field 32OffsetChannelDataTypes contains further information.
    SInt32 s32ThumbnailX; // Width in pixels of a thumbnail.
    SInt32 s32ThumbnailY; // Height in pixels of a thumbnail.
    Float64 f64VoxelSizeX; // Distance of the pixels in x-direction in meter.
    Float64 f64VoxelSizeY; // Distance of the pixels in y-direction in meter.
    Float64 f64VoxelSizeZ; // Distance of the pixels in z-direction in meter.
    Float64 f64OriginX; // Not used.
    Float64 f64OriginY; // Not used.
    Float64 f64OriginZ; // Not used.
    UInt16 u16ScanType; // Scan type
                        // 0 - normal x-y-z-scan
                        // 1 - z-Scan (x-z-plane)
                        // 2 - line scan
                        // 3 - time series x-y
                        // 4 - time series x-z (release 2.0 or later)
                        // 5 - time series “Mean of ROIs” (release 2.0 or later)
                        // 6 - time series x-y-z (release 2.3 or later)
                        // 7 - spline scan (release 2.5 or later)
                        // 8 - spline plane x-z (release 2.5 or later)
                        // 9 - time series spline plane x-z (release 2.5 or later) 10 - point mode (release 3.0 or later)
    UInt16 u16SpectralScan; // Spectral scan flag
                            // 0 – no spectral scan.
                            // 1 – image has been acquired in spectral scan mode
                            // with a META detector (release 3.0 or later).
    UInt32 u32DataType; // Data type
                        // 0 - Original scan data 1 - Calculated data
                        // 2 – Animation
    UInt32 u32OffsetVectorOverlay; // File offset to the description of the vector overlay (can be 0, if not present).
    UInt32 u32OffsetInputLut; // File offset to the channel input LUT with brightness and contrast properties (can be 0, if not present).
    UInt32 u32OffsetOutputLut; // File offset to the color palette (can be 0, if not present).
    UInt32 u32OffsetChannelColors; // File offset to the list of channel colors and channel names (can be 0, if not present).
    Float64 f64TimeIntervall; // Time interval for time series in “s” (can be 0, if it is not a time series or if there is more detailed information in u32OffsetTimeStamps).
    UInt32 u32OffsetChannelDataTypes; // File offset to an array with UINT32-values with the format of the intensity values for the respective channels (can be 0, if not present).
                                      //The contents of the array elements are:
                                      // 1 - for 8-bit unsigned integer,
                                      // 2 - for 12-bit unsigned integer and
                                      // 5 - for 32-bit float (for “Time Series Mean-of-ROIs” ).
    UInt32 u32OffsetScanInformation; // File offset to a structure with information of the device settings used to scan the image (can be 0, if not present).
    UInt32 u32OffsetKsData; // File offset to “Zeiss Vision KS-3D” specific data (can be 0, if not present).
    UInt32 u32OffsetTimeStamps; // File offset to a structure containing the time stamps for the time indexes (can be 0, if it is not a time series).
    UInt32 u32OffsetEventList; // File offset to a structure containing the experimental notations recorded during a time series (can be 0, if not present).
    UInt32 u32OffsetRoi; // File offset to a structure containing a list of the ROIs used during the scan operation (can be 0, if not present).
    UInt32 u32OffsetBleachRoi; // File offset to a structure containing a description of the bleach region used during the scan operation (can be 0, if not present).
    UInt32 u32OffsetNextRecording; // For “Time Series Mean-of-ROIs” and for „Line scans“ it is possible that a second image is stored in the file (can be 0, if not present).
                                   //For “Time Series Mean-of-ROIs” it is an image with the ROIs. For „Line scans“ it is the image with the selected line.
                                   //In these cases u32OffsetNextRecording contains a file offset to a second file header. This TIFF-header and all sub-structures are built exactly the same way as a simple LSM 510 file. All offsets without exception are given there relative to the start of the second TIFF- header.
    Float64 f64DisplayAspectX; // Zoom factor for the image display in x-direction (0.0 for release 2.3 and earlier).
    Float64 f64DisplayAspectY; // Zoom factor for the image display in y-direction (0.0 for release 2.3 and earlier).
    Float64 f64DisplayAspectZ; // Zoom factor for the image display in z-direction (0.0 for release 2.3 and earlier).
    Float64 f64DisplayAspectTime; // Zoom factor for the image display in time-direction (0.0 for release 2.3 and earlier).
    UInt32 u32OffsetMeanOfRoisOverlay; // File offset to the description of the vector overlay with the ROIs used during a scan in “Mean of ROIs” mode (can be 0, if not present).
    UInt32 u32OffsetTopoIsolineOverlay; // File offset to the description of the vector overlay for the topography–iso–lines and height display with the profile selection line (can be 0, if not present).
    UInt32 u32OffsetTopoProfileOverlay; // File offset to the description of the vector overlay for the topography–profile display (can be 0, if not present).
    UInt32 u32OffsetLinescanOverlay; // File offset to the description of the vector overlay for the line scan line selection with the selected line or Bezier curve (can be 0, if not present).
    UInt32 u32ToolbarFlags; // Bit-field for disabled toolbar buttons:
                            // bit 0 - “Corp” button
                            // bit 1 - “Reuse” button.
                            // If the bit is set the corresponding button is disabled.
    UInt32 u32OffsetChannelWavelength; // Offset to memory block with the wavelength range used during acquisition for the individual channels (new for release 3.0; can be 0, if not present).
    UInt32 u32OffsetChannelFactors; // Offset to memory block with scaling factor, offset and unit for each image channel.
    Float64 f64ObjectiveSphereCorrection; // The inverse radius of the spherical error of the objective that was used during acquisition. This is the radius of the sphere that can be fitted on the topography reconstruction of an absolutely plane object that has been recorded with the objective.
    UInt32 u32OffsetUnmixParameters; // File offset to the parameters for linear unmixing that have been used to generate the image data from scan data of the spectral detector (new for release 3.2; can be 0, if not present).
    UInt32 u32Reserved [69]; //  ￼69 reserved 32-bit words, must be 0.
};

typedef struct LSMInfo LSMInfo;

@class SFNDDimension, SFVoxelInfo,SFTIFFReader;

@interface SFZeissLSMInfo : NSObject {
	NSData* sfData;
	SFTIFFReader* sfTIFFReader;
}

- (id)initTIFFReader:(SFTIFFReader*)reader;

    //- (unsigned)numberOfChannels;
    //- (SFNDDimension*)nDDimension;
    //- (SFVoxelInfo*)voxelInfoForChannel:(unsigned)value;

@end
