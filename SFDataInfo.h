//
//  SFDataInfo.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SFDataInfo : NSObject {
	NSNumber* sfSequenceNumber;
	NSString* sfName;
	NSNumber* _dimension_count;
	NSNumber* _channel_count;
	NSString* _scan_mode;
	NSString* _voxel_type;
	BOOL sfIsInMemory;
}

- (void)setSequenceNumber:(NSNumber*)value;
- (void)setName:(NSString*)value;
- (void)setDimensionCount:(NSNumber*)value;
- (void)setChannelCount:(NSNumber*)value;
- (void)setDimensionString:(NSString*)value;
- (void)setVoxelType:(NSString*)value;
- (void)setIsInMemory:(BOOL)value;

- (NSNumber*)sequenceNumber;
- (NSString*)name;
- (NSNumber*)dimensionCount;
- (NSNumber*)channelCount;
- (NSString*)dimensionString;
- (NSString*)voxelType;
- (BOOL)isInMemory;

@end
