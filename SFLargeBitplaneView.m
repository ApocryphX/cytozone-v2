//
//  SGLargeImageView.m
//  SanFrancisco
//
//
//

#import <OpenGL/gl.h>

#import "SFLargeBitplaneView.h"
#import "SFNDImporter.h"
#import "SFNDDimension.h"

NSString *SFKTileCountKey = @"tile_count";
NSString *SFKTiledTextureColorKey = @"color";

NSString *SFKTiledTextureDataKey = @"texture_data";
NSString *SFKTiledTextureIDKey = @"texture_ids";
NSString *SFKTileDataKey = @"tile_data";
NSString *SFKOpenGLContextKey = @"opengl_context";


#define checkImageWidth 256
#define checkImageHeight 256
static GLubyte checkImage[checkImageHeight][checkImageWidth];

void makeCheckImage(void)
{
    int i, j, c;
    for (i = 0; i < checkImageHeight; i++) {
        for (j = 0; j < checkImageWidth; j++) {
            c = ((((i&0x10)==0)^(((j&0x10))==0)))*255;
            checkImage[i][j] = (GLubyte) c;
        }
    }
}

@implementation SFLargeBitplaneView

@synthesize tileArray = m_TileArray;
@synthesize imageRect = m_ImageRect;
@synthesize scaleBarLength;
@synthesize showScalebar;
@synthesize  zoom;
@synthesize  translationX;
@synthesize  translationY;



- (id) initWithFrame: (NSRect) theFrame
{
    
    NSOpenGLPixelFormatAttribute attribs [] = {
        //NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFABackingStore,
        (NSOpenGLPixelFormatAttribute)nil
    };
    NSOpenGLPixelFormat *fmt = [[NSOpenGLPixelFormat alloc] initWithAttributes: attribs];
    self = [super initWithFrame:theFrame pixelFormat:fmt];
    if(self) {
            // Make this openGL context current to the thread
            // (i.e. all openGL on this thread calls will go to this context)
        [[self openGLContext] makeCurrentContext];
            // Synchronize buffer swaps with vertical refresh rate
        GLint swapInt = 1;
        [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
        // Enable the multi-threading
        CGLEnable([[self openGLContext] CGLContextObj],kCGLCEMPEngine);
        [self setWantsBestResolutionOpenGLSurface:YES];
        m_TileArray = [NSMutableArray new];
        sfTileSize[0] = checkImageWidth;
        sfTileSize[1] = checkImageHeight;
        zoom = 1.0;
        translationX=0.0;
        translationY=0.0;
    }
    return self;
};

- (void) prepareOpenGL
{
	[super prepareOpenGL];
    // Make all the OpenGL calls to setup rendering
    // and build the necessary rendering objects
    
};

- (void)reshape {
    [super reshape];
    NSRect rect = [self convertRectToBacking:[self bounds]];
    
    [[self openGLContext] makeCurrentContext];

	CGLLockContext([[self openGLContext] CGLContextObj]);
	// set viewport to full bounding rectangle size
	// reset rotations
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glScalef(1.0, -1.0, 1.0);

    //NSLog(@"Viewport: \n origin: %f, %f\nsize: %f, %f\n",rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
	glViewport (rect.origin.x, rect.origin.y, rect.size.width, rect.size.height );
    if(rect.size.width>rect.size.height)
        glScalef(rect.size.height/rect.size.width, 1.0, 1.0);
    else
        glScalef(1.0,rect.size.width/rect.size.height, 1.0);
    glScalef(zoom,zoom, 1.0);
    glScalef(1.9,1.9, 1.0);
    if(! NSIsEmptyRect(m_ImageRect)) {
        CGFloat viewRatio = self.bounds.size.width/self.bounds.size.height;
        CGFloat imageRatio = m_ImageRect.size.width/m_ImageRect.size.height;
        if(viewRatio>1.0){
            if(imageRatio>1.0) {
                if(viewRatio>imageRatio)
                    glScaled(imageRatio,imageRatio,1.0);
                else
                    glScaled(viewRatio,viewRatio,1.0);
            }
        }
        else {
            if(imageRatio<1.0) {
                if(viewRatio>imageRatio)
                    glScaled(1.0/viewRatio,1.0/viewRatio,1.0);
                else
                    glScaled(1.0/imageRatio,1.0/imageRatio,1.0);
            }
        }
    }
    glTranslatef(translationX, translationY, 0.0);
    CGLUnlockContext([[self openGLContext] CGLContextObj]);
}

- (void)drawRect:(NSRect)rect
{
    [[self openGLContext] makeCurrentContext];

	CGLLockContext([[self openGLContext] CGLContextObj]);

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
    // enable blending
    glEnable(GL_BLEND);
    
    //enable maxiumum intensity
    glBlendEquation(GL_MAX);
    
    if(! NSIsEmptyRect(m_ImageRect)) {
        glColor3f(0.5,1.0,0.5);
        glEnable(GL_TEXTURE_RECTANGLE_EXT);
        
        @synchronized(m_TileArray) {
            for(NSDictionary *tileDictionary in m_TileArray) {
                NSColor *color = tileDictionary[SFKTiledTextureColorKey];
                color = [color colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
                CGFloat colors[4] = {1.0,1.0,1.0,1.0};
                if(color)
                    [color getRed:&colors[0] green:&colors[1] blue:&colors[2] alpha:&colors[3]];
                
                NSNumber *tileCountNumber = tileDictionary[SFKTileCountKey];
                NSUInteger tileIndex, tileCount = [tileCountNumber unsignedIntegerValue];
                NSData *tileData = tileDictionary[SFKTileDataKey];
                const SFBitplaneTile *tile = tileData.bytes;
                for(tileIndex=0;tileIndex<tileCount;tileIndex++) {
                    glColor3f(colors[0],colors[1],colors[2]);
                    NSRect drawingRect = tile[tileIndex].rect;
                    if(! NSIsEmptyRect(m_ImageRect)) {
                            // set viewport to full bounding rectangle size
                            // reset rotations
                            // draw scene
                        glBindTexture(GL_TEXTURE_RECTANGLE_EXT, tile[tileIndex].texID);
                        GLuint texcoord[2] = {(GLuint)tile[tileIndex].size[0],(GLuint)tile[tileIndex].size[1]};
                        glBegin(GL_QUADS);
                        glTexCoord2i(0, 0);
                        glVertex3f(drawingRect.origin.x,drawingRect.origin.y, 0.0);
                        glTexCoord2i(0, texcoord[1]);
                        glVertex3f(drawingRect.origin.x,drawingRect.origin.y+drawingRect.size.height, 0.0);
                        glTexCoord2i(texcoord[0], texcoord[1]);
                        glVertex3f(drawingRect.origin.x+drawingRect.size.width,drawingRect.origin.y+drawingRect.size.height, 0.0);
                        glTexCoord2i(texcoord[0], 0);
                        glVertex3f(drawingRect.origin.x+drawingRect.size.width,drawingRect.origin.y, 0.0);
                        glEnd();
                        
                        glBindTexture(GL_TEXTURE_RECTANGLE_EXT, 0);
                        /*
                         glColor3f(0.1,0.3,0.1);
                         glBegin(GL_LINE_LOOP);
                         glVertex3f(drawingRect.origin.x,drawingRect.origin.y, 0.0);
                         glVertex3f(drawingRect.origin.x,drawingRect.origin.y+drawingRect.size.height, 0.0);
                         glVertex3f(drawingRect.origin.x+drawingRect.size.width,drawingRect.origin.y+drawingRect.size.height, 0.0);
                         glVertex3f(drawingRect.origin.x+drawingRect.size.width,drawingRect.origin.y, 0.0);
                         glEnd();
                         */
                    }
                }
            }
        }

        /*
        //draw image outline  
        glColor3f(1.0,0.5,0.5);
        NSRect *drawingRect = &m_ImageRect;
        glBegin(GL_LINE_LOOP);
        glVertex3f(drawingRect->origin.x,drawingRect->origin.y, 0.0);
        glVertex3f(drawingRect->origin.x,drawingRect->origin.y+drawingRect->size.height, 0.0);
        glVertex3f(drawingRect->origin.x+drawingRect->size.width,drawingRect->origin.y+drawingRect->size.height, 0.0);
        glVertex3f(drawingRect->origin.x+drawingRect->size.width,drawingRect->origin.y, 0.0);
        glEnd();
         */
       
    }

    glDisable(GL_TEXTURE_RECTANGLE_EXT);	// finish OGL texturing
    if(showScalebar) {
        glColor3ub(255, 255, 255);
        double thickness = 0.015;
        // double length = scaleBarLength/m_ScaleFactor;
        NSPoint offset = {0.05,0.05};
        NSRect scaleBarRect = {0.5-offset.x,-0.5+offset.y,-1.0*scaleBarLength,thickness};

        glBegin(GL_QUADS);
        glVertex3f(scaleBarRect.origin.x,scaleBarRect.origin.y, 0.0);
        glVertex3f(scaleBarRect.origin.x,scaleBarRect.origin.y+scaleBarRect.size.height, 0.0);
        glVertex3f(scaleBarRect.origin.x+scaleBarRect.size.width,scaleBarRect.origin.y+scaleBarRect.size.height, 0.0);
        glVertex3f(scaleBarRect.origin.x+scaleBarRect.size.width,scaleBarRect.origin.y, 0.0);
        glEnd();
        
    }
        //glFlush();
    CGLUnlockContext([[self openGLContext] CGLContextObj]);
	[[self openGLContext] flushBuffer];
}

- (NSImage*) snapShot {
	//    struct  {
	//        uint8_t red; uint8_t green; uint8_t blue; uint8_t alpha;
	//    } *RGBA;
	NSUInteger bytesperpixel = 4;
    NSBitmapImageRep* rep;
    NSImage* ret;
    NSUInteger i,row, size;
	void *src, *dst;
    size = [self frame].size.width* [self frame].size.height * bytesperpixel;
	NSUInteger height = [self frame].size.height;
	NSUInteger width = [self frame].size.width;
	
    rep = [[NSBitmapImageRep alloc]
           initWithBitmapDataPlanes: nil  // Nil pointer tells the kit to allocate the pixel buffer for us.
           pixelsWide: width //width
           pixelsHigh: height //height
           bitsPerSample: 8
           samplesPerPixel: bytesperpixel
           hasAlpha: YES
           isPlanar: NO
           colorSpaceName: NSCalibratedRGBColorSpace // 0 = black, 1 = white in this color space.
           bytesPerRow: 0    // Passing zero means "you figure it out."
           bitsPerPixel: 8*bytesperpixel];  // This must agree with bitsPerSample and samplesPerPixel.
	
	char* pixelbuffer = malloc(size);
    memset(pixelbuffer, 0, size);
    [[self openGLContext] makeCurrentContext];

    CGLLockContext([[self openGLContext] CGLContextObj]);
    glReadPixels ( 0, 0,(GLsizei) width,(GLsizei)height, GL_RGBA, GL_UNSIGNED_BYTE, pixelbuffer);
    CGLUnlockContext([[self openGLContext] CGLContextObj]);
    
	for (i=3;i<size;i+=4) {
		pixelbuffer[i]=255;
	}
	for (row=0;row < height;row++) {
		src = pixelbuffer+ width*bytesperpixel*row;
		dst = (void*)[rep bitmapData]+([rep bytesPerRow]*(height-1-row));
		memcpy(dst,src,width*bytesperpixel);
	}
	free(pixelbuffer);
    ret = [ [NSImage alloc] init];
    [ret addRepresentation:rep];
    return ret;
};


- (NSMutableDictionary*)createTiledTexturesForImage:(vImage_Buffer)imageBuffer imageRect:(NSRect)imageRect {
    
    NSUInteger gridSize[2];
    gridSize[0] = (imageBuffer.width+(sfTileSize[0]-1))/sfTileSize[0];
    gridSize[1] = (imageBuffer.height+(sfTileSize[1]-1))/sfTileSize[1];
    
    NSMutableDictionary *tiledImageDictionary = [NSMutableDictionary new];
    
    NSUInteger textureCount = gridSize[0]*gridSize[1];
    tiledImageDictionary[SFKTileCountKey] = [NSNumber numberWithUnsignedInteger:textureCount];
    
    NSMutableData *textureData = [NSMutableData dataWithLength:imageBuffer.width*imageBuffer.height];
    tiledImageDictionary[SFKTiledTextureDataKey] = textureData;
    
    NSMutableData *tileData = [NSMutableData dataWithLength:sizeof(SFBitplaneTile)*textureCount];
    tiledImageDictionary[SFKTileDataKey] = tileData;
    
    NSMutableData *texIDData = [NSMutableData dataWithLength:textureCount*sizeof(GLuint)];
    tiledImageDictionary[SFKTiledTextureIDKey] = texIDData;
    
    [[self openGLContext] makeCurrentContext];
    CGLLockContext([[self openGLContext] CGLContextObj]);

    glGenTextures((GLuint)textureCount, texIDData.mutableBytes);


    // Enable the rectangle texture extenstion
    glEnable(GL_TEXTURE_RECTANGLE_EXT);
    // Eliminate a data copy by the OpenGL driver using the Apple texture range extension along with the rectangle texture extension
    // This specifies an area of memory to be mapped for all the textures. It is useful for tiled or multiple textures in contiguous memory.
    glTextureRangeAPPLE(GL_TEXTURE_RECTANGLE_EXT, (GLsizei)textureData.length, textureData.mutableBytes);
    
    NSSize fractionalTileSize = NSMakeSize((double)sfTileSize[0]/(double)imageBuffer.width,
                                           (double)sfTileSize[1]/(double)imageBuffer.height);
    NSSize scaledTileSize = NSMakeSize(fractionalTileSize.width*imageRect.size.width,
                                       fractionalTileSize.height*imageRect.size.height);
    
    
    NSUInteger gridIndex[2];
    SFBitplaneTile *tileArray = tileData.mutableBytes;
    GLuint *texIDs = texIDData.mutableBytes;
    void *pTextureBuffer = textureData.mutableBytes;
    void *pImageBuffer = imageBuffer.data;
    SFBitplaneTile *tile = tileData.mutableBytes;
    
    for(gridIndex[1]=0;gridIndex[1]<gridSize[1];gridIndex[1]++) {
        NSUInteger heightOffset = gridIndex[1]*imageBuffer.rowBytes*sfTileSize[1];
        for(gridIndex[0]=0;gridIndex[0]<gridSize[0];gridIndex[0]++) {
            NSUInteger widthOffset = gridIndex[0]*sfTileSize[0];
            NSUInteger tileIndex = gridIndex[1]*gridSize[0]+gridIndex[0];
            // calculate the floating point coordinated of texture tiles
            tileArray[tileIndex].rect.size = scaledTileSize;
            tileArray[tileIndex].rect.origin = NSMakePoint(scaledTileSize.width*gridIndex[0]+imageRect.origin.x,
                                                           scaledTileSize.height*gridIndex[1]+imageRect.origin.y);
            // clip the texture tile to the image boundaries
            tileArray[tileIndex].rect = NSIntersectionRect(m_ImageRect, tileArray[tileIndex].rect);
            // texture tile size: full tile or partial tile if the tile is clipped
            tile[tileIndex].size[0] = MIN(sfTileSize[0],imageBuffer.width-sfTileSize[0]*gridIndex[0]);
            tile[tileIndex].size[1] = MIN(sfTileSize[1],imageBuffer.height-sfTileSize[1]*gridIndex[1]);
            
            NSUInteger lineIndex, lineCount = tile[tileIndex].size[1]; // number of line to copy
            NSUInteger bytesToCopy = tile[tileIndex].size[0]; // number of bytes per line
            NSUInteger srcStride = imageBuffer.rowBytes; //checkImageWidth; //
            NSUInteger dstStride = tile[tileIndex].size[0];
            void *src = pImageBuffer+heightOffset+widthOffset;//checkImage;
            void *dst = pTextureBuffer;
            for (lineIndex=0;lineIndex<lineCount;lineIndex++) {
                memcpy(dst,src,bytesToCopy);
                src+=srcStride;
                dst+=dstStride;
            }
            
            tileArray[tileIndex].texID = texIDs[tileIndex];
            glBindTexture(GL_TEXTURE_RECTANGLE_EXT, tileArray[tileIndex].texID);
            glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            if(tile[tileIndex].size[0]%4==0)
                glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
            else
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTexImage2D(GL_TEXTURE_RECTANGLE_EXT, 0, GL_INTENSITY,(GLsizei)tile[tileIndex].size[0],(GLsizei)tile[tileIndex].size[1],0, GL_LUMINANCE, GL_UNSIGNED_BYTE, pTextureBuffer);
            pTextureBuffer+=lineCount*bytesToCopy;
        }
    }
    glBindTexture(GL_TEXTURE_RECTANGLE_EXT, 0);
    CGLUnlockContext([[self openGLContext] CGLContextObj]);
        //[m_TileArray addObject:tiledImageDictionary];
    return tiledImageDictionary;
}


- (void)deleteTiledTexturesForDictionary:(NSMutableDictionary*)textureDictionary {
    [self.openGLContext makeCurrentContext];

    CGLLockContext([self.openGLContext CGLContextObj]);
    NSData *textureIDData = textureDictionary[SFKTiledTextureIDKey];
    if(textureIDData) {
        GLsizei tileCount = (GLsizei)textureIDData.length/sizeof(GLuint);
        glDeleteTextures(tileCount, textureIDData.bytes);
    }
    CGLUnlockContext([self.openGLContext CGLContextObj]);
};


/*

-(void)removeImageAtIndex:(NSUInteger)index {
    @synchronized (m_TileArray) {
        NSDictionary *tileDictionary = [m_TileArray objectAtIndex:index];
        NSData *textureIDData = tileDictionary[SFKTiledTextureIDKey];
        GLsizei tileCount = (GLsizei)textureIDData.length/sizeof(GLuint);
        NSOpenGLContext *context = [super openGLContext];
        CGLLockContext([context CGLContextObj]);
        [context makeCurrentContext];
        glDeleteTextures(tileCount, textureIDData.bytes);
        CGLUnlockContext([context CGLContextObj]);
        [m_TileArray removeObjectAtIndex:index];
    }
}
*/

- (void)magnifyWithEvent:(NSEvent *)event {
    zoom = ([event magnification] + 1.0)*zoom;
    [self reshape];
    self.needsDisplay=YES;
}

- (void)scrollWheel:(NSEvent *)theEvent {
	translationX = [theEvent deltaX]/200.0/zoom+translationX;
    translationY = [theEvent deltaY]*-1.0/-200.0/zoom+translationY;
    [self reshape];
    [self setNeedsDisplay:YES];
}



-(void)removeAllImages {
    NSArray *oldTiles;
    @synchronized (m_TileArray) {
        oldTiles = m_TileArray;
        m_TileArray = [NSMutableArray new];
    }
    for(NSMutableDictionary *tileDictionary in oldTiles) {
        [self deleteTiledTexturesForDictionary:tileDictionary];
    }
    
}


@end
