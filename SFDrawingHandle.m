//
//  SFDrawingHandle.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFDrawingHandle.h"

@implementation SFDrawingHandle

@dynamic position;
@synthesize size;
@synthesize color;
@synthesize highlightColor;
@synthesize shadowColor;
@synthesize bounds;

- (id)init
{
    self = [super init];
    if (self) {
        size = NSMakeSize(6.0,6.0);
        color = [NSColor redColor];
        highlightColor = [NSColor orangeColor];
        shadowColor = [NSColor darkGrayColor];
    }
    return self;
}

+ (id)drawingHandleAtPoint:(NSPoint)point {
    SFDrawingHandle *drawingHandle = [SFDrawingHandle new];
    if(drawingHandle) {
        drawingHandle.position = point;
    }
    return drawingHandle;
}

- (void)setPosition:(NSPoint)position {
    sfPosition = position;
    // Figure out a rectangle that's centered on the point but lined up with device pixels.
    NSRect handleBounds;
    handleBounds.origin.x = position.x - size.width/2.0;
    handleBounds.origin.y = position.y - size.height/2.0;
    handleBounds.size.width = size.width;
    handleBounds.size.height = size.height;
    self.bounds = handleBounds;
}

- (NSPoint)position {
    return sfPosition;
}

- (void)draw {
    // Draw the shadow of the handle.
    NSRect shadowBounds = NSOffsetRect(self.bounds, 1.0f, 1.0f);
    [shadowColor set];
    NSRectFill(shadowBounds);
    [NSBezierPath strokeRect:shadowBounds];
    // Draw the handle itself.
    [color set];
    NSRectFill(self.bounds);
    [NSBezierPath strokeRect:self.bounds];
}



@end
