//
//  SFOGLTransform.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

enum SFTrackMode {
	kRotation = 0,
	kZoom = 1,
	kTranslation = 2,
};

@class SFOGLRotation, SFTrackball;

@interface SFOGLTransform : NSObject {
	SFOGLRotation* sfTrackRotation;
	SFTrackball* sfTrackball;

	SFOGLRotation* sfRotation;
	NSPoint sfTranslation;
	float sfZoom;
	
	enum SFTrackMode sfTrackMode;
	NSPoint sfStartPoint;
	NSPoint sfOldTranslation;
	float sfOldZoom;
	NSSize sfSize;
}

- (void) useZoom;
- (void) useRotation;
- (void) useTranslation;

- (void)setTrackmode:(enum SFTrackMode)mode;
- (enum SFTrackMode)trackMode;

- (void)startTracking:(NSPoint)point inRectangle:(NSRect)rect;	
- (void)draggedTo:(NSPoint)point;
- (void)stopTracking;

- (void)setTransform:(SFOGLTransform*)value;

- (void)setZoom:(float)value;
- (void)setTranslation:(NSPoint)value;
- (void)setRotation:(SFOGLRotation*)value;

- (float)zoom;
- (NSPoint)translation;
- (SFOGLRotation*)rotation;

- (SFOGLTransform*)interpolatedTransformWithFraction:(float)frac ofTransform:(SFOGLTransform*)transform;

- (void)execute;
- (void)executeRotation;

@end
