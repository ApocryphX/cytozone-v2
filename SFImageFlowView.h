//
//  SFImageFlowView.h
//  Image Flow
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFImageFlowView : NSView {
    
    CGFloat imagePadding;
    NSMutableIndexSet *indexSet;
    NSMutableArray *images;
}

@property (assign) CGFloat imagePadding;
@property (retain) NSMutableIndexSet *indexSet;
@property (readonly) NSMutableArray *images;

- (void) recalculateFrameRect;

@end
