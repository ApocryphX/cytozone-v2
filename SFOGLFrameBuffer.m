//
//  SFOGLFrameBuffer.m
//  Cytozone
//
//  Created by Kolja Wawrowsky on 11/2/16.
//  Copyright © 2016 Elarity, LLC. All rights reserved.
//

#import "SFOGLFrameBuffer.h"

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

@implementation SFOGLFrameBuffer


- (void) fragment {
    
        NSOpenGLPixelFormatAttribute attrs[] = {
            
            // Specifying "NoRecovery" gives us a context that cannot fall back to the software renderer.  This makes the View-based context a compatible with the layer-backed context, enabling us to use the "shareContext" feature to share textures, display lists, and other OpenGL objects between the two.
            NSOpenGLPFANoRecovery, // Enable automatic use of OpenGL "share" contexts.
            
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADepthSize, 16,
            NSOpenGLPFADoubleBuffer,
            NSOpenGLPFAAccelerated,
            NSOpenGLProfileVersion4_1Core,
            0
        };
        NSOpenGLPixelFormat* pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
        NSOpenGLContext *context = [[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil];
        [context makeCurrentContext];
        
        NSRect bounds = NSMakeRect(0.0, 0.0, 1024, 768); //[self bounds];
        int height = bounds.size.height;
        int width = bounds.size.width;
        
        
        NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc]
                                      initWithBitmapDataPlanes:NULL
                                      pixelsWide:width
                                      pixelsHigh:height
                                      bitsPerSample:8
                                      samplesPerPixel:4
                                      hasAlpha:YES
                                      isPlanar:NO
                                      colorSpaceName:NSDeviceRGBColorSpace
                                      bytesPerRow:4 * width
                                      bitsPerPixel:0
                                      ];
        
        GLuint framebuffer, renderbuffer;
        GLenum status;
        // Set the width and height appropriately for your image
        GLuint imageWidth = width, imageHeight = height;
        //Set up a FBO with one renderbuffer attachment
        glGenFramebuffers(1, &framebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER_EXT, framebuffer);
        glGenRenderbuffers(1, &renderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, imageWidth, imageHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER, renderbuffer);
    
    // for depth buffer attachement
    
    // create a renderbuffer object to store depth info
    GLuint rboId;
    glGenRenderbuffersEXT(1, &rboId);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, rboId);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, imageWidth, imageHeight);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
    
    // attach the renderbuffer to depth attachment point
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, rboId);
    
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
            //CHECK_GL_ERROR();
            // Handle errors
        }
        //Your code to draw content to the renderbuffer
    
        glBindFramebuffer( GL_FRAMEBUFFER, framebuffer );
        glDrawBuffer(GL_COLOR_ATTACHMENT0);

    
        glViewport( 0.0, 0.0, width, height );
        
        glMatrixMode( GL_PROJECTION );
        glLoadIdentity();
        //gluPerspective( 30, width / height, 0.5, 1000.0 );
        
        glMatrixMode( GL_MODELVIEW );
        glLoadIdentity();
        
        glClearColor(0.0, 1.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glRotatef(45.0, 0.0, 0.0, 1.0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 0.0, 0.0);
        glVertex2f(-0.5, -0.5);
        glVertex2f(-0.5,  0.5);
        glVertex2f( 0.5,  0.5);
        glVertex2f( 0.5, -0.5);
        glEnd();
        glPopMatrix();
        
        glFinish();
        
        //Your code to use the contents
        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, [imageRep bitmapData]);
        
        // Make the window the target
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        // Delete the renderbuffer attachment
        glDeleteRenderbuffers(1, &renderbuffer);
        glDeleteFramebuffers(1, &framebuffer);
    
        NSImage *renderedImage=[[NSImage alloc] initWithSize:NSMakeSize(width,height)];
        [renderedImage addRepresentation:imageRep];
        //[renderedImage setFlipped:YES];
        //[renderedImage lockFocusOnRepresentation:imageRep]; // This will flip the rep.
        //[renderedImage unlockFocus];
    
    //read directly from pixel buffer, map the buffer
        void *pixelBuffer = glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
    //read pixels here
    // unmap teh buffer when done
        glUnmapBuffer (GL_PIXEL_PACK_BUFFER);
        return;// renderedImage;
        
    
}
@end
