//
//  SFOGLVolumeProcessing.m
//  CytoFX
//
//  Created by ApocryphX on 9/25/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolume.h"
#import "SFNDVoxelData.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFVoxelProcessorFactory.h"
#import "SFVoxelProcessor.h"
#import "SFOGLTextureRectangle.h"
#import "SFOGLTextureStack3D.h"
#import "SFOGLTexture3D.h"
#import "SFOGLBackplane.h"
#import "SFVoxelData.h"
#import "SFAlignment2D.h"
#import "SFOGLChannelSetting.h"

@implementation SFOGLVolume (Processing)

- (void)processAllChannels {
	SFNDVoxelData* volume = [sfNDVoxelData sliceAtRank:2 atIndex:sfVolumeIndex];
	NSUInteger i, count = [[volume channels]count];
	for (i=0;i<count;i++) {
		[self processChannel:i];
	}
};


- (void)processChannel:(NSUInteger)idx {

	SFNDVoxelData* volume = [sfNDVoxelData sliceAtRank:2 atIndex:sfVolumeIndex]; 
	if( idx >= [[volume channels]count])
		return;
	NSUInteger framesperstack = [volume extentAtIndex:2];
	NSUInteger j;
	
//	@autoreleasepool {
		SFVoxelData* srcvdata = [[volume channels]objectAtIndex:idx];
		SFOGLTextureStack3D* tex3d = [sfVolumeTextures objectAtIndex:idx];
		SFOGLChannelSetting* channelsetting = [sfChannelSettings objectAtIndex:idx];
		
		SFAlignment2D* alignment = [SFAlignment2D alignmentWithAlignment:[tex3d alignment]];
		[alignment setAlignment:SFKVectorAlignedName];
		NSMutableData* backdrop = [NSMutableData dataWithLength:[alignment bytesPerFrame]];
		
		Class <SFVoxelProcessing> proclass = [SFVoxelProcessorFactory classForAlignment:[srcvdata alignment]];
		NSMutableData* tempdata[2];
		tempdata[0] = [NSMutableData dataWithLength:[alignment bytesPerFrame]];
		tempdata[1] = [NSMutableData dataWithLength:[alignment bytesPerFrame]];
		for (j=0;j<framesperstack;j++) {
			NSData *srcdata; 
			NSMutableData *dstdata = tempdata[0];
			if (sfIsZInverted)
				srcdata = [srcvdata dataAtFrameIndex:framesperstack-j-1];
			else
				srcdata = [srcvdata dataAtFrameIndex:j];
			
			if([tex3d isDownsampled]) {
				[proclass getScaled:[dstdata mutableBytes] 
							  scale:alignment 
							 source:[srcdata bytes]
						  alignment:[srcvdata alignment] ];
				if (dstdata == tempdata[0]) {
					dstdata = tempdata[1];
					srcdata = tempdata[0];
				}
				else {
					dstdata = tempdata[0];
					srcdata = tempdata[1];
				}
			}
			
			if( [channelsetting noiseFilterSize] > 1 ) {
				[proclass removeNoise:[dstdata mutableBytes] 
							   source:[srcdata bytes]
							alignment:alignment 
						   kernelSize:(uint32_t)[channelsetting noiseFilterSize] ];
				if (dstdata == tempdata[0]) {
					dstdata = tempdata[1];
					srcdata = tempdata[0];
				}
				else {
					dstdata = tempdata[0];
					srcdata = tempdata[1];
				}
			}
			if( [channelsetting isIntensitySquare]) {
				[proclass getSquareRoot:[dstdata mutableBytes] source:[srcdata bytes] alignment:alignment];
				if (dstdata == tempdata[0]) {
					dstdata = tempdata[1];
					srcdata = tempdata[0];
				}
				else {
					dstdata = tempdata[0];
					srcdata = tempdata[1];
				}					
			}
			switch ([channelsetting volumeMode]) {
				case 0: {
					break;
				}
				case 1: {
					[proclass getBinary:[dstdata mutableBytes] 
								 source:[srcdata bytes]
							  alignment:alignment 
							  threshold:[channelsetting threshold]];
					if (dstdata == tempdata[0]) {
						dstdata = tempdata[1];
						srcdata = tempdata[0];
					}
					else {
						dstdata = tempdata[0];
						srcdata = tempdata[1];
					}
					[proclass edgeDetect:[dstdata mutableBytes] source:[srcdata bytes] alignment:alignment];
					if (dstdata == tempdata[0]) {
						//dstdata = tempdata[1];
						srcdata = tempdata[0];
					}
					else {
						//dstdata = tempdata[0];
						srcdata = tempdata[1];
					}
					break;
				}
				case 2: {
					NSData *top, *bottom;
					[proclass getBinary:[dstdata mutableBytes] 
								 source:[srcdata bytes]
							  alignment:alignment 
							  threshold:[channelsetting threshold]];
					if (dstdata == tempdata[0]) {
						dstdata = tempdata[1];
						srcdata = tempdata[0];
					}
					else {
						dstdata = tempdata[0];
						srcdata = tempdata[1];
					}
					if ( j>0 && j<(framesperstack-1) ) {
						[proclass edgeDetect:[dstdata mutableBytes] source:[srcdata bytes] alignment:alignment];
						if (sfIsZInverted) {
							top = [srcvdata dataAtFrameIndex:framesperstack-j-2];
							bottom = [srcvdata dataAtFrameIndex:framesperstack-j];
						}
						else {
							top = [srcvdata dataAtFrameIndex:j-1];
							bottom = [srcvdata dataAtFrameIndex:j+1];					
						}
						[proclass getSurface:[dstdata mutableBytes] 
										 top:[top bytes]
									  middle:[srcdata bytes]
									  bottom:[bottom bytes]
								   alignment:alignment ];
						if (dstdata == tempdata[0]) {
							//dstdata = tempdata[1];
							srcdata = tempdata[0];
						}
						else {
							//dstdata = tempdata[0];
							srcdata = tempdata[1];
						}					
					}
					break;
				}
				case 3: {
					[proclass getBinary:[dstdata mutableBytes] 
								 source:[srcdata bytes]
							  alignment:alignment 
							  threshold:[channelsetting threshold]];
					if (dstdata == tempdata[0]) {
						//dstdata = tempdata[1];
						srcdata = tempdata[0];
					}
					else {
						//dstdata = tempdata[0];
						srcdata = tempdata[1];
					}				
					break;
				}				
			}
			[tex3d replaceBytes:[srcdata bytes] alignment:alignment index:framesperstack-j-1];
			[proclass getMaximum:[backdrop mutableBytes] source:[srcdata bytes] alignment:alignment];
		}
    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
    tex3d = [sfVolumeTextures objectAtIndex:0];
    if([tex3d isDownsampled])
        [sfBackplane replaceBytes:[backdrop mutableBytes] alignment:alignment index:idx];
    else
        [sfBackplane replaceBytes:[backdrop mutableBytes] alignment:[srcvdata alignment] index:idx];
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
    
};

@end

