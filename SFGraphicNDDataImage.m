//
//  SFGraphicNDDataImage.m
//  SanFrancisco
//
//
//

#import "SFGraphicNDDataImage.h"

#import "SFNDImporter.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFNDVoxelData.h"

@implementation SFGraphicNDDataImage

@dynamic nodeIndex;
@synthesize importer;

- (id)init
{
    self = [super init];
    if (self) {
        NSOpenGLPixelFormatAttribute attrs[] = {
            NSOpenGLPFAColorSize, 24,
            NSOpenGLPFAAlphaSize, 8,
            NSOpenGLPFADepthSize, 16,
            NSOpenGLPFADoubleBuffer,
            NSOpenGLPFAAccelerated,
            0
        };
        NSOpenGLPixelFormat* pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
        openGLContext = [[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil];
    }
    return self;
}

- (void)createTextures {
    if(importer) {
        [openGLContext makeCurrentContext];
        nDVoxelData =  [importer readNDVoxelDataAtNodeIndex:sfNodeIndex];
        NSUInteger channelIndex;
        SFNDDimension *nDDimension = nDVoxelData.nDDimension;
        if(textureCount>0&&textureIDs!=0) {
            glDeleteTextures(textureCount, textureIDs);
            free(textureIDs);
            textureCount=0;
            textureIDs=NULL;
        }
        channelCount = nDVoxelData.channels.count;
        textureIDs = malloc(channelCount*sizeof(GLuint));
        glGenTextures ((GLuint)channelCount,textureIDs);
        
        for(channelIndex=0;channelIndex<channelCount;channelIndex++) {
            
            glBindTexture (GL_TEXTURE_2D,textureIDs[channelIndex]);
            
            glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
            glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
            glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            
            GLuint width = (GLuint)[nDDimension extentAtIndex:0];
            GLuint height = (GLuint)[nDDimension extentAtIndex:1];
            SFVoxelData *vdata = [nDVoxelData voxelDataAtIndex:channelIndex];
            void *pImageData = (void*)vdata.bytes;
            glTexImage2D (GL_TEXTURE_2D,0,GL_LUMINANCE,width,height,0, GL_LUMINANCE,GL_UNSIGNED_BYTE, pImageData);
        }
            // [self setNeedsDisplay:YES];
    }
}

- (void)createRenderBuffer {
    [openGLContext makeCurrentContext];
    
    NSRect bounds = [self bounds];
    int height = bounds.size.height;
    int width = bounds.size.width;
    
    if(renderbuffer)
        glDeleteRenderbuffers(1, &renderbuffer);
    if(framebuffer)
        glDeleteFramebuffers(1, &framebuffer);
    
    GLenum status;
        // Set the width and height appropriately for your image
        //Set up a FBO with one renderbuffer attachment
    glGenFramebuffersEXT(1, &framebuffer);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebuffer);
    glGenRenderbuffersEXT(1, &renderbuffer);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, renderbuffer);
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_RGBA8, width, height);
    glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
                                 GL_RENDERBUFFER_EXT, renderbuffer);
    status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
    if (status != GL_FRAMEBUFFER_COMPLETE_EXT){
            // Handle errors
    }
       
}

- (void)renderImage
{
    [openGLContext makeCurrentContext];
	CGLLockContext([openGLContext CGLContextObj]);

    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebuffer);
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, renderbuffer);

    GLsizei h,w;
    h = self.bounds.size.height;
    w = self.bounds.size.width;
    glViewport (0,0,(GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glTranslatef(-1.0,-1.0, 0.0);
    glScalef(2.0, 2.0, 1.0);
        //gluPerspective (60.0, (GLfloat) w/(GLfloat) h, 1.0,30.0);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
        // enable blending
    glEnable(GL_BLEND);
        //enable maxiumum intensity
    glBlendEquation(GL_MAX);
    
        // turn on texture
    glEnable (GL_TEXTURE_2D);
    
    NSUInteger channelIndex;
    channelCount = nDVoxelData.channels.count;
    
    for(channelIndex=0;channelIndex<channelCount;channelIndex++)  {
        glBindTexture (GL_TEXTURE_2D, textureIDs[channelIndex]);
        
        NSColor *color = [importer colorAtNodeIndex:sfNodeIndex channelIndex:channelIndex];
        color = [color colorUsingColorSpace:[NSColorSpace genericRGBColorSpace]];
        float colorArray[3] = {color.redComponent,color.greenComponent,color.blueComponent};
        glColor3f(colorArray[0],colorArray[1],colorArray[2]);
        
            // attach to polygons = note that have only one
            // texture but two different polygons
        glBegin (GL_QUADS);
            // first checker board parallel to screen
        glTexCoord2f(0.0,0.0); glVertex3f (0.0,0.0,0.0);
        glTexCoord2f(0.0,1.0); glVertex3f (0.0,1.0,0.0);
        glTexCoord2f(1.0,1.0); glVertex3f (1.0,1.0,0.0);
        glTexCoord2f(1.0,0.0); glVertex3f (1.0,0.0,0.0);
        glEnd();
        
    }
    glFlush();
    glDisable(GL_TEXTURE_2D);
    
    [openGLContext flushBuffer];
    
    NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc]
                                  initWithBitmapDataPlanes:NULL
                                  pixelsWide:w
                                  pixelsHigh:h
                                  bitsPerSample:8
                                  samplesPerPixel:4
                                  hasAlpha:YES
                                  isPlanar:NO
                                  colorSpaceName:NSDeviceRGBColorSpace
                                  bytesPerRow:4 * w
                                  bitsPerPixel:0
                                  ];

    glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, [imageRep bitmapData]);
    
        // Make the window the target
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
        // Delete the renderbuffer attachment
    glDeleteRenderbuffersEXT(1, &renderbuffer);
    
    CGLUnlockContext([openGLContext CGLContextObj]);

    NSImage *renderedImage=[[NSImage alloc] initWithSize:NSMakeSize(w,h)];
    [renderedImage addRepresentation:imageRep];
    [renderedImage setFlipped:YES];
    [renderedImage lockFocusOnRepresentation:imageRep]; // This will flip the rep.
    [renderedImage unlockFocus];
    self.image = renderedImage;
}

- (void)draw
{
        //[image drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    
        // Or stretch image to fill view
    if(_image)
        [_image drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
}


#pragma mark NSOutlineViewDelegate methods


- (void)setNodeIndex:(NSUInteger)nodeIndex {
    sfNodeIndex = nodeIndex;
}

- (NSUInteger)nodeIndex {
    return sfNodeIndex;
}

@end
