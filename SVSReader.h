//
//  SVSReader.h
//  Cytozone
//
//  Created by Kolja Wawrowsky on 3/31/15.
//  Copyright (c) 2015 Elarity, LLC. All rights reserved.
//

#ifndef __Cytozone__SVSReader__
#define __Cytozone__SVSReader__

#include <stdio.h>

#define SVSHandle void *

SVSHandle SVSOpen(const char *path);
void SVSClose(SVSHandle handle);

#endif /* defined(__Cytozone__SVSReader__) */

