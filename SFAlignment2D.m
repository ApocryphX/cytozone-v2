//
//  SFAlignment2D.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFVoxelInfo.h"
#import "SFAlignment2D.h"
#import "SFNDDimension.h"

@implementation SFAlignment2D

NSString *SFKNotAlignedName = @"notaligned";
NSString* SFKAppleAlignedName = @"apple";
NSString* SFKVectorAlignedName = @"vector";
NSString* SFKPower2AlignedName = @"power2";

@synthesize voxelInfo = sfVoxelInfo;

- (void) recalculate
{
    sfRect.size.width = sfSize.width;
    sfRect.size.height = sfSize.height;
    sfRect.origin.x = 0;
    sfRect.origin.y = 0;
    sfBytesPerRow = sfRect.size.width * [sfVoxelInfo bytesPerVoxel];

	if ([_alignment isEqualToString:SFKPower2AlignedName] ) {
		sfRect.size.width = pow(2.0f, ceil(log2( (double) sfSize.width)));
		sfRect.size.height = pow(2.0f, ceil(log2( (double) sfSize.height)));	
		sfRect.origin.x = (sfRect.size.width - sfSize.width) / 2;
		sfRect.origin.y = (sfRect.size.height - sfSize.height) / 2;
	}
	if ([_alignment isEqualToString:SFKVectorAlignedName] ) {
		sfBytesPerRow = (sfBytesPerRow+15) & ~15;
		while (0==(sfBytesPerRow&(sfBytesPerRow-1)))
			sfBytesPerRow += 16;
	}
    if ([_alignment isEqualToString:SFKAppleAlignedName] ) {
        if((sfBytesPerRow%4) != 0)
            sfBytesPerRow = sfBytesPerRow + 4 - sfBytesPerRow%4;
    }
};

-(id)initWithAlignment:(SFAlignment2D*)alignment 
{
	self = [super init];
	sfSize = alignment->sfSize;
	sfVoxelInfo = alignment->sfVoxelInfo;
    _alignment = alignment->_alignment;
	[self recalculate];
	return self;
};

-(id)initWithSize:(SFSize)size voxelInfo:(SFVoxelInfo*)vinfo {
	self = [super init];
	sfSize = size;
	sfVoxelInfo = vinfo;
	[self recalculate];
	return self;
};

+ (id)alignmentWithAlignment:(SFAlignment2D*)alignment {
	SFAlignment2D* ret = [[SFAlignment2D alloc]initWithAlignment:alignment];
	return ret;
};

+ (id)alignmentWithSize:(SFSize)size voxelInfo:(SFVoxelInfo*)vinfo {
	SFAlignment2D* ret = [[SFAlignment2D alloc]initWithSize:size voxelInfo:vinfo];
	return ret;	
};

+ (id)alignmentWithNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo {
	SFSize size = {[nddim extentAtIndex:0], [nddim extentAtIndex:1]	};
	SFAlignment2D* ret = [[SFAlignment2D alloc]initWithSize:size voxelInfo:vinfo];
	return ret;	
	
};

-(SFSize)size
{
	return sfSize;
};

-(SFSize)alignedSize
{
	return sfRect.size;
};

-(SFVoxelInfo*)voxelInfo {
	return sfVoxelInfo;
};

-(NSUInteger)bytesPerRow {
	return sfBytesPerRow;
};

-(NSUInteger)bytesPerFrame {
	return sfBytesPerRow * sfRect.size.height;
};

-(NSUInteger)byteOffsetForPoint:(SFPoint)point {
	return (sfRect.origin.y + point.y) * sfBytesPerRow + (sfRect.origin.x + point.x) * [sfVoxelInfo bytesPerVoxel];
};

-(void)getRealignedData:(void*)dest source:(const void*)src alignment:(SFAlignment2D*)align
{
	NSUInteger width = sfSize.width <= [align size].width ? sfSize.width : [align size].width;
	NSUInteger height = sfSize.height <= [align size].height ? sfSize.height : [align size].height;
	NSUInteger vsize = [sfVoxelInfo bytesPerVoxel];
	if (sfRect.origin.y>0)
		memset(dest, 0, sfBytesPerRow*sfRect.origin.y);
	SFPoint point;
	point.x = 0;
	for (point.y=0; point.y<height; point.y++) { 
		memset ( dest + vsize*(sfRect.origin.y+point.y), 0, sfBytesPerRow); 
		memcpy ( dest+[self byteOffsetForPoint:point],
				 src+[align byteOffsetForPoint:point],
				 width*vsize);
	}
	NSUInteger memsize = (sfRect.origin.y+point.y)*sfBytesPerRow;
	if (memsize<[self bytesPerFrame])
		memset(dest+memsize, 0, [self bytesPerFrame] - memsize);
};

-(void)getAlignedData:(void*)dest source:(const void*)src size:(SFSize)size 
{
	SFAlignment2D* align = [[SFAlignment2D alloc] initWithSize:size voxelInfo:sfVoxelInfo];
	[self getRealignedData:dest source:src alignment:align];
};


@end
