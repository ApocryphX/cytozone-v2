/*
 *  SFOGLTexturing.h
 *  CytoFX
 *
 *  Copyright 2014 Elarity, LLC. All rights reserved.
 *
 */

#import <Cocoa/Cocoa.h>

@class SFNDDimension, SFNDVoxelData, SFVoxelData, SFAlignment2D, SFAlignment2D, SFVoxelInfo;

@protocol SFOGLTexturing3D

- (void)setNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo;

- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment index:(NSUInteger)idx;

- (SFNDDimension*) nDDimension;
- (SFAlignment2D*) alignment;
- (BOOL) isDownsampled;

@end


