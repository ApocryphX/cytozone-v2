/*
 *  utility.c
 *  VoxelMachine
 *
 *  Copyright 2014 Elarity, LLC. All rights reserved.
 *
 */

#import "utility.h"

const float mat[3][5][3] = {
{ { 0.0f,0.0f,0.0f },{ 0.0f,1.0f,0.0f },{ 0.0f,1.0f,1.0f },{ 0.0f,0.0f,1.0f },{ 1.0f,0.0f,0.0f } },
{ { 0.0f,0.0f,0.0f },{ 0.0f,0.0f,1.0f },{ 1.0f,0.0f,1.0f },{ 1.0f,0.0f,0.0f },{ 0.0f,1.0f,0.0f } },
{ { 0.0f,0.0f,0.0f },{ 1.0f,0.0f,0.0f },{ 1.0f,1.0f,0.0f },{ 0.0f,1.0f,0.0f },{ 0.0f,0.0f,1.0f } }
};

//
// inversion for 4x4 matrix only containing rotations
//  and translations
///////////////////////////////////////////////////////
void invertMat(float matrix[16]){
	float temp;
	
	temp=matrix[1];
	matrix[1]=matrix[4];
	matrix[4]=temp;
	
	temp=matrix[2];
	matrix[2]=matrix[8];
	matrix[8]=temp;
	
	temp=matrix[6];
	matrix[6]=matrix[9];
	matrix[9]=temp;
	
	matrix[12]=-matrix[12];
	matrix[13]=-matrix[13];
	matrix[14]=-matrix[14];
};

float clamp (float value, float min, float max) {
	float ret = value;
	if (value > max)
		ret = max;
	if (value < min)
		ret = min;
	return ret;
};


void convertRGBToPlanar(void* red, void* green, void* blue, const void* rgb, unsigned long count, unsigned long bytes) {
	long i;
	switch (bytes) {
		case 1: {
			int8_t *pred= (int8_t *)red, *pgreen=(int8_t *)green, *pblue=(int8_t *)blue, *prgb=(int8_t *)rgb;
			for(i=0;i<count;i++) {
				pred[i] = prgb[i*3];
				pgreen[i] = prgb[i*3+1];
				pblue[i] = prgb[i*3+2];
			}
			break;
		}
		case 2: {
			int16_t *pred=(int16_t *) red, *pgreen=(int16_t *)green, *pblue=(int16_t *)blue, *prgb=(int16_t *)rgb;			
			for(i=0;i<count;i++) {
				pred[i] = prgb[i*3];
				pgreen[i] = prgb[i*3+1];
				pblue[i] = prgb[i*3+2];
			}	
			break;
		}
		case 4: {
			int32_t *pred=(int32_t *) red, *pgreen=(int32_t *)green, *pblue=(int32_t *)blue, *prgb=(int32_t *)rgb;			
			for(i=0;i<count;i++) {
				pred[i] = prgb[i*3];
				pgreen[i] = prgb[i*3+1];
				pblue[i] = prgb[i*3+2];
			}
			break;
		}
	}
};