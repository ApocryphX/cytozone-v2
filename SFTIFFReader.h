//
//  SFTIFFReader.h
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Mon Mar 24 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFTIFFFile.h"

@interface SFTIFFReader : NSObject {
    SFTIFFFile* sfTIFFFile;
}


- (id)initWithFileName:(NSString*)fileName;
- (instancetype)initWithURL:(NSURL *) fileURL;

- (NSNumber*)widthAtIndex:(NSUInteger)number;
- (NSNumber*)lengthAtIndex:(NSUInteger)number;
- (NSNumber*)samplesPerPixelAtIndex:(NSUInteger)number;
- (NSNumber*)bitsPerSampleAtIndex:(NSUInteger)number;
- (NSData*)colorMapAtIndex:(NSUInteger)number;
- (NSMutableData*)dataAtIndex:(NSUInteger)number;
- (NSString*)descriptionAtIndex:(NSUInteger)number;

	// primitive functions
- (uint32_t)stripCountAtIndex:(NSUInteger)idx;
- (uint32_t)byteCountAtStrip:(NSUInteger)number index:(NSUInteger)idx;
- (NSData*)dataForTagWithIndex:(uint32_t)ifdindex withID:(uint16_t)tagID;
- (NSData*)dataAtOffset:(uint32_t)offset;
- (NSData*)dataWithLength:(NSUInteger)length offset:(uint32_t)offset;
- (NSMutableData*)dataAtStrip:(uint32_t)number index:(NSUInteger)idx;

- (SFTIFFFile*)tiffFile;


@end

// [todo]:
// - support tiled images
// - support endian changes for image data > 8bit
// - support RGB & grayscale data
// - check validity of tags and image data