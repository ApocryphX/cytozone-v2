//
//  SFVolumeFactory.h
//  CytoFX
//
//  Created by ApocryphX on 2/12/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFOGLVolume;

@interface SFVolumeFactory : NSObject {
	NSUInteger sfDefaultVolumeMode;
}

+ (id) sharedInstance;
+ (SFOGLVolume*) volumeForMode:(NSUInteger)mode;
+ (Class) classForMode:(NSUInteger)mode;
- (SFOGLVolume*) defaultVolume;
- (Class) defaultClass;

@end
