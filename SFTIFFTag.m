//
//  SFTIFFTag.m
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Mon Mar 17 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import "SFTIFFTag.h"
#import "SFTiff.h"

// holds sizes (in bytes) of tiff data types from 0 through 11
uint16_t TIFFDataSizeMap[] = {1,1,1,2,4,8,1,1,2,4,8,4,8}; 

// TIFFValue union is use to hold and translate the contents of the value field
#pragma pack (push, 1)

union TIFFValue {
    uint8_t ucharacter[8];
    uint16_t ushortint[4];
    uint32_t ulongint[2];
    int8_t character[8];
    int16_t shortint[4];
    int32_t longint[2];
    float fp[2];
    uint64_t longlong;
};

union TIFFArrayTypes {
    uint16_t shortint;
    uint32_t longint;
    uint64_t longlong;
};

// structure describes the entries in IFD fileds;
// interpretation of "value" filed depends on "sfDataType"
struct _TIFFIFDEntry {
    uint16_t tag;
    uint16_t type;
    uint32_t count;
    uint32_t value;
};
typedef struct _TIFFIFDEntry TIFFIFDEntry;
#pragma pack (pop )

@implementation SFTIFFTag

// return the size in bytes of one diretory entry in an IFD;
// used for offset calaculations while reading TIFF files
+(unsigned)sizeDirEntry{
    return sizeof(TIFFIFDEntry);
}

// returns true if the value field contains an offset to the actual data
// returns false if the value field contains the actual data
-(BOOL)isValueOffset {
    bool ret = NO;
    if ( [self length] > sizeof(uint32_t) )
		ret = YES;
    return ret;
}

// returns the number of bytes in the value field or at offset location
- (uint32_t)length {
	uint32_t ret = 0;
	if (sfDataType < 12) 
		ret = TIFFDataSizeMap[sfDataType] * count;
	return ret;
}


-(id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian {
     TIFFIFDEntry *direntry;
    self = [super init];
    NSData* buffer = [handle readDataOfLength:sizeof( TIFFIFDEntry)];
    direntry = (void*)[buffer bytes];
    // adjust endian
    if(isBigEndian) {
        tag   = NSSwapBigShortToHost(direntry->tag) ;
        sfDataType  = NSSwapBigShortToHost(direntry->type);
        count = NSSwapBigIntToHost(direntry->count);

    }
    else {
        tag   = NSSwapLittleShortToHost(direntry->tag) ;
        sfDataType  = NSSwapLittleShortToHost(direntry->type);
        count = NSSwapLittleIntToHost(direntry->count);
        
    }
    // put TIFF value field contents into a NSData or,
    //if value represents offset: read array of values into NSBuffer at direntry->value offset
/*
    if (tag == COLORMAP) {
        NSUInteger valueOffset;
        if(isBigEndian)
            valueOffset = NSSwapBigIntToHost(direntry->value);
        else
            valueOffset = NSSwapLittleIntToHost(direntry->value);
		[handle seekToFileOffset:valueOffset];
		NSUInteger size = TIFFDataSizeMap[sfDataType];
		valueData = [handle readDataOfLength:count * size];
    }
    else */
 
    if ( [self isValueOffset] ) {
		//offset needs to be Endian adjusted
        NSUInteger valueOffset;
        if(isBigEndian)
            valueOffset = NSSwapBigIntToHost(direntry->value);
        else
            valueOffset = NSSwapLittleIntToHost(direntry->value);
	    // seek to position of data block
		[handle seekToFileOffset:valueOffset];
		valueData = [handle readDataOfLength:count*TIFFDataSizeMap[sfDataType]];
    }
    else {
		valueData = [NSData dataWithBytes:&(direntry->value) length:sizeof(uint32_t)];
    }
    if([valueData length]== 0)
        NSLog(@"");
    NSUInteger valueIndex, valueCount = count;
    switch(sfDataType) {
        case (BYTE):
        case (ASCII):
            break;  // byte value: nothing to do
        case (SHORT):
        case (SSHORT): {
            uint16_t *value = (void*)valueData.bytes;
            if (isBigEndian) {
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    value[valueIndex] = NSSwapBigShortToHost(value[valueIndex]);
                }
            }
            else {
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    value[valueIndex] = NSSwapLittleShortToHost(value[valueIndex]);
                }
            }
            break;
        }
        case(LONG):
        case(SLONG):
        case(IFD): {
            uint32_t *value = (void*)valueData.bytes;
            if (isBigEndian) {
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    value[valueIndex] = NSSwapBigIntToHost(value[valueIndex]);
                }
            }
            else {
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    value[valueIndex] = NSSwapLittleIntToHost(value[valueIndex]);
                }
            }
            break;
        }
        case(FLOAT): {
            float *value = (void*)valueData.bytes;
            if (isBigEndian) {
                NSSwappedFloat bigEndianFloat;
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    memcpy(&bigEndianFloat,&value[valueIndex], sizeof(float));
                    value[valueIndex] = NSSwapBigFloatToHost(bigEndianFloat);
                }
            }
            else {
                NSSwappedFloat littleEndianFloat;
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    memcpy(&littleEndianFloat,&value[valueIndex], sizeof(float));
                    value[valueIndex] = NSSwapBigFloatToHost(littleEndianFloat);
                }
            }
            break;
        }
        case(RATIONAL): {
            uint32_t *value = (void*)valueData.bytes;
            if (isBigEndian) {
                for(valueIndex=0;valueIndex<valueCount*2;valueIndex++) {
                    value[valueIndex] = NSSwapBigIntToHost(value[valueIndex]);
                }
            }
            else {
                for(valueIndex=0;valueIndex<valueCount*2;valueIndex++) {
                    value[valueIndex] = NSSwapLittleIntToHost(value[valueIndex]);
                }
            }
            break;
        }
        case (DOUBLE):{
            double *value = (void*)valueData.bytes;
            if (isBigEndian) {
                NSSwappedDouble bigEndianDouble;
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    memcpy(&bigEndianDouble,&value[valueIndex], sizeof(float));
                    value[valueIndex] = NSSwapBigDoubleToHost(bigEndianDouble);
                }
            }
            else {
                NSSwappedDouble littleEndianDouble;
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    memcpy(&littleEndianDouble,&value[valueIndex], sizeof(float));
                    value[valueIndex] = NSSwapBigDoubleToHost(littleEndianDouble);
                }
            }
            break;
        }
        case(LONG8):
        case (SLONG8):
        case (IFD8): {
            uint64_t *value = (void*)valueData.bytes;
            if (isBigEndian) {
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    value[valueIndex] = NSSwapBigLongLongToHost(value[valueIndex]);
                }
            }
            else {
                for(valueIndex=0;valueIndex<valueCount;valueIndex++) {
                    value[valueIndex] = NSSwapLittleLongLongToHost(value[valueIndex]);
                }
            }
            break;
        }
        default:
            return nil;
    }

    return self;
}

-(uint16_t)tagID {
    return tag;
}

-(uint16_t)tagType {
    return sfDataType;
}

-(uint32_t)tagCount{
    return count;
}

-(NSData*)valueAsData {
    return valueData;
}

// return the tag field id as NSNumber for use in collections etc.
-(NSNumber*)valueAsNumberAtIndex:(uint32_t)i{
    if (i>=count)
		return nil;
    switch(sfDataType) {
		case (BYTE):
			return [NSNumber numberWithChar:((uint8_t*)[valueData bytes])[i]];
		case (ASCII):
			return [NSNumber numberWithChar:((uint8_t*)[valueData bytes])[i]];
		case (SHORT):
			return [NSNumber numberWithUnsignedShort:((uint16_t*)[valueData bytes])[i]];
		case (SSHORT):
			return [NSNumber numberWithShort:((int16_t*)[valueData bytes])[i]];
		case (LONG):
			return [NSNumber numberWithUnsignedLong:((uint32_t*)[valueData bytes])[i]];
		case(SLONG):
			return [NSNumber numberWithLong:((int32_t*)[valueData bytes])[i]];
		case(FLOAT):
			return [NSNumber numberWithFloat:((float*)[valueData bytes])[i]];
		case (DOUBLE):
			return [NSNumber numberWithDouble:((double*)[valueData bytes])[i]];
		default:
			return nil;
    }
}




@end
