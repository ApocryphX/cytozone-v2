//
//  SFFileNodeTag.h
//  Cytozone
//
//  Created by Kolja A. Wawrowsky on 11/18/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


// channel information
const NSString *SFKChannelInfoKey;

// information about the voxel type
const NSString *SFKVoxelInfoKey;
// voxel data organization and types
const NSString *SFKVoxelBytesPerSampleKey;
const NSString *SFKVoxelSamplesPerPixelKey;
const NSString *SFKVoxelDataPrecisionKey;
const NSString *SFKVoxelDataTypeKey;

// integer / float type
const NSString *SFKVoxelDataTypeFloat;
const NSString *SFKVoxelDataTypeInteger;

const NSString *SFKVoxelDataCompressionKey;

// dimension description
const NSString *SFKSpatialDimensionArrayKey;

// Values for each dimension
const NSString *SFKDimensionNameKey;
const NSString *SFKDimensionSymbolKey;
const NSString *SFKDimensionSizeKey;
const NSString *SFKDimensionPhysicalSizeKey;
const NSString *SFKDimensionPhysicalOffsetKey;


const NSString *SFKCollectionTypeMosaicKey;
const NSString *SFKCollectionTypeSpectralKey;
const NSString *SFKCollectionTypeTimeKey;
const NSString *SFKCollectionTypeRotationKey;
const NSString *SFKCollectionTypeRandomPositionKey;
const NSString *SFKCollectionTypeIlluminationKey;
const NSString *SFKCollectionTypeBlockIndexKey;
const NSString *SFKCollectionTypePhaseKey;
const NSString *SFKCollectionTypeViewIndexKey;
const NSString *SFKCollectionTypeOtherKey;


@interface SFFileNodeTag : NSObject

@end
