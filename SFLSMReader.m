//
//  SFLSMReader.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFLSMReader.h"
#import "SFTIFFReader.h"
#import "SFZeissLSMInfo.h"
#import "SFAlignment2D.h"
#import "SFChannelInfo.h"
#import "SFVoxelData.h"
#import "SFNDVoxelData.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"

#define MAPSIZE 6

const unichar LSMSymbolMap[2][MAPSIZE] = {
    { 0x0074,0x0078,0x0079,0x007A,0x03BB,0x2014},  // unicode char for dimension symbol: t,x,y,z,lambda,-
    { 0x0073,0x006D,0x006D,0x006D,0x006D,0x2014}   // unicode char for dimension unit: s,m,m,m,m,-
};

void* LSMOpen (const char *path );
short LSMNumberOfDirectories(void* handle);
void  LSMSetDirectory(void* handle, short number);
void  LSMClose(void* handle);
void* LSMPrivateTagData(void* handle);

#pragma pack(push, 1)                        

struct SFLSMChannelInfo {
    SInt32 s32NumberColors;
    SInt32 s32NumberNames;
    SInt32 s32ColorsOffset;
    SInt32 s32NamesOffset;
    SInt32 s32Mono;
    SInt32 s32Reserved[4];
};

typedef struct SFLSMChannelInfo SFLSMChannelInfo;

struct SFLSMRGBColor {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t alpha;
};

typedef struct SFLSMRGBColor SFLSMRGBColor;

#pragma pack(pop)


@implementation SFLSMReader

- (id)initWithPath:(NSString*)fileName {
	self = [super init];
    if(self) {
            //void *handle = LSMOpen([fileName cStringUsingEncoding:NSUTF8StringEncoding]);
            //LSMPrivateTagData(handle);
        sfTIFFReader = [[SFTIFFReader alloc]initWithFileName:fileName];
            //sfLSMInfo = [[SFZeissLSMInfo alloc]initTIFFReader:sfTIFFReader];
        sfZeissTagData = [sfTIFFReader dataForTagWithIndex:0 withID:TIF_CZ_LSMINFO];
        const LSMInfo *info = sfZeissTagData.bytes;
        // load color info
        if(info->u32OffsetChannelColors) {
            NSData *blockSizeData = [sfTIFFReader dataAtOffset:info->u32OffsetChannelColors];
            const uint32_t *blockSize = blockSizeData.bytes;
            channelColors = [sfTIFFReader dataWithLength:(*blockSize)-4 offset:info->u32OffsetChannelColors+4];
            if(channelColors) {
                const SFLSMChannelInfo *channelInfo = channelColors.bytes;
                rgbColorArrayData = [sfTIFFReader dataWithLength:channelInfo->s32NumberColors*sizeof(SFLSMRGBColor)
                                                          offset:info->u32OffsetChannelColors+channelInfo->s32ColorsOffset];
            }
        }
        if(info->s32DataType==0) {
            dataTypeData = [sfTIFFReader dataWithLength:sizeof(uint32_t)*[self channelCountAtNodeIndex:0]
                                                 offset:info->u32OffsetChannelDataTypes];
        }
        sfFileName = fileName;
    }
	return self;
};

-(NSColor *)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex {
        //    SFVoxelInfo *voxelInfo = [self voxelInfoAtNodeIndex:nodeIndex channelIndex:channelIndex];
    NSColor *ret = [NSColor whiteColor];
    if(rgbColorArrayData) {
        if(rgbColorArrayData.length>channelIndex*sizeof(SFLSMRGBColor) ) {
            const SFLSMRGBColor *rgbColor = rgbColorArrayData.bytes+channelIndex*4;
            ret = [NSColor colorWithCalibratedRed:(CGFloat)rgbColor->red/254.0
                                            green:(CGFloat)rgbColor->green/254.0
                                             blue:(CGFloat)rgbColor->blue/254.0
                                            alpha:1.0];
        }
    }
    else {
        if(channelIndex<3) {
            NSColor *color[3] = {[NSColor redColor],[NSColor greenColor],[NSColor blueColor]};
            ret = color[channelIndex];
        }
    }
    return ret;
}

+ (BOOL)isConfocalFile:(NSString*)filePath {
    return YES;
}

+ (NSArray*)fileExtensions {
    return [NSArray arrayWithObject:@"lsm"];
};


- (NSArray*)fileNodes {
    NSMutableDictionary *dataSetDictionary = [NSMutableDictionary new];
    dataSetDictionary[SFKNodeDataIndexKey]=[NSNumber numberWithUnsignedInteger:0];
    dataSetDictionary[SFKNodeTitleKey]=[[sfFileName lastPathComponent]stringByDeletingPathExtension];
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    NSString *imagePath = [[NSBundle bundleForClass:[self class]] pathForResource:SFKZeissFileIconName ofType:@"icns"];
    dictionary[SFKNodeIconKey] = [[NSImage alloc]initByReferencingFile:imagePath];
    dictionary[SFKNodeTitleKey]=[sfFileName lastPathComponent];
    dictionary[SFKNodeChildrenKey] = [NSArray arrayWithObject:dataSetDictionary];
    return [NSArray arrayWithObject:dictionary];
}

-(NSString*)fileName {
    return sfFileName;
};


- (NSUInteger)count {
	return 1;
};

- (NSString*)name {
	return sfFileName;
};


- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex {
    const LSMInfo *lsmInfo = [sfZeissTagData bytes];
    NSUInteger channelCount = lsmInfo->s32DimensionChannels;
    return channelCount;
};

- (SFNDDimension*)nDDimensionAtNodeIndex:(NSUInteger)nodeIndex {
	
    const LSMInfo *info = sfZeissTagData.bytes;
    SFNDDimension *ndDimension = [SFNDDimension new]; // return value
    
    
    // X-Dimension
    [ndDimension addDimensionWithExtent:info->s32DimensionX
                              magnitude:[NSNumber numberWithDouble:info->f64VoxelSizeX*info->s32DimensionX ]
                          dimensionType:SFKDimensionTypeSpaceName
                                 symbol:[NSString stringWithCharacters:&LSMSymbolMap[0][1] length:1]
                                   unit:[NSString stringWithCharacters:&LSMSymbolMap[1][1] length:1]];

    // Y-Dimension
    [ndDimension addDimensionWithExtent:info->s32DimensionY
                              magnitude:[NSNumber numberWithDouble:info->f64VoxelSizeY*info->s32DimensionY]
                          dimensionType:SFKDimensionTypeSpaceName
                                 symbol:[NSString stringWithCharacters:&LSMSymbolMap[0][2] length:1]
                                   unit:[NSString stringWithCharacters:&LSMSymbolMap[1][2] length:1]];

    
    if (info->s32DimensionZ>1)  { // xyz stack
        
        // Z-Dimension
        [ndDimension addDimensionWithExtent:info->s32DimensionZ
                                  magnitude:[NSNumber numberWithDouble:info->f64VoxelSizeZ*info->s32DimensionZ]
                              dimensionType:SFKDimensionTypeSpaceName
                                     symbol:[NSString stringWithCharacters:&LSMSymbolMap[0][3] length:1]
                                       unit:[NSString stringWithCharacters:&LSMSymbolMap[1][3] length:1]];

        
    }
    if(info->s32DimensionTime>1) { // xyzt time series
        // T-Dimension
        [ndDimension addDimensionWithExtent:info->s32DimensionTime
                                  magnitude:[NSNumber numberWithDouble:info->f64TimeIntervall*info->s32DimensionTime]
                              dimensionType:SFKDimensionTypeTimeName
                                     symbol:[NSString stringWithCharacters:&LSMSymbolMap[0][0] length:1]
                                       unit:[NSString stringWithCharacters:&LSMSymbolMap[1][0] length:1]];

    
    }
	return ndDimension;
};


- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex  {
    const LSMInfo *info = sfZeissTagData.bytes;
    SFVoxelInfo *vinfo = [SFVoxelInfo new];
        //    if(channelColors) {
        //        const SFLSMChannelInfo *colors = channelColors.bytes;
        //    }
    uint32_t type = 0;
    if(info->s32DataType==0) {
        const uint32_t *typeArray = dataTypeData.bytes;
        type = typeArray[channelIndex];
    }
    else
        type = info->s32DataType;
    switch (type) {
		case 1: { // 8 bit int
			[vinfo setBytesPerSample:1];
			[vinfo setPrecision:8];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kInteger]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		case 2: { // 12 bit int
			[vinfo setBytesPerSample:2];
			[vinfo setPrecision:12];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kInteger]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		case 3: { // 12 bit int
			[vinfo setBytesPerSample:2];
			[vinfo setPrecision:16];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kInteger]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		case 5: { // 32 bit float
			[vinfo setBytesPerSample:4];
			[vinfo setPrecision:32];
			[vinfo setSamplesPerVoxel:1];
			[vinfo setDataType:kFloatingPoint]; 	//kInteger,kFloatingPoint,kComplex
			break;
		}
		default: {
			break;
		}
	}
    return vinfo;
}

-(NSData*)dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex {
        // NSUInteger stripCount= [sfTIFFReader stripCountAtIndex:planeIndex];
    NSData* voxdata = [sfTIFFReader dataAtStrip:(uint32)channelIndex index:planeIndex*2];
    return voxdata;
}




@end
