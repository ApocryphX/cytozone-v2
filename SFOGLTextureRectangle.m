//
//  SFOGLTexture2D.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLTextureRectangle.h"

#import <OpenGL/gl.h>

#import "SFGeometry.h"
#import "SFNDDimension.h"
#import "SFAlignment2D.h"
#import "SFVoxelData.h"

#import "SFGeometry.h"
#import "SFOGLVoxelInfo.h"
#import "SFOGLVoxelFormat.h"

#import <Accelerate/Accelerate.h>

//#define GL_TEXTURE_RECTANGLE_EXT          0x84F5

@interface SFOGLTextureRectangle (PrivateMethods)
- (void) deleteOGLTexture;
- (void) createTexture;
@end

@implementation SFOGLTextureRectangle


- (id)init {
	self = [super init];
    if(self) {
        sfTexName = 0;
        sfMaxTextureSize = 1024;
    }
	return self;   
};

- (id)initWithContext:(NSOpenGLContext*)cxt {
	self = [self init];
    if(self) {
        sfContext = cxt;
    }
	return self;
};

- (void)setContext:(NSOpenGLContext*)cxt {
	if (cxt != sfContext ) {
		[self deleteOGLTexture];
		sfContext = cxt;
		[self createTexture];
	}
};

- (void)setNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo {
//	sfNDDimension = [nddim dimensionsSlicedAtIndex:1];
	sfIsDownsampled = NO;
    double factor = 1.0;
    double maximumExtent = MAX([nddim extentAtIndex:0], [nddim extentAtIndex:1]);
	if (  maximumExtent > (double) sfMaxTextureSize ) {
		factor = (double) sfMaxTextureSize  / maximumExtent ;
		sfIsDownsampled = YES;
	}
    //else {
    //    NSLog(@"not downsampling");
    //}
	
	SFSize size = { [nddim extentAtIndex:0]*factor, [nddim extentAtIndex:1]*factor };
	
	sfAlignment = [SFAlignment2D alignmentWithSize:size voxelInfo:vinfo];
	[sfAlignment setAlignment:SFKAppleAlignedName];
 	
	sfTexData = [NSMutableData dataWithLength:[sfAlignment bytesPerFrame]];
	
	sfOGLVoxelInfo = [[SFOGLVoxelInfo alloc]initWithVoxelInfo:vinfo];
	
	[self deleteOGLTexture];
	[self createTexture];
}

- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment {
    
	[sfAlignment getRealignedData:[sfTexData mutableBytes] source:bytes alignment:alignment ];
	if(sfTexName) {
        CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
		glBindTexture( GL_TEXTURE_RECTANGLE_EXT, sfTexName);
		glTexImage2D ( GL_TEXTURE_RECTANGLE_EXT, 0, GL_INTENSITY, (GLsizei)sfAlignment.size.width ,(GLsizei)sfAlignment.size.height,  0,
					   GL_LUMINANCE,  GL_UNSIGNED_BYTE, [sfTexData mutableBytes]);
        CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	}
};

- (void)setFormat:(GLenum)value {
		sfFormat = value;
};

- (SFAlignment2D*)alignment {
	return sfAlignment;
};

- (SFSize)size {
	return [sfAlignment size];
};

- (BOOL) isDownsampled {
	return sfIsDownsampled;
};

-(void) bind {	
	glBindTexture(GL_TEXTURE_RECTANGLE_EXT, sfTexName);
}

+ (void) unbind {
	glBindTexture(GL_TEXTURE_RECTANGLE_EXT, 0);	
	glDisable(GL_TEXTURE_RECTANGLE_EXT);
}

- (void) deleteOGLTexture {
	if (sfTexName != 0) {
        CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
		glDeleteTextures( 1,&sfTexName);
		sfTexName = 0;
        CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	}
};

-(void) createTexture {
	if (sfTexData != nil) {
        CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
		
		glEnable(GL_TEXTURE_RECTANGLE_EXT);
		
		if(!sfTexName)
			glGenTextures(1, &sfTexName);
		glBindTexture(GL_TEXTURE_RECTANGLE_EXT, sfTexName);
				
		// enable AGP texturing and main memory storage		
		glTexParameterf(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_PRIORITY, 1.0);
		glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_STORAGE_HINT_APPLE , GL_STORAGE_SHARED_APPLE);
        if(sfAlignment.bytesPerRow%4==0)
            glPixelStorei(GL_UNPACK_CLIENT_STORAGE_APPLE, GL_TRUE);
        else
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        
	    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // required if no mipmap levels defined
		glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // required if no mipmap levels defined
//		GLenum type =  [SFOGLVoxelFormat lookupType:sfOGLVoxelInfo ];
		glTexImage2D ( GL_TEXTURE_RECTANGLE_EXT, 0, [sfOGLVoxelInfo internalFormat] /*sfFormat*/,
                      (GLsizei)sfAlignment.size.width ,(GLsizei)sfAlignment.size.height,  0,
                      [sfOGLVoxelInfo format], [sfOGLVoxelInfo type] /*type*/, [sfTexData mutableBytes]);
        CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	}
};

-(void)dealloc {
    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	glBindTexture(GL_TEXTURE_RECTANGLE_EXT, 0);
	glDeleteTextures( 1,&sfTexName);	
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
};



@end
