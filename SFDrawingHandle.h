//
//  SFDrawingHandle.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicsObject.h"

@interface SFDrawingHandle : NSObject {
    NSPoint sfPosition;
}

@property (assign) NSRect bounds;
@property (assign) NSPoint position;
@property (assign) NSSize size;
@property (retain) NSColor *color;
@property (retain) NSColor *highlightColor;
@property (retain) NSColor *shadowColor;

+ (id)drawingHandleAtPoint:(NSPoint)point;

- (void)draw;

@end
