//
//  SFOGLTextureStack2D.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//


#import "SFOGLTextureStack3D.h"

#import "SFNDVoxelData.h"
#import "SFOGLRectangle.h"
#import "SFOGLTextureRectangle.h"
#import "SFVoxelInfo.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFAlignment2D.h"
#import "SFOGLVoxelFormat.h"

@interface SFOGLTextureStack3D (PrivateMethods)
- (void)createTextures;
@end

@implementation SFOGLTextureStack3D

- (void)setOpenGLContext:(NSOpenGLContext*)cxt {
	[super setOpenGLContext:cxt];
	NSUInteger i, count = [sfTextures count];
	for (i=0; i<count; i++) {
		[[sfTextures objectAtIndex:i]setContext:cxt];
	}
};

- (void)setNDDimension:(SFNDDimension*)nddim voxelInfo:(SFVoxelInfo*)vinfo {
	sfNDDimension =  [nddim dimensionsSlicedAtIndex:2];	
	sfTextures = [NSMutableArray new];
	
	NSUInteger i, count = [sfNDDimension extentAtIndex:2];
	for(i=0; i<count; i++) {
		SFOGLTextureRectangle* newtex = [SFOGLTextureRectangle new];
		[newtex setNDDimension:sfNDDimension voxelInfo:vinfo];
		[newtex setContext:sfContext];		
		[sfTextures addObject:newtex];
	}
	SFOGLTextureRectangle* tex2D = [sfTextures objectAtIndex:0];
	sfAlignment = [tex2D alignment];
	if([tex2D isDownsampled]) {
		sfIsDownsampled = YES;
        //[sfNDDimension setExtent:[tex2D size].width atIndex:0];
        //[sfNDDimension setExtent:[tex2D size].height atIndex:1];
	}
};


- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment index:(NSUInteger)idx {
	[[sfTextures objectAtIndex:idx]replaceBytes:bytes alignment:alignment];
};

- (BOOL) isDownsampled {
	return sfIsDownsampled;
};

- (SFAlignment2D*) alignment {
	SFAlignment2D* ret=nil;
	SFOGLTextureRectangle* tex = [sfTextures objectAtIndex:0];
	if (tex)
		ret = [tex alignment];
	return ret;
};

- (SFNDDimension*)nDDimension {
	return sfNDDimension;
};

- (void)bindTextureAtIndex:(NSUInteger)idx {
	[[sfTextures objectAtIndex:idx]bind];
};

- (NSUInteger)count {
	return [sfTextures count];
};

;


@end
