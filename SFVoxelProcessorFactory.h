//
//  SFVoxelProcessorFactory.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFAlignment2D;
@protocol SFVoxelProcessing;



@interface SFVoxelProcessorFactory : NSObject {

}

+ (BOOL) hasSupportedVectorUnit;
+ (BOOL) isAlignmentSupported:(SFAlignment2D*)align;
+ (Class <SFVoxelProcessing>) classForAlignment:(SFAlignment2D*)align;


@end
