//
//  SFVolumeFactory.m
//  CytoFX
//
//  Created by ApocryphX on 2/12/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFVolumeFactory.h"
#import "SFOGLVolume2D.h"
#import "SFOGLVolume25D.h"
#import "SFOGLVolume3D.h"

static id sfSharedVolumeFactory = nil;

@implementation SFVolumeFactory

- (id) init {
	self = [super init];
	sfDefaultVolumeMode = 0;
	return self;
};

+ (id) sharedInstance {
	if (!sfSharedVolumeFactory) {
		sfSharedVolumeFactory = [SFVolumeFactory new];
	}
	return sfSharedVolumeFactory;
};

+ (SFOGLVolume*) volumeForMode:(NSUInteger)mode {
	switch (mode) {
		case 0: 
			return [SFOGLVolume2D new];
		case 1: 
			return [SFOGLVolume25D new];
		case 2: 
			return [SFOGLVolume3D new];
		default: 
			return nil;
	}
};

+ (Class) classForMode:(NSUInteger)mode {
	switch (mode) {
		case 0: 
			return [SFOGLVolume2D class];
		case 1: 
			return [SFOGLVolume25D class];
		case 2: 
			return [SFOGLVolume3D class];
		default: 
			return Nil;
	}
};

- (SFOGLVolume*) defaultVolume {
	return [SFVolumeFactory volumeForMode:sfDefaultVolumeMode];
};

- (Class) defaultClass {
	return [SFVolumeFactory classForMode:sfDefaultVolumeMode];
}; 

@end
