//
//  SFOGLVolumeSetting.h
//  VoxelMachine
//
//  Created by ApocryphX on 4/12/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SFOGLVolumeSetting : NSObject <NSCopying> {
	NSUInteger sfBackplaneMode;
	NSColor* sfBackplaneColor;
	float sfAlphaThreshold;
	float sfAlpha;
	float sfBrightness;
	float sfBlendFactor;
	NSColor* sfBlendColor;
	NSUInteger sfRenderMode;
	float sfZScale;
	NSUInteger sfSampling;
}

- (id)initWithVolumeSetting:(SFOGLVolumeSetting*)settings;

- (void) setBackplaneMode:(NSUInteger)value;
- (void) setBackplaneColor:(NSColor*)value;
- (void) setAlphaThreshold:(float)value;
- (void) setAlpha:(float)value;
- (void) setBrightness:(float)value;
- (void) setBlendFactor:(float)value; 
- (void) setBlendColor:(NSColor*)value;
- (void) setRenderMode:(NSUInteger)value;
- (void) setZScale:(float)value;
- (void) setSampling:(NSUInteger)value;

//------------------------------------------------

- (NSUInteger) backplaneMode;
- (NSColor*) backplaneColor;
- (float) alphaThreshold;
- (float) alpha;
- (float) brightness;
- (float) blendFactor;
- (NSColor*)blendColor;
- (NSUInteger) renderMode;
- (float) zScale;
- (NSUInteger) sampling;


@end
