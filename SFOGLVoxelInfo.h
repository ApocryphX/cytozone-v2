//
//  SFOGLVoxelInfo.h
//  CytoFX
//
//  Created by ApocryphX on 10/22/06.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFVoxelInfo.h"


@interface SFOGLVoxelInfo : SFVoxelInfo {

}

- (GLenum) internalFormat;
- (GLenum) format;
- (GLenum) type;
	
@end
