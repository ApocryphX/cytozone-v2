//
//  SFOpenGLView.m
//  VoxelMachine
//
//  Created by ApocryphX on Sat Jul 10 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFOpenGLView.h"

#import <OpenGL/OpenGL.h>

#import "SFOGLVolumeScene.h"
#import "SFOGLTransform.h"

#import <OpenGL/gl.h>

@implementation SFOpenGLView

 - (id) initWithFrame: (NSRect) theFrame
 {
	 NSOpenGLPixelFormatAttribute attribs [] = {
         NSOpenGLPFADoubleBuffer,
         NSOpenGLPFABackingStore,
		 NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)32,
		 (NSOpenGLPixelFormatAttribute)nil	
	 };
	 
	 NSOpenGLPixelFormat *fmt = [[NSOpenGLPixelFormat alloc] initWithAttributes: attribs];
	 self = [super initWithFrame:theFrame pixelFormat:fmt];
     NSOpenGLContext *context = [super openGLContext];
	 [context makeCurrentContext];
         //GLint  swapInterval = 1;
         //[context setValues:&swapInterval forParameter:NSOpenGLCPSwapInterval];
     // Enable OpenGL multi-threading
     CGLEnable( [context CGLContextObj], kCGLCEMPEngine);
	 [self setClearColor:[NSColor blackColor]];

	 return self;
 };
 

-(void)awakeFromNib 
{
	[self setClearColor:[NSColor blackColor]];

};

- (void)setScene:(SFOGLVolumeScene*)value {
	sfScene = value;
};

- (SFOGLVolumeScene*)scene {
	return sfScene;
};


- (BOOL)acceptsFirstResponder {
	return YES;
};

- (void)setTextureTransform:(SFOGLTransform*)value {
//	[sfTextureTransform release];
//	sfTextureTransform = value;
//	[sfScene setTextureTransform:value];
//	[self setMouseTracker:sfTextureTransform];
};

- (SFOGLTransform*)textureTransform {
	return [sfScene textureTransform];
};


- (NSImage*) snapShot {
	//    struct  {
	//        uint8_t red; uint8_t green; uint8_t blue; uint8_t alpha;
	//    } *RGBA;
	NSUInteger bytesperpixel = 4;
    NSBitmapImageRep* rep;
    NSImage* ret;
    NSUInteger i,row, size;
	void *src, *dst;
    size = [self frame].size.width* [self frame].size.height * bytesperpixel;
	NSUInteger height = [self frame].size.height;
	NSUInteger width = [self frame].size.width;
	
    rep = [[NSBitmapImageRep alloc]
        initWithBitmapDataPlanes: nil  // Nil pointer tells the kit to allocate the pixel buffer for us.
                      pixelsWide: width //width
                      pixelsHigh: height //height
                   bitsPerSample: 8
                 samplesPerPixel: bytesperpixel
                        hasAlpha: YES
                        isPlanar: NO
                  colorSpaceName: NSCalibratedRGBColorSpace // 0 = black, 1 = white in this color space.
                     bytesPerRow: 0    // Passing zero means "you figure it out."
                    bitsPerPixel: 8*bytesperpixel];  // This must agree with bitsPerSample and samplesPerPixel.
	
	char* pixelbuffer = malloc(size);
    
    [[self openGLContext] makeCurrentContext];

    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
    glReadPixels ( 0, 0,(GLsizei) width,(GLsizei)height, GL_RGBA, GL_UNSIGNED_BYTE, pixelbuffer);
    
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
    
	for (i=3;i<size;i+=4) {
		pixelbuffer[i]=255;
	}
	for (row=0;row < height;row++) {
		src = pixelbuffer+ width*bytesperpixel*row;
		dst = (void*)[rep bitmapData]+([rep bytesPerRow]*(height-1-row));
		memcpy(dst,src,width*bytesperpixel);
	}
	free(pixelbuffer);
    ret = [ [NSImage alloc] init];
    [ret addRepresentation:rep];
    return ret;
};

- (void)renewGState
{
	/* Overload this function to ensure the NSOpenGLView doesn't
     flicker when you resize it.                               */
	NSWindow *window;
	[super renewGState];
	window = [self window];
    
	/* Only available in 10.4 and later, so check that it exists */
	if([window respondsToSelector:@selector(disableScreenUpdatesUntilFlush)])
		[window disableScreenUpdatesUntilFlush];
}

- (void) reshape
{
	// We draw on a secondary thread through the display link
	// When resizing the view, -reshape is called automatically on the main thread
	// Add a mutex around to avoid the threads accessing the context simultaneously when resizing
    [[self openGLContext] makeCurrentContext];
    
    CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	NSRect rect = [self bounds];
	glViewport (rect.origin.x, rect.origin.y, rect.size.width, rect.size.height );
	
	glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	if ( rect.size.width <= rect.size.height )
		glOrtho (-1.0, 1.0, -1.0 * rect.size.height / rect.size.width, rect.size.height / rect.size.width, -1.4f, 1.4f );
	else
		glOrtho (-1.0 * rect.size.width / rect.size.height, rect.size.width / rect.size.height, -1.0, 1.0, -1.4f, 1.4f );
    
	glMatrixMode(GL_MODELVIEW);
	CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
}



- (void)drawRect:(NSRect)rect
{
    [[self openGLContext] makeCurrentContext];
    CGLLockContext([[self openGLContext] CGLContextObj]);

    // make depth buffer writable
	NSColor* rgbacolor = [sfClearColor colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
	glClearColor([rgbacolor  redComponent], [rgbacolor greenComponent],
				 [rgbacolor  blueComponent], 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
	
	// clear depth buffer
	glDepthMask(GL_TRUE);
	glClearColor (0.0f, 0.0f, 0.0f, 0.0f );
	glClearDepth (1.0f);
	glClear( GL_DEPTH_BUFFER_BIT);
	// set viewport to full bounding rectangle size
	// reset rotations
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	
	//glViewport (rect.origin.x, rect.origin.y, rect.size.width, rect.size.height );

	
	// draw scene
	[sfScene drawScene];
    glFlush();
    CGLUnlockContext([[self openGLContext] CGLContextObj]);

	// finish OGL part of drawing
	[[self openGLContext] flushBuffer];

} 

- (IBAction)updateView:(id)sender {
	[self setNeedsDisplay:YES];	
};


- (void) setClearColor:(NSColor*)value {
	sfClearColor = [value colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
	if([self openGLContext]) 
		[self setNeedsDisplay:YES]; 
};

- (NSColor*) clearColor {
	return sfClearColor;
};

- (void)setMouseTracker:(SFOGLTransform*)tracker {
	sfMouseTracker = tracker;		
};

- (BOOL)acceptsFirstMouse:(NSEvent *)theEvent {
    return YES;
};

- (void)mouseDown:(NSEvent *)theEvent
{
	[sfMouseTracker startTracking:[theEvent locationInWindow] inRectangle:[self frame]];
};

- (void)mouseUp:(NSEvent *)theEvent
{
	[sfMouseTracker stopTracking];
};

- (void)mouseDragged:(NSEvent *)theEvent
{
	[sfMouseTracker draggedTo:[theEvent locationInWindow]];
    [self display];
};

- (void)magnifyWithEvent:(NSEvent *)event {
    sfMouseTracker.zoom = ([event magnification] + 1.0)*sfMouseTracker.zoom;
    self.needsDisplay=YES;
}

/*
- (void)scrollWheel:(NSEvent *)theEvent {
	CGFloat deltaY = [theEvent deltaY];
    float new_zoom = sfMouseTracker.zoom + deltaY/100.0f*sfMouseTracker.zoom;
    if (new_zoom < 0.01 )
        sfMouseTracker.zoom = 0.01;
    else 
        sfMouseTracker.zoom = new_zoom;
    [self setNeedsDisplay:YES];
}
*/
- (void)scrollWheel:(NSEvent *)theEvent {
    NSPoint translation;
	translation.x = [theEvent deltaX]/200.0/sfMouseTracker.zoom+sfMouseTracker.translation.x;
    translation.y = [theEvent deltaY]/-200.0/sfMouseTracker.zoom+sfMouseTracker.translation.y;
    [sfMouseTracker setTranslation:translation];
    [self setNeedsDisplay:YES];
}


@end
