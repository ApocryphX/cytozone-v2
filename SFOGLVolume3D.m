//
//  SFOGLVolume3D.m
//  VoxelMachine
//
//  Created by ApocryphX on 7/10/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolume3D.h"

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

#import "SFGeometry.h"
#import "SFOGLTexture3D.h"
#import "SFOGLTextureStack3D.h"
#import "SFNDDimension.h"
#import "SFNDVoxelData.h"
#import "SFVoxelData.h"
#import "SFVolumeWindowController.h"
#import "SFOGLTransform.h"
#import "SFOGLBackplane.h"
#import "SFOGLChannelSetting.h"
#import "SFOGLColor.h"

#include "utility.h"

@implementation SFOGLVolume3D	

+ (void)initialize {
	
	int ii,gg;
	float temp;
		
	//create the points used to draw the slices
	for (ii=0;ii<8;ii++){
		for (gg=0;gg<8;gg++){
			points[ii*8+gg][0]=(float)gg/7.0f-0.5f;
			points[ii*8+gg][1]=(float)ii/7.0f-0.5f;
			points[ii*8+gg][2]=-0.5f;
			temp=points[ii*8+gg][0]*points[ii*8+gg][0]+
				points[ii*8+gg][1]*points[ii*8+gg][1]+
				points[ii*8+gg][2]*points[ii*8+gg][2];
			temp=1.0f/(float)sqrt(temp);
			points[ii*8+gg][0]*=temp;
			points[ii*8+gg][1]*=temp;
			points[ii*8+gg][2]*=temp;
			points[ii*8+gg][0]*=2.0f;
			points[ii*8+gg][1]*=2.0f;
			points[ii*8+gg][2]+=1.0f;
		}
	}
	
}

- (void) setNDVoxelData:(SFNDVoxelData*)nddata {
	[super setNDVoxelData:nddata];
	if (nddata) {
		NSUInteger i, count = [[nddata channels]count];
		
		for(i=0; i<count; i++) {
			SFOGLTexture3D *tex3D = [[SFOGLTexture3D alloc]initWithContext:[[self openGLView]openGLContext]];
			[(SFOGLTexture3D*)tex3D setRenderMode:[self renderMode]];
			[tex3D setNDDimension:[sfNDVoxelData nDDimension] voxelInfo:[[nddata voxelDataAtIndex:i]voxelInfo]];
			if(sfOGLView)
				[tex3D setOpenGLContext:[sfOGLView openGLContext]];
			[sfVolumeTextures addObject:tex3D];
			
			SFOGLTextureStack3D *texStack = [[SFOGLTextureStack3D alloc]initWithContext:[[self openGLView]openGLContext]];
			[texStack setNDDimension:[sfNDVoxelData nDDimension] voxelInfo:[[nddata voxelDataAtIndex:i]voxelInfo]];
			[sfShadowTextures addObject:texStack];
		}
	[self setBackplaneNDDimension:[[sfVolumeTextures objectAtIndex:0]nDDimension]];
	[self processAllChannels];
	}	
};



- (void)scaleVolume {
	
	float xscale,yscale,zscale,maxscale;
	SFNDVoxelData* nddata = [self nDVoxelData];
	xscale = fabs([[nddata magnitudeAtIndex:0]doubleValue]);
	yscale = fabs([[nddata magnitudeAtIndex:1]doubleValue]);
	zscale = fabs([[nddata magnitudeAtIndex:2]doubleValue]);
	maxscale = (xscale>yscale)?xscale:yscale;
	maxscale = (maxscale>zscale)?maxscale:zscale;
	SFOGLTexture3D* tex3D = [sfVolumeTextures objectAtIndex:0];
	zscale = zscale / maxscale * sfZScale * [tex3D paddingRatioAtAxis:2];
	yscale = yscale / maxscale * [tex3D paddingRatioAtAxis:1];
	xscale = xscale / maxscale * [tex3D paddingRatioAtAxis:0];
	glScalef(1.0f/xscale,1.0f/yscale,1.0f/zscale);
}

-(BOOL)draw {
    [super draw];
	
    NSUInteger i,j;
	//texgen planes
	float xPlane[] = {1.0f, 0.0f, 0.0f, 0.0f};
	float yPlane[] = {0.0f, 1.0f, 0.0f, 0.0f};
	float zPlane[] = {0.0f, 0.0f, 1.0f, 0.0f};
	float mat[16];
	int ii,gg,hh;
	
	
	
	glEnable(GL_DEPTH_TEST);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	static float ang = 0.0f;
	ang += 2.5;
	//glRotatef(ang, 1.0f, 0.0f, 0.0f);
	[[self transform] executeRotation];

	[self scaleVolume];
	
	
	glGetFloatv(GL_MODELVIEW_MATRIX,mat);
	invertMat(mat);
	
	//want rotation only, zero the translation
	mat[12]=0.0f;
	mat[13]=0.0f;
	mat[14]=0.0f;
	
	//setup the texture coord generation
	glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);
	glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);
	glTexGeni(GL_R,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);  
	
	
	//enable the alpha blending/testing
	glEnable(GL_ALPHA_TEST);
	
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	
	
	//set up the texture matrix
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	glEnable(GL_TEXTURE_3D);
	
	glTranslatef(0.5f,0.5f,0.5f);
	glScalef(0.7f,0.7f,1.0f);
	
	glMultMatrixf(mat);
	//glTranslatef(0.0f,0.0f,-viewDistance);
	
	//set modelview to identity
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

    
	GLenum texunit[] = {GL_TEXTURE0, GL_TEXTURE1};
	
	for (i=0;i<1;i++) {
		glActiveTexture(texunit[i]);
		//setup the tex gen
		glTexGenfv(GL_S,GL_EYE_PLANE,xPlane);
		glTexGenfv(GL_T,GL_EYE_PLANE,yPlane);
		glTexGenfv(GL_R,GL_EYE_PLANE,zPlane);
		
		glTexGeni(GL_S,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);
		glTexGeni(GL_T,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);
		glTexGeni(GL_R,GL_TEXTURE_GEN_MODE,GL_EYE_LINEAR);  
		
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);
		glEnable(GL_TEXTURE_GEN_R);
		
		glEnable(GL_TEXTURE_3D);
	}
	
	[self programBlendMode];
	
	int slices = 200;
	//draw the slices
	for (ii=0;ii<slices;ii++){
		glPushMatrix();
		glTranslatef(0.0f, 0.0f, -1.0f +(float)ii*2.0f/(float)(slices-1));
		glScalef(0.7,0.7,0.7);

		//[self scaleVolume];

		for(hh=0;hh<7;hh++){
			for(gg=0;gg<7;gg++){
				for(j=0; j < [sfVolumeTextures count]; j++) {
					if ([[sfChannelSettings objectAtIndex:j] isActive]) {
						if([[sfChannelSettings objectAtIndex:j]volumeMode]==0)
							glAlphaFunc(GL_GREATER,[[sfChannelSettings objectAtIndex:j]threshold]*[self alpha]);
						else 
							glAlphaFunc(GL_ALWAYS,0);
						glActiveTexture(GL_TEXTURE0);
						[[sfColors objectAtIndex:j]setOpenGLColor];
						[[sfVolumeTextures objectAtIndex:j]bind];
						glBegin(GL_QUADS);
						glVertex3fv(points[hh*8+gg]);
						glVertex3fv(points[hh*8+gg+1]);
						glVertex3fv(points[(hh+1)*8+gg+1]);
						glVertex3fv(points[(hh+1)*8+gg]);
						glEnd();
					}					
				}
			}
		}
		//glPopMatrix();
	}
	glDisable(GL_TEXTURE_3D);
	for (i=0;i<1;i++) {
		glActiveTexture(texunit[i]);		
		glDisable(GL_TEXTURE_GEN_S);
		glDisable(GL_TEXTURE_GEN_T);
		glDisable(GL_TEXTURE_GEN_R);
		glDisable(GL_TEXTURE_3D);
	}
	glActiveTexture(GL_TEXTURE0);
	
	glPopMatrix();	
	return YES;
}; 

@end
/*
 for(hh=0;hh<7;hh++){
	 for(gg=0;gg<7;gg++){
		 for(j=0; j < [sfVolumeTextures count]; j++) {
			 if ([[sfChannelSettings objectAtIndex:j] isActive]) {
				 glAlphaFunc(GL_GREATER,[[sfChannelSettings objectAtIndex:j]threshold]);
				 glActiveTexture(GL_TEXTURE0);
				 [[sfColors objectAtIndex:j]set];
				 [[sfVolumeTextures objectAtIndex:j]bind];
				 glBegin(GL_QUADS);
				 glVertex3fv(points[hh*8+gg]);
				 glVertex3fv(points[hh*8+gg+1]);
				 glVertex3fv(points[(hh+1)*8+gg+1]);
				 glVertex3fv(points[(hh+1)*8+gg]);
				 glEnd();
			 }					
		 }
	 }
 }
 */