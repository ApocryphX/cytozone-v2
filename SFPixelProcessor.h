//
//  SFPixelProcessor.h
//  Monterey
//
//  Created by ApocryphX on 3/25/07.
//  Copyright 2014 Elarity, LLC.  All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFVoxelType,SFBitplane,SFCLComputeKernel;

@protocol SFPixelProcessing 

- (BOOL)hasInplacePrcessing;
- (void) processBitplane;

@end

@interface SFPixelProcessor : NSObject  {
	SFBitplane			*sourceBitplane;
	SFBitplane			*targetBitplane;
	NSMutableDictionary *parameterDictionary;
	

}

@property (retain)		SFBitplane *sourceBitplane;
@property (readonly)	NSMutableDictionary *parameterDictionary;
@property (readonly)	SFBitplane *targetBitplane;

@end


@interface SFPixelMaximum : SFPixelProcessor <SFPixelProcessing>  {
	SFCLComputeKernel	*computeKernel;
}
@end

