//
//  SFOGLRectangle.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLRectangle.h"

#import <OpenGL/gl.h>

@implementation SFOGLRectangle

- (id) init {
	self = [super init];
	return self;
}

- (void)setTexture:(SFOGLTextureRectangle*)value {
	sfTexture = value;
};

- (void)setZPosition:(float)value {
	sfZPosition = value;
};

- (float)zPosition {
	return sfZPosition;
}

- (SFOGLTextureRectangle*)texture {
	return sfTexture;
};

- (BOOL) draw {
	if (sfTexture == Nil){
		glBegin(GL_POLYGON);
		glVertex3f ( 0.0,  0.0, 0.0f);
		glVertex3f ( 1.0,  0.0, 0.0f);
		glVertex3f ( 1.0,  1.0, 0.0f);
		glVertex3f ( 0.0,  1.0, 0.0f);
		glEnd(); //GL_POLYGON
	}
	else {
		[sfTexture bind];
		glBegin(GL_POLYGON);
		glTexCoord2f (0.0f, 0.0f);;
		glVertex3f ( 0.0,  0.0, 0.0f);
		glTexCoord2f (1.0f, 0.0f);;
		glVertex3f ( 1.0,  0.0, 0.0f);
		glTexCoord2f (1.0f, 1.0f);;
		glVertex3f ( 1.0,  1.0, 0.0f);
		glTexCoord2f (0.0f, 1.0f);
		glVertex3f ( 0.0,  1.0, 0.0f);
		glEnd(); //GL_POLYGON
		[SFOGLTextureRectangle unbind];	
	}
	return YES;
};


@end
