//
//  SFOGLChannelSetting.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLChannelSetting.h"
#import "SFChannelInfo.h"
#import "SFOGLColor.h"
#import "SFOGLVolumeScene.h"
#import "SFOGLVolume.h"

@implementation SFOGLChannelSetting

- (id)initWithChannelInfo:(SFChannelInfo*)chinfo {
	self = [super init];
	[self setColorOfVolume:[[chinfo color]copy]];
	[self setColorOfShadow:[NSColor blackColor]];
	[self setName:[[chinfo name]copy]];  
	[self setIsActive:YES];
	[self setVolumeMode:0];
	[self setNoiseFilterSize:0];
	[self setThreshold:0.01];
	return self;
};

- (id)initWithOGLChannelSetting:(SFOGLChannelSetting*)setting {
	self = [super init];
	[self setName:[setting name]];  
	[self setIsActive:[setting isActive]];
	[self setColorOfVolume:[setting colorOfVolume]];
	[self setColorOfShadow:[setting colorOfShadow]];
	[self setThreshold:[setting threshold]];
	[self setVolumeMode:0];
	[self setNoiseFilterSize:[setting noiseFilterSize]]; 
	sfOpenGLView = setting->sfOpenGLView;
	return self;
};


- (void)setOpenGLView:(NSOpenGLView*)view {
	sfOpenGLView = view;
};


- (void) setColorOfVolume:(NSColor*)value {
	sfColorOfVolume = [[SFOGLColor alloc]initWithColor:value];
	[sfOpenGLView setNeedsDisplay:YES];
};

- (void) setColorOfShadow:(NSColor*)value {
	sfColorOfShadow = [[SFOGLColor alloc]initWithColor:value];
	[sfOpenGLView setNeedsDisplay:YES];
};


- (void) setName:(NSString*)value {
	sfName = value;
};

- (void) setIsActive:(BOOL)value {
	if(value != sfIsActive) {
		sfIsActive = value;
		[sfOpenGLView setNeedsDisplay:YES];
	}
};

- (void) setIsIntensitySquare:(BOOL)value {
	sfIsIntensitySquare = value;
};

- (void) setThreshold:(float)value {
	sfThreshold = value;
};

- (void) setVolumeMode:(NSUInteger)value {
	sfVolumeMode = value;
};

- (void) setShowVolume:(BOOL)value {
	sfShowVolume = value;
};

- (void) setNoiseFilterSize:(NSUInteger)value {
	sfNoiseFilterSize = value;
};

- (NSString*) name {
	return sfName;
};

- (NSColor*) colorOfVolume {
	return [sfColorOfVolume color];
};

- (NSColor*) colorOfShadow {
	return [sfColorOfShadow color];
};

- (BOOL) isActive {
	return sfIsActive;
};

- (float) threshold {
	return sfThreshold;
};

- (NSUInteger) volumeMode {
	return sfVolumeMode;
};

- (NSUInteger) noiseFilterSize {
	return sfNoiseFilterSize;
};

- (void) setOGLColorWithShadowColor {
	[sfColorOfShadow setOpenGLColor];
};

- (void) setOGLColorWithVolumeColor {
	[sfColorOfVolume setOpenGLColor];
};

- (void) setOGLColorWithVolumeColorWithAlpha:(float)alpha {
	[sfColorOfVolume setWithAlpha:alpha];
};

- (BOOL) isIntensitySquare {
	return sfIsIntensitySquare;
};

- (id)copyWithZone:(NSZone *)zone
{
    SFChannelInfo *copy = [ [ [self class] allocWithZone: zone] initWithOGLChannelSetting:self];
    return copy;
};

- (id)copy {
	return [self copy];
};


@end
