//
//  SFVolumeViewController.m
//  VoxelMachine
//
//  Created by ApocryphX on Sat Jul 10 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFVolumeWindowController.h"
#import "SFOpenGLView.h"
#import "SFOGLTransform.h"
#import "SFOGLVolumeScene.h"
#import "SFOGLVolume.h"
#import "SFOGLRotation.h"
#import "SFNDVoxelData.h"
#import "SFNDDataset.h"
#import "SFChannelInfo.h"
#import "SFNDExperiment.h"
#import "SFNDDimension.h"

#import "SFNDExperiment.h"
#import "SFImageScrollView.h"
#import "SFImageFlowView.h"

#import "SFMovieFrameViewController.h"
#import "SFApplicationDelegate.h"

@implementation SFVolumeWindowController

@dynamic nDDataset;
@synthesize isTimeSeries;
@synthesize isVolume;
@synthesize progressIndicator;
@synthesize nodeTitle;

- (void)awakeFromNib {
};


- (void)windowDidLoad {
    [super windowDidLoad];
}

- (NSString *)windowTitleForDocumentDisplayName:(NSString *)displayName {
	NSString* ret;
    if(nodeTitle) {
        ret = [displayName stringByAppendingString:@" - "];
        ret = [ret stringByAppendingString:nodeTitle];
    }
    else
        ret = displayName;
	return ret;
}

- (IBAction) showSelectedDataset:(id)sender {
	NSArray* array = [sfDatasetsController selectedObjects];
	SFNDExperiment* doc = [self document];
	[doc showSelected:array];
}

- (IBAction) showInfoPanel:(id)sender {
    SFApplicationDelegate *delegte = [[NSApplication sharedApplication]delegate];
	[delegte applicationShowRenderPanel:self];
}

- (IBAction)updateView:(id)sender {
	[sfOGLView setNeedsDisplay:YES];
}


// In this case, just moving an image view back and forth.
// Note the use of the animator.
-(IBAction)moveView:(id)sender {
    NSView *volumeView = [[self window]contentView];
    NSRect frame = [volumeView frame];
    frame.origin.y = frame.origin.y + frame.size.height/2.0;
    frame.size.height=frame.size.height/2.0;
    [[volumeView animator] setFrame: frame];
}


- (IBAction) useZoom:(id)sender {
	[sfOGLView setMouseTracker:[self textureTransform]];
	[[self textureTransform] useZoom];
	[self setTransformIndex:2];
};

- (IBAction) useRotation:(id)sender {
	[sfOGLView setMouseTracker:[self textureTransform]];
	[[self textureTransform] useRotation];
	[self setTransformIndex:0];
};

- (IBAction) useTranslation:(id)sender {
	[sfOGLView setMouseTracker:[self textureTransform]];
	[[self textureTransform] useTranslation];
	[self setTransformIndex:1];
};

- (IBAction) resetRotation:(id)sender {
	if ([sender class] == [NSMenuItem class] ) {
		NSMenuItem* item = sender;
		if ([[item title] compare:@"Reset All"] == NSOrderedSame) {
			[[[sfOGLView textureTransform]rotation]reset];
			[sfOGLView setNeedsDisplay:YES];			
		}
		if ([[item title] compare:@"Reset X"] == NSOrderedSame) {
			[[[sfOGLView textureTransform]rotation]resetAxis:0];
			[sfOGLView setNeedsDisplay:YES];			
		}
		if ([[item title] compare:@"Reset Y"] == NSOrderedSame) {
			[[[sfOGLView textureTransform]rotation]resetAxis:1];
			[sfOGLView setNeedsDisplay:YES];			
		}
		if ([[item title] compare:@"Reset Z"] == NSOrderedSame) {
			[[[sfOGLView textureTransform]rotation]resetAxis:2];
			[sfOGLView setNeedsDisplay:YES];			
		}
	}
};

- (IBAction) resetTranslation:(id)sender {
	if ([sender class] == [NSMenuItem class] ) {
		NSMenuItem* item = sender;
		NSPoint trans = [[sfOGLView textureTransform]translation];
		if ([[item title] compare:@"Reset All"] == NSOrderedSame) {
			trans.x = 0.0f;
			trans.y = 0.0f;
			[sfOGLView setNeedsDisplay:YES];			
		}
		if ([[item title] compare:@"Reset X"] == NSOrderedSame) {
			trans.x = 0.0f;
			[sfOGLView setNeedsDisplay:YES];			
		}
		if ([[item title] compare:@"Reset Y"] == NSOrderedSame) {
			trans.y = 0.0f;
			[sfOGLView setNeedsDisplay:YES];			
		}
		[[sfOGLView textureTransform]setTranslation:trans];
	}
};

- (IBAction) setZoom:(id)sender {
	if ([sender class] == [NSMenuItem class] ) {
		NSMenuItem* item = sender;
		NSString *title = [item title];
		float value = [title floatValue];
		if (value != 0.0f ) {
			[[sfOGLView textureTransform]setZoom:value];
			[sfOGLView setNeedsDisplay:YES];						
		}
	}
};

- (IBAction) resetTransform:(id)sender {
	NSPoint trans = [[sfOGLView textureTransform]translation];
	trans.x = 0.0f;
	trans.y = 0.0f;
	[[sfOGLView textureTransform]setTranslation:trans];
	[[[sfOGLView textureTransform]rotation]reset];
	[[sfOGLView textureTransform]setZoom:1.0f];
	[sfOGLView setNeedsDisplay:YES];
};

- (IBAction) setTexture2D:(id)sender {
	[sfVolumeScene setTextureMode:0];
};

- (IBAction) setTexture25D:(id)sender {
	[sfVolumeScene setTextureMode:1];
};

- (IBAction) setTexture3D:(id)sender {
	[sfVolumeScene setTextureMode:2];
};


- (IBAction)firstCube:(id)sender {
	[sfVolumeScene setCubeIndex:1];
    [sfOGLView setNeedsDisplay:YES];
};

- (IBAction)lastCube:(id)sender {
	[sfVolumeScene setCubeIndex:[sfVolumeScene numberOfCubes]];
    [sfOGLView setNeedsDisplay:YES];

};

- (IBAction)nextCube:(id)sender {
	[sfVolumeScene setCubeIndex:[sfVolumeScene cubeIndex]+1];
    [sfOGLView setNeedsDisplay:YES];

};

- (IBAction)prevCube:(id)sender {
	[sfVolumeScene setCubeIndex:[sfVolumeScene cubeIndex]-1];
    [sfOGLView setNeedsDisplay:YES];

};


- (IBAction) transformTexture:(id)sender {
	[sfOGLView setMouseTracker:[sfVolumeScene textureTransform]];
};

- (IBAction) setRenderModeAlpha:(id)sender {
	[[sfVolumeScene volume]setRenderMode:0];
};

- (IBAction) setRenderModeMIP:(id)sender {
	[[sfVolumeScene volume]setRenderMode:1];
};

- (IBAction) setRenderModeBlend:(id)sender {
	[[sfVolumeScene volume]setRenderMode:2];
};

- (IBAction) setInvertVolume:(id)sender {
	[[sfVolumeScene volume]setIsZInverted:YES];
};

- (IBAction) setUprightVolume:(id)sender {
	[[sfVolumeScene volume]setIsZInverted:NO];
};

- (void)setVolumeScene:(SFOGLVolumeScene*)value {
	sfVolumeScene = value;
	[sfOGLView setScene:sfVolumeScene];
	[sfVolumeScene setView:sfOGLView];
};

- (SFOGLVolumeScene*)volumeScene {
	return sfVolumeScene;
};

- (SFOGLTransform*)textureTransform {
	return [sfVolumeScene textureTransform];
};

- (void)setTransformIndex:(NSUInteger)idx {
	sfTransformIndex = idx;
};

- (NSUInteger)transformIndex {
	return sfTransformIndex;
};

- (void)setIndex:(NSUInteger)idx {
	sfIndex = idx;
};

- (NSUInteger)index {
	return sfIndex;
};

//---------------------------------------

- (void)setNDDataset:(SFNDDataset *)nDDataset {
    [self setVolumeScene:[SFOGLVolumeScene new]];
	[self transformTexture:self];
    sfNDDataset = nDDataset;
	[sfVolumeScene setNDVoxelData:[sfNDDataset nDVoxelData]];
    SFNDDimension *ndDimension = [[sfNDDataset nDVoxelData] nDDimension];
    self.isTimeSeries = ndDimension.isTimeSeries;
    self.isVolume = [ndDimension isVolume];
    
    NSBundle *frameworkBundle = [NSBundle bundleForClass:[SFMovieFrameViewController class]];
    movieFrameViewController = [[SFMovieFrameViewController alloc]initWithNibName:@"SFMovieFrameViewController" bundle:frameworkBundle];
    
    movieFrameViewController.openGLView = sfOGLView;
    movieFrameViewController.volumeScene = sfVolumeScene;
    [lowerViewPlaceholder addSubview:[movieFrameViewController view]];
    NSView *movieView = [movieFrameViewController view];
    movieViewHeight = movieView.frame.size.height;
    [[movieFrameViewController view] setFrameSize:NSMakeSize(lowerViewPlaceholder.frame.size.width,
                                                             movieViewHeight)];
    [self showInfoPanel:self];
    
}

-(SFNDDataset *)nDDataset {
    return sfNDDataset;
}

- (SFNDVoxelData*)nDVoxelData {
	return [sfNDDataset nDVoxelData]; //check
};

- (SFOpenGLView*)oGLView {
	return sfOGLView;
};

- (IBAction)toggleMovieView:(id)sender {
    
    if(lowerViewPlaceholder.frame.size.height==0.0) {
        [NSAnimationContext beginGrouping];
        [[lowerViewPlaceholder animator]setFrameSize:NSMakeSize(sfOGLView.frame.size.width,movieViewHeight)];
        [[sfOGLView animator ]setFrame:NSMakeRect (sfOGLView.frame.origin.x,
                                                   lowerViewPlaceholder.frame.origin.y+movieViewHeight,
                                                   sfOGLView.frame.size.width,
                                                   sfOGLView.frame.size.height-movieViewHeight)];
        [NSAnimationContext endGrouping];
    }
    else {
        movieViewHeight = lowerViewPlaceholder.frame.size.height;
        [NSAnimationContext beginGrouping];
        [[sfOGLView animator ]setFrame:NSMakeRect (lowerViewPlaceholder.frame.origin.x,
                                                   lowerViewPlaceholder.frame.origin.y,
                                                   sfOGLView.frame.size.width,
                                                   sfOGLView.frame.size.height+lowerViewPlaceholder.frame.size.height)];
        [[lowerViewPlaceholder animator]setFrameSize:NSMakeSize(sfOGLView.frame.size.width,0.0)];
        [NSAnimationContext endGrouping];
        
    }
}



- (IBAction) makeMovie:(id)sender {
    NSSavePanel *movieSavePanel = [NSSavePanel savePanel];
    [movieSavePanel beginSheetModalForWindow:self.window  completionHandler:^(NSInteger returnCode) {
        if (returnCode == NSFileHandlingPanelOKButton) {
            [movieFrameViewController makeMovieForPath:[[movieSavePanel URL]path]];
        }
    }];

}
 
#ifndef DEMO

- (IBAction)saveShapshot:(id)sender {
    NSHomeDirectory();
	NSSavePanel* panel = [NSSavePanel savePanel];
	//[panel setRequiredFileType:@"tiff"];
    [panel setAllowedFileTypes:[NSArray arrayWithObject:@"tiff"]];
	[panel setAllowsOtherFileTypes:NO];
    
    [panel beginSheetModalForWindow:[self window]
                  completionHandler:^(NSInteger returnCode){
                      if (returnCode == NSOKButton){
                          NSImage* image = [sfOGLView snapShot];
                          NSData* tiffdata = [image TIFFRepresentation];
                          [tiffdata writeToFile:[[panel URL] path] atomically:NO];		
                      }
                  }];
}

#endif



- (void)dealloc {
	[sfOGLView setScene:nil];
	[sfVolumeScene setView:nil];
}




@end
