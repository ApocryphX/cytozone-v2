//
//  SFLCSReader.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFLCSBlockReader : NSObject {
	NSString* sfFileName;
	NSData* sfLCSData;
}

- (id)initWithFile:(NSString *)fileName;
- (NSString*)fileName;

- (NSMutableDictionary*)blockWithIDSeries;
- (NSMutableDictionary*)blockWithIDImages:(uint32_t)number;
- (NSMutableDictionary*)blockWithIDDimDescr:(uint32_t)number;
- (NSMutableDictionary*)blockWithIDLUTDescr:(uint32_t)number;

@end
