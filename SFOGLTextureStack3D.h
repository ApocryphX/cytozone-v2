//
//  SFOGLTextureStack2D.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "SFOGLTexturing.h"
#import "SFOGLVolumeTexture.h"


@class SFNDVoxelData, SFNDDimension, SFAlignment2D, SFVoxelInfo, SFOGLVoxelFormat;

@interface SFOGLTextureStack3D : SFOGLVolumeTexture <SFOGLTexturing3D> {
	SFNDDimension* sfNDDimension;
	SFAlignment2D* sfAlignment;
	NSMutableArray* sfTextures;
	BOOL sfIsDownsampled;
}

- (void)bindTextureAtIndex:(NSUInteger)idx;
- (NSUInteger)count;

@end
