//
//  SFOGLVolumeScene.h
//  VoxelMachine
//
//  Created by ApocryphX on Mon Jul 26 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class	SFOGLContext, SFNDVoxelData, SFOGLVolume, SFOGLTransform, SFOpenGLView, 
		SFVolumeWindowController, SFOGLBackplane, SFOGLRenderState;

@interface SFOGLVolumeScene : NSObject {

	SFNDVoxelData* sfNDVoxelData;
	SFOpenGLView* sfOGLView;
	//SFOGLTransform* sfTextureTransform;
	NSMutableArray* sfChannelSettings;
	SFOGLVolume* sfVolume;
	
	float sfClipPlane;	
	float sfClipWidth;
	NSUInteger sfNumberOfCubes;
	NSUInteger sfCubeIndex;
	BOOL active;
	NSUInteger sfVolumeIndex;	
	NSUInteger sfTextureMode;
}

@property (assign) NSUInteger cubeIndex;

- (id)initWithView:(SFOpenGLView*)view;

- (void)setCubeIndex:(NSUInteger)value;
- (void)setNDVoxelData:(SFNDVoxelData*)nddata;

- (void)setTextureTransform:(SFOGLTransform*)value;
- (SFOGLTransform*)textureTransform;

- (void)setClipPlane:(float)value;
- (float)clipPlane;

- (void)setClipWidth:(float)value;
- (float)clipWidth;


- (void) setTextureMode:(NSUInteger)value;
- (NSUInteger) textureMode;

- (NSUInteger)numberOfCubes;
- (NSUInteger)cubeIndex;

- (void) setVolumeIndex:(NSUInteger)value;
- (NSUInteger) volumeIndex;


- (void)setVolume:(SFOGLVolume*)volume;
- (SFOGLVolume*)volume;

- (void) setView:(SFOpenGLView*)value;
- (SFOpenGLView*)view;

- (void)drawScene;

- (NSUInteger)countOfChannelSettings;
- (id)objectInChannelSettingsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inChannelSettingsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromChannelSettingsAtIndex:(NSUInteger)idx;

@end


