//
//  SFFrameBufferObject.m
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFFrameBufferObject.h"
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>

@implementation SFFrameBufferObject

@synthesize size = FBOSize;

- (id)initWithSize:(NSSize)imageSize
{
    self = [super init];
    if (self) {
        // Initialization code here.
        
        // If not previously setup
        // generate IDs for FBO and its associated texture
        
        // Make sure the framebuffer extenstion is supported
        const GLubyte* strExt;
        GLboolean isFBO;
        // Get the extenstion name string.
        // It is a space-delimited list of the OpenGL extenstions 
        // that are supported by the current renderer
        strExt = glGetString(GL_EXTENSIONS);
        isFBO = gluCheckExtension((const GLubyte*)"GL_EXT_framebuffer_object", strExt);
        if (!isFBO)
        {
            NSLog(@"Your system does not support framebuffer extension");
            return nil;
        }
        
        // create FBO object
        glGenFramebuffersEXT(1, &FBOid);
        // the texture
        glGenTextures(1, &FBOTextureId);
        
        // Bind to FBO
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, FBOid);
        
        // Sanity check against maximum OpenGL texture size
        // If bigger adjust to maximum possible size
        // while maintain the aspect ratio
        GLint maxTexSize; 
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTexSize);
        if (imageSize.width > maxTexSize || imageSize.height > maxTexSize) 
        {
            double maxSize = MAX(imageSize.height, imageSize.width);
            double scale = maxSize/(double)maxTexSize;
            imageSize.width= imageSize.width*scale;
            imageSize.height = imageSize.height*scale;
            NSLog(@"Framebuffer Object scaled to maximum texture size!");
        }
        FBOSize = imageSize;
        // Initialize FBO Texture
        glBindTexture(GL_TEXTURE_RECTANGLE_ARB, FBOTextureId);
        // Using GL_LINEAR because we want a linear sampling for this particular case
        // if your intention is to simply get the bitmap data out of Core Image
        // you might want to use a 1:1 rendering and GL_NEAREST
        glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        // the GPUs like the GL_BGRA / GL_UNSIGNED_INT_8_8_8_8_REV combination
        // others are also valid, but might incur a costly software translation.
        glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, imageSize.width, imageSize.height, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, NULL);
        
        // and attach texture to the FBO as its color destination
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, FBOTextureId, 0);
        
        // Initialize Depth Render Buffer
        GLuint depth_rb;
        glGenRenderbuffersEXT(1, &depth_rb);
        glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depth_rb);
        glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, imageSize.width, imageSize.height);
        // and attach it to the FBO
        glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depth_rb);
        
        // Make sure the FBO was created succesfully.
        GLuint check = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
        if (GL_FRAMEBUFFER_COMPLETE_EXT == check) 
        {
            NSLog(@"Framebuffer Object created");    
            glClearColor(1.0, 0.0,0.0, 1.0);
            glClear(GL_COLOR_BUFFER_BIT);
            
        }
        else
        {
            NSLog(@"Framebuffer Object creation or update failed");
        }
        // unbind FBO 
        glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    }
    return self;
}

- (void) bind {
   glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, FBOid); 
}

+ (void) unbind {
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}


- (void)dealloc {
    // Delete the texture
    glDeleteTextures(1, &FBOTextureId);
    // and the FBO
    glDeleteFramebuffersEXT(1, &FBOid);
    FBOid = 0;
}

@end
