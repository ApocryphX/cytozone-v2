//
//  SFGraphicsObject.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//


@protocol SFGrapicInteraction

@property (retain) NSAffineTransform *transform;
@property (assign) NSRect bounds;

- (void)mouseDownAtPoint:(NSPoint)point clickCount:(NSUInteger)count;
- (void)mouseDragToPoint:(NSPoint)point;
- (void)mouseUpAtPoint:(NSPoint)point;

@end

@interface SFAbstractGraphicsObject : NSObject

@property (retain) NSAffineTransform *transform;
@property (assign) NSRect bounds;
@property (retain) NSBezierPath *bezierPath;
@property (assign) BOOL isCreationFinished;
- (void)draw;

@end

typedef  SFAbstractGraphicsObject <SFGrapicInteraction> SFGraphicsObject;