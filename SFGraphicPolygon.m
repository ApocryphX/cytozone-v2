//
//  SFGraphicPolygon.m
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicPolygon.h"

@implementation SFGraphicPolygon

@synthesize points;

- (void)mouseDownAtPoint:(NSPoint)point clickCount:(NSUInteger)clickCount {
    if(self.points) {
        if(clickCount==2) {
            isComplete=YES;
        }
        else {
            [points appendBytes:&point length:sizeof(NSPoint)];
        }
    }
    else {
        isComplete=NO;
        self.points = [NSMutableData new];
        [points appendBytes:&point length:sizeof(NSPoint)];
    }
};

- (void)mouseDragToPoint:(NSPoint)point {
    [points appendBytes:&point length:sizeof(NSPoint)];    
};

- (void)mouseUpAtPoint:(NSPoint)point {
    
};


- (NSBezierPath*)bezierPath {
    NSBezierPath *path = nil;
    if(points) {
        NSUInteger pointCount = points.length/sizeof(NSPoint);
        if(pointCount>1) {
            path = [NSBezierPath bezierPath];
            [path setLineWidth:1.0];
            [path  appendBezierPathWithPoints:points.mutableBytes count:pointCount];
            if(isComplete)
              [path closePath];
            [path stroke];
        }
    }
    return path;
};

- (BOOL)isCreationFinished {
    if(points!=nil)
        return isComplete;
    else
        return NO;
            
};

@end
