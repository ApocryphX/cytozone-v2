//
//  SFOGLBackplane.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLBackplane.h"

#include <OpenGL/gl.h>

#import "utility.h"
#import "SFVolumeWindowController.h"
#import "SFNDVoxelData.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFVoxelInfo.h"
#import "SFAlignment2D.h"
#import "SFOGLTextureRectangle.h"
#import "SFOGLChannelSetting.h"
#import "SFOGLColor.h"

@interface SFOGLBackplane (PrivateMethods)
- (void) drawColoredRectangle;
- (void) createTextures;
- (void) drawShadow;
@end

@implementation SFOGLBackplane


- (id)init {
	self = [super init];
	sfIndex = 0;
	[self setColor: [NSColor darkGrayColor]];
	sfTextures = [NSMutableArray new];
	return self;
}

- (void) createTextures {
};

- (void)setContext:(NSOpenGLContext*)cxt {
	if (cxt != sfContext) {
		sfContext = cxt;
		if (sfTextures!=nil && cxt != nil) {
			NSUInteger i;
			for(i=0;i<[sfTextures count];i++) 
				[[sfTextures objectAtIndex:i]setContext:cxt];			
		}
	}
};


- (void)setNDDimension:(SFNDDimension*)nddim voxelInfos:(NSArray*)vinfos  {
	sfTextures = [NSMutableArray new];
	sfShadows = nil;
	NSUInteger i;
	for(i=0; i<[vinfos count];i++) {
		SFOGLTextureRectangle* newtex = [[SFOGLTextureRectangle alloc]initWithContext:sfContext];
		SFVoxelInfo* vinfo = [vinfos objectAtIndex:i];
		[newtex setNDDimension:nddim voxelInfo:vinfo];
		//[newtex setFormat:GL_ALPHA];
		//[newtex loadTexture]; //opengl
		[sfTextures addObject:newtex];
	}
}

- (void)setChannelSettings:(NSArray*)value {
	sfChannelSettings = value;
};
 

- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment index:(NSUInteger)idx {
	SFOGLTextureRectangle* tex = [sfTextures objectAtIndex:idx];
	[tex replaceBytes:bytes alignment:alignment];
};


- (void) setMode:(NSUInteger)value {
	sfMode = value;
};

- (void)setColor:(NSColor*)value {
	sfColor = value;
};

- (void)setAlpha:(float)value {
	sfAlpha = value;
};


- (NSColor*)color {
	return sfColor;
};

- (NSUInteger) mode {
	return sfMode;
};

- (SFOGLTextureRectangle*)textureAtIndex:(NSUInteger)idx {
	return [sfTextures objectAtIndex:idx];	
};

- (void) draw {
	switch (sfMode) {
		case 1: {
			[self drawColoredRectangle];
			[self drawShadow]; 
			break;
		}			
		default :{
			break;
		}
	}
};

- (void)drawColoredRectangle {
	

	// disable alpha blending & test
	// enable alpha blending
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_RECTANGLE_EXT);
	
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	NSColor* rgbacolor = [sfColor colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
	glColor4f([rgbacolor  redComponent], [rgbacolor greenComponent],
			  [rgbacolor  blueComponent], [rgbacolor alphaComponent]);
	
	glBegin(GL_QUADS);
	glVertex3f ( 0.0,  0.0, -0.01f);
	glVertex3f ( 1.0,  0.0, -0.01f);
	glVertex3f ( 1.0,  1.0, -0.01f);
	glVertex3f ( 0.0,  1.0, -0.01f);
	glEnd(); //GL_POLYGON
    glDisable(GL_DEPTH_TEST);
};

- (void)drawShadow {
	glEnable(GL_TEXTURE_RECTANGLE_EXT);

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	//Disable alpha testing (=threshold)
	glDisable(GL_ALPHA_TEST);
	//setup texture units
	glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_MODULATE );

	// disable Depth Test
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	
	NSUInteger i;
	//glPushMatrix();
	//glTranslatef(0.0f,0.0f,-0.01f);
	for (i=0; i < [sfTextures count]; i++) {
		if ([[sfChannelSettings objectAtIndex:i] isActive]) {
			if (sfChannelSettings)
				[[sfChannelSettings objectAtIndex:i]setOGLColorWithShadowColor];
			else
				glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
			
			SFOGLTextureRectangle* tex = [sfTextures objectAtIndex:i];
			float xsize = [tex size].width;
			float ysize = [tex size].height;
			[tex bind];
			glBegin(GL_QUADS);
			glTexCoord2f (1.0f, 1.0f);
			glVertex3f ( 0.0,  0.0, 0.0f);
			glTexCoord2f (xsize, 1.0f);
			glVertex3f ( 1.0,  0.0, 0.0f);
			glTexCoord2f (xsize, ysize);
			glVertex3f ( 1.0,  1.0, 0.0f);
			glTexCoord2f (1.0f, ysize);
			glVertex3f ( 0.0,  1.0, 0.0f);
			glEnd(); //GL_POLYGON									
		}
	}
	glDisable(GL_TEXTURE_RECTANGLE_EXT);
	//glPopMatrix();
};


;


@end
