//
//  SFVoxelData.h
//  VoxelMachine
//
//  Created by ApocryphX on Fri May 28 2014.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFGeometry.h"

@class SFVoxelInfo, SFAlignment2D;

@interface SFVoxelData : NSObject {
	SFVoxelData* sfParent;
	SFAlignment2D* sfAlignment;
	NSData* sfData;
} 

- (id)initWithAlignment:(SFAlignment2D*)alignment;
- (id)initWithSize:(SFSize)size voxelInfo:(SFVoxelInfo*)vinfo;
- (id)initWithData:(NSData*)data alignment:(SFAlignment2D*)alignment;

- (NSUInteger) bytesPerVoxel;
- (NSUInteger) samplesPerVoxel;
- (const void*) bytes;
- (NSData*) data;

- (NSData*)dataAtFrameIndex:(NSUInteger)frame;

- (SFAlignment2D*)alignment;
- (SFVoxelInfo*)voxelInfo;

- (NSUInteger) numberOfFrames;
- (NSUInteger) bytesPerFrame;
- (SFVoxelData*)frameAtIndex:(NSUInteger)idx;
- (SFVoxelData*)slice:(NSUInteger)offset length:(NSUInteger)length;

- (SFVoxelData*)sliceAtFrame:(NSUInteger)idx count:(NSUInteger)length;

- (void)replaceBytes:(const void*)data alignment:(SFAlignment2D*)alignment;
- (void)clearBorderPixels;

@end
