//
//  SFSelectionRectangle.m
//  ROI Prototypes
//
//  Created by ApocryphX on 4/7/13.
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFSelectionRectangle.h"
#import "SFDrawingHandle.h"

const NSString *SFKDrawingHandleKey = @"drawing_handle";

@implementation SFSelectionRectangle

@synthesize bounds;
@synthesize handleSize;
@synthesize handleColor;
@synthesize handleShadowColor;

- (id)initWithBounds:(NSRect)rect
{
    self = [super init];
    if (self) {
        self.bounds=rect;
        handleSize = NSMakeSize(6.0,6.0);
        handleColor = [NSColor redColor];
        handleShadowColor = [NSColor darkGrayColor];
        handelPoints[0]=NSMakePoint(NSMinX(self.bounds), NSMinY(self.bounds));
        handelPoints[1]=NSMakePoint(NSMidX(self.bounds), NSMinY(self.bounds));
        handelPoints[2]=NSMakePoint(NSMaxX(self.bounds), NSMinY(self.bounds));
        handelPoints[3]=NSMakePoint(NSMinX(self.bounds), NSMidY(self.bounds));
        handelPoints[4]=NSMakePoint(NSMaxX(self.bounds), NSMidY(self.bounds));
        handelPoints[5]=NSMakePoint(NSMinX(self.bounds), NSMaxY(self.bounds));
        handelPoints[6]=NSMakePoint(NSMidX(self.bounds), NSMaxY(self.bounds));
        handelPoints[7]=NSMakePoint(NSMaxX(self.bounds), NSMaxY(self.bounds));
    }
    return self;
}



- (void)createHandles {
    // Draw handles at the corners and on the sides.
}


- (void)draw {
    
}

@end
