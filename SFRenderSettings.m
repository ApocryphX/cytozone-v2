//
//  SFRenderSettings.m
//  VoxelMachine
//
//  Created by ApocryphX on 6/19/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFRenderSettings.h"
#import "SFOGLVolumeScene.h"
#import "SFOpenGLView.h"
#import "SFOGLTransform.h"
#import "SFOGLVolume.h"

@implementation SFRenderSettings

- (id)init {
	self = [super init];
	if(self) {
		sfTextureTransform = [SFOGLTransform new];
	}
	return self;
};

- (id)initWithVolumeScene:(SFOGLVolumeScene*)scene {
	if((self = [super init])) {
        sfTextureTransform = [SFOGLTransform new];
		[self readStateWithVolumeScene:scene];
	}
	return self;
    
}
- (void)readStateWithVolumeScene:(SFOGLVolumeScene*)scene {
	
	SFOGLVolume* volume = [scene volume];
	sfRenderMode = [volume renderMode];
	sfBlendColor = [volume blendColor];
	sfBackplaneColor = [volume backplaneColor];
	sfAlpha = [volume alpha];
	sfZScale = [volume zScale];
	sfIsZInverted = [volume isZInverted];
	sfBackplaneMode = [volume backplaneMode];
	
	sfVolumeIndex = [scene cubeIndex];
	[sfTextureTransform setTransform:[scene textureTransform]];
	sfClipPlane = [scene clipPlane];
	sfClipWidth = [scene clipWidth];
};

- (void)restoreStateWithVolumeScene:(SFOGLVolumeScene*)scene {
	SFOGLVolume* volume = [scene volume];
	
	[volume setBlendColor:sfBlendColor];
	[volume setAlpha:sfAlpha];
	[volume setIsZInverted:sfIsZInverted];
	[volume setRenderMode:sfRenderMode];
	[volume setZScale:sfZScale];
	[volume setBackplaneColor:sfBackplaneColor];
	[volume setBackplaneMode:sfBackplaneMode];

	[scene setCubeIndex:sfVolumeIndex];
	[scene setTextureTransform:sfTextureTransform];
	[scene setClipPlane:sfClipPlane];
	[scene setClipWidth:sfClipWidth];
};

- (SFRenderSettings*)interpolatedStateWithFraction:(double)frac ofState:(SFRenderSettings*)state {
	SFRenderSettings* ret = [SFRenderSettings new];
	double ifrac = 1.0f - frac;
	ret->sfClipPlane = ifrac * sfClipPlane + state->sfClipPlane * frac;
	ret->sfClipWidth = ifrac * sfClipWidth + state->sfClipWidth * frac;
	ret->sfNumberOfCubes = sfNumberOfCubes;
	ret->sfCubeIndex = ifrac * (float) sfCubeIndex + (float) state->sfCubeIndex * frac;
	if (frac < 0.5) {
		ret->active = active;	
		ret->sfRenderMode = sfRenderMode;
		ret->sfBackplaneMode = sfBackplaneMode;
		ret->sfIsZInverted = sfIsZInverted;
		ret->sfIsPlaneTextureVisible = sfIsPlaneTextureVisible;
	}
	else {
		ret->active = state->active;	
		ret->sfRenderMode = state->sfRenderMode;
		ret->sfBackplaneMode = state->sfBackplaneMode;
		ret->sfIsZInverted = state->sfIsZInverted;
		ret->sfIsPlaneTextureVisible = state->sfIsPlaneTextureVisible;
	}
	ret->sfBlendColor = [sfBlendColor blendedColorWithFraction:frac ofColor:state->sfBlendColor];
	ret->sfBackplaneColor = [sfBackplaneColor blendedColorWithFraction:frac ofColor:state->sfBackplaneColor];
	ret->sfVolumeIndex = ifrac * (double)sfVolumeIndex + frac * (double)state->sfVolumeIndex;	
	ret->sfAlphaThreshold = ifrac * sfAlphaThreshold + frac * state->sfAlphaThreshold;
	ret->sfAlpha = ifrac * sfAlpha + frac * state->sfAlpha;
	ret->sfZScale = ifrac * sfZScale + frac * state->sfZScale;
	[ret->sfTextureTransform setTransform:[sfTextureTransform interpolatedTransformWithFraction:frac ofTransform:state->sfTextureTransform]];
	return ret;	
};


@end
