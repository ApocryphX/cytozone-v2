//
//  SFGraphicsViewController.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SFGraphicsView, SFNDImporter;

@interface SFGraphicsViewController : NSViewController {
    NSUInteger toolMode;
}

@property (assign) NSUInteger toolMode;

- (SFGraphicsView*)insertViewInParentView:(NSView*)parentView;

- (IBAction)setModeSelection:(id)sender;
- (IBAction)setModeRectangle:(id)sender;
- (IBAction)setModeOval:(id)sender;
- (IBAction)setModePolygon:(id)sender;

@end
