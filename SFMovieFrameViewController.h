//
//  SFMovieFrameViewController.h
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>

@class SFVolumeAnimator, SFOpenGLView, SFOGLVolumeScene, SFImageScrollView;

@interface SFMovieFrameViewController : NSViewController {
    SFOpenGLView        *openGLView;
    SFOGLVolumeScene    *volumeScene;
    NSMutableArray      *renderStates;
    CGFloat             defaultFrameDuration;

    IBOutlet SFImageScrollView *imageScrollView;
}

@property  (retain) SFOGLVolumeScene     *volumeScene;
@property  (retain) SFOpenGLView         *openGLView;

- (IBAction) addMovieFrame:(id)sender;
- (IBAction) deleteSelectedImages:(id)sender;
- (IBAction) restoreState:(id)sender;
- (IBAction) deleteAllMovieFrames:(id)sender;

- (void) makeMovieForPath:(NSString*)path;

@end
