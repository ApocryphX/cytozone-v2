//
//  SFDataInfo.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFDataInfo.h"

@implementation SFDataInfo

- (void)setSequenceNumber:(NSNumber*)value {
	sfSequenceNumber = value;	
};

- (void)setName:(NSString*)value 
{
	sfName = value;
};

- (void)setDimensionCount:(NSNumber*)value
{
	_dimension_count = value;
};

- (void)setChannelCount:(NSNumber*)value 
{	
	_channel_count = value;
};

- (void)setDimensionString:(NSString*)value 
{
	 _scan_mode = value;
};

- (void)setVoxelType:(NSString*)value
{	
	_voxel_type = value;	
};

- (void)setIsInMemory:(BOOL)value
{
	sfIsInMemory = value;
};

- (NSNumber*)sequenceNumber
{
	return sfSequenceNumber;
};

- (NSString*)name 
{
	return sfName;
};

- (NSNumber*)dimensionCount 
{
	return _dimension_count;
};

- (NSNumber*)channelCount 
{
	return _channel_count;
};

- (NSString*)dimensionString 
{
	return _scan_mode;
};

- (NSString*)voxelType 
{
	return _voxel_type;
};

- (BOOL)isInMemory
{
	return sfIsInMemory;
};

@end
