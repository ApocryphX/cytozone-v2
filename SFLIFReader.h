//
//  SFLIFReader.h
//  SanFrancisco
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFNDImporter.h"

@class SFVoxelInfo, SFNDDimension;

@interface SFLIFReader : NSObject <SFNDReading> {
	NSFileHandle		*sfFileHandle;
	NSXMLDocument		*sfXMLDocument;
	NSMutableDictionary *sfBlockOffsetDictionary;
	NSMutableArray		*sfImageArray;
	NSArray				*sfFileNodes;
	NSUInteger			sfLIFVersion;
	//NSUInteger			sfNodeIndex;
}


@property (readonly) NSArray* fileNodes;

- (id)initWithPath:(NSString*)path;
+ (BOOL)isConfocalFile:(NSString*)filePath;


@end
