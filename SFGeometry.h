/*
 *  SFGeometry.h
 *  VoxelMachine
 *
 *  Copyright 2014 Elarity, LLC. All rights reserved.
 *
 */

typedef struct _SFPoint {
    NSUInteger x;
    NSUInteger y;
} SFPoint;

typedef SFPoint *SFPointPointer;
typedef SFPoint *SFPointArray;

typedef struct _SFSize {
    NSUInteger width;		/* should never be negative */
    NSUInteger height;		/* should never be negative */
} SFSize;

typedef SFSize *SFSizePointer;
typedef SFSize *SFSizeArray;

typedef struct _SFRect {
    SFPoint origin;
    SFSize size;
} SFRect;

