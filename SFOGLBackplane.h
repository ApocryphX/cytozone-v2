//
//  SFOGLBackplane.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <OpenGL/OpenGL.h>

@class SFNDVoxelData, SFNDDimension, SFVolumeWindowController, SFOGLTextureRectangle, SFAlignment2D;

@interface SFOGLBackplane : NSObject {
	
	NSOpenGLContext* sfContext;
	SFNDVoxelData* sfShadows;
	NSArray* sfChannelSettings;
	NSMutableArray* sfTextures;
	NSColor* sfColor;
	
	float sfAlpha;
	NSUInteger sfMode;
	NSUInteger sfIndex;
}

- (void) draw;
- (void)drawShadow;
- (void)drawColoredRectangle;

- (void)setContext:(NSOpenGLContext*)cxt;
- (void)setNDDimension:(SFNDDimension*)nddim voxelInfos:(NSArray*)vinfos;
- (void)setChannelSettings:(NSArray*)value;
- (void)setMode:(NSUInteger)value;
- (void)setColor:(NSColor*)value;
- (void)setAlpha:(float)value;

- (NSColor*)color;
- (NSUInteger) mode;
- (SFOGLTextureRectangle*)textureAtIndex:(NSUInteger)idx;
- (void)replaceBytes:(const void*)bytes alignment:(SFAlignment2D*)alignment index:(NSUInteger)idx;

@end
