//
//  SFFrameBufferObject.h
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFFrameBufferObject : NSObject {
    GLuint					FBOid;
	GLuint					FBOTextureId;
    NSSize                  FBOSize;

}

@property (readonly) NSSize size;

- (id)initWithSize:(NSSize)imageSize;

- (void) bind ;
+ (void) unbind;

@end
