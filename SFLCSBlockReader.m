//
//  SFLCSReader.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//


#import "SFLCSBlockReader.h"
//#import <math.h>

@implementation SFLCSBlockReader

- (id)initWithFile:(NSString *) fileName {
    self = [super init];
    sfFileName =  fileName;
    sfLCSData = [NSData dataWithContentsOfFile:fileName];
    return self;
};
//---------------------------------------------------------------------------------

-(NSString*)fileName{
    return sfFileName;
};

//---------------------------------------------------------------------------------

-(NSString*) stringFromUTF16Buffer:(unichar*)source length:(uint32_t)length
{
	NSString* ret = nil;
	unichar* target = (unichar*)malloc(length*sizeof(unichar));
	unsigned i;
	for (i=0;i<length;i++)
		target[i] = NSSwapLittleShortToHost(source[i]);
	if (source[length-1] ==  0) {
		for(i=length-1;i>0&&source[length-1];i--);
		if(i==0)
			ret = [NSString string];
		else
			ret=[NSString stringWithCharacters:target length:i];
	}
	else
		ret=[NSString stringWithCharacters:target length:length];
	free(target);
	return ret;
}

//---------------------------------------------------------------------------------
- (void*) blockAddressWithID: (uint32_t) BlockID ofSeries:(uint32_t) SeriesNumber
{
	void *base_pointer;
	int32_t *data_pointer,  *block_address;
	NSUInteger i, count=0;
	// calculate C-style pointer to the first block in memory
	data_pointer = base_pointer = (void*) [sfLCSData bytes];
	data_pointer =  base_pointer + data_pointer[3];
    
	// iterate list until SeriesNumber
	block_address = 0;
	for (i=0; i<=SeriesNumber && data_pointer!=base_pointer; i++) {
		block_address = data_pointer;
		count = NSSwapLittleIntToHost ( data_pointer[0] );
		data_pointer = base_pointer + data_pointer[count*2];
	}
	// look for pointer of block with ID block-id
	if ( i == SeriesNumber+1 && block_address != 0 )
		for ( i = 0; i < count &&  block_address[i*2 + 1] != BlockID; i++ );

    int32_t offset = 0;
    if(block_address)
        offset = block_address[(i+1)*2];
	return (void*) base_pointer + offset;
};

//---------------------------------------------------------------------------------

- (NSMutableDictionary *)blockWithIDSeries 
{
	uint32_t* data_pointer = [self blockAddressWithID:10 ofSeries:0];
	NSMutableDictionary* ret = [NSMutableDictionary dictionary];
	[ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost(data_pointer[5])] forKey:@"series count"];
	[ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost(data_pointer[6])] forKey:@"filename length in wchar"];
	unichar* pstr = (unichar*)&data_pointer[8];
	[ret setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[7])/2] forKey:@"filename extension"];
	return ret;
};

- (NSMutableDictionary *)blockWithIDImages:(uint32_t)number 
{
	NSMutableDictionary* ret = [NSMutableDictionary dictionary];
    uint32_t* data_pointer = [self blockAddressWithID: 15 ofSeries:(uint32_t)number];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[4])] forKey:@"number of images"];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[5])] forKey:@"image width"];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[6])] forKey:@"image heigth"];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[7])] forKey:@"bits per sample"];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[8])] forKey:@"samples per pixel"];
	NSMutableArray* filenames = [NSMutableArray array];
	NSUInteger imagecount = NSSwapLittleIntToHost (data_pointer[4]);
	NSUInteger namesize = [[[self blockWithIDSeries]  objectForKey:@"filename length in wchar"] intValue];
	unichar* pbuffer = (unichar*) &data_pointer[9];
	NSUInteger i;
	for (i=0; i < imagecount; i++) {
		[filenames addObject:[[self stringFromUTF16Buffer:&pbuffer[i*namesize] length:(uint32_t)namesize]stringByDeletingPathExtension]];
	}
	[ret setObject:filenames forKey:@"filenames"]; 
	return ret;
};


- (NSMutableDictionary*)blockWithIDLUTDescr:(uint32_t)number 
{

	NSMutableDictionary* ret = [NSMutableDictionary dictionary];
    UInt32* data_pointer = [self blockAddressWithID:70 ofSeries:number];
	[ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[4])] forKey:@"number of LUTs"];
	uint32_t count = NSSwapLittleIntToHost (data_pointer[4]); //number of LUTs
	[ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[5])] forKey:@"Dimension ID"];
	data_pointer = &data_pointer[6];
	NSUInteger i;
	NSMutableArray* luts = [NSMutableArray array];
	for (i=0; i<count; i++) {
		UInt8* pc = (UInt8*)&data_pointer[1];
		data_pointer = (UInt32*)&pc[1];
		UInt32 length = NSSwapLittleIntToHost (data_pointer[0]);
		void *buffer = &data_pointer[1];
		data_pointer = buffer+length;
		length = NSSwapLittleIntToHost (data_pointer[0]);
		buffer = &data_pointer[1];
		data_pointer = buffer+length;
		length = NSSwapLittleIntToHost (data_pointer[0]);
		buffer = &data_pointer[1];
		NSString *lutname = [self stringFromUTF16Buffer:(unichar*)buffer length:length/2];
		[luts addObject:lutname];
		data_pointer = buffer+length;
		data_pointer = &data_pointer[2];
	}
	[ret setObject:luts forKey:@"LUT names"];
	return ret;
};	


- (NSMutableDictionary*)blockWithIDDimDescr:(uint32_t)number 
{
	NSMutableDictionary* ret = [NSMutableDictionary dictionary];
    uint32_t* data_pointer = [self blockAddressWithID:20 ofSeries:(uint32_t)number];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[5])] forKey:@"voxel type"];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[6])] forKey:@"voxel size in bytes"];
    [ret setObject:[NSNumber numberWithInt:NSSwapLittleIntToHost (data_pointer[7])] forKey:@"real world resolution"];
	unichar* pstr = (unichar*)&data_pointer[9];
	[ret setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[8])] forKey:@"maximum value"];
	data_pointer = (uint32_t*)&pstr[NSSwapLittleIntToHost(data_pointer[8])];
	pstr = (unichar*)&data_pointer[1];
	[ret setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[0])] forKey:@"minimum value"];
	data_pointer = (uint32_t*)&pstr[NSSwapLittleIntToHost(data_pointer[0])];
	pstr = (unichar*)&data_pointer[1];
	[ret setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[0])] forKey:@"intensity"];
	data_pointer = (uint32_t*)&pstr[NSSwapLittleIntToHost(data_pointer[0])];
	NSUInteger count = NSSwapLittleIntToHost (data_pointer[1]);
    [ret setObject:[NSNumber numberWithInteger:count] forKey:@"number of dimensions"];
	NSUInteger i;
	NSMutableArray* dimensions = [NSMutableArray array];
	data_pointer = &data_pointer[2];
	for(i=0;i<count;i++) {
		NSMutableDictionary* dim = [NSMutableDictionary dictionary]; 	
		[dim setObject:[NSNumber numberWithUnsignedInt:NSSwapLittleIntToHost (data_pointer[0])] forKey:@"dimension id"];
		[dim setObject:[NSNumber numberWithUnsignedInt:NSSwapLittleIntToHost (data_pointer[1])] forKey:@"extent"];
		[dim setObject:[NSNumber numberWithUnsignedInt:NSSwapLittleIntToHost (data_pointer[2])] forKey:@"stride"];
		pstr = (unichar*)&data_pointer[4];
        NSString *magnitudeString = [self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[3])];
		[dim setObject:[NSNumber numberWithDouble:[magnitudeString floatValue]] forKey:@"magnitude"];
		data_pointer = (uint32_t*)&pstr[NSSwapLittleIntToHost(data_pointer[3])];
		pstr = (unichar*)&data_pointer[1];
		[dim setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[0])] forKey:@"origin"];
		data_pointer = (uint32_t*)&pstr[NSSwapLittleIntToHost(data_pointer[0])];	
		[dimensions addObject:dim];
	}	
	[ret setObject:dimensions forKey:@"dimensions"];
	pstr = (unichar*)&data_pointer[1];
	[ret setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[0])/2] forKey:@"name"];
	data_pointer = (uint32_t*)&pstr[NSSwapLittleIntToHost(data_pointer[0])/2];
	pstr = (unichar*)&data_pointer[1];
	[ret setObject:[self stringFromUTF16Buffer:pstr length:NSSwapLittleIntToHost(data_pointer[0])/2] forKey:@"description"];
	return ret;
};

;

@end

