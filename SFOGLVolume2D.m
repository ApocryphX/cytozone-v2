//
//  SFOGLVolume2D.m
//  VoxelMachine
//
//  Created by ApocryphX on 1/9/05.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLVolume2D.h"

#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

@import GLKit;

#import "SFGeometry.h"
#import "SFOGLTextureRectangle.h"
#import "SFNDVoxelData.h"
#import "SFNDDimension.h"
#import "SFVoxelData.h"
#import "SFOGLTextureStack3D.h"
#import "SFVolumeWindowController.h"
#import "SFOGLTransform.h"
#import "SFOGLBackplane.h"
#import "SFOGLChannelSetting.h"
#import "SFAlignment2D.h"
#import "SFOGLColor.h"

#import "utility.h"

@implementation SFOGLVolume2D

- (void)setNDVoxelData:(SFNDVoxelData*)nddata {
	[super setNDVoxelData:nddata];
	if (nddata) {
		NSUInteger channelIndex, channelCount = [[nddata channels]count];
		for(channelIndex=0; channelIndex<channelCount; channelIndex++) {
			SFOGLTextureStack3D* tex3D; 
			tex3D = [SFOGLTextureStack3D new];
			[tex3D setRenderMode:[self renderMode]];
			[tex3D setNDDimension:[sfNDVoxelData nDDimension] voxelInfo:[[nddata voxelDataAtIndex:channelIndex]voxelInfo]];
			if(sfOGLView)
				[tex3D setOpenGLContext:[sfOGLView openGLContext]];
			[sfVolumeTextures addObject:tex3D];
		}
	[self setBackplaneNDDimension:[[sfVolumeTextures objectAtIndex:0]nDDimension]];
	[self processAllChannels];
	}
};

- (void)scaleVolume {

	float xscale,yscale,zscale,maxscale;
	SFNDVoxelData* nddata = [self nDVoxelData];
    if([nddata.nDDimension isCalibrated]) {
        xscale = fabs([[nddata magnitudeAtIndex:0]doubleValue]);
        yscale = fabs([[nddata magnitudeAtIndex:1]doubleValue]);
        zscale = fabs([[nddata magnitudeAtIndex:2]doubleValue]);
    }
    else {
        xscale = fabs((float)[nddata extentAtIndex:0]);
        yscale = fabs((float)[nddata extentAtIndex:1]);
        zscale = fabs((float)[nddata extentAtIndex:2]);
    }
	maxscale = (xscale>yscale)?xscale:yscale;
	maxscale = (maxscale>zscale)?maxscale:zscale;
	zscale = zscale / maxscale * sfZScale;
	yscale = yscale / maxscale;
	xscale = xscale / maxscale;
	glScalef(xscale,yscale,zscale);
}


- (BOOL) draw {
	[super draw];
	NSUInteger i,j;
	NSUInteger supersample = pow(2.0f, sfSampling);
	
    GLint viewport[4];
    //GLdouble mvmatrix[16], projmatrix[16], wx,wy,wz;
    int axis; 
	float dir;
	CGLLockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	// transform
	glMatrixMode(GL_MODELVIEW);
	[[self transform] execute];
	
	NSRect rect = [sfOGLView bounds];

	//float deltax = rect.size.width - rect.origin.x;
	//float deltay = rect.size.height - rect.origin.y;
	//calculate major viewing axis
    //glGetIntegerv(GL_VIEWPORT, viewport);
    //glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
    //glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);
    //gluUnProject( deltax/2.0f, deltay/2.0f ,1.0,mvmatrix,projmatrix,viewport,&wx,&wy,&wz);
    
    //GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    GLKMatrix4 modelviewMatrix;
    glGetFloatv(GL_MODELVIEW_MATRIX, (float*)&modelviewMatrix);
    GLKMatrix4 projectionMatrix;
    glGetFloatv(GL_PROJECTION_MATRIX, (float*)&projectionMatrix);
    bool result;
    GLKVector3 window_coord = GLKVector3Make((rect.size.width - rect.origin.x)/2.0f, (rect.size.height - rect.origin.y)/2.0f ,1.0);
    GLKVector3 transformedPoint = GLKMathUnproject(window_coord, modelviewMatrix, projectionMatrix, &viewport[0], &result);

	// ______________
	axis = 2;
	if (transformedPoint.z < 0.0f)
		dir = -1.0f; 
	else
		dir = 1.0f; 

	// ---------------
	[self scaleVolume];
	glScalef(1.0, -1.0, 1.0);
    
	glTranslatef(-0.5f, -0.5f, 0.0f );

	if (dir < 0.0f) {
		[[self backdrop] draw];	
	}
	
	// enable 2D texturing
	//glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_RECTANGLE_EXT);
	
	//set texture blending
	[self programBlendMode];   	
	
	// depth test and depth mask read-only
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE); 

	//enable alpha testing (=threshold)
	glEnable(GL_ALPHA_TEST);
	
    SFOGLTextureStack3D *stack3D = [sfVolumeTextures objectAtIndex:0];
	NSUInteger planecount = [stack3D count];
	glPushMatrix();
		
	if (dir > 0.0f)
		glTranslatef(0.0,0.0,1.0f );
    
    float width = stack3D.alignment.size.width;
    float height = stack3D.alignment.size.height;
    
    
	for(i=0; i<planecount*supersample; i++) {
		for(j=0; j<[sfVolumeTextures count]; j++) {
			if ([[sfChannelSettings objectAtIndex:j] isActive]) {
				if([[sfChannelSettings objectAtIndex:j]volumeMode]==0)
					glAlphaFunc(GL_GREATER,[[sfChannelSettings objectAtIndex:j]threshold]*[self alpha]);
				else 
					glAlphaFunc(GL_ALWAYS,0);
				if (  dir < 0.0f )
					[[sfVolumeTextures objectAtIndex:j] bindTextureAtIndex:i/supersample];
				else
					[[sfVolumeTextures objectAtIndex:j] bindTextureAtIndex:planecount-i/supersample-1];
				//[[[self channelSettings] objectAtIndex:ii]setOGLColorWithVolumeColorWithAlpha:sfAlpha];
				//SFNDDimension* nddim = [[sfVolumeTextures objectAtIndex:j]nDDimension];
                
				[[sfColors objectAtIndex:j]setOpenGLColor];
				glBegin(GL_QUADS);
				glTexCoord2f ( 1.0f, 1.0f);
				glVertex2f  ( mat[axis][0][0], mat[axis][0][1] );

				glTexCoord2f ( width, 1.0f);
				glVertex2f  ( mat[axis][1][0], mat[axis][1][1] );

				glTexCoord2f ( width, height );
				glVertex2f  ( mat[axis][2][0], mat[axis][2][1] );

				glTexCoord2f (1.0f, height );
				glVertex2f  ( mat[axis][3][0], mat[axis][3][1] );

				glEnd(); //GL_POLYGON				
			}
		}
		glMatrixMode(GL_MODELVIEW);
		float steps = (float)planecount*(float)supersample-1.0f;
		glTranslatef(0.0,0.0, -1.0f*dir/steps);
	}	
	
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_RECTANGLE_EXT);

	glPopMatrix();
	if (/*[self renderMode] == 0 && */ dir > 0.0f) {
		[[self backdrop] draw];	
	}
    CGLUnlockContext([[NSOpenGLContext currentContext] CGLContextObj]);
	return YES;
};

;

@end
