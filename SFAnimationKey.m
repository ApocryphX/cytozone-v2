//
//  SFAnimationKey.m
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFAnimationKey.h"

@implementation SFAnimationKey

@synthesize keyframeIndex;

@dynamic duration;
@dynamic framesPerSecond;

@dynamic stepwiseFraction;
@dynamic numberOfFrames;

- (id)init {
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)recalculate {
    numberOfFrames = duration*framesPerSecond;
    if(numberOfFrames!=0.0) 
        stepwiseFraction = 1.0/numberOfFrames;
    else 
        stepwiseFraction = 0.0;
}

- (void) setDuration:(CGFloat)value {
    duration = value;  
    [self recalculate];
}

- (CGFloat) duration {
    return duration;
}

- (void) setFramesPerSecond:(CGFloat)value {
    framesPerSecond = value;  
    [self recalculate];
}

- (CGFloat) framesPerSecond {
    return framesPerSecond;
}

@end
