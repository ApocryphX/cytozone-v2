//
//  SFTIFFIFD.h
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Sun Mar 16 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFTIFFTag.h"

@interface SFTIFFIFD : NSObject {

    uint32_t nextOffset;
    uint16_t IFDTagCount;
    NSMutableDictionary* IFDDictionary;
}

- (id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian;
- (uint32_t)nextIFDOffset;

- (SFTIFFTag*)tagWithID:(NSNumber*)tagID;
- (NSDictionary*)ditionaryOfTags;

@end
