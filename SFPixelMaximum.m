//
//  SFPixelMaximum.m
//  Monterey
//
//  Created by ApocryphX on 3/31/07.
//  Copyright 2014 Elarity, LLC.  All rights reserved.
//

#import "SFPixelProcessor.h"

#import "SFBitplane.h"
#import "SFCLComputeKernel.h"

@implementation SFPixelMaximum


static SFCLComputeKernel *kernelF;
static SFCLComputeKernel *kernel8;
static SFCLComputeKernel *kernel16;

static const NSUInteger vectorSize = 16;


+(void)initialize {
	NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"maximum.cl" ofType:nil];
	NSString *sourceString = [NSString stringWithContentsOfFile:sourcePath encoding:NSASCIIStringEncoding error:nil];
	kernel8  = [[SFCLComputeKernel alloc]initWithSource:sourceString functionName:@"maximum_8"]; 
	kernel16  = [[SFCLComputeKernel alloc]initWithSource:sourceString functionName:@"maximum_16"]; 
	kernelF  = [[SFCLComputeKernel alloc]initWithSource:sourceString functionName:@"maximum_F"]; 
}

- (BOOL)hasInplacePrcessing {
	return NO;
};

- (void) setSourceBitplane:(SFBitplane *)bitplane  {
	[super setSourceBitplane:bitplane];
	if(sourceBitplane) {
        if (sourceBitplane.bytesPerSample == 1)
			computeKernel = kernel8;
		else if (sourceBitplane.bytesPerSample == 2)
			computeKernel = kernel16;
		targetBitplane = [SFBitplane bitplaneWithBytesPerSample:sourceBitplane.bytesPerSample
                                                          width:sourceBitplane.width
                                                         height:sourceBitplane.height
                                                        padding:sourceBitplane.padding];
	}
	else {
		targetBitplane=nil;
	}	
}


- (void) processBitplane {
	
	int err;			// error code returned from api calls
	
	size_t global;		// global domain size for our calculation
	size_t local;		// local domain size for our calculation
	
	cl_mem input;		// device memory used for the input array
	cl_mem output;		// device memory used for the output array
	
	void *src = sourceBitplane.mutableBytes;
    void *dst = targetBitplane.mutableBytes;
	input = clCreateBuffer(computeKernel.context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,  sourceBitplane.bytesPerPlane, src, NULL);
	output = clCreateBuffer(computeKernel.context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, targetBitplane.bytesPerPlane, dst, NULL);
	
	if (!input || !output) {
		NSLog(@"PixelMaximum Processor Error: Failed to allocate device memory!\n");
	}    
	else {
		// Write our data set into the input array in device memory 
		//
		err = clEnqueueWriteBuffer(computeKernel.commands, input, CL_TRUE, 0, sourceBitplane.bytesPerPlane, src, 0, NULL, NULL);
		if (err != CL_SUCCESS) {
			NSLog(@"PixelMaximum Processor Error: Failed to write to source array!\n");
		}
		else {
			// Set the arguments to our compute kernel
			//
			unsigned int count = sourceBitplane.bytesPerPlane/vectorSize;
			err  = clSetKernelArg(computeKernel.kernel, 0, sizeof(cl_mem), &input);
			err |= clSetKernelArg(computeKernel.kernel, 1, sizeof(cl_mem), &output);
			err |= clSetKernelArg(computeKernel.kernel, 2, sizeof(unsigned int), &count);
			if (err != CL_SUCCESS) {
				NSLog(@"PixelMaximum Processor Error: Failed to set kernel arguments! %d\n", err);
			}
			else {
				// Get the maximum work group size for executing the kernel on the device
				err = clGetKernelWorkGroupInfo(computeKernel.kernel, computeKernel.device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
				if (err != CL_SUCCESS) {
					NSLog(@"PixelMaximum Processor Error: Failed to retrieve kernel work group info! %d\n", err);
				}
				else {
					[targetBitplane willChangeValueForKey:@"data"];

					// Execute the kernel over the entire range of our 1d input data set
					// using the maximum number of work group items for this device
					global = count;
					err = clEnqueueNDRangeKernel(computeKernel.commands, computeKernel.kernel, 1, NULL, &global, &local, 0, NULL, NULL);
					if (err)  {
						NSLog(@"PixelMaximum Processor Error: Failed to execute kernel!\n");
					}
					else {
						// Wait for the command commands to get serviced before reading back results
						clFinish(computeKernel.commands);
						NSLog(@"Maximum successfully calculated");
					}
					[targetBitplane didChangeValueForKey:@"data"];
				}
			}
		}
		// Shutdown and cleanup
		//
		clReleaseMemObject(input);
		clReleaseMemObject(output);
	}	
}


@end
