//
//  SFTIFFFile.h
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Mon Mar 24 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SFTIFFFile : NSObject {
    NSData *headerData;
    NSFileHandle* sfFileHandle;
    NSMutableArray* IFDs;
    //BOOL _isBigEndian;
    //BOOL _isBigTIFF;
}

@property (readonly) BOOL isBigEndian;
@property (readonly) BOOL isBigTIFF;


- (instancetype)initWithFileName:(NSString *)fileName;
- (instancetype)initWithURL:(NSURL *)fileURL;

- (BOOL)isBigEndian;
- (NSFileHandle*)fileHandle;
- (NSArray*)arrayOfIFDs;

@end
