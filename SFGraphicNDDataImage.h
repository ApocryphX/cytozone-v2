//
//  SFGraphicNDDataImage.h
//  SanFrancisco
//
//
//

#import "SFGraphicRectangle.h"
#import "SFNDDataOutlineViewController.h"

@class SFNDImporter, SFNDVoxelData;

@interface SFGraphicNDDataImage : SFGraphicRectangle <SFNodeIndexSelection> {
    
    GLsizei textureCount;
    GLuint *textureIDs;
    
    NSMutableArray *textures;
    NSUInteger channelCount;
    NSUInteger textureWidth;
    NSUInteger textureHeight;
    NSUInteger sfNodeIndex;
    
    GLuint framebuffer;
    GLuint renderbuffer;
    
    SFNDVoxelData *nDVoxelData;
    
    NSOpenGLContext *openGLContext;
}

@property (retain) SFNDImporter *importer;
@property (assign) NSUInteger nodeIndex;

@property (retain) NSImage *image;

- (void)createRenderBuffer;
- (void)createTextures;
- (void)renderImage;


@end
