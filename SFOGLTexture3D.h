//
//  SFOGLTexture3D.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>
#import "SFOGLVolumeTexture.h"
#import "SFOGLTexturing.h"

@class SFNDVoxelData, SFVoxelData, SFAlignment2D,SFNDDimension, SFVoxelInfo, SFOGLVoxelInfo;

@interface SFOGLTexture3D : SFOGLVolumeTexture <SFOGLTexturing3D> {

	SFVoxelData* sfTexData;
	
	SFOGLVoxelInfo* sfOGLVoxelInfo;
	SFNDDimension* sfNDDimension;
	SFAlignment2D* sfAlignment;
	
	BOOL sfIsDownsampled;
	GLuint sfTexName;	
	NSUInteger sfPlaneCount;
	NSUInteger sfPaddingPlanes;
    NSUInteger sfMaxTextureSize;
}

- (float) paddingRatioAtAxis:(NSUInteger)axis;
- (NSUInteger)extentAtAxis:(NSUInteger)axis;

- (void) bind;
+ (void) unbind;


@end
