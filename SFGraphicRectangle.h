//
//  SFRectangleObject.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicsObject.h"

@interface SFGraphicRectangle : SFAbstractGraphicsObject <SFGrapicInteraction>

@property (retain) NSColor *fillColor;
@property (retain) NSColor *color;

@end
