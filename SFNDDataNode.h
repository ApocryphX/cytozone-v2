//
//  SFNDDataNode.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface SFNDDataNode : NSObject {
	id sfNodeInfo;
}

- (id)initWithNodeInfo:(id)value;
- (void)setNodeInfo:(id)value;
- (id)nodeInfo;

@end
