//
//  SFFileNodeTag.m
//  Cytozone
//
//  Created by Kolja A. Wawrowsky on 11/18/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFFileNodeTag.h"

// channel information
const NSString *SFKChannelInfoKey = @"channel_info";

// information about the voxel type
const NSString *SFKVoxelInfoKey = @"voxel_info";
// voxel data organization and types
const NSString *SFKVoxelBytesPerSampleKey = @"bytes_per_sample";
const NSString *SFKVoxelSamplesPerPixelKey = @"samples_per_pixel";
const NSString *SFKVoxelDataPrecisionKey = @"data_precision";
const NSString *SFKVoxelDataTypeKey = @"data_Type";

// integer / float type
const NSString *SFKVoxelDataTypeFloat = @"data_type_float";
const NSString *SFKVoxelDataTypeInteger = @"data_type_integer";

const NSString *SFKVoxelDataCompressionTypeKey = @"data_compression_type";

// dimension description
const NSString *SFKDimensionArrayKey = @"dimension_array";

// Values for each dimension
const NSString *SFKDimensionNameKey = @"dimension_name";
const NSString *SFKDimensionSymbolKey = @"dimension_symbol";
const NSString *SFKDimensionSizeKey = @"dimension_size";
const NSString *SFKDimensionPhysicalSizeKey = @"dimension_physical_size";
const NSString *SFKDimensionPhysicalOffsetKey = @"dimension_physical_offset";
const NSString *SFKDimensionMetadataKey = @"dimension_meta_data";

const NSString *SFKDimensionTypeSpaceKey = @"space";
const NSString *SFKDimensionTypeTimeKey = @"time";
const NSString *SFKDimensionTypeChannelKey = @"channel";
const NSString *SFKDimensionTypeMosaicKey = @"mosaic";
const NSString *SFKDimensionTypeSpectralKey = @"spectral";
const NSString *SFKDimensionTypeRotationKey = @"rotation";
const NSString *SFKDimensionTypeRandomPositionKey = @"random_position";
const NSString *SFKDimensionTypeIlluminationKey = @"illumination_direction";
const NSString *SFKDimensionTypeBlockIndexKey = @"block_index";
const NSString *SFKDimensionTypePhaseKey = @"phase";
const NSString *SFKDimensionTypeViewIndexKey = @"view_index";
const NSString *SFKDimensionTypeOtherKey = @"other";

@implementation SFFileNodeTag

@end
