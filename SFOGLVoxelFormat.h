//
//  SFOGLVoxelFormat.h
//  VoxelMachine
//
//  Created by ApocryphX on 12/6/04.
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

@class SFVoxelInfo;

@interface SFOGLVoxelFormat : NSObject {
	GLenum sfSourceFormat;
	GLenum sfType;
	GLenum sfTargetFormat;
	BOOL sfIsPlanar;
}

+ (GLenum)lookupFormat:(SFVoxelInfo*)vinfo;
+ (GLenum)lookupType:(SFVoxelInfo*)vinfo;


@end
