//
//  SFLIFReader.m
//  SanFrancisco
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFLIFReader.h"

#import <Accelerate/Accelerate.h>

#import "SFVoxelInfo.h"
//#import "SFDimension.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"

//const uint32_t LIFIDTypeMap[9] = { kVoid, kSpace,kSpace,kSpace,kTime,kWavelength,kDegree,kTime,kTime };

static NSArray *LIFIDTypeMap;

const unichar LIFSymbolMap[2][10] = {
{ 0x2014,0x0078,0x0079,0x007A,0x0074,0x03BB,0x2014,0x0074,0x0074,0x2014},  // unicode char for dimension symbol: -,x,y,z,t,lambda,-
{ 0x2014,0x006D,0x006D,0x006D,0x0073,0x006D,0x2014,0x0073,0x0073,0x2014}   // unicode char for dimension unit: -,m,m,m,m,-
};


@interface SFLIFReader (private)

- (NSUInteger)countOfNodes;
- (NSString*) seriesTitleAtIndex:(NSUInteger)idx;
- (NSUInteger) countOfChannelsAtNodeIndex:(NSUInteger)idx;
//- (SFNDDimension*) nDDimensionAtIndex:(NSUInteger)idx;
- (SFVoxelInfo*) voxelTypeAtNodeIndex:(NSUInteger)idx  channelIndex:(NSUInteger)channelNumber;
- (NSData*) dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex bitplaneIndex:(NSUInteger)bitplaneIndex;

- (struct LIFHeaderInfo) readNextBlock;
- (struct LIFMemoryBlockHeader) readMemoryBlock;
//- (NSArray*) channelDictionariesForIndex:(NSUInteger)idx;
//- (NSArray*) dimensionDictionariesForIndex:(NSUInteger)idx;

- (void) readBlockOffsets;
- (void) parseXMLDocument;

- (NSMutableArray*) parseNode:(NSXMLNode *)xmlNode level:(NSUInteger)level;
- (void) parseImageNode:(NSXMLNode *)xmlNode toDictionary:(NSMutableDictionary*) imageDictionary;
//- (SFVoxelInfo*) pixelTypeForChannelDictionaries:(NSArray *)channelDescriptionDictionaries  forChannel:(NSUInteger)channelNumber;
//- (SFNDDimension*) dimensionsForDictionaries:(NSArray *) dimensionDescriptionDictionaries;

@end

static NSMutableDictionary* sfLIFColorMap;


@implementation SFLIFReader

@synthesize fileNodes = sfFileNodes;

#pragma pack(1)

// Info for the next block to read out of the file 
struct LIFHeaderInfo 
{ 
	uint32_t Test; // Test == 0x70 
	uint32_t Length; // Number of bytes to read 
}; 

// XML Description 
struct LIFXMLBlockHeader 
{ 
	uint8_t Test;  // Test == 0x2A 
	uint32_t XMLLength;  // Number of unicode characters 
}; 

// Memory Description 
struct LIFMemoryBlockHeaderV1 
{ 
	uint8_t Test1;  // Test == 0x2A 
	uint32_t MemorySize; // Memory size of the object 
	uint8_t Test2;  // Test == 0x2A 
	uint32_t StringLength; // Memory ID Text of the object 
};
typedef struct LIFMemoryBlockHeaderV1 LIFMemoryBlockHeaderV1;


// Memory Description 
struct LIFMemoryBlockHeaderV2 
{ 
	uint8_t Test1;  // Test == 0x2A 
	uint64_t MemorySize; // Memory size of the object 
	uint8_t Test2;  // Test == 0x2A 
	uint32_t StringLength; // Memory ID Text of the object 
};
typedef struct LIFMemoryBlockHeaderV2 LIFMemoryBlockHeaderV2;

#pragma pack() 

+ (void)initialize {
	sfLIFColorMap = [NSMutableDictionary new];
	//RGB colors
	[sfLIFColorMap setObject:[NSColor redColor] forKey:@"Red"];
	[sfLIFColorMap setObject:[NSColor greenColor] forKey:@"Green"];
	[sfLIFColorMap setObject:[NSColor blueColor] forKey:@"Blue"];
	//CYMK colors
	[sfLIFColorMap setObject:[NSColor yellowColor] forKey:@"Yellow"];
	[sfLIFColorMap setObject:[NSColor cyanColor] forKey:@"Cyan"];
	[sfLIFColorMap setObject:[NSColor magentaColor] forKey:@"Magenta"];
	[sfLIFColorMap setObject:[NSColor whiteColor] forKey:@"Gray"];
	// other
	[sfLIFColorMap setObject:[NSColor orangeColor] forKey:@"Glow"];
	
	LIFIDTypeMap = [NSArray arrayWithObjects:  // kVoid, kSpace,kSpace,kSpace,kTime,kWavelength,kDegree,kTime,kTime
                    SFKDimensionTypeVoidName,
                    SFKDimensionTypeSpaceName,
                    SFKDimensionTypeSpaceName,
                    SFKDimensionTypeSpaceName,
                    SFKDimensionTypeTimeName,
                    SFKDimensionTypeWavelengthName,
                    SFKDimensionTypeTimeName,
                    SFKDimensionTypeTimeName,
                    SFKDimensionTypeTimeName,
                    nil];
};

+ (NSArray*)fileExtensions {
    return [NSArray arrayWithObject:@"lif"];
};

+ (BOOL)isConfocalFile:(NSString*)filePath {
	BOOL ret = NO;
	NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
	if(fileHandle) {
		struct LIFHeaderInfo headerInfo;
		NSData *infoData = [fileHandle readDataOfLength:sizeof(struct LIFHeaderInfo)];
		memcpy(&headerInfo, [infoData bytes], sizeof(struct LIFHeaderInfo));
		headerInfo.Test = CFSwapInt32LittleToHost(headerInfo.Test);
		if (headerInfo.Test == 0x70)
			ret = YES;
	}
	return ret;
}

- (id)initWithPath:(NSString*)path {
	self = [super init];
	if(self) {
        sfBlockOffsetDictionary = [NSMutableDictionary new];
        sfFileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
        sfImageArray = [NSMutableArray new];
        
        struct LIFHeaderInfo headerInfo;
        NSData *infoData = [sfFileHandle readDataOfLength:sizeof(struct LIFHeaderInfo)];
        memcpy(&headerInfo, [infoData bytes], sizeof(struct LIFHeaderInfo));
        
        struct LIFXMLBlockHeader xmlHeader;
        NSData *xmlHeaderData = [sfFileHandle readDataOfLength:headerInfo.Length];
        memcpy(&xmlHeader, [xmlHeaderData bytes], sizeof(struct LIFXMLBlockHeader));
        xmlHeader.XMLLength = CFSwapInt32LittleToHost(xmlHeader.XMLLength);
        NSData *xmlStringData = [NSData dataWithBytes:[xmlHeaderData bytes]+sizeof(struct LIFXMLBlockHeader) length:xmlHeader.XMLLength*sizeof(unichar)];
        NSString *xmlString = [[NSString alloc]initWithData:xmlStringData encoding:NSUTF16LittleEndianStringEncoding];
        NSError *error;
        //sfXMLDocument = [[NSXMLDocument alloc]initWithXMLString:xmlString options:NSXMLNodeOptionsNone error:&error];
        sfXMLDocument = [[NSXMLDocument alloc]initWithXMLString:xmlString options:NSXMLDocumentTidyXML error:&error];
        NSXMLElement *xmlElement = [[sfXMLDocument nodesForXPath:@"/LMSDataContainerHeader" error:&error] objectAtIndex:0];
        NSXMLNode *attribute = [xmlElement attributeForName:@"Version"];
        NSString *versionString = [attribute stringValue];
        sfLIFVersion = [versionString integerValue];
        [[sfXMLDocument XMLData] writeToFile:[@"~/LIF-data.xml" stringByExpandingTildeInPath] atomically:YES];
        //[NSThread detachNewThreadSelector:@selector(parseXMLDocumentThreaded) toTarget:self withObject:nil];
        [self parseXMLDocument];
        [self readBlockOffsets];
        //[NSThread detachNewThreadSelector:@selector(readBlockOffsetsThreaded) toTarget:self withObject:nil];
        //[self readBlockOffsets];
	}
	return self;
};

- (NSUInteger)count {
	return [sfImageArray count]; 
};


- (NSString*)name {
	return @"LIF File";
};

- (NSUInteger)channelCountAtNodeIndex:(NSUInteger)nodeIndex {
	return [self countOfChannelsAtNodeIndex:nodeIndex];
};

- (NSColor*)colorAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex {
	NSColor *ret = [NSColor whiteColor];
	NSDictionary *imageDictionary = [sfImageArray objectAtIndex:nodeIndex];
	NSArray *channelDescriptionDictionaries = [imageDictionary objectForKey:@"LIFChannelDescriptions"];
	if(channelDescriptionDictionaries) {
		NSDictionary *channelDictionary = [channelDescriptionDictionaries objectAtIndex:channelIndex];
		if(channelDictionary) {
			NSString * lutNameString = [channelDictionary valueForKey:@"LUTName"];
			NSColor *color = [sfLIFColorMap valueForKey:lutNameString];
			if(color)
				ret = color;
		}
	}
	return ret;
};



- (NSUInteger)countOfNodes {
	return [sfImageArray count];
};

- (NSString*) seriesTitleAtIndex:(NSUInteger)idx {
	NSString *ret = @"not found";
	if([sfImageArray count] > idx) {
		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		if(imageDictionary) {
			ret = [imageDictionary objectForKey:@"Name"];
		}
		
	}
	return ret;
};

- (NSArray*) fileNodes {
	return sfFileNodes;
};

//____________________________________________________________________________
//
//  internal methods
//____________________________________________________________________________

- (NSUInteger) countOfChannelsAtNodeIndex:(NSUInteger)idx {
	NSUInteger ret = 0;
	if([sfImageArray count] > idx) {
		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		NSArray *channelDescriptionDictionaries = [imageDictionary objectForKey:@"LIFChannelDescriptions"];
		if(channelDescriptionDictionaries) {
			NSDictionary *channelDictionary = [channelDescriptionDictionaries objectAtIndex:0];
			NSString * channelTagString = [channelDictionary valueForKey:@"ChannelTag"];
			if([channelTagString isEqualToString:@"0"]) 
				ret = [channelDescriptionDictionaries count];
			else 
				ret = 1;
		}
	}
	return ret;
};



- (SFVoxelInfo*)voxelInfoAtNodeIndex:(NSUInteger)idx channelIndex:(NSUInteger)channelIndex {
	SFVoxelInfo* ret = nil;
	if([sfImageArray count] > idx) {
		ret = [SFVoxelInfo new];
		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		NSArray *channelDescriptionDictionaries = [imageDictionary objectForKey:@"LIFChannelDescriptions"];
		if(channelDescriptionDictionaries) {
			NSDictionary *channelDictionary = [channelDescriptionDictionaries objectAtIndex:channelIndex];
			if(channelDictionary) {
				NSString *channelTagString = [channelDictionary valueForKey:@"ChannelTag"]; // is it RGB?
				if([channelTagString isEqualToString:@"0"]) {
					NSString * dataTypeString = [channelDictionary valueForKey:@"DataType"]; //datatype
					if ([dataTypeString isEqualToString:@"0"]) {
						NSString * resolutionString = [channelDictionary valueForKey:@"Resolution"]; //bits per sample
						if ([resolutionString isEqualToString:@"8"]) {							
//							ret = [SFVoxelInfo pixelTypeForDataType:@"Planar8"];
							[ret setSamplesPerVoxel:1];
							[ret setBytesPerSample:1];
							[ret setPrecision:8];
							[ret setDataType:kInteger];
						}
						else if ([resolutionString isEqualToString:@"12"]) {
//							ret = [SFVoxelInfo pixelTypeForDataType:@"Planar16"];
//							ret.precision = 12;
							[ret setSamplesPerVoxel:1];
							[ret setBytesPerSample:2];
							[ret setPrecision:12];
							[ret setDataType:kInteger];
						} 
						else if ([resolutionString isEqualToString:@"16"]) {
//							ret = [SFVoxelInfo pixelTypeForDataType:@"Planar16"];
							[ret setSamplesPerVoxel:1];
							[ret setBytesPerSample:2];
							[ret setPrecision:16];
							[ret setDataType:kInteger];
						} 
					}
					else if ([dataTypeString isEqualToString:@"1"]) {
//						ret = [SFVoxelInfo pixelTypeForDataType:@"PlanarF"];
						[ret setSamplesPerVoxel:1];
						[ret setBytesPerSample:4];
						[ret setPrecision:24];
						[ret setDataType:kFloatingPoint];
					}
				}
				else {
					NSString * dataTypeString = [channelDictionary valueForKey:@"DataType"];
					if ([dataTypeString isEqualToString:@"0"]) {
						NSString * resolutionString = [channelDictionary valueForKey:@"Resolution"];
						if ([resolutionString isEqualToString:@"8"]) {
//							ret = [SFVoxelInfo pixelTypeForDataType:@"RGB8"];
							[ret setSamplesPerVoxel:3];
							[ret setBytesPerSample:1];
							[ret setPrecision:8];
							[ret setDataType:kInteger];
							[ret setInterleaveType:kInterleavedRGB];
							
						}
						else if ([resolutionString isEqualToString:@"12"]) {
//							ret = [SFVoxelInfo pixelTypeForDataType:@"RGB16"];
//							ret.precision = 12;
							[ret setSamplesPerVoxel:3];
							[ret setBytesPerSample:2];
							[ret setPrecision:12];
							[ret setDataType:kInteger];		
							[ret setInterleaveType:kInterleavedRGB];
						} 
						else if ([resolutionString isEqualToString:@"16"]) {
//							ret = [SFVoxelInfo pixelTypeForDataType:@"RGB16"];
							[ret setSamplesPerVoxel:3];
							[ret setBytesPerSample:2];
							[ret setPrecision:16];
							[ret setDataType:kInteger];				
							[ret setInterleaveType:kInterleavedRGB];
						} 
					}
					else if ([dataTypeString isEqualToString:@"1"]) {
//						ret = [SFVoxelInfo pixelTypeForDataType:@"RGBF"];
						[ret setSamplesPerVoxel:1];
						[ret setBytesPerSample:4];
						[ret setPrecision:24];
						[ret setDataType:kFloatingPoint];
						[ret setInterleaveType:kInterleavedRGB];
						
					}
				}
			}
		}
	}
	return ret;
};


- (NSMutableDictionary*)dictionaryAtNodeIndex:(NSUInteger)idx {
    return nil;
}
- (SFNDDimension*) nDDimensionAtNodeIndex:(NSUInteger)idx {
	SFNDDimension *ret = nil;
	if([sfImageArray count] > idx) {
		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		NSArray *dimensionDescriptionDictionaries = [imageDictionary objectForKey:@"LIFDimensionDescriptions"];
		if(dimensionDescriptionDictionaries) {
			ret = [SFNDDimension new];
			for(NSDictionary *dimensionDictionary in dimensionDescriptionDictionaries) {
				//SFDimension *newDimension = [SFDimension new];
				NSString *extentString = [dimensionDictionary valueForKey:@"NumberOfElements"];
				//newDimension.extent = [extentString integerValue];
				NSString *magnitudeString = [dimensionDictionary valueForKey:@"Length"];
                double value = [magnitudeString doubleValue];
				NSNumber *magnitude = [NSNumber numberWithDouble:value];
				
				NSString *dimIDString = [dimensionDictionary valueForKey:@"DimID"];
				if(dimIDString) {
					NSUInteger k = [dimIDString integerValue];
					//newDimension.dimensionType = LIFIDTypeMap[k];
					//newDimension.symbol = [NSString stringWithCharacters:&LIFSymbolMap[0][k] length:1];
					//newDimension.unit = [NSString stringWithCharacters:&LIFSymbolMap[1][k] length:1];
                    if(k<=9)
                    [ret addDimensionWithExtent:[extentString integerValue]
                                              magnitude:magnitude
                                          dimensionType:LIFIDTypeMap[k]
                                                 symbol:[NSString stringWithCharacters:&LIFSymbolMap[0][k] length:1]
                                                   unit:[NSString stringWithCharacters:&LIFSymbolMap[1][k] length:1]];
                    else
                        [ret addDimensionWithExtent:[extentString integerValue]
                                          magnitude:magnitude
                                      dimensionType:SFKDimensionTypeVoidName
                                             symbol:@""
                                               unit:@""];
				}
				//[ret addDimension:newDimension];
			}
		}
	}
	return ret;			
};


-(void)seekToNodeIndex:(NSNumber*)number {
	NSUInteger idx = [number integerValue];
	if( idx < [sfImageArray count]) {

		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		//seek to data within file
		NSString *memoryBlockIDString = [imageDictionary valueForKey:@"MemoryBlockID"];
		NSNumber *memoryBlockOffsetNumber = [sfBlockOffsetDictionary valueForKey:memoryBlockIDString];
		unsigned long long memoryBlockOffset = [memoryBlockOffsetNumber longLongValue];
		[sfFileHandle seekToFileOffset:memoryBlockOffset];
	}
};


- (NSData*) dataAtNodeIndex:(NSUInteger)nodeIndex channelIndex:(NSUInteger)channelIndex planeIndex:(NSUInteger)planeIndex  {
	NSData *ret = nil;
	
	if([sfImageArray count] > nodeIndex) {

		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:nodeIndex];
		NSArray *dimensionDictionaries = [imageDictionary objectForKey:@"LIFDimensionDescriptions"];
		NSDictionary *channelDictionary = [[imageDictionary objectForKey:@"LIFChannelDescriptions"]objectAtIndex:channelIndex];
		
		
		NSUInteger rowBytes = [[[dimensionDictionaries objectAtIndex:1]valueForKey:@"BytesInc"]integerValue];
		NSUInteger lineCount = [[[dimensionDictionaries objectAtIndex:1]valueForKey:@"NumberOfElements"]integerValue];
		
		NSString *memoryBlockIDString = [imageDictionary valueForKey:@"MemoryBlockID"];
		NSNumber *memoryBlockOffsetNumber = [sfBlockOffsetDictionary valueForKey:memoryBlockIDString];
		unsigned long long memoryBlockOffset = [memoryBlockOffsetNumber longLongValue];
		
		NSUInteger planeSize = rowBytes*lineCount;
		unsigned long long planeOffset = 0;
		if(planeIndex > 0) 
			planeOffset = [[[dimensionDictionaries objectAtIndex:2]valueForKey:@"BytesInc"]integerValue]*planeIndex;
		
		unsigned long long channelOffset = [[channelDictionary valueForKey:@"BytesInc"] longLongValue];
		unsigned long long offset = memoryBlockOffset + planeOffset + channelOffset;
		
		//SFVoxelInfo *pixelType = [self voxelInfoAtNodeIndex:nodeIndex channelIndex:channelIndex];
		
		[sfFileHandle seekToFileOffset:offset];
		ret = [sfFileHandle readDataOfLength:planeSize];
        
        //SFNDDimension *nDDimension =  [self nDDimensionAtNodeIndex:nodeIndex];
        /*
		if( [pixelType bytesPerSample] == 2) {
            
            NSMutableData *targetData = [NSMutableData dataWithLength:lineCount*[nDDimension extentAtIndex:0]];

            NSUInteger lineIndex;
            NSUInteger pixelIndex, pixelCount = [nDDimension extentAtIndex:0];
            NSUInteger bitShift = pixelType.precision-8;
            //NSUInteger scale = pow(2.0, 16-pixelType.precision);

            for(lineIndex=0;lineIndex<lineCount;lineIndex++) {
                uint16_t *src = (void*)[ret bytes]+lineIndex*rowBytes;
                uint8_t *dst = targetData.mutableBytes+lineIndex*[nDDimension extentAtIndex:0];
                for (pixelIndex=0; pixelIndex<pixelCount; pixelIndex++) {
                    dst[pixelIndex] = src[pixelIndex]>>bitShift;
                }
            }
            ret = targetData;
		} */
        
		NSUInteger pixelSize = [[[dimensionDictionaries objectAtIndex:0]valueForKey:@"BytesInc"]integerValue];
		NSUInteger lineSize = [[[dimensionDictionaries objectAtIndex:0] valueForKey:@"NumberOfElements"]integerValue]*pixelSize;
		if( lineSize != rowBytes ) {
			
			NSMutableData *convertedData = [NSMutableData dataWithLength:lineSize*lineCount]; 
			
			const unsigned char *src =  [ret bytes];
			unsigned char *dst = [convertedData mutableBytes];
			NSUInteger srcBytesPerRow = rowBytes;
			NSUInteger dstBytesPerRow = lineSize;
			NSUInteger bytesToCopy = (srcBytesPerRow<dstBytesPerRow) ? srcBytesPerRow : dstBytesPerRow;
			NSUInteger i;
			for (i=0;i<lineCount;i++) {
				memcpy (dst+dstBytesPerRow*i, src+srcBytesPerRow*i, bytesToCopy);
			}
			ret = convertedData;
		}

	}

	return ret;
};


- (NSArray*)channelDictionariesForIndex:(NSUInteger)idx  {
	NSArray* ret = nil;
	if([sfImageArray count] > idx) {
		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		ret = [imageDictionary objectForKey:@"LIFChannelDescriptions"];
	}
	return ret;
}

- (NSArray*) dimensionDictionariesForIndex:(NSUInteger)idx {
	NSArray *ret = nil;
	if([sfImageArray count] > idx) {
		NSDictionary *imageDictionary = [sfImageArray objectAtIndex:idx];
		ret = [imageDictionary objectForKey:@"LIFDimensionDescriptions"];
	}
	return ret;		
}


// recursive parsing (descent) of XML "Element" nodes; could be folders or data nodes
// the method performs two different tasks simulataneously:
// 1.) constructs a tree of all nodes in the tree (as SFNDFileNode) and returns an array of root nodes
//     these can be use for display with SFSourceListController (outline view)
// 2.) gathers all datasets with image data in sfImageArray (a flat array)

- (NSMutableArray*) parseNode:(NSXMLNode *)xmlNode level:(NSUInteger)level {
	NSMutableArray *ret = [NSMutableArray new];
	NSError *error;
	NSArray *elements = [xmlNode nodesForXPath:@"Element" error:&error];
	if([elements count] > 0) {
		NSLog(@"level ->");
		for ( NSXMLElement *element in elements) {
			NSArray *attributes = [element attributes];
			SFFileNodeDictionary *newNodeDictionary = [SFNDImporter fileNodeDictionary];
			
			newNodeDictionary[SFKNodeTitleKey] = [[attributes objectAtIndex:0] stringValue];
			NSMutableDictionary *imageDictionary = [NSMutableDictionary new];
			for (NSXMLNode *attribute in attributes) {
				NSLog(@"Node Info:%@ = %@", [attribute name],[attribute stringValue] );
				[imageDictionary setObject:[attribute stringValue] forKey:[attribute name]];
			}
			NSArray *imageNodes = [element nodesForXPath:@"Data/Image" error:&error];
			NSArray *memoryNodes = [element nodesForXPath:@"Memory" error:&error];
			if ([imageNodes count] == 1 && [memoryNodes count] == 1) {
				[self parseImageNode:[imageNodes objectAtIndex:0] toDictionary:imageDictionary];
				attributes = [[memoryNodes objectAtIndex:0] attributes];
				for (NSXMLNode *attribute in attributes) {
					NSLog(@"ImageDictionary: %@ = %@", [attribute name],[attribute stringValue] );
					[imageDictionary setObject:[attribute stringValue] forKey:[attribute name]];
				}
				[sfImageArray addObject:imageDictionary];
				NSUInteger idx = [sfImageArray indexOfObject:imageDictionary];
				newNodeDictionary[SFKNodeDataIndexKey] = [NSNumber numberWithUnsignedInteger:idx];
				newNodeDictionary[SFKNodeIconKey] = [NSImage imageNamed:@"stack"];
			}
			NSArray *children = [element nodesForXPath:@"Children" error:&error];
			if([children count] > 0) {
				NSArray *childrenArray =  [self parseNode:[children objectAtIndex:0] level:level+1];
                if(childrenArray.count>0) {
                    newNodeDictionary[SFKNodeChildrenKey] = childrenArray;
                    newNodeDictionary[SFKNodeIconKey] = [NSImage imageNamed:NSImageNameFolder];
                }
			}
			[ret addObject:newNodeDictionary];
			NSLog(@"Node Info: %@", [[attributes objectAtIndex:0] stringValue] );
		}
		NSLog(@"<- level");		
	}
	return ret;
}


- (void) parseImageNode:(NSXMLNode *)xmlNode toDictionary:(NSMutableDictionary*) imageDictionary{
	NSError *error;
	
	NSArray *xmlChannelDescriptions = [xmlNode nodesForXPath:@"ImageDescription/Channels/ChannelDescription" error:&error];
	NSArray *xmlDimensionsDescriptions = [xmlNode nodesForXPath:@"ImageDescription/Dimensions/DimensionDescription" error:&error];
	NSArray *xmlScannerSettingArray = [xmlNode nodesForXPath:@"Attachment/HardwareSetting/FilterSetting/FilterSettingRecord" error:&error];
	if([xmlChannelDescriptions count] > 0 && [xmlDimensionsDescriptions count] > 0) {
		NSLog(@"Image Description:");
		NSMutableArray *channelDescriptionArray = [NSMutableArray new];
		for (NSXMLElement *xmlChannelDescription in xmlChannelDescriptions) {
			NSMutableDictionary *channelAttributeDictionary = [NSMutableDictionary new];
			NSArray *attributes = [xmlChannelDescription attributes];
			for (NSXMLNode *attribute in attributes) {
				NSLog(@"%@ = %@", [attribute name],[attribute stringValue] );
				[channelAttributeDictionary setObject:[attribute stringValue] forKey:[attribute name]];
			}	
			[channelDescriptionArray addObject:channelAttributeDictionary];
		}
		NSMutableArray *dimensionDescriptionArray = [NSMutableArray new];
		NSLog(@"Dimension Description:");
		for (NSXMLElement *xmlDimensionDescription in xmlDimensionsDescriptions) {
			NSArray *attributes = [xmlDimensionDescription attributes];
			NSMutableDictionary *dimensionAttributeDictionary = [NSMutableDictionary new];
			for (NSXMLNode *attribute in attributes) {
				NSLog(@"%@ = %@", [attribute name],[attribute stringValue] );
				[dimensionAttributeDictionary setObject:[attribute stringValue] forKey:[attribute name]];
			}			
			[dimensionDescriptionArray addObject:dimensionAttributeDictionary];
		}
		[imageDictionary setObject:dimensionDescriptionArray forKey:@"LIFDimensionDescriptions"];
		[imageDictionary setObject:channelDescriptionArray forKey:@"LIFChannelDescriptions"];
	}
    NSMutableDictionary *stagePositionDictionary = [NSMutableDictionary new];
    for (NSXMLElement *xmlScannerSetting in xmlScannerSettingArray) {
        NSXMLNode *objectNameNode = [xmlScannerSetting attributeForName:@"ClassName"];
        //NSLog(@" %@",[objectNameNode stringValue]);
        if([@"CXYZStage" isEqualToString:[objectNameNode stringValue]])  {
            //NSLog(@" %@",[objectNameNode stringValue]);
            objectNameNode = [xmlScannerSetting attributeForName:@"Attribute"];
            //NSLog(@" %@",[objectNameNode stringValue]);
            if([@"XPos" isEqualToString:[objectNameNode stringValue]]) {
                objectNameNode = [xmlScannerSetting attributeForName:@"Variant"];
                stagePositionDictionary[SFKStage_X_Position] = @([[objectNameNode stringValue]doubleValue]);
                NSLog(@"SFKStage_X_Position %f",[stagePositionDictionary[SFKStage_X_Position]doubleValue]);
            }
            else if ([@"YPos" isEqualToString:[objectNameNode stringValue]]) {
                objectNameNode = [xmlScannerSetting attributeForName:@"Variant"];
                stagePositionDictionary[SFKStage_Y_Position] = @([[objectNameNode stringValue]doubleValue]);
                NSLog(@"SFKStage_Y_Position %f",[stagePositionDictionary[SFKStage_Y_Position]doubleValue]);
            }
        }
    }
}

- (void) parseXMLDocument {
	NSArray *rootNodes = [sfXMLDocument children];
	//LMSDataContainerHeader
	NSError *error;
	NSArray *LMSDataContainerNodes = [[rootNodes objectAtIndex:0] nodesForXPath:@"/LMSDataContainerHeader" error:&error];
	sfFileNodes = [self parseNode:[LMSDataContainerNodes objectAtIndex:0] level:0];
	for(id node in sfFileNodes)
        if([node isKindOfClass:[NSMutableDictionary class]]) {
            NSMutableDictionary *dictionary = node;
            [dictionary removeObjectForKey:SFKNodeIconKey];
        }
}

- (void) readBlockOffsets {
	NSError *error;
	NSXMLElement *xmlElement = [[sfXMLDocument nodesForXPath:@"/LMSDataContainerHeader" error:&error] objectAtIndex:0];
	NSXMLNode *attribute = [xmlElement attributeForName:@"Version"];
	NSString *versionString = [attribute stringValue];
	
	struct LIFHeaderInfo headerInfo;
	NSData *infoData = [sfFileHandle readDataOfLength:sizeof(struct LIFHeaderInfo)];
	
	if([versionString isEqualToString:@"1"]) {
		
		while ( [infoData length] != 0 ) {
			memcpy(&headerInfo, [infoData bytes], sizeof(struct LIFHeaderInfo));
			headerInfo.Test = CFSwapInt32LittleToHost(headerInfo.Test);
			headerInfo.Length = CFSwapInt32LittleToHost(headerInfo.Length);
			struct LIFMemoryBlockHeaderV1 blockHeader;
			NSData *blockData = [sfFileHandle readDataOfLength:headerInfo.Length];
			memcpy(&blockHeader, [blockData bytes], sizeof(blockHeader));
			if (blockHeader.Test1 == 0x2A &&  blockHeader.Test2 == 0x2A  ) {
				blockHeader.MemorySize = CFSwapInt32LittleToHost(blockHeader.MemorySize); // Memory size of the object 
				blockHeader.StringLength = CFSwapInt32LittleToHost(blockHeader.StringLength);  // Length of the String 
				
				unsigned long long nextBlockOffset = [sfFileHandle offsetInFile] + blockHeader.MemorySize;
				
				NSString *blockString = [[NSString alloc] initWithBytes:[blockData bytes]+sizeof(blockHeader) 
																 length:blockHeader.StringLength*sizeof(unichar) 
															   encoding:NSUTF16LittleEndianStringEncoding];
				
				NSNumber *offsetAsNumber = [NSNumber numberWithUnsignedLongLong:[sfFileHandle offsetInFile]];
				if(blockHeader.MemorySize > 0)
					[sfBlockOffsetDictionary setObject:offsetAsNumber forKey:blockString];
				[sfFileHandle seekToFileOffset:nextBlockOffset];
				
			}  
			infoData = [sfFileHandle readDataOfLength:sizeof(struct LIFHeaderInfo)]; 
		}
	}
	else if([versionString isEqualToString:@"2"]) {
		
		while ( [infoData length] != 0 ) {
			memcpy(&headerInfo, [infoData bytes], sizeof(struct LIFHeaderInfo));
			LIFMemoryBlockHeaderV2 blockHeader;
			NSData *blockData = [sfFileHandle readDataOfLength:headerInfo.Length];
			memcpy(&blockHeader, [blockData bytes], sizeof(blockHeader));
			if (blockHeader.Test1 == 0x2A &&  blockHeader.Test2 == 0x2A  ) {
				unsigned long long nextBlockOffset = [sfFileHandle offsetInFile] + blockHeader.MemorySize;
				NSString *blockString = [[NSString alloc] initWithBytes:[blockData bytes]+sizeof(blockHeader)
																 length:blockHeader.StringLength*sizeof(unichar) 
															   encoding:NSUTF16LittleEndianStringEncoding];
				
				NSNumber *offsetAsNumber = [NSNumber numberWithUnsignedLongLong:[sfFileHandle offsetInFile]];
				if(blockHeader.MemorySize > 0)
					[sfBlockOffsetDictionary setObject:offsetAsNumber forKey:blockString];
				[sfFileHandle seekToFileOffset:nextBlockOffset];
				
			}  
			infoData = [sfFileHandle readDataOfLength:sizeof(struct LIFHeaderInfo)]; 
		}
	}
}


@end
