//
//  SFCZIReader.m
//  SanFrancisco
//
//
//

#import "SFCZIReader.h"
#import "SFNDDimension.h"
#import "SFVoxelInfo.h"

#import "czifile.h"

@interface SFCZIReader ()

@property (retain) NSString *fileName;
@property (retain) NSFileHandle	*fileHandle;
@property (retain) NSData *fileHeaderData;

@property (retain) NSData *xmlSegmentData;
@property (retain) NSData *directorySegmentData;
@property (retain) NSData *attachementDirectorySegmentData;

@property (retain) NSData *subBlockDirectoryData;
@property (retain) NSData *metadataData;
@property (retain) NSData *attachmentDirectory;

@property (retain) NSData *directoryEntryArrayData;

@end



@implementation SFCZIReader



- (instancetype)initWithPath:(NSString*)path
{
    self = [super init];
    if (self) {
        _fileName = path;
        _fileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
        [self readSegmentHeaders];
        [_fileHandle seekToFileOffset:0];
        NSData *headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
        const CZISegmentHeader *header = [headerData bytes];
        
        NSString *headerIDString = [NSString stringWithCString:header->segmentID encoding:NSASCIIStringEncoding];
        BOOL isFileHeader = [headerIDString  isEqualToString:@"ZISRAWFILE"];
        if(isFileHeader) {
            _fileHeaderData = [_fileHandle readDataOfLength:header->usedSize];
            const CZIFileHeader *fileHeader = _fileHeaderData.bytes;
            // read XML segment
            if(fileHeader->metadataPosition != 0) {
                [_fileHandle seekToFileOffset:fileHeader->metadataPosition];
                NSData *headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
                header = headerData.bytes;
                if(!memcmp(&header->segmentID,"ZISRAWMETADATA" , 15))
                    _xmlSegmentData =[_fileHandle readDataOfLength:header->usedSize];
            }
            // read directory segment
            if(fileHeader->directoryPosition != 0) {
                [_fileHandle seekToFileOffset:fileHeader->directoryPosition];
                headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
                header = headerData.bytes;
                if(!memcmp(&header->segmentID,"ZISRAWDIRECTORY" , 15))
                    _directorySegmentData =[_fileHandle readDataOfLength:header->usedSize*32];
                if(_directorySegmentData) {
                    const CZIDirectorySegment *directory = _directorySegmentData.bytes;
                    NSMutableData *arrayData = [NSMutableData dataWithCapacity:directory->count*sizeof(CZIDirectoryEntryDV)];
                    const CZIDirectoryEntryDV *pDirectoryEntry = _directorySegmentData.bytes+sizeof(CZIDirectorySegment);
                    NSUInteger directoryBlockIndex;
                    for(directoryBlockIndex=0;directoryBlockIndex<directory->count;directoryBlockIndex++) {
                        [arrayData appendBytes:pDirectoryEntry length:sizeof(CZIDirectoryEntryDV)];
                        pDirectoryEntry = (void*)pDirectoryEntry+sizeof(CZIDirectoryEntryDV)+sizeof(CZIDimensionEntryDV1)*pDirectoryEntry->DimensionCount;
                    }
                    _directoryEntryArrayData = arrayData;
                }
            }
            // read attachement segment
            if(fileHeader->attachementDirectoryPosition != 0) {
                [_fileHandle seekToFileOffset:fileHeader->attachementDirectoryPosition];
                headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
                header = headerData.bytes;
                if(!memcmp(&header->segmentID,"ZISRAWATTDIR" , 12))
                    _attachementDirectorySegmentData =[_fileHandle readDataOfLength:header->usedSize];
                if(_attachementDirectorySegmentData) {
                    
                }
            }
        }
    }
    return self;
}




-(void)scanDirectory {
    if(_directorySegmentData) {
        const CZIDirectorySegment *directory = _directorySegmentData.bytes;
        NSUInteger directoryBlockIndex;
        const CZIDirectoryEntryDV *pDirectoryEntry = _directorySegmentData.bytes+sizeof(CZIDirectorySegment);
        for(directoryBlockIndex=0;directoryBlockIndex<directory->count;directoryBlockIndex++) {
            //NSLog(@"directory block: %lu",(unsigned long)directoryBlockIndex);
            const CZIDimensionEntryDV1 *pDimensionEntry = (void*)pDirectoryEntry+sizeof(CZIDirectoryEntryDV);
            NSUInteger dimensionIndex;
            for(dimensionIndex=0;dimensionIndex<pDirectoryEntry->DimensionCount;dimensionIndex++) {
                const CZIDimensionEntryDV1 *entry = &pDimensionEntry[dimensionIndex];
                NSLog(@"dimension:%s start:%d size:%d start:%f space:%d",entry->Dimension, entry->Start, entry->Size, entry->StartCoordinate, entry->StoredSize);
            }
            pDirectoryEntry = (void*)pDirectoryEntry+sizeof(CZIDirectoryEntryDV)+sizeof(CZIDimensionEntryDV1)*pDirectoryEntry->DimensionCount;
        }
        
    }
    
}



/*
 uint64_t directoryPosition;
 uint64_t metadataPosition;
 uint32_t updatePending;
 uint64_t attachementDirectoryPosition;
*/

/*
 const CZIXmlSegment *xmlsegment = _xmlSegmentData.bytes;
 NSString *xmlString = [[NSString alloc]initWithUTF8String:xmlsegment->xmlString];
 NSError *error;
 NSXMLDocument *document = [[NSXMLDocument alloc]initWithXMLString:xmlString options:0 error:&error];
 if(error = nil)
 NSLog(@"no error");
 */


- (NSArray*)readSegmentHeaders {
    unsigned long long fileSize = [_fileHandle seekToEndOfFile];
    NSMutableArray *headerArray = [NSMutableArray new];
    NSData *headerData = nil;
    [_fileHandle seekToFileOffset:0];
    do {
        headerData = [_fileHandle readDataOfLength:sizeof(CZISegmentHeader)];
        if(headerData) {
            [headerArray addObject:headerData];
            const CZISegmentHeader *header = [headerData bytes];
            [_fileHandle seekToFileOffset:_fileHandle.offsetInFile+header->allocatedSize];

            //NSLog(@"header type:%s", header->segmentID);
        }
    }
    while(_fileHandle.offsetInFile < fileSize && headerData);
    return headerArray;
}

@end
