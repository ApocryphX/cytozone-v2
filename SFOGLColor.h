//
//  SFOGLColor.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>

@interface SFOGLColor : NSObject {
	NSColor* sfColor;
	GLfloat sfOGLColor[4];
}


+ (id)colorWithColor:(NSColor*)color;
- (id)initWithColor:(NSColor*)color;

- (void)setColor:(NSColor*)color;
- (NSColor*)color;

- (void)setOpenGLColor;
- (void) setWithAlpha:(float)alpha;

@end
