//
//  SFPixelProcessor.m
//  Monterey
//
//  Created by ApocryphX on 3/25/07.
//  Copyright 2014 Elarity, LLC.  All rights reserved.
//

#import "SFPixelProcessor.h"
#import "SFBitplane.h"

@interface SFPixelProcessor (private)

@end

@implementation SFPixelProcessor

@synthesize sourceBitplane;
@synthesize targetBitplane;
@synthesize parameterDictionary;

-(id)init {
	if((self=[super init])) {
		parameterDictionary = [NSMutableDictionary new];
	}
	return self;
}

@end

