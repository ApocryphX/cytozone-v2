//
//  SFLSMReader.h
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFNDImporter.h"

@class SFTIFFReader, SFZeissLSMInfo ;

@interface SFLSMReader : NSObject <SFNDReading> {
	NSString        *sfFileName;
	SFTIFFReader    *sfTIFFReader;
        //SFZeissLSMInfo  *sfLSMInfo;
    NSData          *sfZeissTagData;
    NSData          *channelColors;
    NSData          *rgbColorArrayData;
    NSData          *dataTypeData;
}

- (NSString*)fileName;

@end
