//
//  AppDelegate.m
//  CytoImage
//
//  Created by Kolja A. Wawrowsky on 11/14/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
