//
//  Document.m
//  CytoImage
//
//  Created by Kolja A. Wawrowsky on 11/14/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "Document.h"

#import "SFTIFFReader.h"
#include <openslide/openslide.h>
#include "SFSVSReader.h"


@interface Document ()

@end

@implementation Document

- (instancetype)init {
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController {
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

+ (BOOL)autosavesInPlace {
    return YES;
}

- (void)makeWindowControllers {
    // Override to return the Storyboard file name of the document.
    [self addWindowController:[[NSStoryboard storyboardWithName:@"Main" bundle:nil] instantiateControllerWithIdentifier:@"Document Window Controller"]];
}


- (BOOL)readFromURL:(NSURL *)url ofType:(NSString *)typeName error:(NSError **)outError {

    const char *path = [[[url path]stringByExpandingTildeInPath] cStringUsingEncoding:NSUTF8StringEncoding];
    SFSVSReader *reader = [[SFSVSReader alloc]initWithPath:[url path]];
    BOOL success = 	openslide_can_open(path);
    if(success == NO)
        return NO;
    openslide_t *osr = openslide_open(path);
    const char *error = openslide_get_error(osr);
    if(osr == NULL || error != NULL)
        return NO;

    int32_t level_count =  openslide_get_level_count(osr);
    int64_t w;
    int64_t h;
    openslide_get_level0_dimensions(osr, &w, &h);
    const char * const *names = openslide_get_property_names(osr);
    unsigned long index = 0;
    while (names[index] != NULL) {
        NSLog (@"%s",names[index]);
        index++;
    }
    const char *image_description =  openslide_get_property_value(osr, "tiff.ImageDescription");
    NSLog (@"%s",image_description);

    return YES;
};


- (BOOL)writeSafelyToURL:(NSURL *)url ofType:(NSString *)typeName forSaveOperation:(NSSaveOperationType)saveOperation error:(NSError **)outError {
    return NO;
};


@end
