//
//  ViewController.m
//  CytoImage
//
//  Created by Kolja A. Wawrowsky on 11/14/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

@end
