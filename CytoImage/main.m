//
//  main.m
//  CytoImage
//
//  Created by Kolja A. Wawrowsky on 11/14/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
