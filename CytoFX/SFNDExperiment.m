//
//  SFNDExperiment.m
//  VoxelMachine
//
//  Created by Kolja A. Wawrowsky on Sun May 23 2004.
//  Copyright (c) 2004 Kolja A. Wawrowsky. All rights reserved.
//

#import "SFNDExperiment.h"

#import "SFNDVoxelData.h"
#import "SFVolumeWindowController.h"
#import "SFNDDataset.h"
#import "SFNDImporter.h"
#import "SFLoadingController.h"
#import "SFNDDimension.h"

@interface SFNDExperiment (private)
@end

@implementation SFNDExperiment

@synthesize importer = sfReader;
@synthesize outlineNodes = sfOutlineNodes;
@dynamic nodeIndex;

- (id)init
{
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
		//sfDatasets =[NSMutableArray new];
        // Assume the default file type for now, since -initWithType:error: does not currently get called when creating documents using AppleScript. (4165700)
        //NSString *defaultType = [[NSDocumentController sharedDocumentController] defaultType];
        //[self setFileType:defaultType];

    }
    return self;
}

// -------------------------------------------------------------------------------
//  autosavesInPlace
// -------------------------------------------------------------------------------
+ (BOOL)autosavesInPlace
{
    // this gives us autosave and versioning for free in 10.7 and later
    return NO;
}

// -------------------------------------------------------------------------------
//  canAsynchronouslyWriteToURL:url:typeName:saveOperation
//
//  Turn this on for async saving allowing saving to be asynchronous, making all our
//  save methods (dataOfType, saveToURL) to be called on a background thread.
// -------------------------------------------------------------------------------
- (BOOL)canAsynchronouslyWriteToURL:(NSURL *)url ofType:(NSString *)typeName forSaveOperation:(NSSaveOperationType)saveOperation
{
    return NO;
}

/*
#pragma mark - Package Support

// -------------------------------------------------------------------------------
//  fileWrapperOfType:typeName:error
//
//  Called when the user saves this document or when autosave is performed.
//  Create and return a file wrapper that contains the contents of this document,
//  formatted for our specified type.
// -------------------------------------------------------------------------------
- (NSFileWrapper *)fileWrapperOfType:(NSString *)typeName error:(NSError **)outError
{
    // If the document was not read from file or has not previously been saved,
    // it doesn't have a file wrapper, so create one.
    //
    if ([self documentFileWrapper] == nil)
    {
        NSFileWrapper *docfileWrapper = [[NSFileWrapper alloc] initDirectoryWithFileWrappers:nil];
        [self setDocumentFileWrapper:docfileWrapper];
    }
    
    NSDictionary *fileWrappers = [[self documentFileWrapper] fileWrappers];
    return [self documentFileWrapper];
}

// -------------------------------------------------------------------------------
//  readFromFileWrapper:fileWrapper:typeName:outError
//
//  Set the contents of this document by reading from a file wrapper of a specified type,
//  and return YES if successful.
// -------------------------------------------------------------------------------
- (BOOL)readFromFileWrapper:(NSFileWrapper *)fileWrapper ofType:(NSString *)typeName error:(NSError **)outError
{
 
     // When opening a document, look for the image and text file wrappers. For each wrapper,
     // extract the data from it and keep the file wrapper itself. The file wrappers are kept
     // so that, if the corresponding data hasn't been changed, they can be resused during a
     // save and thus the source file itself can be reused rather than rewritten. This avoids
     // the overhead of syncing data unnecessarily. If the data related to a file wrapper changes
     // (a new image is added or the text is edited), the corresponding file wrapper object is
     // disposed of and a new file wrapper created on save (see fileWrapperOfType:error:).
 
    //NSDictionary *fileWrappers = [fileWrapper fileWrappers];
    
    // load the text file from it's wrapper
    
    //NSString *defaultType = [[NSDocumentController sharedDocumentController] defaultType];
    //[self setFileType:defaultType];
    
    return YES;
}


 - (NSString *)windowNibName
 {
	 // Override returning the nib file name of the document
	 // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
	return Nil;
    // return @"SFNDExperiment";
 };
*/

- (void)makeWindowControllers {
	SFLoadingController* ctrl = [[SFLoadingController  alloc] initWithWindowNibName:@"LoadingController"];
	[ctrl setShouldCloseDocument:NO];
	[self addWindowController:ctrl];
	[ctrl showWindow:self];
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
	
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window
//    SFLoadingController* ctrl = [[SFLoadingController  alloc] initWithWindowNibName:@"LoadingController"];
//	[ctrl setShouldCloseDocument:NO];
//	[self addWindowController:ctrl];
//	[ctrl showWindow:self];
    
//	[ctrl readWithReader:sfReader];


};

/*
- (BOOL)writeToURL:(NSURL *)url options:(NSFileWrapperWritingOptions)options originalContentsURL:(NSURL *)originalContentsURL error:(NSError **)outError {
    
    return YES;
}
*/
- (BOOL)readFromURL:(NSURL *)absoluteURL ofType:(NSString *)typeName error:(NSError **)outError {
	BOOL ret = NO;
	if ([absoluteURL isFileURL]) {
		sfReader = [[SFNDImporter alloc]initWithPath:[absoluteURL path] ofType:typeName]; 
		if (sfReader) {
			self.outlineNodes = [sfReader outlineNodes];
           // NSString *defaultType = [[NSDocumentController sharedDocumentController] defaultType];
           // [self setFileType:defaultType];
			ret = YES;
		} 
	}
	return YES;
};

/*
- (BOOL)revertToContentsOfURL:(NSURL *)url ofType:(NSString *)type error:(NSError **)outError {
    BOOL success = [super revertToContentsOfURL:url ofType:type error:outError];
    if (success) {
        if (fileTypeToSet) {	// If we're reverting, NSDocument will set the file type behind out backs. This enables restoring that type.
            [self setFileType:fileTypeToSet];
            [fileTypeToSet release];
            fileTypeToSet = nil;
        }
        [self setHasMultiplePages:hasMultiplePages];
        [[self windowControllers] makeObjectsPerformSelector:@selector(setupTextViewForDocument)];
    }
    return success;
}
*/

- (void)showSelected:(NSArray*)array {
	NSUInteger i;
	for(i=0;i<[array count];i++) {
		SFVolumeWindowController* ctrl = [[SFVolumeWindowController alloc] initWithWindowNibName:@"VolumeView"];
		[ctrl setNDDataset:[array objectAtIndex:i]];
		[ctrl setShouldCloseDocument:NO];
		[ctrl showWindow:self];
		[self addWindowController:ctrl];
	}				
}

/*
- (void) setNodeIndex:(NSUInteger)idx {
	sfNodeIndex = idx;
//	NSUInteger i;	
	sfReader.nodeIndex = idx;
//	sfChannelCount = [sfConfocalImporter countOfChannels];
//	SFNDDimension *ndDimension = [sfConfocalImporter dimensions];
//	
//	if([ndDimension extentAtIndex:2] > 2) 
//		self.imageIndex = [ndDimension extentAtIndex:2]/2;
//	else
//		self.imageIndex = 0;
//	
//	NSMutableArray *bitplanes = [NSMutableArray arrayWithCapacity:sfChannelCount];
//	
//	for(i=0;i<sfChannelCount;i++) {
//		
//		SFPixelType* pixelType = [sfConfocalImporter pixelTypeForChannel:i];
//		SFBitplane *newBitplane = [SFBitplane bitplaneWithSize:NSMakeSize([ndDimension extentAtIndex:0], [ndDimension extentAtIndex:1]) 
//													 pixelType:pixelType];
//		[bitplanes addObject:newBitplane];
//	}
//	self.bitplanes = bitplanes;
//	
	// start loading bitplane data in background
//	[sfOperationQueue cancelAllOperations];
//	for(i=0;i<sfChannelCount;i++) {
//		
//		SF2DLoadOperation *loadOperation = [[SF2DLoadOperation alloc] initWithImporter:sfConfocalImporter 
//																		 bitplaneIndex:sfImageIndex  
//																		  channelIndex:i];
//		loadOperation.notificationTarget = self;
//		[sfOperationQueue addOperation:loadOperation];
//		[loadOperation release];
//	}
	
}
*/
-(NSUInteger)nodeIndex {
    return sfNodeIndex;
}
/*
- (void)addNDDataset:(SFNDDataset*)anObject {
	//if([sfDatasets count]==0)
    SFNDDimension *nddim = [[anObject nDVoxelData]nDDimension];
	if ([nddim count] > 2) {
		SFVolumeWindowController* ctrl = [[SFVolumeWindowController alloc] initWithWindowNibName:@"VolumeView"];
		[self addWindowController:ctrl];
		[ctrl setNDDataset:anObject];
		//[ctrl showOutline:self.outlineNodes];
		[ctrl setShouldCloseDocument:NO];
		[ctrl showWindow:self];

	}
	[self insertObject:anObject inDatasetsAtIndex:[sfDatasets count]];
}
*/
-(id)datasets {
    return nil;
}

// KVC complient interface for Datasets array
/*
- (NSUInteger)countOfDatasets {
	return [sfDatasets count];
};

- (id)objectInDatasetsAtIndex:(NSUInteger)idx {
	id ret = nil;
	@synchronized (sfArrayController) {	
		ret = [sfDatasets objectAtIndex:idx];
	}
	return ret;
};

- (void)insertObject:(id)anObject inDatasetsAtIndex:(NSUInteger)idx {
	@synchronized (sfArrayController) {	
		[sfDatasets insertObject:anObject atIndex:idx];
	}
};

- (void)removeObjectFromDatasetsAtIndex:(NSUInteger)idx {
	@synchronized (sfArrayController) {	
		[sfDatasets removeObjectAtIndex:idx];
	}
};

- (NSUInteger)count {
	return [sfDatasets count];
};
*/
/*
- (BOOL)prepareSavePanel:(NSSavePanel *)savePanel {
	[savePanel setRequiredFileType:@"tiff"];
	[savePanel setAllowsOtherFileTypes:NO];	
	return YES;
	ret = [NSKeyedArchiver archiveRootObject:sfDatasets toFile:[absoluteURL path]];		
};


- (BOOL)writeSafelyToURL:(NSURL *)absoluteURL ofType:(NSString *)typeName forSaveOperation:(NSSaveOperationType)saveOperation error:(NSError **)outError{
	BOOL ret = NO;
	return ret;
};
*/
- (void)dealloc 
{	
	[[NSNotificationCenter defaultCenter]removeObserver:self];
};

@end
