//
//  main.m
//  CytoFX
//
//  Created by Kolja Wawrowsky on 1/14/14.
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
