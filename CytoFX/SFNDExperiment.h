//
//  SFNDExperiment.h
//  VoxelMachine
//
//  Created by Kolja A. Wawrowsky on Sun May 23 2004.
//  Copyright (c) 2004 Kolja A. Wawrowsky. All rights reserved.
//


#import <Cocoa/Cocoa.h>

@protocol SFNDReading;
@class SFNDDataset, SFNDImporter;

@interface SFNDExperiment : NSDocument {
	IBOutlet NSArrayController* sfArrayController;
    IBOutlet NSTreeController *treeController;
    
    
    NSArray*		sfOutlineNodes;
	SFNDImporter*	sfReader;
	NSUInteger		sfNodeIndex;
    
}

@property (retain) NSFileWrapper *documentFileWrapper;
@property (retain)	NSArray*	outlineNodes;
@property (assign)	NSUInteger	nodeIndex;
@property (readonly) SFNDImporter*	importer;

- (void)showSelected:(NSArray*)array ;

    //- (void)addNDDataset:(SFNDDataset*)anObject;


@end
