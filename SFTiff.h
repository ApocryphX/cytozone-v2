/*
 *  tiff.h
 *  Proto-Cocoa-TiffReader
 *
 *  Created by ApocryphX on Sun Mar 09 2003.
 *  Copyright (c) 2003 Elarity, LLC. All rights reserved.
 *
 */
#ifndef TIFF_H
#define TIFF_H

enum TIFFDataType
{
    NOTYPE = 0, /* placeholder */
    BYTE = 1,   /* 8-bit NSUIntegerinteger */
    ASCII = 2,  /* 8-bit bytes w/ last byte null */
    SHORT = 3,  /* 16-bit NSUIntegerinteger */
    LONG = 4,   /* 32-bit NSUIntegerinteger */
    RATIONAL = 5, /* 64-bit NSUIntegerfraction */
    SBYTE = 6,  /* !8-bit signed integer */
    UND = 7,    /* !8-bit untyped data */
    SSHORT = 8, /* !16-bit signed integer */
    SLONG = 9,  /* !32-bit signed integer */
    SRATIONAL = 10, /* !64-bit signed fraction */
    FLOAT = 11,  /* !32-bit IEEE floating point */
    DOUBLE = 12, /* !64-bit IEEE floating point */
    IFD = 13, /* 32-bit unsigned integer (offset) */
    LONG8 = 16,  /* unsigned 8byte integer */
    SLONG8 = 17, /* being signed 8byte integer */
    IFD8 = 18    /* being a new unsigned 8byte IFD offset. */
};

enum TIFFPhotometricInterpretation
{
	   PHOTOMETRIC_MINISWHITE = 0, /* min value is white */
	   PHOTOMETRIC_MINISBLACK = 1, /* min value is black */
	   PHOTOMETRIC_RGB = 2, /* RGB color model */
	   PHOTOMETRIC_PALETTE = 3, /* color map indexed */
	   PHOTOMETRIC_MASK = 4, /* $holdout mask */
	   PHOTOMETRIC_SEPARATED = 5, /* !color separations */
	   PHOTOMETRIC_YCBCR = 6, /* !CCIR 601 */
	   PHOTOMETRIC_CIELAB = 8 /* !1976 CIE L*a*b* */
};
	enum TIFFCompressionType
{
	   COMPRESSION_NONE = 1,             /* dump mode */
	   COMPRESSION_CCITTRLE = 2,         /* CCITT modified Huffman RLE */
	   COMPRESSION_CCITTFAX3 = 3, /* CCITT Group 3 fax encoding */
	   COMPRESSION_CCITTFAX4 = 4, /* CCITT Group 4 fax encoding */
	   COMPRESSION_LZW = 5, /* Lempel-Ziv & Welch */
	   COMPRESSION_JPEG = 6, /* !JPEG compression */
	   COMPRESSION_NEXT = 32766, /* NeXT 2-bit RLE */
	   COMPRESSION_CCITTRLEW = 32771, /* #1 w/ word alignment */
	   COMPRESSION_PACKBITS = 32773, /* Macintosh RLE */
	   COMPRESSION_THUNDERSCAN = 32809 /* ThunderScan RLE */
};
	enum TIFFSUBFILETYPE
{
	   FILETYPE_NORMAL = 0x0,
	   FILETYPE_REDUCEDIMAGE = 0x1,    /* reduced resolution version */
	   FILETYPE_PAGE = 0x2,            /* one page of many */
	   FILETYPE_MASK = 0x4            /* transparency mask */
};
	enum TIFFOSUBFILETYPE
{
	   OFILETYPE_IMAGE = 1,            /* full resolution image data */
	   OFILETYPE_REDUCEDIMAGE = 2,     /* reduced size image data */
	   OFILETYPE_PAGE = 3             /* one page of many */
};
	enum TIFFOrientation
{
	   ORIENTATION_TOPLEFT = 1, /* row 0 top, col 0 lhs */
	   ORIENTATION_TOPRIGHT = 2, /* row 0 top, col 0 rhs */
	   ORIENTATION_BOTRIGHT = 3, /* row 0 bottom, col 0 rhs */
	   ORIENTATION_BOTLEFT = 4, /* row 0 bottom, col 0 lhs */
	   ORIENTATION_LEFTTOP = 5, /* row 0 lhs, col 0 top */
	   ORIENTATION_RIGHTTOP = 6, /* row 0 rhs, col 0 top */
	   ORIENTATION_RIGHTBOT = 7, /* row 0 rhs, col 0 bottom */
	   ORIENTATION_LEFTBOT = 8 /* row 0 lhs, col 0 bottom */
};
	enum TIFFThresholding
{
	   THRESHHOLD_BILEVEL = 1, /* b&w art scan */
	   THRESHHOLD_HALFTONE = 2, /* or dithered scan */
	   THRESHHOLD_ERRORDIFFUSE = 3 /* usually floyd-steinberg */
};
	enum TIFFFillOrder
{
	   FILLORDER_MSB2LSB = 1, /* most significant -> least */
	   FILLORDER_LSB2MSB = 2 /* least significant -> most */
};
	enum TIFFPlanarConfig
{
	   PLANARCONFIG_CONFIG = 1, /* single image plane */
	   PLANARCONFIG_SEPARATE = 2 /* separate planes of data */
};
	enum TIFFGrayResponseUnit
{
	   GRAYRESPONSEUNIT_10S = 1, /* tenths of a unit */
	   GRAYRESPONSEUNIT_100S = 2, /* hundredths of a unit */
	   GRAYRESPONSEUNIT_1000S = 3, /* thousandths of a unit */
	   GRAYRESPONSEUNIT_10000S = 4, /* ten-thousandths of a unit */
	   GRAYRESPONSEUNIT_100000S = 5 /* hundred-thousandths */
};
	enum TIFFGroup3Options
{
	   GROUP3OPT_2DENCODING = 0x1, /* 2-dimensional coding */
	   GROUP3OPT_UNCOMPRESSED = 0x2, /* data not compressed */
	   GROUP3OPT_FILLBITS = 0x4 /* fill to byte boundary */
};
	enum TIFFGroup4Options
{
	   GROUP4OPT_UNCOMPRESSED = 0x2 /* data not compressed */
};
	enum TIFFResolutionUnit
{
	   RESUNIT_NONE = 1, /* no meaningful units */
	   RESUNIT_INCH = 2, /* english */
	   RESUNIT_CENTIMETER = 3 /* metric */
};
	enum TIFFColorResponseUnit
{
	   COLORRESPONSEUNIT_10S = 1, /* tenths of a unit */
	   COLORRESPONSEUNIT_100S = 2, /* hundredths of a unit */
	   COLORRESPONSEUNIT_1000S = 3, /* thousandths of a unit */
	   COLORRESPONSEUNIT_10000S = 4, /* ten-thousandths of a unit */
	   COLORRESPONSEUNIT_100000S = 5 /* hundred-thousandths */
};
	enum TIFFCleanFaxData
{
	   CLEANFAXDATA_CLEAN = 0, /* no errors detected */
	   CLEANFAXDATA_REGENERATED = 1, /* receiver regenerated lines */
	   CLEANFAXDATA_UNCLEAN = 2 /* uncorrected errors exist */
};
	enum TIFFExtraSamples
{
	   EXTRASAMPLE_UNSPECIFIED = 0, /* !unspecified data */
	   EXTRASAMPLE_ASSOCALPHA = 1, /* !associated alpha data */
	   EXTRASAMPLE_UNASSALPHA = 2 /* !unassociated alpha data */
};
	enum TIFFInkSet
{
	   INKSET_CMYK = 1 /* !cyan-magenta-yellow-black */
};
	enum TIFFSampleFormat
{
	   SAMPLEFORMAT_UINT = 1, /* !NSUIntegerinteger data */
	   SAMPLEFORMAT_INT = 2, /* !signed integer data */
	   SAMPLEFORMAT_IEEEFP = 3, /* !IEEE floating point data */
	   SAMPLEFORMAT_VOID = 4 /* !untyped data */
};
	enum TIFFJPEGProc
{
	   JPEGPROC_BASELINE = 1, /* !baseline sequential */
	   JPEGPROC_LOSSLESS = 14 /* !Huffman coded lossless */
};
	enum TIFFYCBCRPositioning
{
	   YCBCRPOSITION_CENTERED = 1, /* !as in PostScript Level 2 */
	   YCBCRPOSITION_COSITED = 2 /* !as in CCIR 601-1 */
};
/*
 * TIFF Tag Definitions.
 */
enum TIFFInfo
{
	   SUBFILETYPE = 254,         /* subfile data descriptor */
	   OSUBFILETYPE = 255,       /* +kind of data in subfile */
	   IMAGEWIDTH = 256,          /* image width in pixels */
	   IMAGELENGTH = 257,         /* image height in pixels */
	   BITSPERSAMPLE = 258,      /* bits per channel (sample) */
	   COMPRESSION = 259,         /* data compression technique */
	   PHOTOMETRIC = 262, /* photometric interpretation */
	   THRESHHOLDING = 263, /* +thresholding used on data */
	   CELLWIDTH = 264, /* +dithering matrix width */
	   CELLLENGTH = 265, /* +dithering matrix height */
	   FILLORDER = 266, /* data order within a byte */
	   DOCUMENTNAME = 269, /* name of doc. image is from */
	   IMAGEDESCRIPTION = 270, /* info about image */
	   MAKE = 271, /* scanner manufacturer name */
	   MODEL = 272, /* scanner model name/number */
	   STRIPOFFSETS = 273, /* offsets to data strips */
	   ORIENTATION = 274, /* +image orientation */
	   SAMPLESPERPIXEL = 277, /* samples per pixel */
	   ROWSPERSTRIP = 278, /* rows per strip of data */
	   STRIPBYTECOUNTS = 279, /* bytes counts for strips */
	   MINSAMPLEVALUE = 280, /* +minimum sample value */
	   MAXSAMPLEVALUE = 281, /* +maximum sample value */
	   XRESOLUTION = 282, /* pixels/resolution in x */
	   YRESOLUTION = 283, /* pixels/resolution in y */
	   PLANARCONFIG = 284, /* storage organization */
	   PAGENAME = 285, /* page name image is from */
	   XPOSITION = 286, /* x page offset of image lhs */
	   YPOSITION = 287, /* y page offset of image lhs */
	   FREEOFFSETS = 288, /* +byte offset to free block */
	   FREEBYTECOUNTS = 289, /* +sizes of free blocks */
	   GRAYRESPONSEUNIT = 290, /* $gray scale curve accuracy */
	   GRAYRESPONSECURVE = 291, /* $gray scale response curve */
	   GROUP3OPTIONS = 292, /* 32 flag bits */
	   GROUP4OPTIONS = 293, /* 32 flag bits */
	   RESOLUTIONUNIT = 296, /* units of resolutions */
	   PAGENUMBER = 297, /* page numbers of multi-page */
	   COLORRESPONSEUNIT = 300, /* $color curve accuracy */
	   TRANSFERFUNCTION = 301, /* !colorimetry info */
	   SOFTWARE = 305, /* name & release */
	   DATETIME = 306, /* creation date and time */
	   ARTIST = 315, /* creator of image */
	   HOSTCOMPUTER = 316, /* machine where created */
	   PREDICTOR = 317, /* prediction scheme w/ LZW */
	   WHITEPOINT = 318, /* image white point */
	   PRIMARYCHROMATICITIES = 319, /* !primary chromaticities */
	   COLORMAP = 320, /* RGB map for pallette image */
	   HALFTONEHINTS = 321, /* !highlight+shadow info */
	   TILEWIDTH = 322, /* !rows/data tile */
	   TILELENGTH = 323, /* !cols/data tile */
	   TILEOFFSETS = 324, /* !offsets to data tiles */
	   TILEBYTECOUNTS = 325, /* !byte counts for tiles */
	   BADFAXLINES = 326, /* lines w/ wrong pixel count */
	   CLEANFAXDATA = 327, /* regenerated line info */
	   CONSECUTIVEBADFAXLINES = 328, /* max consecutive bad lines */
	   INKSET = 332, /* !inks in separated image */
	   INKNAMES = 333, /* !ascii names of inks */
	   DOTRANGE = 336, /* !0% and 100% dot codes */
	   TARGETPRINTER = 337, /* !separation target */
	   EXTRASAMPLES = 338, /* !info about extra samples */
	   SAMPLEFORMAT = 339, /* !data sample format */
	   SMINSAMPLEVALUE = 340, /* !variable MinSampleValue */
	   SMAXSAMPLEVALUE = 341, /* !variable MaxSampleValue */
	   JPEGPROC = 512, /* !JPEG processing algorithm */
	   JPEGIFOFFSET = 513, /* !pointer to SOI marker */
	   JPEGIFBYTECOUNT = 514, /* !JFIF stream length */
	   JPEGRESTARTINTERVAL = 515, /* !restart interval length */
	   JPEGLOSSLESSPREDICTORS = 517, /* !lossless proc predictor */
	   JPEGPOINTTRANSFORM = 518, /* !lossless point transform */
	   JPEGQTABLES = 519, /* !Q matrice offsets */
	   JPEGDCTABLES = 520, /* !DCT table offsets */
	   JPEGACTABLES = 521, /* !AC coefficient offsets */
	   YCBCRCOEFFICIENTS = 529, /* !RGB -> YCbCr transform */
	   YCBCRSUBSAMPLING = 530, /* !YCbCr subsampling factors */
	   YCBCRPOSITIONING = 531, /* !subsample positioning */
	   REFERENCEBLACKWHITE = 532, /* !colorimetry info */
	   /* tags 32952-32956 are private tags registered to Island Graphics */
	   REFPTS = 32953, /* image reference points */
	   REGIONTACKPOINT = 32954, /* region-xform tack point */
	   REGIONWARPCORNERS = 32955, /* warp quadrilateral */
	   REGIONAFFINE = 32956, /* affine transformation mat */
	   /* tags 32995-32999 are private tags registered to SGI */
	   MATTEING = 32995, /* $use ExtraSamples */
	   DATATYPE = 32996, /* $use SampleFormat */
	   IMAGEDEPTH = 32997, /* z depth of image */
	   TILEDEPTH = 32998 /* z depth/data tile */
};

#endif
