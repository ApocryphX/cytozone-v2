//
//  SFCLComputeKernel.h
//  Monterey
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include <OpenCL/opencl.h>

@interface SFCLComputeKernel : NSObject {
	cl_program program;                 // compute program

	cl_device_id device_id;             // compute device id 
	cl_context context;                 // compute context
	cl_command_queue commands;			// command queue
	cl_kernel kernel;                   // compute kernel	
}

@property (readonly) cl_device_id device_id;
@property (readonly) cl_context context;
@property (readonly) cl_command_queue commands;
@property (readonly) cl_kernel kernel;

-(id)initWithSource:(NSString*)source functionName:(NSString*)functionName;

@end
