//
//  SFImageScrollView.m
//  CytoFX64
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFImageScrollView.h"
#import "SFImageFlowView.h"
#import "SFAnimationKey.h"

@implementation SFImageScrollView

@dynamic duration;
@synthesize animationKeys;

- (void)updateUIState {
    SFImageFlowView *documentView = [self  documentView];
    if(documentView.indexSet && [documentView.indexSet count]>0) {
        movieDeleteFrameButton.enabled = YES;
        restoreButton.enabled = YES;
        SFAnimationKey *key = [animationKeys objectAtIndex:[documentView.indexSet lastIndex]];
        self.duration = [NSNumber numberWithFloat:key.duration];
    }
    else {
        movieDeleteFrameButton.enabled = NO;
        restoreButton.enabled = NO;
        self.duration = nil;
    }
    if(documentView.images.count>1) 
        movieRecordButton.enabled = YES;
    else
        movieRecordButton.enabled = NO;
    if(documentView.images.count>0) 
        movieDeleteButton.enabled = YES;
    else
        movieDeleteButton.enabled = NO;

}

- (void)setupView {
    //[self setDrawsBackground:YES];
    [self setBackgroundColor:[NSColor darkGrayColor]];
    //[self setAutoresizingMask: NSViewWidthSizable ];
    [self setHasHorizontalScroller:YES];
    //create and insert SFImageFlowView view into clip view
    
    //get the clip view frame rectangle
    SFImageFlowView *documentView = [[SFImageFlowView alloc]initWithFrame:self.contentView.frame];
    // width depends on number and size of inserted images, make only the height autoscale
    [documentView setAutoresizingMask:NSViewNotSizable];             
    [self setDocumentView:documentView];
    animationKeys = [NSMutableArray new];
}

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.        
        [self setupView];
    }
    return self;
}

-(void)awakeFromNib {
    [self setupView];
}
 

- (void)mouseDown:(NSEvent *)theEvent {
    NSPoint curPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
    SFImageFlowView *documentView = self.documentView;
    if(NSPointInRect(curPoint, documentView.frame)) {
        documentView.indexSet = documentView.indexSet;
    }
    else {
        documentView.indexSet = nil;
    }
    //[super mouseDown:theEvent];
    [self updateUIState];
}

- (void) setDuration:(NSNumber *)value {
    duration = value;
    SFImageFlowView *documentView = [self  documentView];
    if(documentView.indexSet && [documentView.indexSet count]>0) {
        SFAnimationKey *key = [animationKeys objectAtIndex:[documentView.indexSet lastIndex]];
        CGFloat newDuration = [value floatValue];
        if(newDuration != key.duration) {
            key.duration = newDuration;
        }
    }
}

- (NSNumber*) duration {
    return duration; 
}

@end
