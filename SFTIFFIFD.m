 //
//  SFTIFFIFD.m
//  Proto-Cocoa-TiffReader
//
//  Created by ApocryphX on Sun Mar 16 2003.
//  Copyright (c) 2003 Elarity, LLC. All rights reserved.
//

#import "SFTIFFIFD.h"
#import "SFTiff.h"

@implementation SFTIFFIFD

// init contstructs all Tags of IFD and reads offset to the next IFD
-(id)initWithFileHandle:(NSFileHandle*)handle withEndian:(bool)isBigEndian {
    uint32_t i;
    uint32_t baseoffset, nextoffset;
	
    self = [super init];
    baseoffset = (uint32_t)[handle offsetInFile]; //read current offset (=start of IFD)
	
    //read number of entries in the directory
    NSData *tagCountData = [handle readDataOfLength:sizeof(uint16_t)];
    const uint16_t *count = tagCountData.bytes;
    if(isBigEndian)
        IFDTagCount = NSSwapBigShortToHost(*count);
    else
        IFDTagCount = NSSwapLittleShortToHost(*count);
    
    IFDDictionary = [NSMutableDictionary dictionaryWithCapacity:IFDTagCount]; //init temp dictionary with count
    for (i=0; i<IFDTagCount; i++) {
		nextoffset = baseoffset + sizeof(uint16_t) + i*[SFTIFFTag sizeDirEntry]; // calc offset for next tag
		[handle seekToFileOffset:nextoffset]; //seek to the next tag posistion in IFD
		SFTIFFTag* temp = [[SFTIFFTag alloc] initWithFileHandle:handle withEndian:isBigEndian]; //contruct a SFTIFFTag object 
		NSNumber* tagnumber = [NSNumber numberWithUnsignedInt:(unsigned)[temp tagID]];
		[IFDDictionary setObject:temp forKey:tagnumber];
    }
    //seek to position holding uint32_t offset to next IFD and read / rotate the offset
    [handle seekToFileOffset:baseoffset + sizeof(uint16_t) + IFDTagCount*[SFTIFFTag sizeDirEntry]];
    NSData *offsetData = [handle readDataOfLength:sizeof(uint32_t)];
    const uint32_t *pOffset = offsetData.bytes;
    if(isBigEndian)
        nextOffset = NSSwapBigIntToHost(*pOffset);
    else
        nextOffset = NSSwapLittleIntToHost(*pOffset);
    //create NSDictionary holding all IFD Tags from mutable dictionary
    //IFDDictionary = [NSDictionary dictionaryWithDictionary:newIFDDictionary];
    return self;
}

-(SFTIFFTag*)tagWithID:(NSNumber*)tagID{
    return [IFDDictionary objectForKey:tagID];
}

-(NSDictionary*)ditionaryOfTags{
    return IFDDictionary;
}


-(uint32_t)nextIFDOffset{
    return nextOffset;
};


@end
