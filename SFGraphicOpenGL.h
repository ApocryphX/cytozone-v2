//
//  SFGraphicOpenGL.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicRectangle.h"

@interface SFGraphicOpenGL : SFGraphicRectangle

@property (retain) NSImage *image;

@end
