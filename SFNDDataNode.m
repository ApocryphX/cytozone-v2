//
//  SFNDDataNode.m
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFNDDataNode.h"


@implementation SFNDDataNode


- (id)initWithNodeInfo:(id)value {
	self = [super init];
	sfNodeInfo = value;
	return self;
};

- (void)setNodeInfo:(id)value {
	sfNodeInfo = value;
	
};

- (id)nodeInfo {
	return sfNodeInfo;
};


@end
