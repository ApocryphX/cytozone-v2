//
//  SFOGLColor.m
//  CytoFX
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import "SFOGLColor.h"

#include <OpenGL/gl.h>

@implementation SFOGLColor

+ (id)colorWithColor:(NSColor*)color {
	SFOGLColor* ret = nil;
	ret = [[SFOGLColor alloc] initWithColor:color];
	return ret;
}

- (id)initWithColor:(NSColor*)color {
	self = [super init];
	sfColor = color;
	NSColor* rgbcolor = [color colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
	sfOGLColor[0] = [rgbcolor redComponent];
	sfOGLColor[1] = [rgbcolor greenComponent];
	sfOGLColor[2] = [rgbcolor blueComponent];
	sfOGLColor[3] = [rgbcolor alphaComponent];
	return self;
};

- (void)setColor:(NSColor*)color {
	sfColor = color;
	NSColor* rgbcolor = [color colorUsingColorSpaceName:@"NSCalibratedRGBColorSpace"];
	sfOGLColor[0] = [rgbcolor redComponent];
	sfOGLColor[1] = [rgbcolor greenComponent];
	sfOGLColor[2] = [rgbcolor blueComponent];
	sfOGLColor[3] = [rgbcolor alphaComponent];	
};

- (NSColor*)color {
	return sfColor;
};

- (void)setOpenGLColor {
	glColor4fv(sfOGLColor);
};

- (void) setWithAlpha:(float)alpha {
	glColor4f(sfOGLColor[0],sfOGLColor[1],sfOGLColor[2],sfOGLColor[3]*alpha);
};


@end
