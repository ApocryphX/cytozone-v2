//
//  SFSVSReader.h
//  Cytozone
//
//  Created by Kolja Wawrowsky on 3/31/15.
//  Copyright (c) 2015 Elarity, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SFSVSReader : NSObject 

- (instancetype)initWithPath:(NSString *)path NS_DESIGNATED_INITIALIZER;

@end
