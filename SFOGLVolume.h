//
//  SFOGLVolume.h
//  VoxelMachine
//
//  Copyright 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SFGeometry.h"

@class SFNDVoxelData, SFVolumeWindowController, SFOGLTextureStack3D, SFNDDimension,
		SFOGLTextureRectangle, SFOGLTransform, SFOGLBackplane, SFOGLRenderState;

@interface SFOGLVolume : NSObject {

	SFNDVoxelData* sfNDVoxelData;
    NSOpenGLView* sfOGLView;
	
	SFOGLBackplane* sfBackplane;
	SFOGLTransform* sfTransform;
	
	SFNDVoxelData* sfCube;
	SFNDVoxelData* sfShadow;

	NSArray* sfChannelSettings;
	
	NSMutableArray* sfColors;
	
	NSMutableArray* sfVolumeTextures;
	NSMutableArray* sfShadowTextures;
	
	NSUInteger sfVolumeIndex;	
	
	NSColor* sfBlendColor;
	NSColor* sfBackplaneColor;	
	NSUInteger sfRenderMode;
	NSUInteger sfBackplaneMode;
	NSUInteger sfSampling;
	float sfBrightness;
	float sfAlpha;
	float sfZScale;
	BOOL sfIsZInverted;
	
//	BOOL sfIsDownsampled;
}

- (void) setTextureTransform:(SFOGLTransform*)value;
- (void) setOpenGLView:(NSOpenGLView*)value;
- (void) setNDVoxelData:(SFNDVoxelData*)nddata;
- (void) setBackplane:(SFOGLBackplane*)value;
- (void) setBackplaneNDDimension:(SFNDDimension*)nddim;
- (void) setChannelSettings:(NSArray*)value;
- (void) setVolumeIndex:(NSUInteger)index;
- (void) setBlendColor:(NSColor*)value;
- (void) setBrightness:(float)value;
- (void) setBackplaneColor:(NSColor*)value;
- (void) setBackplaneMode:(NSUInteger)value;
- (void) setRenderMode:(NSUInteger)value;
- (void) setAlpha:(float)value;
- (void) setIsZInverted:(BOOL)value;
- (void) setTransform:(SFOGLTransform*)value;
- (void) setZScale:(float)value;
- (void) setSampling:(NSUInteger)value;

- (void) setRenderState:(SFOGLRenderState*)state;

- (void)programBlendMode;

- (NSUInteger) renderMode;

- (NSColor*) blendColor;
- (NSColor*) backplaneColor;
- (NSUInteger) backplaneMode;
- (float) alpha;
- (float) zScale;
- (BOOL) isZInverted;
- (NSUInteger) volumeIndex;
- (SFOGLTransform*)transform;
- (NSOpenGLView*)openGLView;
- (SFNDVoxelData*) nDVoxelData;
- (SFOGLBackplane*) backdrop;

- (NSUInteger) sampling;

- (SFSize) adjustedTextureSize:(SFSize)value;

- (BOOL) draw;
- (SFOGLRenderState*) renderState;


@end

@interface SFOGLVolume (Processing)
- (void)processAllChannels;
- (void)processChannel:(NSUInteger)i;
@end

