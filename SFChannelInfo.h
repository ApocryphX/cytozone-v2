//
//  SFChannelInfo.h
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SFChannelInfo : NSObject <NSCopying> {
	NSColor*  sfColor;
	NSString* sfName;
}

- (void) setColor:(NSColor*)value;
- (void) setName:(NSString*)value;
	
- (NSString*) name;
- (NSColor*) color;

 	

@end

