//
//  SFChannelInfo.m
//  VoxelMachine
//
//  Copyright (c) 2014 Elarity, LLC. All rights reserved.
//

#import "SFChannelInfo.h"


@implementation SFChannelInfo

- (id)initWithChannelInfo:(SFChannelInfo*)chinfo {
	self = [super init];
	[self setColor:[chinfo color]];
	[self setName:[chinfo name]];  
	return self;
};

- (void) setColor:(NSColor*)value {
	sfColor = value;
};

- (void) setName:(NSString*)value {
	sfName = value;

};

- (NSString*) name {
	return sfName;
};

- (NSColor*) color {
	return sfColor;
};

- (id)copyWithZone:(NSZone *)zone
{
    SFChannelInfo *copy = [ [ [self class] allocWithZone: zone] initWithChannelInfo:self];
    return copy;
};

- (id)copy {
	return [self copy];
}

- (id)initWithCoder:(NSCoder *)coder
{
	self = [super init];
    if ( [coder allowsKeyedCoding] ) {
        // Can decode keys in any order
        sfName = [coder decodeObjectForKey:@"Name"];
		sfColor = [coder decodeObjectForKey:@"Color"];
    } 
	else {
        // Must decode keys in same order as encodeWithCoder:
		sfName = [coder decodeObject];
		sfColor = [coder decodeObject];
    }
    return self;
};

- (void)encodeWithCoder:(NSCoder *)coder
{
    if ( [coder allowsKeyedCoding] ) {
		[coder encodeObject:sfName forKey:@"Name"];
        [coder encodeObject:sfColor forKey:@"Color"];
    } 
	else {
        [coder encodeObject:sfName];
        [coder encodeObject:sfColor];
    }
    return;
};



@end
