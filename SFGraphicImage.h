//
//  SFGraphicImage.h
//  ROI Prototypes
//
//  Copyright (c) 2013 Elarity, LLC. All rights reserved.
//

#import "SFGraphicRectangle.h"

@interface SFGraphicImage : SFGraphicRectangle

@property (retain) NSImage *image;

@end
